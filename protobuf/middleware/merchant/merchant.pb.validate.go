// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: merchant.proto

package api_merchant

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/golang/protobuf/ptypes"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = ptypes.DynamicAny{}
)

// define the regex for a UUID once up-front
var _merchant_uuidPattern = regexp.MustCompile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")

// Validate checks the field values on DeleteBranchRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *DeleteBranchRequest) Validate() error {
	if m == nil {
		return nil
	}

	if l := utf8.RuneCountInString(m.GetBranchId()); l < 1 || l > 32 {
		return DeleteBranchRequestValidationError{
			field:  "BranchId",
			reason: "value length must be between 1 and 32 runes, inclusive",
		}
	}

	return nil
}

// DeleteBranchRequestValidationError is the validation error returned by
// DeleteBranchRequest.Validate if the designated constraints aren't met.
type DeleteBranchRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e DeleteBranchRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e DeleteBranchRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e DeleteBranchRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e DeleteBranchRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e DeleteBranchRequestValidationError) ErrorName() string {
	return "DeleteBranchRequestValidationError"
}

// Error satisfies the builtin error interface
func (e DeleteBranchRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sDeleteBranchRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = DeleteBranchRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = DeleteBranchRequestValidationError{}

// Validate checks the field values on CreateBranchRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *CreateBranchRequest) Validate() error {
	if m == nil {
		return nil
	}

	if l := utf8.RuneCountInString(m.GetBranchName()); l < 1 || l > 64 {
		return CreateBranchRequestValidationError{
			field:  "BranchName",
			reason: "value length must be between 1 and 64 runes, inclusive",
		}
	}

	// no validation rules for PhoneNumber

	return nil
}

// CreateBranchRequestValidationError is the validation error returned by
// CreateBranchRequest.Validate if the designated constraints aren't met.
type CreateBranchRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e CreateBranchRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e CreateBranchRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e CreateBranchRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e CreateBranchRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e CreateBranchRequestValidationError) ErrorName() string {
	return "CreateBranchRequestValidationError"
}

// Error satisfies the builtin error interface
func (e CreateBranchRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sCreateBranchRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = CreateBranchRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = CreateBranchRequestValidationError{}

// Validate checks the field values on Branch with the rules defined in the
// proto definition for this message. If any rules are violated, an error is returned.
func (m *Branch) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for Id

	// no validation rules for Name

	// no validation rules for PhoneNumber

	if v, ok := interface{}(m.GetCreatedDate()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return BranchValidationError{
				field:  "CreatedDate",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if v, ok := interface{}(m.GetUpdatedDate()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return BranchValidationError{
				field:  "UpdatedDate",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	return nil
}

// BranchValidationError is the validation error returned by Branch.Validate if
// the designated constraints aren't met.
type BranchValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e BranchValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e BranchValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e BranchValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e BranchValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e BranchValidationError) ErrorName() string { return "BranchValidationError" }

// Error satisfies the builtin error interface
func (e BranchValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sBranch.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = BranchValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = BranchValidationError{}

// Validate checks the field values on ListBranchsRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *ListBranchsRequest) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for PageIndex

	// no validation rules for PageSize

	// no validation rules for GetTotal

	// no validation rules for BranchId

	// no validation rules for PhoneNumber

	// no validation rules for Status

	return nil
}

// ListBranchsRequestValidationError is the validation error returned by
// ListBranchsRequest.Validate if the designated constraints aren't met.
type ListBranchsRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListBranchsRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListBranchsRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListBranchsRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListBranchsRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListBranchsRequestValidationError) ErrorName() string {
	return "ListBranchsRequestValidationError"
}

// Error satisfies the builtin error interface
func (e ListBranchsRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListBranchsRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListBranchsRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListBranchsRequestValidationError{}

// Validate checks the field values on ListBranchsResponse with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *ListBranchsResponse) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for TotalSize

	for idx, item := range m.GetBranchs() {
		_, _ = idx, item

		if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ListBranchsResponseValidationError{
					field:  fmt.Sprintf("Branchs[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	return nil
}

// ListBranchsResponseValidationError is the validation error returned by
// ListBranchsResponse.Validate if the designated constraints aren't met.
type ListBranchsResponseValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListBranchsResponseValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListBranchsResponseValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListBranchsResponseValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListBranchsResponseValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListBranchsResponseValidationError) ErrorName() string {
	return "ListBranchsResponseValidationError"
}

// Error satisfies the builtin error interface
func (e ListBranchsResponseValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListBranchsResponse.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListBranchsResponseValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListBranchsResponseValidationError{}

// Validate checks the field values on GetBranchRequest with the rules defined
// in the proto definition for this message. If any rules are violated, an
// error is returned.
func (m *GetBranchRequest) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for Id

	return nil
}

// GetBranchRequestValidationError is the validation error returned by
// GetBranchRequest.Validate if the designated constraints aren't met.
type GetBranchRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e GetBranchRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e GetBranchRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e GetBranchRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e GetBranchRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e GetBranchRequestValidationError) ErrorName() string { return "GetBranchRequestValidationError" }

// Error satisfies the builtin error interface
func (e GetBranchRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sGetBranchRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = GetBranchRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = GetBranchRequestValidationError{}
