

INCLUDE_PATH="-I$GOPATH/src/ \
-I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway \
-I${GOPATH}/src/github.com/envoyproxy/protoc-gen-validate \
-I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis"

LIST_APPS=(
    ../../../exchange-admin/src/app/proto
    # ../../zalopay-merchant-web
)

for projectname in "${LIST_APPS[@]}"
do
    rm -rf "$projectname"
    mkdir $projectname
    OUT_PATH="$projectname/"
    JS_OUT_CMD="--js_out=import_style=commonjs:$OUT_PATH"
    GRPC_WEB_OUT_CMD="--grpc-web_out=import_style=commonjs+dts,mode=grpcweb:$OUT_PATH"

    LIST_FILES=(
        github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis/google/api/annotations.proto 
        google/api/http.proto
        google/api/httpbody.proto
        google/protobuf/empty.proto
        google/protobuf/timestamp.proto
        google/api/annotations.proto
        github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options/annotations.proto
        github.com/gogo/protobuf/gogoproto/gogo.proto
        protoc-gen-swagger/options/annotations.proto
        protoc-gen-swagger/options/openapiv2.proto
        validate/validate.proto
        gitlab.com/tvduc/product-management/core-grpc-project/helper/go-generator/protoc-gen-es/protobuf/es.proto
        gitlab.com/tvduc/product-management/core-grpc-project/helper/proto/common.proto
        gitlab.com/tvduc/product-management/core-grpc-project/helper/proto/type.proto
        gitlab.com/tvduc/product-management/core-grpc-project/helper/proto/timestamp.proto
        gitlab.com/tvduc/product-management/core-grpc-project/protobuf/middleware/merchant/merchant.proto
    )

    for filepath in "${LIST_FILES[@]}"
    do
        protoc -I/usr/local/include -I. $INCLUDE_PATH $JS_OUT_CMD $GRPC_WEB_OUT_CMD $filepath
    done

done

