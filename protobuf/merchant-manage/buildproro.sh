protoc -I/usr/local/include -I. \
  -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway \
  -I ${GOPATH}/src/github.com/envoyproxy/protoc-gen-validate \
  -I${GOPATH}/src/git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es \
  --go_out=plugins=grpc:. \
  --validate_out="lang=go:." \
  --es_out="lang=go:." \
  *.proto &&
  protoc-go-inject-tag -input merchant-manage.pb.go