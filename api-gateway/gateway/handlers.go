package gateway

import (
	"net/http"
	"path"
	"strings"

	"github.com/golang/glog"
)

// SwaggerServer ...
func SwaggerServer(dir string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		glog.Infof("Serving %s", r.URL.Path)
		p := strings.TrimPrefix(r.URL.Path, "/swagger/")
		p = path.Join(dir, p)
		http.ServeFile(w, r, p)
	}
}
