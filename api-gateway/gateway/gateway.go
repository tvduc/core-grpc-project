package gateway

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/golang/glog"
	"github.com/mwitkow/grpc-proxy/proxy"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/domain"
	helper "gitlab.com/tvduc/product-management/core-grpc-project/helper/grpc"

	goproto "github.com/golang/protobuf/proto"
	gwruntime "github.com/grpc-ecosystem/grpc-gateway/runtime"
	merchantService "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/middleware/merchant"
	"google.golang.org/genproto/googleapis/api/httpbody"
	"google.golang.org/grpc"
)

var (
	grpcConnTbl map[string]*grpc.ClientConn
)

func init() {
	grpcConnTbl = make(map[string]*grpc.ClientConn)
}

type cbFn func(context.Context, *gwruntime.ServeMux, *grpc.ClientConn) error
type registerParam struct {
	consulKey, serviceName string
	fn                     cbFn
}

func registerServiceAPIHandler(ctx context.Context, mux *gwruntime.ServeMux, consulAdd string, param registerParam) {
	conn := helper.CreateGrpcConn(ctx, consulAdd, param.consulKey, helper.UnaryClientErrInterceptor())
	if err := param.fn(ctx, mux, conn); err != nil {
		glog.Errorln(err)
		return
	}
	if param.serviceName != "" {
		grpcConnTbl[param.serviceName] = helper.CreateProxyGrpcConn(ctx, consulAdd, param.consulKey, proxy.Codec())

	} else {
		glog.Errorln("registerServiceAPIHandler " + param.consulKey + " Empty")
	}

}

// NewGateway returns a new gateway server which translates HTTP into gRPC.
func NewGateway(ctx context.Context, opts []gwruntime.ServeMuxOption, consulAdd string, grpcPort int) (http.Handler, error) {
	marshalOption := gwruntime.WithMarshalerOption(gwruntime.MIMEWildcard, &gwruntime.HTTPBodyMarshaler{Marshaler: &gwruntime.JSONPb{}})
	// marshalOption := gwruntime.WithMarshalerOption(gwruntime.MIMEWildcard, &gwruntime.JSONPb{})
	responseOption := gwruntime.WithForwardResponseOption(filterResponse)
	opts = append(opts, marshalOption, responseOption)
	mux := gwruntime.NewServeMux(opts...)

	registerParams := []registerParam{
		{serviceName: "api.merchant.MerchantAPI", fn: merchantService.RegisterMerchantAPIHandler, consulKey: domain.MiddlewareMerchantManage},
	}
	for _, param := range registerParams {
		registerServiceAPIHandler(ctx, mux, consulAdd, param)
	}

	formWrapper := func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.ToLower(strings.Split(r.Header.Get("Content-Type"), ";")[0]) == "multipart/form-data" {
				glog.Infof("Got request: %#v\n", r)
				err := r.ParseMultipartForm(32000)
				if err != nil {
					glog.Errorf("Parse multipart error %v", err)
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}

				jsonData := make(map[string]interface{})
				for k, v := range r.Form {
					if len(v) > 0 {
						jsonData[k] = v[0]
					}
				}
				var Buf bytes.Buffer
				file, header, err := r.FormFile("uploadFile")
				if err != nil {
					glog.Errorf("Parse FormFile error %v", err)
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}
				defer file.Close()
				io.Copy(&Buf, file)
				contents := Buf.String()
				Buf.Reset()
				jsonData["content-type"] = header.Header["Content-Type"][0]
				jsonData["filename"] = header.Filename
				jsonData["size"] = header.Size
				jsonDataStr, _ := json.Marshal(jsonData)

				jsonMap := make(map[string]interface{})
				jsonMap["data"] = jsonDataStr
				jsonMap["file"] = []byte(contents)

				jsonBody, err := json.Marshal(jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				r.Body = ioutil.NopCloser(bytes.NewReader(jsonBody))
				r.ContentLength = int64(len(jsonBody))
				r.Header.Set("Content-Type", "application/json")
			}
			mux.ServeHTTP(w, r)
		})
	}

	return formWrapper(mux), nil
}

func filterResponse(ctx context.Context, w http.ResponseWriter, resp goproto.Message) error {
	if body, ok := resp.(*httpbody.HttpBody); ok {
		for _, ext := range body.GetExtensions() {
			w.Header().Set(ext.GetTypeUrl(), string(ext.GetValue()))
		}

	}
	return nil
}
