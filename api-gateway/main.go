package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/golang/glog"
	gwruntime "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/rs/cors"

	"gitlab.com/tvduc/product-management/core-grpc-project/api-gateway/gateway"
	config "gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/domain"
	wonaming "gitlab.com/tvduc/product-management/core-grpc-project/helper/naming"
)

// Options is a set of options to be passed to Run
type Options struct {
	// Addr is the address to listen
	Addr string

	// SwaggerDir is a path to a directory from which the server
	// serves swagger specs.
	SwaggerDir string

	// Mux is a list of options to be passed to the grpc-gateway multiplexer
	Mux       []gwruntime.ServeMuxOption
	ConsulAdd string
}
type configuration struct {
	config.ServiceBase
	Swagger     bool
	ProxyUrl    string
	ProxyPrefix string
	HttpProxy   string
}

var (
	serviceCfg = &configuration{
		Swagger: true,
	}
)

// Run starts a HTTP server and blocks while running if successful.
// The server will be shutdown when "ctx" is canceled.
func Run(ctx context.Context, opts Options) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	if serviceCfg.Swagger {
		mux.HandleFunc("/swagger/", gateway.SwaggerServer(opts.SwaggerDir))
	}

	gw, err := gateway.NewGateway(ctx, opts.Mux, opts.ConsulAdd, serviceCfg.GrpcPort)
	if err != nil {
		return err
	}

	mux.Handle("/", gw)

	s := &http.Server{
		Addr:    opts.Addr,
		Handler: cors.AllowAll().Handler(mux), //gateway.AllowCORS(mux),
	}
	go func() {
		<-ctx.Done()
		glog.Infof("Shutting down the http server")
		if err := s.Shutdown(context.Background()); err != nil {
			glog.Errorf("Failed to shutdown http server: %v", err)
		}
	}()

	glog.Infof("Starting listening at %s", opts.Addr)
	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		glog.Errorf("Failed to listen and serve: %v", err)
		return err
	}
	return nil
}
func main() {

	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()
	defer glog.Flush()
	defer os.Exit(3)
	serviceName := domain.ApiGateWay
	if err := config.Init(serviceName, serviceCfg); err != nil {
		glog.Infoln(err)
	}
	if serviceCfg.Port == 0 {
		serviceCfg.Port = 8080
	}
	if serviceCfg.GrpcPort == 0 {
		serviceCfg.GrpcPort = 8081
	}
	wonaming.Register(serviceName, "", serviceCfg.Port, serviceCfg.GetConsul(), time.Second*10, 15)

	ctx := context.Background()
	opts := Options{
		Addr:       fmt.Sprintf(":%d", serviceCfg.GetPort()),
		SwaggerDir: "swagger",
		ConsulAdd:  serviceCfg.GetConsul(),
	}
	// glog.Infoln("opts", opts)
	if err := Run(ctx, opts); err != nil {
		glog.Fatal(err)
	}

}
