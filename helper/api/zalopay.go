package api

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"

	"git.zapa.cloud/merchant-tools/helper/crypto"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	md "google.golang.org/grpc/metadata"
)

type CheckSumType int32
type ApiMethod int32

const (
	noneCheckSum CheckSumType = iota
	//HmacSha256Sum ...
	HmacSha256Sum
	//Sha256Sum ...
	Sha256Sum
)
const (
	noneMethod ApiMethod = iota
	//PostMethod ...
	PostMethod
	//GetMethod ...
	GetMethod
	//PutMethod ...
	PutMethod
	//PostFormMethod ...
	PostFormMethod
)

const (
	defaultJoin     = "|"
	sigTag          = "sig"
	sumTag          = "sum"
	defaultCheckSum = noneCheckSum
	urlAPI          = "urlzalopayapi"
)

//ZaloPayRequest ...
type ZaloPayRequest struct {
	join, url, key string
	sumType        CheckSumType
	apiMethod      ApiMethod
}

//NewZaloPayRequest ...
func NewZaloPayRequest() *ZaloPayRequest {
	return &ZaloPayRequest{
		join:    defaultJoin,
		sumType: noneCheckSum,
	}
}

//Clone ...
func (r *ZaloPayRequest) Clone() *ZaloPayRequest {
	n := &ZaloPayRequest{}
	*n = *r
	return n
}

//Url ...
func (r *ZaloPayRequest) Url(url string) *ZaloPayRequest {
	r.url = url
	return r
}

//Join ...
func (r *ZaloPayRequest) Join(join string) *ZaloPayRequest {
	r.join = join
	return r
}

//Key ...
func (r *ZaloPayRequest) Key(key string) *ZaloPayRequest {
	r.key = key
	return r
}

//Method ...
func (r *ZaloPayRequest) Method(apiMethod ApiMethod) *ZaloPayRequest {
	r.apiMethod = apiMethod
	return r
}

//SumType ...
func (r *ZaloPayRequest) SumType(sum CheckSumType) *ZaloPayRequest {
	r.sumType = sum
	return r
}

//MakeRequest ...
func (r *ZaloPayRequest) MakeRequest(ctx context.Context, msg interface{}) (context.Context, error) {
	val := reflect.Indirect(reflect.ValueOf(msg))
	vt := val.Type()
	type sig struct {
		id   int
		name string
	}
	sigs := []*sig{}
	var (
		sumVal reflect.Value
	)
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if tag, ok := field.Tag.Lookup(sigTag); ok {
			//sigs = append(sigs, fmt.Sprintf("%v", val.Field(i)))
			id, err := strconv.Atoi(tag)
			if err != nil {
				return ctx, err
			}
			sigs = append(sigs, &sig{
				id:   id,
				name: fmt.Sprintf("%v", val.Field(i)),
			})

		} else if _, ok := field.Tag.Lookup(sumTag); ok {
			sumVal = val.Field(i)
		}
	}

	if sumVal.CanSet() && len(sigs) > 0 {
		sort.Slice(sigs, func(i, j int) bool {
			return sigs[i].id < sigs[j].id
		})
		n := len(sigs)
		params := make([]string, n, n)
		for i := 0; i < n; i++ {
			params[i] = sigs[i].name

		}
		switch r.sumType {
		case HmacSha256Sum:
			sumVal.SetString(crypto.HmacSha256Sum(r.key, r.join, params...))
		case Sha256Sum:
			params = append(params, r.key)
			sumVal.SetString(crypto.Sha256Sum(r.join, params...))
		default:
			glog.Errorln("Set checkSumType")
			return ctx, fmt.Errorf("None sum method")
		}
	}
	ctx = md.AppendToOutgoingContext(ctx, urlAPI, fmt.Sprintf("%d:%s", r.apiMethod, r.url))

	return ctx, nil
}

//ExtractContext ...
func ExtractContext(ctx context.Context) (ApiMethod, string, error) {
	val, ok := metautils.ExtractIncoming(ctx)[urlAPI]
	if !ok {
		return 0, "", fmt.Errorf("Invalid context")
	}
	if len(val) != 1 {
		return 0, "", fmt.Errorf("Invalid info %v", val)
	}
	vals := strings.SplitN(val[0], ":", 2)
	if len(vals) != 2 {
		return 0, "", fmt.Errorf("Invalid info %s", vals[0])
	}
	method, err := strconv.Atoi(vals[0])
	if err != nil {
		return 0, "", err
	}
	return ApiMethod(method), vals[1], nil
}
