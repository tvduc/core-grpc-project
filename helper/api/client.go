package api

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"bytes"

	tracingHelper "git.zapa.cloud/merchant-tools/helper/opentracing"
	"github.com/golang/glog"
	"github.com/google/go-querystring/query"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var (
	apiTag = opentracing.Tag{Key: string(ext.Component), Value: "apiTag"}
)

const (
	ClientID = "clientid"
	ReqDate  = "reqdate"
	Mac      = "mac"
)

//Client ...
type Client interface {
	PostForm(ctx context.Context, url string, request, response interface{}) error
	Get(ctx context.Context, url, clientID, reqDate, mac string, response interface{}) error
	Get2(ctx context.Context, url string, request, response interface{}) error
	PostJson(ctx context.Context, url string, request, response interface{}) error
	PostJsonWithHeader(ctx context.Context, url string, header map[string]string, bodyReq, response interface{}) error
	PostXml(ctx context.Context, url string, request, response interface{}) error
}
type client struct {
	client http.Client
}

//New ...
func New(timeOut time.Duration) Client {
	return &client{
		client: http.Client{Timeout: timeOut},
	}
}

func (c *client) PostForm(ctx context.Context, url string, req, response interface{}) error {
	data, err := query.Values(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}
	_, span := tracingHelper.NewClientSpanFromContext(ctx, apiTag, fmt.Sprintf("%s|%v", url, req))
	ret, err := c.client.PostForm(url, data)
	tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		glog.Errorln(err)
		return err
	}
	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}
	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = json.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}

	} else {
		glog.Errorln(err)
	}
	return err
}

func (c *client) Get(ctx context.Context, url, clientID, reqDate, mac string, response interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Add(ClientID, clientID)
	req.Header.Add(ReqDate, reqDate)
	req.Header.Add(Mac, mac)
	ret, err := c.client.Do(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}

	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = json.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}
	} else {
		glog.Errorln(err)
	}
	return err
}

func (c *client) Get2(ctx context.Context, url string, request, response interface{}) error {
	data, err := query.Values(request)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	url = url + "?" + data.Encode()
	req, err := http.NewRequest(http.MethodGet, url, nil)
	ret, err := c.client.Do(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}

	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = json.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}
	} else {
		glog.Errorln(err)
	}
	return err
}

func (c *client) PostJson(ctx context.Context, url string, req, response interface{}) error {
	data, err := json.Marshal(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	_, span := tracingHelper.NewClientSpanFromContext(ctx, apiTag, fmt.Sprintf("%s|%v", url, req))
	ret, err := c.client.Post(url, "application/json", bytes.NewBuffer(data))
	tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		glog.Errorln(err)
		return err
	}
	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}
	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = json.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}

	} else {
		glog.Errorln(err)
	}
	return err
}

func (c *client) PostJsonWithHeader(ctx context.Context, url string, header map[string]string, bodyReq, response interface{}) error {
	data, err := json.Marshal(bodyReq)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(data))
	for key, value := range header {
		req.Header.Add(key, value)
	}
	ret, err := c.client.Do(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}

	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = json.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}
	} else {
		glog.Errorln(err)
	}
	return err
}

func (c *client) PostXml(ctx context.Context, url string, req, response interface{}) error {
	data, err := xml.Marshal(req)
	if err != nil {
		glog.Errorln(err)
		return err
	}

	_, span := tracingHelper.NewClientSpanFromContext(ctx, apiTag, fmt.Sprintf("%s|%v", url, req))
	ret, err := c.client.Post(url, "text/xml;charset=utf-8", bytes.NewBuffer(data))
	tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		glog.Errorln(err)
		return err
	}
	defer ret.Body.Close()
	if ret.StatusCode != http.StatusOK {
		err = fmt.Errorf("http error %d:%s", ret.StatusCode, ret.Status)
		glog.Errorln(err)
		return err
	}
	body, err := ioutil.ReadAll(ret.Body)
	if err == nil {
		if err = xml.Unmarshal(body, response); err != nil {
			glog.Errorln(err, string(body))
		}

	} else {
		glog.Errorln(err)
	}
	return err
}
