package notify

// var (
// 	endpoint      = "https://outlook.office.com/webhook/fa6383d2-5244-436f-8e68-1d07c9f5b2e4@7c112a6e-10e2-4e09-afc4-2e37bc60d821/IncomingWebhook/ad53ac8ef19c469abdc29ed0f0478827/01dde83e-d213-49ed-87f0-b7fcfe15ccd4"
// 	srvPrefix     = os.Getenv("SERVICE_PREFIX_NAME")
// 	sendRateLimit = rate.NewLimiter(rate.Every(time.Minute*1), 10)
// 	chanWebHooks  = make(chan *webHook, 1024)
// )

type webHook struct {
	Text  string `json:"text,omitempty"`
	Title string `json:"title,omitempty"`
}

func sendMsg(msg *webHook) {
	// enc, err := json.Marshal(msg)
	// if err != nil {
	// 	glog.Errorln(err)
	// 	return
	// }
	// resp, err := http.Post(endpoint, "application/json", bytes.NewBuffer(enc))
	// if err != nil {
	// 	glog.Errorln(err)

	// }
	// if resp != nil && resp.Body != nil {
	// 	resp.Body.Close()
	// }
}
func init() {
	// if srvPrefix == "prod." {
	// 	srvPrefix = "[PROD] "
	// 	endpoint = "http://10.50.32.3:9091/notify/webhook/fa6383d2-5244-436f-8e68-1d07c9f5b2e4@7c112a6e-10e2-4e09-afc4-2e37bc60d821/IncomingWebhook/ad53ac8ef19c469abdc29ed0f0478827/01dde83e-d213-49ed-87f0-b7fcfe15ccd4"
	// } else {
	// 	if len(srvPrefix) <= 0 {
	// 		srvPrefix = "[DEV] "
	// 		endpoint = "http://10.50.32.3:9091/notify/webhook/fa6383d2-5244-436f-8e68-1d07c9f5b2e4@7c112a6e-10e2-4e09-afc4-2e37bc60d821/IncomingWebhook/c8d31fd63f624836aa3034e4140cdf4e/01dde83e-d213-49ed-87f0-b7fcfe15ccd4"
	// 	} else {
	// 		srvPrefix = fmt.Sprintf("[%s] ", strings.ToUpper(strings.Trim(srvPrefix, ".")))
	// 		endpoint = "http://10.50.32.3:9091/notify/webhook/fa6383d2-5244-436f-8e68-1d07c9f5b2e4@7c112a6e-10e2-4e09-afc4-2e37bc60d821/IncomingWebhook/b7fe1c714ed249cf9c071226f2583ce5/01dde83e-d213-49ed-87f0-b7fcfe15ccd4"
	// 	}
	// }
	// go func() {
	// 	for msg := range chanWebHooks {

	// 		sendMsg(msg)
	// 	}
	// }()
}

//StartService ...
func StartService(name string) {
	// srvPrefix = fmt.Sprintf("%s[%s]", srvPrefix, name)
	// SendToTeams("", "Start")
}

//SendToTeams ...
func SendToTeams(title, text string) error {
	// if len(text) < 8*1024 {
	// 	if sendRateLimit.Allow() {
	// 		chanWebHooks <- &webHook{
	// 			Text:  text,
	// 			Title: srvPrefix + title,
	// 		}
	// 	}
	// }
	return nil
}

//SendToTeamsEx ...
func SendToTeamsEx(title, text string) error {
	// if len(text) < 8*1024 {
	// 	sendMsg(&webHook{
	// 		Text:  text,
	// 		Title: srvPrefix + title,
	// 	})
	// }
	return nil
}

//SetConfig ...
func SetConfig(name, addr string) {
	// srvPrefix = fmt.Sprintf("[%s]", name)
	// endpoint = addr

}

//SetEndPoint ...
func SetEndPoint(addr string) {
	// endpoint = addr
}

//SetPrefix
func SetPrefix(prefix string) {
	// srvPrefix = prefix
}
