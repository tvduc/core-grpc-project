package token

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/go-chi/jwtauth"
)

type metaKey struct{}
type Metadata map[string]string
type JsonTokenData struct {
	Token string `protobuf:"bytes,1,opt,name=token" json:"token,omitempty"`
}

const (
	CookieName     = "esessionid"
	TokenParameter = "token"
)

const (
	TokenCtxKey      = "Token"
	AgencyIdCtxKey   = "Agency"
	EmployeeIdCtxKey = "Employee"
)

// override token parser to get token from our cookie and parameters
func FromQuery(r *http.Request) string {
	// Get token from query param named "token".
	return r.URL.Query().Get(TokenParameter)
}

func FromCookie(r *http.Request) string {
	cookie, err := r.Cookie(CookieName)
	if err != nil {
		return ""
	}
	return cookie.Value
}

func FromPostData(r *http.Request) string {
	var tokenData JsonTokenData

	// Read the content
	var bodyBytes []byte
	if r.Body == nil {
		return ""
	}

	// need to read request body and copy it back
	// otherwise body will be destroyed after being read
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return ""
	}
	// Restore the io.ReadCloser to its original state
	r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	err = json.Unmarshal(bodyBytes, &tokenData)
	if err != nil {
		return ""
	}
	return tokenData.Token
}

func NewContext(ctx context.Context, tk string, agencyId, employeeId int64) context.Context {
	fmt.Println(tk, agencyId, employeeId)
	return context.WithValue(ctx, metaKey{},
		Metadata{
			TokenCtxKey:      tk,
			EmployeeIdCtxKey: strconv.Itoa(int(employeeId)),
			AgencyIdCtxKey:   strconv.Itoa(int(agencyId))})

}

func FromContext(ctx context.Context) (string, int64, int64) {
	var (
		tkStr                string
		agencyId, employeeId int
	)
	md, ok := ctx.Value(metaKey{}).(Metadata)
	if ok {
		tkStr = md[TokenCtxKey]
		agencyId, _ = strconv.Atoi(md[AgencyIdCtxKey])
		employeeId, _ = strconv.Atoi(md[EmployeeIdCtxKey])
	}
	return tkStr, int64(agencyId), int64(employeeId)
}

func Parse(r *http.Request, findTokenFns ...func(r *http.Request) string) (*string, error) {
	var tokenStr string
	for _, fn := range findTokenFns {
		tokenStr = fn(r)
		if tokenStr != "" {
			break
		}
	}
	if tokenStr == "" {
		return nil, jwtauth.ErrNoTokenFound
	}
	return &tokenStr, nil
}

func ParseTokenFromRequest(r *http.Request) (*string, error) {
	return Parse(r, FromPostData, FromCookie, FromQuery)
}
