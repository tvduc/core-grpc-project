package mcc_info

import (
	pb "git.zapa.cloud/merchant-tools/protobuf/merchant-manage"
)

var (
	MccLists = []*pb.MccInfo{
		&pb.MccInfo{MccId: "58140000", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Nhà hàng/ Tiệm ăn", SubCatEng: "Restaurant"},
		&pb.MccInfo{MccId: "58140001", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Tiệm ăn", SubCatEng: "Diner"},
		&pb.MccInfo{MccId: "58140002", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Tiệm ăn", SubCatEng: "Diner"},
		&pb.MccInfo{MccId: "58140003", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Quán ăn nhanh", SubCatEng: "Fast Food Restaurant "},
		&pb.MccInfo{MccId: "58140004", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Quán ăn đường phố", SubCatEng: "Street Food Stall"},
		&pb.MccInfo{MccId: "58140005", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Tiệm bánh kẹo", SubCatEng: "Sweet Shop"},
		&pb.MccInfo{MccId: "58140006", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Cà phê", SubCatEng: "Coffee Shop"},
		&pb.MccInfo{MccId: "58140007", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Sinh tố", SubCatEng: "Smoothie Shop"},
		&pb.MccInfo{MccId: "58140008", MccCode: "5814", Category: "Ăn uống", CatEng: "FnB", SubCategory: "Ăn uống khác", SubCatEng: "Other FnB"},

		&pb.MccInfo{MccId: "48140000", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Thương mại điện tử", SubCatEng: "E-commerce"},
		&pb.MccInfo{MccId: "48140001", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Cửa hàng tiện lợi", SubCatEng: "Convenience Store"},
		&pb.MccInfo{MccId: "48140002", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Siêu thị", SubCatEng: "Supermarket"},
		&pb.MccInfo{MccId: "48140003", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Siêu thị điện máy", SubCatEng: "Electronics Supermarket"},
		&pb.MccInfo{MccId: "48140004", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Tạp hóa", SubCatEng: "Grocery Store"},
		&pb.MccInfo{MccId: "48140005", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Thời trang", SubCatEng: "Fashion Boutique"},
		&pb.MccInfo{MccId: "48140006", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Giày dép", SubCatEng: "Shoe Store"},
		&pb.MccInfo{MccId: "48140007", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Đồ lót", SubCatEng: "Lingerie/Underwear Store"},
		&pb.MccInfo{MccId: "48140008", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Phụ kiện thời trang", SubCatEng: "Accessories Store"},
		&pb.MccInfo{MccId: "48140009", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Phụ kiện điện tử", SubCatEng: "Electronics Accessories"},
		&pb.MccInfo{MccId: "48140010", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Dược phẩm/ Mỹ phẩm", SubCatEng: "Drug Store"},
		&pb.MccInfo{MccId: "48140011", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Nội thất", SubCatEng: "Furniture Store"},
		&pb.MccInfo{MccId: "48140012", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Mua thẻ cào điện thoại", SubCatEng: "Teclo Card Retails"},
		&pb.MccInfo{MccId: "48140013", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Mua sắm/Bán hàng khác", SubCatEng: "Other Shopping"},
		&pb.MccInfo{MccId: "48140014", MccCode: "4814", Category: "Mua sắm/ Bán hàng", CatEng: "Shopping", SubCategory: "Thực phẩm & Đồ tươi sống", SubCatEng: "Fresh Food"},

		&pb.MccInfo{MccId: "48140100", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Xem phim", SubCatEng: "Movies"},
		&pb.MccInfo{MccId: "48140101", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Karaoke", SubCatEng: "Karaoke"},
		&pb.MccInfo{MccId: "48140102", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Bar", SubCatEng: "Bar"},
		&pb.MccInfo{MccId: "48140103", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Game", SubCatEng: "Game"},
		&pb.MccInfo{MccId: "48140104", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Xổ Số", SubCatEng: "Lottery"},
		&pb.MccInfo{MccId: "48140105", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Nội dung số", SubCatEng: "Digital Goods"},
		&pb.MccInfo{MccId: "48140106", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Application store", SubCatEng: "Application Store"},
		&pb.MccInfo{MccId: "48140107", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Khu vui chơi giải trí", SubCatEng: "Amusement Center"},
		&pb.MccInfo{MccId: "48140108", MccCode: "4814", Category: "Giải trí", CatEng: "Entertainment", SubCategory: "Giải trí khác", SubCatEng: "Giải trí khác"},

		&pb.MccInfo{MccId: "48140200", MccCode: "4814", Category: "Du lịch & Khách sạn", CatEng: "Travelling", SubCategory: "Đặt phòng khách sạn/Homestay", SubCatEng: "Hotel/Homestay"},
		&pb.MccInfo{MccId: "48140201", MccCode: "4814", Category: "Du lịch & Khách sạn", CatEng: "Travelling", SubCategory: "Đại lý du lịch", SubCatEng: "Travel Agent"},
		&pb.MccInfo{MccId: "48140202", MccCode: "4814", Category: "Du lịch & Khách sạn", CatEng: "Travelling", SubCategory: "Mua vé tàu hỏa", SubCatEng: "Train Ticket"},
		&pb.MccInfo{MccId: "48140203", MccCode: "4814", Category: "Du lịch & Khách sạn", CatEng: "Travelling", SubCategory: "Vé máy bay", SubCatEng: "Air Ticket"},
		&pb.MccInfo{MccId: "48140204", MccCode: "4814", Category: "Du lịch & Khách sạn", CatEng: "Travelling", SubCategory: "Vé xe khách liên tỉnh", SubCatEng: "Long-Distance Bus Ticket"},

		&pb.MccInfo{MccId: "49000000", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Taxi/ Xe máy", SubCatEng: "Taxi/Bike"},
		&pb.MccInfo{MccId: "49000001", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Phương tiện công cộng", SubCatEng: "Public Transportation"},
		&pb.MccInfo{MccId: "49000002", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Vận chuyển hàng hóa/ Bưu phẩm", SubCatEng: "Shipping"},
		&pb.MccInfo{MccId: "49000003", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Giao đồ ăn", SubCatEng: "Food Delivery"},
		&pb.MccInfo{MccId: "49000004", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Dịch vụ đỗ xe", SubCatEng: "Parking"},
		&pb.MccInfo{MccId: "49000005", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Xăng dầu", SubCatEng: "Gas"},
		&pb.MccInfo{MccId: "49000006", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Phí cầu đường", SubCatEng: "Toll"},
		&pb.MccInfo{MccId: "49000007", MccCode: "4900", Category: "Dịch vụ vận chuyển và giao hàng", CatEng: "Transportation", SubCategory: "Đặt dịch vụ và vận chuyển khác", SubCatEng: "Other Transportation"},

		&pb.MccInfo{MccId: "48140300", MccCode: "4814", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn chung cư", SubCatEng: "Apartment Utility Bill"},
		&pb.MccInfo{MccId: "49000100", MccCode: "4900", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn điện", SubCatEng: "Electricity Bill"},
		&pb.MccInfo{MccId: "49000101", MccCode: "4900", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn nước", SubCatEng: "Water Bill"},
		&pb.MccInfo{MccId: "48140301", MccCode: "4814", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn truyền hình", SubCatEng: "TV Cable Bill"},
		&pb.MccInfo{MccId: "48140302", MccCode: "4814", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn internet", SubCatEng: "Internet Bill"},
		&pb.MccInfo{MccId: "48140303", MccCode: "4814", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Học phí", SubCatEng: "Tuition Fee"},
		&pb.MccInfo{MccId: "49000102", MccCode: "4900", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Thẻ tín dụng", SubCatEng: "Credit Card Repay"},
		&pb.MccInfo{MccId: "48140304", MccCode: "4814", Category: "Thanh toán hóa đơn", CatEng: "Billing", SubCategory: "Hóa đơn khác", SubCatEng: "Other Billing"},

		&pb.MccInfo{MccId: "48140400", MccCode: "4814", Category: "Dịch vụ viễn thông", CatEng: "Telco", SubCategory: "Điện thoại trả trước", SubCatEng: "Airtime Topup"},
		&pb.MccInfo{MccId: "48140401", MccCode: "4814", Category: "Dịch vụ viễn thông", CatEng: "Telco", SubCategory: "Nạp data 3G/4G", SubCatEng: "Data Card"},
		&pb.MccInfo{MccId: "48140402", MccCode: "4814", Category: "Dịch vụ viễn thông", CatEng: "Telco", SubCategory: "Điện thoại trả sau", SubCatEng: "Post-paid Telco Bill"},
		&pb.MccInfo{MccId: "48140403", MccCode: "4814", Category: "Dịch vụ viễn thông", CatEng: "Telco", SubCategory: "Telco combo, Thẻ điện thoại", SubCatEng: "Teclo Card/Combo"},
		&pb.MccInfo{MccId: "48140404", MccCode: "4814", Category: "Dịch vụ viễn thông", CatEng: "Telco", SubCategory: "Nạp tiền điện thoại", SubCatEng: "Top Up"},

		&pb.MccInfo{MccId: "48140500", MccCode: "4814", Category: "Dịch vụ tài chính", CatEng: "Financial Service", SubCategory: "Tài chính", SubCatEng: "Finance"},
		&pb.MccInfo{MccId: "48140501", MccCode: "4814", Category: "Dịch vụ tài chính", CatEng: "Financial Service", SubCategory: "Kiều hối", SubCatEng: "Overseas National Currency Exchange"},

		&pb.MccInfo{MccId: "49000200", MccCode: "4900", Category: "Dịch vụ bảo hiểm", CatEng: "Insurance", SubCategory: "Bảo hiểm nhân thọ", SubCatEng: "Life Insurance"},
		&pb.MccInfo{MccId: "49000201", MccCode: "4900", Category: "Dịch vụ bảo hiểm", CatEng: "Insurance", SubCategory: "Bảo hiểm phi nhân thọ", SubCatEng: "Non-life Insurance"},

		&pb.MccInfo{MccId: "48140600", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Kỹ thuật số/nội dung số", SubCatEng: "Digital Services"},
		&pb.MccInfo{MccId: "48140601", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Spa/ Fitness", SubCatEng: "Spa/ Fitness"},
		&pb.MccInfo{MccId: "48140602", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Rửa xe", SubCatEng: "Car Washing"},
		&pb.MccInfo{MccId: "48140603", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Giặt ủi", SubCatEng: "Laundry"},
		&pb.MccInfo{MccId: "48140604", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Khách sạn", SubCatEng: "Hotel"},
		&pb.MccInfo{MccId: "48140605", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Giáo dục", SubCatEng: "Education"},
		&pb.MccInfo{MccId: "48140606", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Chăm sóc sức khoẻ", SubCatEng: "Health Care"},
		&pb.MccInfo{MccId: "48140607", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Quyên góp từ thiện", SubCatEng: "Charity/Donate"},
		&pb.MccInfo{MccId: "48140608", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Dịch vụ khác", SubCatEng: "Other Services"},
		&pb.MccInfo{MccId: "48140609", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Thanh toán viện phí", SubCatEng: "Hospital Bills"},
		&pb.MccInfo{MccId: "48140610", MccCode: "4814", Category: "Dịch vụ khác", CatEng: "Other services", SubCategory: "Quà tặng", SubCatEng: "Gifting"},
	}
)
