package registry

import (
	"git.zapa.cloud/merchant-tools/helper/domain"
	"git.zapa.cloud/merchant-tools/helper/env"
)

var (
	consulRegistry = map[string]bool{
		//MiddlewareAuthentication : Tên của service middleware authentication
		domain.MiddlewareAuthentication: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.authentication"
		//MiddlewareAdmin : Tên của service middleware  admin
		domain.MiddlewareAdmin: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.admin"
		//MiddlewareUser : Tên của service middleware user
		domain.MiddlewareUser: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.user"
		//MiddlewareRole : Tên của service middleware role
		domain.MiddlewareRole: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.role"
		//MiddlewareProfile : Tên của service middleware merchant profile
		domain.MiddlewareProfile: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.profile"
		//MiddlewareReport : Tên của service middleware report
		domain.MiddlewareReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.report"
		//MiddlewareReport : Tên của service middleware admin report
		domain.MiddlewareAdminReport: true, //"merchant-tools.middleware.admin-report"
		//MiddlewareWorkflow : Tên của service middleware workflow
		domain.MiddlewareWorkflow: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.workflow"
		//Middleware merchant management
		domain.MiddlewareMerchantManage: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.merchantmanage"
		//MiddlewareSpUser : tên của service middleware SPUser
		domain.MiddlewareSpUser: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.spuser"
		//MiddlewareAdminBo : tên của service middleware Admin-Bo
		domain.MiddlewareAdminBo: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.admin-bo"
		//MiddlewareSpBo : tên của service middleware Sp-Bo
		domain.MiddlewareSpBo: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.sp-bo"
		//MiddlewareSpUser : tên của service middleware SPUser
		domain.MiddlewareSPReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.spreport"
		//MiddlewareCommon : tên của service middleware Common
		domain.MiddlewareCommon: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.common"
		//MiddlewareCommon : tên của service middleware Admin Reconcile
		domain.MiddlewareReconcile: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.reconcile"
		//MiddlewareOperationLog : name define of operation middleware
		domain.MiddlewareOperationLog: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.operation-log"
		//MiddlewareCommon : tên của service middleware dev-tool
		domain.MiddlewareDevTool: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.middleware.dev-tool"
		//MiddlewarePromotion: tên của service middleware Promotion
		domain.MiddlewarePromotion: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.promotion"
		//MiddlewareMepPublic : tên của service middleware mep-pulbic
		domain.MiddlewareMepPublic: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.mep-public"
		//MiddlewareDashboard : tên của service middleware dashboard
		//domain.MiddlewareDashboard:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.dashboard"
		domain.MiddlewareInvoice:      true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.invoice"
		domain.MiddlewareMobileNotify: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.mobile-notify"
		//Middleware promotion config
		domain.MiddlewarePromotionConfig: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.promotion-config"

		//MiddlewareMerchantUser : Tên của service middleware merchant user
		domain.MiddlewareMerchantUser: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.user-store"

		//MiddlewareTaxiIntegration : Tên của service middleware taxi integration
		domain.MiddlewareTaxiIntegration: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.taxi-integration"

		//Authentication : Tên của service authentication
		domain.Authentication: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.authentication"
		//User : Tên của service user
		domain.User: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.user"
		//Role : Tên của service role
		domain.Role: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.role"
		//Profile : Tên của service profile
		domain.Profile: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.profile"
		domain.Refund:  true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.refund"

		//UserSession : Tên của service UserSession
		domain.UserSession: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.usersession"
		//BillingReport : Tên của service BillingReport
		domain.BillingReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.billingreport"
		//UserRegistrationWorkflow : Tên của service UserRegistrationWorkflow
		domain.UserRegistrationWorkflow: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.user-registration-workflow"
		//UserRegistrationReport : Tên của service UserRegistrationReport
		domain.UserRegistrationReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.user-registration-report"
		//ReconcileReport : Tên của service
		domain.ReconcileReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.reconcilereport"
		//ReconcileReport : Tên của service admin reconcile report
		domain.AdminReconcileReport: true, //"merchant-tools.admin-reconcile-report"
		//TransLogReport : Tên của service
		domain.TransLogReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.translogreport"

		//Merchant admin:
		domain.MerchantManage:        true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-manage"
		domain.MerchantConfigService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-config-service"
		domain.CashinService:         true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.cashin-service"
		domain.AppService:            true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.app-service"

		//SPUser : tên service SPUser
		domain.SPUser: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.spuser"

		//SPReport : tên service SPReport
		domain.SPReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.sp-report"

		//SpBo : tên service SpBo
		domain.SpBo: true, //os.Getenv("SERVICE_PREFIX_NAME") + "service-provider.sp-bo"

		//FileServer : Tên của service FileServer
		domain.FileServer: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.file-server"
		//AdminUser ...
		domain.AdminUser: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.admin-user"
		//Common
		domain.Common: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.common"
		//MailService
		domain.MailService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.mail-service"
		//JobsService
		domain.JobsService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.jobs-service"
		//ExcelService
		domain.ExcelService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.excel-service"
		//PDFService
		domain.PDFService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.pdf-service"
		//Reconcile
		domain.Reconcile: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.reconcile"

		domain.OTPService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.otp-service"
		// searchService: service phục vụ search (ex: transaction)
		domain.SearchService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.search-service"
		domain.ApiGateWay:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.api-gateway"

		domain.CronService:     true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.cron-service"
		domain.LogParseService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.logparse-service"
		//AdminReconcileMonthly : Tên của service
		domain.AdminReconcileMonthly: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.admin-reconcile-monthly"

		//AdminReconcileBilling: Tên của service
		domain.AdminReconcileBilling: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.admin-reconcile-billing"

		//MigrateMerchant: Tên của service migrate-merchant
		domain.MigrateMerchant: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.migrate-merchant"

		//OperationLogService
		domain.OperationLogService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.operation-log-service"

		//ReportService
		domain.ReportService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.report-service"

		//MerchantDashboard ..
		domain.MerchantDashboard: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-dashboard"

		//PromotionService
		domain.PromotionService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.promotion-service"

		//PromotionConfigService
		domain.PromotionConfigService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.promotion-config"

		//CashBackPromotionService
		domain.CashBackPromotionService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.cashback-promotion-service"

		//InvoiceService
		domain.InvoiceService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.invoice-service"

		//DevTool
		domain.DevTool: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.dev-tool"

		//DashboardService
		domain.DashboardService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.dashboard-service"

		domain.EVNSPCService:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.evn-spc-service"
		domain.ZALOPAYService:   true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.zalopay-service"
		domain.VinaPhoneService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.vinaphone-service"

		domain.MobileNotifyService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.mobile-notify"
		domain.LogNotifyService:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.log-notify"

		//ReportService
		domain.LogReportService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.log-report-service"

		//TokenReport
		domain.TokenReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.token-report"

		//FTPProxy
		domain.FTPProxyService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.ftp-proxy"

		//DigitalSignatureService
		domain.DigitalSignatureService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.digital-signature"

		//MiddlewareFileService
		domain.MiddlewareFileService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.file-service"

		//StorageService
		domain.StorageService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.storage-service"

		//SettlementFeeCalculatorService
		domain.SettlementFeeCalculatorService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.settlement-fee-calculator-service"

		//MiddlewareSettlement
		domain.MiddlewareSettlement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.settlement"

		//MiddlewareSettlementAgreement
		domain.MiddlewareSettlementAgreement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.settlement.agreement"

		//AgreementService
		domain.AgreementService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.agreement-service"

		//TransactionRecording
		domain.TransactionRecording: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.transaction-recording"

		// StatementService
		domain.GeneratePayoutService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.generate-statement-payout"

		//OpenSettlementPeriod
		domain.OpenSettlementPeriod: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.open-settlement-period"

		//CloseSettlementPeriod
		domain.CloseSettlementPeriod: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.close-settlement-period"

		//ProcessFlowService
		domain.ProcessFlowService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.process-flow"

		//Settlement-reconcile-adapter
		domain.SettlementReconcileAdapter: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.settlement-reconcile-adapter"

		domain.MerchantUserService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.user-store"

		domain.TaxiIntegrationService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.taxi-integration"

		//MiddlewareMerchantZID
		domain.MiddlewareMerchantZAS: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.merchant-zas"

		//MerchantZID
		domain.MerchantZASService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-zas"

		//ZASAdapterService
		domain.ZASAdapterService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.zas-adapter"

		//MiddlewareAgreement
		domain.MiddlewareAgreement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.agreement"

		//UserAgreementService
		domain.UserAgreementService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.user-agreement"

		//For Private Api Gateway: local ZaloPay System
		domain.PrivateApiGateWay: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.private-api-gateway"
		//PrivateMiddlewareMerchantApp
		domain.PrivateMiddlewareMerchantApp: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.private-merchant-app"
		//PrivateMiddlewareMerchantZAS
		domain.PrivateMiddlewareMerchantZAS: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.private-merchant-zas"
		//PrivateMiddlewareUserAgreement
		domain.PrivateMiddlewareUserAgreement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.private-user-agreement"

		//MerchantDataLogService
		domain.MerchantDataLogService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-data-log-service"

		//StoreRegistration
		domain.StoreRegistration: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.store-registration"

		// test
		domain.TestReportService:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.test-report-service"
		domain.MiddlewareTestReport: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.test-report"
		//MiddlewareDocumentTool
		domain.MiddlewareDocumentTool: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.document-tool"

		//DocumentTool
		domain.DocumentToolService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.document-tool"

		//MerchantConfigurationService
		domain.MerchantConfigurationService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-configuration"

		//MiddlewareMerchantConfiguration
		domain.MiddlewareMerchantConfiguration: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.merchant-configuration"

		domain.MiApiGateWay:                true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-integration.api-gateway"
		domain.MiddlewareMiTokenManagement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-integration.middleware.token-management"

		//TokenWalletService
		domain.TokenWalletService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-integration.token-wallet"
		domain.PayTokenService:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-integration.pay-token"

		domain.BillTokenService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solution.bill-token"

		domain.CallBackCenterService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solution.callback-center"

		domain.MiddlewarePortalSettlement: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.portal-settlement"

		domain.MiddlewareOAWebhook: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.oa-webhook"

		//OAService
		domain.OAService: true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.oa-service"

		//BindingService
		domain.MiBindingAuthorizeService:    true, //os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solutions.binding-authorize"
		domain.MiddlewareMiBindingAuthorize: true, // os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solutionshttps://git.zapa.cloud/merchant-tools/protobuf.git
		// .middleware.binding-authorize"

		domain.ItemMgmtCoreService:       true, //ItemMgmtCoreService    = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solutions.item-mgmt-core"
		domain.MiddlewareItemMgmt:        true, //MiddlewareItemMgmt    = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solutions.middleware.item-mgmt-midd"
		domain.PrivateMiddlewareItemMgmt: true, //PrivateMiddlewareItemMgmt  = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-solutions.middleware.private-item-mgmt-zpi"
	}
)

//isConsulRegistry : check service is used Consul Registry
func isConsulRegistry(service string) bool {
	if env.IsDev() {
		return consulRegistry[service]
	}
	return true
}
