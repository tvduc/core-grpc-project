package registry

import (
	"github.com/golang/glog"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/registry/consul"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/registry/etcd"
	"google.golang.org/grpc/naming"
)

func Register(service, host string, port, ttl int) error {
	if isConsulRegistry(service) {
		return consul.Register(service, host, port, ttl)
	}
	glog.Infoln("etcd Register", service)
	return etcd.Register(service, host, port, ttl)
}
func NewResolver(service string) naming.Resolver {
	if isConsulRegistry(service) {
		return consul.NewResolver()
	}
	glog.Infoln("etcd NewResolver", service)
	return etcd.NewResolver()

}
