package consul

import (
	"context"
	"fmt"
	"sync"
	"time"

	consul "github.com/hashicorp/consul/api"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/graceful"
	"google.golang.org/grpc/naming"
)

type resolver struct {
	cc *consul.Client
}

var (
	cc   *consul.Client
	once sync.Once
)

const (
	defaultInterval = 10 * time.Second
)

func getClient() *consul.Client {
	once.Do(func() {
		conf := &consul.Config{
			Scheme:  "http",
			Address: config.GlobalConfig.Consul,
		}
		cc, _ = consul.NewClient(conf)
	})
	return cc
}
func NewResolver() naming.Resolver {

	return &resolver{cc: getClient()}
}
func (r *resolver) Resolve(service string) (naming.Watcher, error) {
	ctx, cancel := context.WithCancel(context.Background())
	return &watcher{
		service: service,
		ctx:     ctx,
		cancel:  cancel,
		t:       time.NewTimer(0),
		cc:      r.cc,
	}, nil
}
func Register(service, host string, port, ttl int) error {
	serviceID := fmt.Sprintf("%s-%s-%d", service, host, port)
	regis := &consul.AgentServiceRegistration{
		ID:      serviceID,
		Name:    service,
		Address: host,
		Port:    port,
	}
	cc := getClient()
	err := cc.Agent().ServiceRegister(regis)
	if err != nil {
		return err
	}

	// initial register service check
	check := consul.AgentServiceCheck{TTL: fmt.Sprintf("%ds", ttl), Status: "passing"}
	if err = cc.Agent().CheckRegister(&consul.AgentCheckRegistration{
		ID:                regis.ID,
		Name:              regis.Name,
		ServiceID:         regis.ID,
		AgentServiceCheck: check}); err != nil {
		return err
	}
	//de-register if meet signhup
	graceful.Stop(time.Second, func() {
		if err := cc.Agent().ServiceDeregister(serviceID); err == nil {
			cc.Agent().CheckDeregister(serviceID)
		}
	})

	// routine to update ttl
	go func() {
		ticker := time.NewTicker(defaultInterval)
		for {
			<-ticker.C
			cc.Agent().UpdateTTL(regis.ID, "", "passing")
		}
	}()
	return nil

}
