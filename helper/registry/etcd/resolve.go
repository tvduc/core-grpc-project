package etcd

import (
	"fmt"
	"strings"
	"sync"

	"git.zapa.cloud/merchant-tools/helper/config"
	etcd "go.etcd.io/etcd/clientv3"
	"google.golang.org/grpc/naming"
)

var (
	client *etcd.Client
	once   sync.Once
)

func getClient() *etcd.Client {
	once.Do(func() {
		client, _ = etcd.NewFromURLs(strings.Split(config.GlobalConfig.Etcd, ","))
	})
	return client
}
func NewResolver() naming.Resolver {

	return &GRPCResolver{Client: getClient()}
}
func Register(service, host string, port, ttl int) error {
	c := getClient()
	if c == nil {
		return fmt.Errorf("Client empty")
	}
	grantResp, err := c.Grant(c.Ctx(), int64(ttl))
	if err != nil {
		return err
	}

	r := &GRPCResolver{Client: c}
	addr := fmt.Sprintf("%s:%d", host, port)
	err = r.Update(c.Ctx(), service, naming.Update{Op: naming.Add, Addr: addr}, etcd.WithLease(grantResp.ID))
	if err != nil {
		return err
	}
	cn, err := r.Client.KeepAlive(c.Ctx(), grantResp.ID)
	if err == nil {
		go func() {
			for range cn {
			}
		}()
	}

	return err

}
