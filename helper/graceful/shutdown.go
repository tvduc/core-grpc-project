package graceful

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/golang/glog"
)

var (
	stop, shutdown sync.WaitGroup
)

func init() {
	stop.Add(1)
	go func() {
		q := make(chan os.Signal, 1)
		signal.Notify(q, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP, syscall.SIGQUIT)
		glog.Infoln(<-q)
		stop.Done()
	}()
}

//AddProcess ...
func AddProcess() {
	stop.Add(1)
}

//DoneProcess ...
func DoneProcess() {
	stop.Done()
}

//Stop ...
func Stop(t time.Duration, f func()) {
	shutdown.Add(1)
	go func() {
		stop.Wait()
		glog.Infoln("Stop")
		time.Sleep(t)
		f()
		shutdown.Done()
	}()
}

//ShutDown ...
func ShutDown() {
	stop.Wait()
	shutdown.Wait()
	glog.Infoln("Shutdown")
	glog.Flush()
	time.Sleep(3 * time.Second)
}
