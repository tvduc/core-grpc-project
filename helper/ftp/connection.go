package ftpHelper

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/error_util"
	"git.zapa.cloud/merchant-tools/helper/notify"
	"github.com/golang/glog"
	"github.com/jlaffaye/ftp"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

type FTPProtocolType int

const (
	PT_FTP  FTPProtocolType = 0 //Normal
	PT_SFTP FTPProtocolType = 1 //FTP Over ssh
	PT_FTPS FTPProtocolType = 2 //Implicit, Explicit
)

type FTPFile struct {
	Path     string
	Contents []byte
}

type FTPInfo struct {
	ProtocolType FTPProtocolType `json:"protocol_type"`
	Host         string          `json:"host"`
	User         string          `json:"user"`
	Password     string          `json:"password"`
	Timeout      uint32          `json:"timeout"` //In milisecond
	Time         uint64          `json:"time"`    //Time write to kafka
	Files        []*FTPFile      `json:"files"`
}

type FTPClient interface {
	Connect(info *FTPInfo) error
	Close() error
	ChangeDir(path string) (bool, error)
	MakeDir(path string) error
	MakeDirAll(path string) error
	MakeDirAllArray(dirs []string) error
	Store(path string, r io.Reader) error
	StoreBytes(path string, buf []byte) error
}

func NewFTPClient(info *FTPInfo) (FTPClient, error) {
	var c FTPClient
	switch info.ProtocolType {
	case PT_FTP:
		c = &NormalFTPClient{}
	case PT_SFTP:
		c = &SFTPClient{}
	default:
		return nil, errorutil.ErrorInvalidData
	}

	err := c.Connect(info)
	if err != nil {
		return nil, err
	}

	return c, nil
}

//FTP Client
type NormalFTPClient struct {
	client *ftp.ServerConn
}

func (c *NormalFTPClient) Connect(info *FTPInfo) (respErr error) {
	if info == nil {
		glog.Errorln("NormalFTPClient: info is null or file is empty")
		return errorutil.ErrorInvalidData
	}

	if len(info.Host) == 0 {
		glog.Errorln("NormalFTPClient: Host is empty")
		return errorutil.ErrorInvalidData
	}

	opts := []ftp.DialOption{}
	if info.Timeout > 0 {
		opts = append(opts, ftp.DialWithTimeout(time.Duration(info.Timeout)*time.Millisecond))
	}

	conn, err := ftp.Dial(info.Host, opts...)
	if err != nil {
		glog.Errorln("NormalFTPClient: dial error: ", err)
		notify.SendToTeams("NormalFTPClient dial", err.Error())
		return err
	}

	defer func() {
		if respErr != nil {
			conn.Quit()
		}
	}()

	if len(info.User) > 0 {
		err = conn.Login(info.User, info.Password)
		if err != nil {
			glog.Errorln("NormalFTPClient: login error: ", err)
			notify.SendToTeams("NormalFTPClient login", err.Error())
			return err
		}
	}

	err = conn.NoOp()
	if err != nil {
		glog.Errorln("NormalFTPClient: call NoOp error: ", err)
		return err
	}

	glog.Infoln("NormalFTPClient.Connect: Success")
	c.client = conn
	return nil
}

func (c *NormalFTPClient) Close() error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	return c.client.Quit()
}

func (c *NormalFTPClient) checkFTPStatusCode(errText string, status int) bool {
	sub := fmt.Sprintf("%v", status)
	return strings.Contains(errText, sub)
}

func (c *NormalFTPClient) ChangeDir(path string) (bool, error) {
	if c == nil || c.client == nil {
		return false, errorutil.ErrorInvalidData
	}

	err := c.client.ChangeDir(path)
	if err != nil {
		if c.checkFTPStatusCode(err.Error(), ftp.StatusFileUnavailable) {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (c *NormalFTPClient) MakeDir(path string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}
	return c.client.MakeDir(path)
}

// Create all directory in path on FTP Server
// With exclude last element in path
// Format: a/b/c/d.txt (exclude d.txt)
func (c *NormalFTPClient) MakeDirAll(path string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	dirs := strings.Split(path, string(os.PathSeparator))
	if len(dirs) == 1 {
		return nil
	}

	if strings.Contains(dirs[len(dirs)-1], ".") {
		dirs = dirs[:len(dirs)-1]
	}

	return c.MakeDirAllArray(dirs)
}

// Create directory recursive on FTP Server
// format: dirs[0]/dirs[1]/dirs[2]/....
func (c *NormalFTPClient) MakeDirAllArray(dirs []string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	if len(dirs) == 0 {
		return nil
	}

	for _, dir := range dirs {
		ok, err := c.ChangeDir(dir)
		if err != nil {
			glog.Errorln("createDirRecursive: change dir error: ", err)
			return err
		}

		if !ok {
			err = c.MakeDir(dir)
			if err != nil {
				glog.Errorln("createDirRecursive: mkdir error: ", err)
				return err
			}
			_, err = c.ChangeDir(dir)
			if err != nil {
				glog.Errorln("createDirRecursive: cd to fodler error: ", err)
				return err
			}
		}
	}

	return nil
}

func (c *NormalFTPClient) StoreBytes(path string, buf []byte) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	return c.Store(path, bytes.NewBuffer(buf))
}

func (c *NormalFTPClient) Store(path string, r io.Reader) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	if strings.Contains(path, string(os.PathSeparator)) {
		err := c.MakeDirAll(path)
		if err != nil {
			glog.Errorln("NormalFTPClient.Store: MakeDirAll error: ", err)
			return err
		}

		dirs := strings.Split(path, string(os.PathSeparator))
		path = dirs[len(dirs)-1]
	}

	return c.client.Stor(path, r)
}

//SFTP Client
type SFTPClient struct {
	conn   *ssh.Client
	client *sftp.Client
}

func (c *SFTPClient) Connect(info *FTPInfo) (respErr error) {
	if info == nil {
		glog.Errorln("SFTPClient.Connect: info is null or file is empty")
		return errorutil.ErrorInvalidData
	}

	if len(info.Host) == 0 {
		glog.Errorln("SFTPClient.Connect: Host is empty")
		return errorutil.ErrorInvalidData
	}

	sshConfig := &ssh.ClientConfig{
		User:            info.User,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	sshConfig.Auth = append(sshConfig.Auth, ssh.Password(info.Password))
	conn, err := ssh.Dial("tcp", info.Host, sshConfig)
	if err != nil {
		glog.Errorln("SFTPClient.Connect: dial error: ", err)
		return err
	}

	c.conn = conn
	defer func() {
		if respErr != nil {
			c.conn.Close()
		}
	}()

	client, err := sftp.NewClient(c.conn)
	if err != nil {
		glog.Errorln("SFTPClient.Connect: new client error: ", err)
		return err
	}

	glog.Infoln("SFTPClient.Connect: Success")
	c.client = client
	return nil
}

func (c *SFTPClient) Close() error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	err := c.client.Close()
	if err != nil {
		glog.Errorln("SFTPClient.Close: close client error: ", err)
		return err
	}

	return c.conn.Close()
}

func (c *SFTPClient) ChangeDir(path string) (bool, error) {
	if c == nil || c.client == nil {
		return false, errorutil.ErrorInvalidData
	}

	return true, nil
}

func (c *SFTPClient) MakeDir(path string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}
	return c.client.Mkdir(path)
}

func (c *SFTPClient) MakeDirAll(path string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	dirs := strings.Split(path, string(os.PathSeparator))
	if len(dirs) == 1 {
		return nil
	}

	if strings.Contains(dirs[len(dirs)-1], ".") {
		dirs = dirs[:len(dirs)-1]
		path = strings.Join(dirs, string(os.PathSeparator))
	}

	return c.client.MkdirAll(path)
}

func (c *SFTPClient) MakeDirAllArray(dirs []string) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	return errorutil.ErrorSystem
}

func (c *SFTPClient) StoreBytes(path string, buf []byte) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}
	return c.Store(path, bytes.NewBuffer(buf))
}

func (c *SFTPClient) Store(path string, r io.Reader) error {
	if c == nil || c.client == nil {
		return errorutil.ErrorInvalidData
	}

	if strings.Contains(path, string(os.PathSeparator)) {
		err := c.MakeDirAll(path)
		if err != nil {
			glog.Errorln("SFTPClient.Store: MakeDirAll error: ", err)
			return err
		}
	}

	f, err := c.client.Create(path)
	if err != nil {
		glog.Errorln("SFTPClient.Create: error: ", err)
		return err
	}

	if _, err := f.ReadFrom(r); err != nil {
		glog.Errorln("SFTPClient.Create: write error: ", err)
		return err
	}

	return f.Close()
}
