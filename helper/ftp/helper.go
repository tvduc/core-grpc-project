package ftpHelper

import (
	"encoding/json"
	"time"

	"git.zapa.cloud/merchant-tools/helper/config"
	errorutil "git.zapa.cloud/merchant-tools/helper/error_util"
	"git.zapa.cloud/merchant-tools/helper/kafka"
	"git.zapa.cloud/merchant-tools/helper/notify"
	"github.com/golang/glog"
)

//Upload file to FTP Server
func FTPUpload(info *FTPInfo) error {
	if info == nil || len(info.Files) == 0 {
		glog.Errorln("FTPUpload: info is null or file is empty")
		return errorutil.ErrorInvalidData
	}

	if len(info.Host) == 0 {
		glog.Errorln("FTPUpload: Host is empty")
		return errorutil.ErrorInvalidData
	}

	glog.Infoln("FTPUpload: Host =", info.Host, ", User =", info.User, ", Pass =", info.Password)

	conn, err := NewFTPClient(info)
	if err != nil {
		glog.Errorln("FTPUpload: new ftp client error: ", err)
		notify.SendToTeams("FTPUpload: new ftp client error: ", err.Error())
		return err
	}

	defer conn.Close()

	for _, file := range info.Files {
		conn.ChangeDir("/")
		err = conn.StoreBytes(file.Path, file.Contents)
		if err != nil {
			glog.Errorln("FTPUpload: Store file error: ", err)
			notify.SendToTeams("Upload store", err.Error())
			return err
		}
	}

	glog.Infoln("FTPUpload: done")
	return nil
}

//Download file from FTP Server
func FTPDownload(info *FTPInfo) error {
	return errorutil.ErrorNotFound
}

//Publish data into kafka for upload to FTP Server
func FTPUploadPublishWithWriter(writer kafka.Writer, info *FTPInfo) error {
	if info == nil {
		glog.Errorln("FTPUploadPublishWithWriter: info is null")
		return errorutil.ErrorInvalidData
	}

	info.Time = uint64(time.Now().Unix())
	buf, err := json.Marshal(info)
	if err != nil {
		glog.Errorln("FTPUploadPublishWithWriter: marshal info error: ", err)
		return err
	}

	writer.WriteRawByTopic(buf, config.FTPProxyUploadTopic)
	return nil
}

//Publish data into kafka for upload to FTP Server
func FTPUploadPublishWithConfig(cfg config.Kafka, info *FTPInfo) error {
	if info == nil {
		glog.Errorln("FTPUploadPublishWithConfig: info is null")
		return errorutil.ErrorInvalidData
	}

	info.Time = uint64(time.Now().Unix())
	buf, err := json.Marshal(info)
	if err != nil {
		glog.Errorln("FTPUploadPublishWithConfig: marshal info error: ", err)
		return err
	}

	return kafka.SyncWriteAndClose(cfg, config.FTPProxyUploadTopic, buf)
}
