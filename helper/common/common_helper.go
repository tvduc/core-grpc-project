package commonhelper

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"github.com/dustin/go-humanize"
	"github.com/fatih/structs"
	"math"
	"reflect"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/location"
	"github.com/golang/glog"
	timeHelper "github.com/vjeantet/jodaTime"

	"regexp"
)

func GetCurrentTime() time.Time {
	return time.Now()
}

func GetCurrentDateNumber() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func GetCurrentDateString() string {
	currentTime := time.Now()
	return currentTime.Format("2006-01-02 15:04:05")
}

func FormatDateString(t time.Time) string {
	return t.Format("2006-01-02")
}

func GetCurrentDateFormat(format string) string {
	currentTime := time.Now()
	return timeHelper.Format(format, currentTime)
}

func ConvertUnicodeToLatin(text string) string {
	text = regexp.MustCompile("à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ").ReplaceAllString(text, "a")
	text = regexp.MustCompile("À|Á|Ạ|Ả|Ã|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|Â|Ấ|Ầ|Ậ|Ẩ|Ẫ").ReplaceAllString(text, "A")
	text = regexp.MustCompile("è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ").ReplaceAllString(text, "e")
	text = regexp.MustCompile("Ể|Ế|Ệ|Ề|Ê|Ễ|É|È|Ẻ|Ẽ|Ẹ").ReplaceAllString(text, "E")
	text = regexp.MustCompile("ì|í|ị|ỉ|ĩ").ReplaceAllString(text, "i")
	text = regexp.MustCompile("Ì|Í|Ị|Ỉ|Ĩ").ReplaceAllString(text, "I")
	text = regexp.MustCompile("ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ").ReplaceAllString(text, "o")
	text = regexp.MustCompile("Ò|Ó|Ọ|Ỏ|Õ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|Ô|Ố|Ổ|Ộ|Ồ|Ỗ").ReplaceAllString(text, "O")
	text = regexp.MustCompile("ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ").ReplaceAllString(text, "u")
	text = regexp.MustCompile("Ừ|Ứ|Ự|Ử|Ư|Ữ|Ù|Ú|Ụ|Ủ|Ũ").ReplaceAllString(text, "U")
	text = regexp.MustCompile("ỳ|ý|ỵ|ỷ|ỹ").ReplaceAllString(text, "y")
	text = regexp.MustCompile("Ỳ|Ý|Ỵ|Ỷ|Ỹ").ReplaceAllString(text, "Y")
	text = regexp.MustCompile("đ").ReplaceAllString(text, "d")
	text = regexp.MustCompile("Đ").ReplaceAllString(text, "D")
	return text
}

//IsExistInSlice ...
func IsExistInSlice(item interface{}, slice interface{}) bool {
	s := reflect.ValueOf(slice)

	if s.Kind() != reflect.Slice {
		glog.Error("IsExistInSlice() given a non-slice type")
		return false
	}

	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Interface() == item {
			return true
		}
	}
	return false
}

//DoubleSlice ...
func DoubleSlice(s interface{}) []interface{} {
	v := reflect.ValueOf(s)
	items := make([]interface{}, v.Len())
	for i := 0; i < v.Len(); i++ {
		items[i] = v.Index(i).Interface()
	}
	return items
}

//TimeToMs ...
func TimeToMs(tm time.Time) int64 {
	return tm.UnixNano() / int64(time.Millisecond)
}

func HmacSHA256Cal(value string, key string) string {
	sig := hmac.New(sha256.New, []byte(key))
	sig.Write([]byte(value))
	return hex.EncodeToString(sig.Sum(nil))
}

func SHA256Cal(text string) string {
	algorithm := sha256.New()
	algorithm.Write([]byte(text))
	return hex.EncodeToString(algorithm.Sum(nil))
}

func GetVNCurrentDateString() string {
	/*
		loc, err := time.LoadLocation("Asia/Ho_Chi_Minh")
		if err != nil {
			glog.Errorln("GetVNCurrentDateString: error = ", err)
			return ""
		}
		return time.Now().In(loc).Format("2006-01-02")
	*/
	return time.Now().In(location.VNLocation).Format("2006-01-02")
}

func GetVNCurrentDateStringFormat(format string) string {
	return time.Now().In(location.VNLocation).Format(format)
}

func ParseIds(ids []string) []string {
	IdsResult := []string{}

	for _, id := range ids {
		id = strings.Replace(id, "[", "", -1)
		id = strings.Replace(id, "]", "", -1)
		if len(id) > 0 {
			arr := strings.Split(id, ",")
			IdsResult = append(IdsResult, arr...)
		}
	}
	return IdsResult
}

func convert3Number(numVal int64, fullStrLen int32, isLast3Num bool) string {
	arrNumInStr := []string{"", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"}
	var strResult string
	uTen := numVal % 10 //const
	numVal /= 10
	ten := numVal % 10
	numVal /= 10
	hundered := numVal % 10
	if hundered > 0 {
		strResult = arrNumInStr[hundered] + " trăm"
	}
	if ten > 0 {
		if hundered == 0 && fullStrLen > 3 && isLast3Num {
			strResult += "không trăm"
		}
		if ten == 1 {
			strResult += " mười"
		} else {
			strResult = strResult + " " + arrNumInStr[ten] + " mươi"
		}
	}
	if uTen > 0 {
		if hundered > 0 && ten == 0 {
			strResult += " lẻ " + arrNumInStr[uTen]
		} else if ten > 0 {
			if ten == 1 && uTen == 1 {
				strResult += " một"
			} else if uTen == 5 {
				strResult += " lăm"
			} else if uTen == 1 {
				strResult += " mốt"
			} else {
				strResult = strResult + " " + arrNumInStr[uTen]
			}
		} else {
			strResult = strResult + " " + arrNumInStr[uTen]
		}
	}
	return strResult
}

//Đổi số tiền thành chữ tiếng Việt
func MoneyToText(num int64) string {
	if num == 0 {
		return "không"
	}
	var result string
	fullStrLen := int32((math.Log10(float64(num))) + 1)
	var billionStr, millionStr, thousandStr, underThousandStr string
	var billion, million, thousand, underThousand int64
	underThousand = num % 1000
	underThousandStr = convert3Number(underThousand, fullStrLen, true)
	num /= 1000
	if num > 0 {
		thousand = num % 1000
		thousandStr = convert3Number(thousand, fullStrLen, false)
		num /= 1000
		if thousand != 0 {
			thousandStr += " nghìn "
		}
	}
	if num > 0 {
		million = num % 1000
		millionStr = convert3Number(million, fullStrLen, false)
		num /= 1000
		if million != 0 {
			millionStr += " triệu "
		}
	}
	if num > 0 {
		billion = num % 1000
		billionStr = convert3Number(billion, fullStrLen, false)
		if billion != 0 {
			billionStr += " tỉ "
		}
		num /= 1000
	}

	result = billionStr + millionStr + thousandStr + underThousandStr
	return result
}

func SplitSubString(str, sep string) (string, string) {
	idx := strings.Index(str, sep)
	if idx == -1 {
		return str, ""
	}

	return str[:idx], str[idx+len(sep):]
}

//ObjectToMap chuyển 1 interface sang dạng map[string]interface{}
func ObjectToMap(obj interface{}) (map[string]interface{}, error) {
	result := structs.Map(obj)
	err := AddCommaToMap(result, obj)
	if err != nil {
		glog.Errorln(err)
	}
	return result, err
}

//sửa số có dạng 6000 thành 6,000 trong lúc xuất file excel
func AddCommaToMap(oriMap map[string]interface{}, obj interface{}) error {
	if oriMap == nil {
		oriMap = make(map[string]interface{})
	}
	val := reflect.ValueOf(obj)
	if val.Kind() == reflect.Ptr {
		if val.IsNil() {
			glog.Errorln("nil ...")
			return errors.New("Object can not be nil")
		}
		val = val.Elem()
	}
	vt := val.Type()

	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		fieldName := field.Name
		v := val.FieldByName(field.Name)
		fieldType := field.Type

		switch fieldType.Name() {

		case "float32", "float64":
			{
				oriMap[fieldName] = humanize.Commaf(math.Round(v.Float()))
			}
		case "int32", "int64":
			{
				oriMap[fieldName] = humanize.Comma(v.Int())
			}

		}
	}
	glog.Info(oriMap)
	return nil
}
