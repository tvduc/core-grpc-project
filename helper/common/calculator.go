package commonhelper

import "math"

func GetFeeVAT(realFeeAmount int64) int64 {
	return int64(math.Round(float64(realFeeAmount)/11))
}