package consul

import (
	"fmt"
	"strconv"

	"git.zapa.cloud/merchant-tools/helper/core"
	"github.com/golang/glog"
	consul "github.com/hashicorp/consul/api"
)

type ConsulStore struct {
	cc *consul.Client
	li uint64
}
type ConsulService struct {
	cc    *consul.Client
	regis *consul.AgentServiceRegistration
	ttl   int
}

func (c *ConsulStore) LookupSRV(serviceName string, all bool) ([]string, error) {
	var q *consul.QueryOptions
	//glog.Infoln("LookupSRV", serviceName)
	if !all {
		q = &consul.QueryOptions{WaitIndex: c.li}
	}
	cs, meta, err := c.cc.Health().Service(serviceName, "", true, q)
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}

	newAddrs := make([]string, len(cs))
	for i, s := range cs {
		// addr should like: 127.0.0.1:8001
		newAddrs[i] = s.Service.Address + ":" + strconv.Itoa(int(s.Service.Port))
	}
	c.li = meta.LastIndex

	return newAddrs, nil
}

//NewService ...
func NewService(name string, host string, port int, target string, ttl int) *ConsulService {
	cc, err := getConsulClient(target)
	if err != nil {
		return nil
	}
	serviceID := fmt.Sprintf("%s-%s-%d", name, host, port)
	regis := &consul.AgentServiceRegistration{
		ID:      serviceID,
		Name:    name,
		Address: host,
		Port:    port,
	}
	return &ConsulService{
		cc:    cc,
		regis: regis,
		ttl:   ttl,
	}
}

//Register ...
func (s *ConsulService) Register() error {
	regis := s.regis
	err := s.cc.Agent().ServiceRegister(regis)
	if err != nil {
		return err
	}

	// initial register service check
	check := consul.AgentServiceCheck{TTL: fmt.Sprintf("%ds", s.ttl), Status: "passing"}
	return s.cc.Agent().CheckRegister(&consul.AgentCheckRegistration{
		ID:                regis.ID,
		Name:              regis.Name,
		ServiceID:         regis.ID,
		AgentServiceCheck: check})

}

//UpdateTTL ...
func (s *ConsulService) UpdateTTL() error {
	return s.cc.Agent().UpdateTTL(s.regis.ID, "", "passing")
}

//DeRegister ...
func (s *ConsulService) DeRegister() error {
	cc := s.cc
	serviceID := s.regis.ID
	if err := cc.Agent().ServiceDeregister(serviceID); err != nil {
		return err
	}
	return cc.Agent().CheckDeregister(serviceID)
}

//NewStore ...
func NewStore(target, serviceName string) *ConsulStore {
	cc, err := getConsulClient(target)
	if err != nil {
		return nil
	}
	return &ConsulStore{cc: cc}
}
func getConsulClient(target string) (*consul.Client, error) {
	c, err := core.GetCache("getConsulClient"+target, func() (interface{}, error) {

		// generate consul client, return if error
		conf := &consul.Config{
			Scheme:  "http",
			Address: target,
		}
		return consul.NewClient(conf)
	})
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}
	return c.(*consul.Client), nil
}
