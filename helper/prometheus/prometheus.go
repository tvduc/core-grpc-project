package prometheus

import (
	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"net/http"
)

var (
	addr = "0.0.0.0:12344"
)

func init() {
	grpc_prometheus.DefaultClientMetrics.EnableClientHandlingTimeHistogram()
	go func() {
		glog.Infoln("prometheus start ", addr)
		reg := prometheus.NewRegistry()
		// Register client metrics to registry.
		reg.MustRegister(grpc_prometheus.DefaultClientMetrics)
		httpServer := &http.Server{Handler: promhttp.HandlerFor(reg, promhttp.HandlerOpts{}), Addr: addr}
		if err := httpServer.ListenAndServe(); err != nil {
			glog.Fatal("Unable to start a http server.", err)
		}
	}()
}

//UnaryClientInterceptor ...
func UnaryClientInterceptor() grpc.UnaryClientInterceptor {
	return grpc_prometheus.UnaryClientInterceptor
}
