package lock

import (
	"os"
	"time"

	"git.zapa.cloud/merchant-tools/helper/notify"

	"github.com/go-redis/redis"
)

type Lock interface {
	//Lock keys: user:userid,key:deviceid
	Lock(keys ...string) bool
	Unlock(keys ...string)
	IsLock(keys ...string) bool
}
type redisLock struct {
	r *redis.Client
}

//NewRedisLock ...
func NewRedisLock(r *redis.Client) Lock {
	return &redisLock{r: r}
}

const (
	expireTime = 15 * time.Minute
)

var (
	redisLockKey = os.Getenv("SERVICE_PREFIX_NAME") + "redisLockKey"
)

func makeLockKey(keys ...string) []string {
	lockKeys := make([]string, len(keys))
	for i, key := range keys {
		lockKeys[i] = redisLockKey + key
	}
	return lockKeys
}
func (l *redisLock) Lock(keys ...string) bool {
	for i, key := range makeLockKey(keys...) {
		if res, err := l.r.SetNX(key, "1", expireTime).Result(); err != nil || !res {
			notify.SendToTeams("locked", keys[i])
			if i > 0 {
				l.Unlock(keys[0:i]...)
			}
			return false
		}
	}
	return true
}
func (l *redisLock) Unlock(keys ...string) {
	if len(keys) > 0 {
		l.r.Del(makeLockKey(keys...)...)
	}

}
func (l *redisLock) IsLock(keys ...string) bool {
	res, err := l.r.Exists(makeLockKey(keys...)...).Result()
	return err != nil || res > 0
}
