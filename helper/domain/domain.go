package domain

import (
	"os"
)

var (
	//MiddlewareMerchantManage merchant management
	MiddlewareMerchantManage = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.middleware.merchantmanage"
	//MerchantManage admin:
	MerchantManage = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.merchant-manage"
	//API GateWay
	ApiGateWay = os.Getenv("SERVICE_PREFIX_NAME") + "merchant-tools.api-gateway"
)
