package random

import (
	"math/rand"
	"time"
)

const numberCharset = "0123456789"
const stringCharset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func StringWithCharset(length int, charset string) string {

	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomDigitString(length int) string {
	return StringWithCharset(length, numberCharset)
}

func RandomOTPDigitString() string {
	return RandomDigitString(6)
}

func RandomDigitStringWithCharset(charset string, length int) string {
	return StringWithCharset(length, charset)
}

func RandomTokenString() string {
	return StringWithCharset(32, stringCharset)
}

func RandomTokenStringWithLength(length int) string {
	return StringWithCharset(length, stringCharset)
}
