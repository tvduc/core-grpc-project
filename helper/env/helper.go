package env

import (
	"os"
	"strings"
)

type envType int

const (
	envDev     envType = iota
	envSandbox
	envProd
)

var (
	envCur = envDev
)

func init() {
	strEnv := strings.ToUpper(os.Getenv("SERVICE_PREFIX_NAME"))
	if strings.HasPrefix(strEnv, "PROD") {
		envCur = envProd
	} else if strings.HasPrefix(strEnv, "SANDBOX") {
		envCur = envSandbox
	}
}

//IsProd ....
func IsProd() bool {
	return envCur == envProd
}

//IsSandbox ....
func IsSandbox() bool {
	return envCur == envSandbox
}

//IsDev ....
func IsDev() bool {
	return envCur == envDev
}

//GetEnvString ....
func GetEnvString(s string) string {
	return os.Getenv("SERVICE_PREFIX_NAME") + s
}
