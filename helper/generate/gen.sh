#!/bin/bash
if [ -z "$1" ]; then 
echo 'Empty project'
exit
fi
ext='.proto'
path='../../'
project="$path$1"
path='../../protobuf/'
protobuf="$path$1"
slash='/'
protofile="$protobuf$slash$1$ext"
if [ -d "$project" -o -d "$protobuf" ]; then
echo 'Project is exist'
exit
fi
cp -R 'service/' $project
cp -R 'protobuf/' $protobuf
echo "generate success"
