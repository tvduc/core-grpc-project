package sms

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/config"
	"github.com/golang/glog"
	"github.com/google/tink/go/subtle/signature"
)

type SendRequest struct {
	//Delay   time.Duration
	Message SMS
}

type SMS struct {
	Phone     string `json:"user_id"` //phone
	Sig       string `json:"sig"`
	ClientId  string `json:"client_id"`
	BrandName string `json:"brand_name"`
	Content   string `json:"content"`
	Time      int64  `json:"time"`
	Type      int32  `json:"type"`
}

type SMSResponse struct {
	// {"transID":201805150004228,"status":1,"message":"Put message to queue success!"}
	TransID int64  `json:"transID"`
	Status  int    `json:"status"`
	Message string `json:"message"`
}

var (
	privateKeyDefault = `-----BEGIN EC PRIVATE KEY-----
MIHcAgEBBEIBVx8zQei4ttKSBloQbZtdD9Xqw5hBjdD1U7Hi8kyYoLtz6ta2FM42
hwhNJj7lstb10AEZUg8fs5lSdItMrLwaZOygBwYFK4EEACOhgYkDgYYABAATjSPv
l26mjQ6O/FX69IFarQvlH04ONLjMkTLxyBMIABBkWQ9NDm0kv7r+sMCCFmCUcKha
C1gnVL4BgSFJAhHkrQANrn4rx6H7MPp+kcW39bgh0BwXrVEoCKVbwjpJDDaNtPU1
K3GTUxvcFUv/5xdxQY7ZkhtB4MDUwkkL6p0pAP/Kig==
-----END EC PRIVATE KEY-----`
)

func rawSignature(privateKey *ecdsa.PrivateKey, hash crypto.Hash, data string) ([]byte, error) {
	if privateKey == nil {
		return nil, errors.New("Private key is null")
	}
	if !hash.Available() {
		glog.Errorln("rawSignature: Hash is invalid")
		return nil, errors.New("Hash is invalid")
	}

	h := hash.New()
	_, err := io.WriteString(h, data)
	if err != nil {
		glog.Errorln("rawSignature: Write hash data error: ", err)
		return nil, err
	}

	digest := h.Sum(nil)
	r, s, err := ecdsa.Sign(rand.Reader, privateKey, digest[:])
	if err != nil {
		glog.Errorln("rawSignature: Sign error: ", err)
		return nil, err
	}

	sig := signature.NewECDSASignature(r, s)
	sigRS, err := sig.EncodeECDSASignature("DER", privateKey.Params().Name)
	if err != nil {
		glog.Errorln("rawSignature: Encode signature error: ", err)
		return nil, err
	}

	return sigRS, nil
}

func buildSigEx(sms *SMS, smsConfig *config.Sms) error {
	if len(smsConfig.PrivateKey) == 0 {
		smsConfig.PrivateKey = privateKeyDefault
	}

	// sig = {client_id:'client_id',user_id:'user_id',brand_name:'brand_name',content:'content',time:'time'}
	msg := fmt.Sprintf("{client_id:'%s',user_id:'%s',brand_name:'%s',content:'%s',time:'%d'}",
		sms.ClientId, sms.Phone, sms.BrandName, sms.Content, sms.Time)

	block, _ := pem.Decode([]byte(smsConfig.PrivateKey))
	if block == nil {
		return errors.New("buildSigEx: cannot decode private key")
	}

	privateKey, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		glog.Errorln("buildSigEx: Parse private key error: ", err)
		return err
	}

	buf, err := rawSignature(privateKey, crypto.SHA384, msg)
	if err != nil {
		glog.Errorln("buildSigEx: Sign error: ", err)
		return err
	}

	sms.Sig = base64.StdEncoding.EncodeToString(buf)
	return nil
}

func buildSig(sms *SMS, smsConfig *config.Sms) {
	// sig = client_id + user_id + content + time + key
	msg := fmt.Sprintf("%s%s%s%d%s", sms.ClientId, sms.Phone, sms.Content, sms.Time, smsConfig.SecretKey)
	h := crypto.SHA256.New()
	h.Write([]byte(msg))
	sms.Sig = fmt.Sprintf("%x", h.Sum(nil))
}

func SendSMS(phone, content string, smsType int32, smsConfig *config.Sms) (*SMSResponse, error) {
	//TODO create order based on SMS type
	sms := &SMS{}
	if strings.HasPrefix(phone, "0") {
		sms.Phone = strings.Replace(phone, "0", "84", 1)
	} else {
		sms.Phone = phone
	}
	sms.BrandName = smsConfig.BrandName
	sms.Content = content
	sms.ClientId = smsConfig.ClientID
	sms.Time = time.Now().Unix()
	sms.Type = smsType

	if strings.Contains(smsConfig.EndPoint, "/Sign") {
		err := buildSigEx(sms, smsConfig)
		if err != nil {
			glog.Errorln("SendSMS: build sig error: ", err)
			return nil, err
		}
	} else {
		buildSig(sms, smsConfig)
	}

	return sms.Send(smsConfig)
}

func (sms *SMS) Send(smsConfig *config.Sms) (*SMSResponse, error) {
	if smsConfig == nil || len(smsConfig.EndPoint) == 0 {
		return nil, nil
	}
	jsonStr, err := json.Marshal(sms)
	if err != nil {
		return nil, err
	}

	glog.Infoln("SMS Send: Phone = ", sms.Phone)
	req, err := http.NewRequest(http.MethodPost, smsConfig.EndPoint, bytes.NewBuffer(jsonStr))
	if err != nil {
		return nil, err
	}

	client := &http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	var smsResponse SMSResponse
	if resp.StatusCode == http.StatusOK {
		glog.Infoln("SMS Send: response = ", string(body))
		err := json.Unmarshal(body, &smsResponse)
		if err != nil {
			return nil, err
		}
		return &smsResponse, nil
	}
	return nil, fmt.Errorf("mvas return bad http status: %d", resp.StatusCode)
}
