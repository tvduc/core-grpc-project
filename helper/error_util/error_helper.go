package errorutil

import (
	"github.com/go-sql-driver/mysql"
	"github.com/golang/glog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//MysqlTranslateError : chuyển đổi lỗi mysql to logic
func MysqlTranslateError(err error, text string) error {
	glog.Error(err)
	me, ok := err.(*mysql.MySQLError)
	if !ok {
		return err
	}

	switch me.Number {
	case 1022, 1062:
		return grpc.Errorf(codes.AlreadyExists, "%s đã tồn tại", text)
	}

	return err
}

func IsInsertExistsError(err error) bool {
	me, ok := err.(*mysql.MySQLError)
	if !ok {
		return false
	}

	if me.Number == 1022 || me.Number == 1062 {
		return true
	}
	return false
}

func IsTableNotExistsError(err error) bool {
	me, ok := err.(*mysql.MySQLError)
	if !ok {
		return false
	}

	if me.Number == 1051 {
		return true
	}
	return false
}
