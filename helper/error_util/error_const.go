package errorutil

import (
	grpcCodes "google.golang.org/grpc/codes"
	grpcStatus "google.golang.org/grpc/status"
)

const (
	ERROR_SYSTEM                 = "Lỗi hệ thống"
	ERROR_REQUIRED_LOGIN         = "Yêu cầu login"
	ERROR_DUPLICATE_USER         = "User đã tồn tại"
	ERROR_DUPLICATE_EMAIL        = "Email đã tồn tại"
	ERROR_DUPLICATE_MC_EMAIL     = "Email Merchant này đã tồn tại"
	ERROR_DUPLICATE_MC_PHONE_NO  = "Số điện thoại Merchant đã tồn tại"
	ERROR_DUPLICATE_PHONE        = "Số điện thoại đã tồn tại"
	ERROR_INVALID_DATA           = "Dữ liệu không hợp lệ"
	ERROR_INVALID_DATA_SEARCH    = "Dữ liệu tra cứu không hợp lệ"
	ERROR_INVALID_OTP            = "OTP không hợp lệ"
	ERROR_INVALID_USERNAME       = "Username không hợp lệ"
	ERROR_INVALID_PASSWORD       = "Mật khẩu không hợp lệ"
	ERROR_PERMISSION_DENIED      = "Permission denied"
	ERROR_PERMISSION_INVALID     = "Không có quyền thực hiện thao tác"
	ERROR_INVALID_OLD_PASSWORD   = "Mật khẩu cũ không đúng"
	ERROR_DUPLICATE_PASSWORD     = "Mật khẩu cũ trùng mật khẩu mới"
	ERROR_AUTHENTICATION_FAIL    = "Thông tin đăng nhập không hợp lệ"
	ERROR_USER_INACTIVE          = "Tài khoản bị ngưng hoạt động"
	ERROR_UPDATE_FAIL            = "Cập nhật thất bại"
	ERROR_TOKEN_EXPIRED          = "Phiên đăng nhập hết hạn hoặc không tồn tại"
	ERROR_CREATE_OTP_RATE_LIMIT  = "Số điện thoại này quá giới hạn tạo OTP 10 lần/giờ"
	ERROR_CHECK_OTP_RATE_LIMIT   = "Nhập OTP quá số lần qui định 10 lần/giờ. Bạn hãy tạo lại OTP"
	ERROR_FORGOT_PASS_RATE_LIMIT = "Quên mật khẩu quá số lần qui định 10 lần/giờ"
	ERROR_LOGIN_RATE_LIMIT       = "Đăng nhập quá số lần qui định 50 lần/giờ"

	ERROR_INVALID_MERCHANT_NAME      = "Tên merchant không hợp lệ"
	ERROR_INSERT_BRANCH_FAIL         = "Thêm chi nhánh thất bại"
	ERROR_UPDATE_BRANCH_FAIL         = "Cập nhật chi nhánh thất bại"
	ERROR_DELETE_BRANCH_FAIL         = "Xóa chi nhánh thất bại"
	ERROR_INSERT_STORE_FAIL          = "Thêm cửa hàng thất bại"
	ERROR_UPDATE_STORE_FAIL          = "Cập nhật cửa hàng thất bại"
	ERROR_DELETE_STORE_FAIL          = "Xóa cửa hàng thất bại"
	ERROR_INSERT_CASHIER_FAIL        = "Thêm quầy thất bại"
	ERROR_UPDATE_CASHIER_FAIL        = "Cập nhật quầy thất bại"
	ERROR_DELETE_CASHIER_FAIL        = "Xóa quầy thất bại"
	ERROR_DUPLICATE_DEPARTMENT       = "Tên đã tồn tại"
	ERROR_DELETE_STORE_ALREADY_TRANS = "Cửa hàng đã tồn tại giao dịch"

	ERROR_INSERT_SP_FAIL   = "Thêm Service Provider thất bại"
	ERROR_UPDATE_SP_FAIL   = "Cập nhật Service Provider thất bại"
	ERROR_INSERT_SALE_FAIL = "Thêm Sale thất bại"
	ERROR_UPDATE_SALE_FAIL = "Cập nhật Sale thất bại"

	ERROR_INSERT_USER_FAIL         = "Thêm user thất bại"
	ERROR_UPDATE_USER_FAIL         = "Cập nhật user thất bại"
	ERROR_ACTIVATE_TOKEN_USER_FAIL = "Kích hoạt tài khoản thất bại"

	//For merchant
	ERROR_MERCHANT_INVALID                  = "Merchant không hợp lệ"
	ERROR_MERCHANT_BLOCKED                  = "Tài khoản bị ngưng hoạt động"
	ERROR_MERCHANT_UPDATE_USER_FAIL         = "Cập nhật user thất bại"
	ERROR_MERCHANT_UPDATE_USER_STATUS_FAIL  = "Chọn status user không hợp lệ"
	ERROR_MERCHANT_UPDATE_FAIL              = "Cập nhật merchant thất bại"
	ERROR_RESET_BILLING                     = "Không thể đặt lại chu kỳ này"
	ERROR_RESET_BILLING_FEE                 = "Không thể đặt lại phí cho chu kỳ này"
	ERROR_DUPLICATE_MERCHANT                = "Merchant đã tồn tại"
	ERROR_DUPLICATE_MERCHANT_NAME           = "Tên Merchant đã tồn tại"
	ERROR_MERCHANT_PROFILE_MISSING_BUSINESS = "Hồ sơ merchant không hợp lệ: chưa có thông tin doanh nghiệp"
	ERROR_MERCHANT_PROFILE_MISSING_PAYMENT  = "Hồ sơ merchant không hợp lệ: chưa có thông tin thanh toán"

	//For app
	ERROR_DUPLICATE_APP                            = "App đã tồn tại"
	ERROR_APP_ID_INVALID                           = "App id không hợp lệ"
	ERROR_ALREADY_FEE_CHARGE_DATE                  = "Đã thiết lập ngày tính phí trước đây (chỉ được thiết lập 1 lần)"
	ERROR_APP_DATA_INVALID_WITH_ZLP                = "Dữ liệu App hiện tại trong database không khớp với TPE"
	ERR_APP_INTEGRATION_METHOD_INVALID_WITH_MC     = "Cấu hình thanh toán không hợp lệ với phương thức tích hợp trong hồ sơ merchant"
	ERR_APP_INTEGRATION_METHOD_INVALID_BANK_STATUS = "Trạng thái cấu hình bank không đồng nhất trong system Ví"
	ERROR_APP_CALLBACK_URL_INVALID                 = "App callback url không hợp lệ"
	ERROR_APP_REDIRECT_URL_INVALID                 = "App redirect url không hợp lệ"
	ERROR_APP_ID_NOT_HAVE_FA_INFO                  = "AppID/PaymentID chưa được tạo mã FA"
	ERROR_FA_CODE_NOT_MAP_ACCOUNT_CODE             = "Mã FA liên quan chưa có account code"
	ERROR_APP_NOT_GRANT_MCC_CODE                   = "AppID chưa được gán MCC Code"
	ERROR_MCC_CODE_NOT_CONFIRM                     = "MCC Code chưa được duyệt"
	ERROR_MCC_CODE_APP_DIFF_MERCHANT               = "MCC Code khác với merchant"

	//For cashin
	ERROR_CASHIN_MERCHANT_INVALID        = "Cashin không hợp lệ"
	ERROR_CASHIN_MERCHANT_BLOCKED        = "Cashin bị ngưng hoạt động"
	ERROR_CASHIN_MERCHANT_UPDATE_FAIL    = "Cập nhập Cashin thất bại"
	ERROR_DUPLICATE_CASHIN_MERCHANT      = "Cashin đã tồn tại"
	ERROR_CASHIN_WALLET_MERCHANT_INVALID = "Ví và chi hộ không cùng merchant"

	//For wallet
	ERROR_WALLET_INVALID          = "Ví không hợp lệ"
	ERROR_WALLET_BLOCKED          = "Ví bị ngưng hoạt động"
	ERROR_WALLET_UPDATE_FAIL      = "Cập nhập Ví thất bại"
	ERROR_DUPLICATE_WALLET        = "Ví đã tồn tại"
	ERROR_WALLET_MONEY_NOT_ENOUGH = "Ví không đủ tiền"

	//For service provider
	ERROR_SP_DUPLICATE_PHONE = "Số điện thoại đăng nhập đã tồn tại"

	//For reconcile app billing cycle
	ERROR_DUPLICATE_APP_BILLING_CYCLE = "Chu kỳ thanh toán đã tồn tại"

	//For user_role
	ERROR_DUPLICATE_NAME_ROLE          = "Tên nhóm quyền đã tồn tại"
	ERROR_USER_ROLE_NOT_ALLOWED_UPDATE = "Không được phép chỉnh sửa nhóm quyền hệ thống"

	//For send mail
	ERROR_SEND_MAIL_REGISTER = "Không thể gửi mail cho tài khoản"

	//For merchant admin
	ERROR_NOT_CHOOSE_TYPE_ADMIN = "Loại tài khoản admin không hợp lệ"

	ERROR_NOT_FOUND    = "Không tồn tại"
	ERROR_SQL_GENERATE = "Generate sql fail"
	//For bo-reconcile
	ERROR_CANCEL_RECONCILIATION = "Không thể huỷ đối soát - Chu kỳ này đã được thanh toán"

	//For payment-info
	ERROR_DUPLICATE_PAYMENT_ID           = "Mã thông tin thanh toán đã tồn tại"
	ERROR_PAYMENT_CHOOSE_DEFAULT_INVALID = "Mã thông tin thanh toán này không thể làm mặc định (hậu tố là mã ngân hàng)"

	//For product
	ERROR_DUPLICATE_PRODUCT_CODE = "Mã sản phẩm đã tồn tại"

	//For fa info
	ERROR_DUPLICATE_FA_CODE           = "Mã fa đã tồn tại"
	ERROR_DUPLICATE_FA_APP_PAYMENT_ID = "Mã App/PaymentID đã được gán"

	//For promotion config
	ERROR_PROMOTION_CONFIG_GROUP_SALE_INVALID = "Cài đặt số tiền group sale không hợp lệ"

	//For Agreement
	ERROR_AQUIRING_INFO_NOT_CREATED  = "Cấu hình cơ bản chưa được tạo"
	ERROR_BILLING_CYCLE_NOT_CREATED  = "Chu kì thanh toán chưa được tạo"
	ERROR_MERCHANT_FEE_NOT_CREATED   = "Phí merchant chưa được tạo"
	ERROR_AGREEMENT_CENTER_DUPLICATE = "Agreement đã tồn tại"
	ERROR_AGREEMENT_SETTLE_CONFIG    = "Cấu hình Settlement Agreement không hợp lệ"

	//For merchant user
	ERROR_MERCHANT_USER_STORE_CODE_INVALID = "Mã cửa hàng không hợp lệ"

	//For taxi integration
	ERROR_TAXI_INTEGRATION_CALL_CREATE_ORDER_ZLP_FAILED = "Không thể kết nối đến zalopay server"
	ERROR_TAXI_INTEGRATION_EMV_CODE_INVALID             = "EMV Code không hợp lệ"
	ERROR_TAXI_INTEGRATION_EMV_AMOUNT_INVALID           = "số tiền không hợp lệ"
	ERROR_TAXI_CHECK_CALLBACK_INVALID                   = "Callback không hợp lệ"

	//For item mgmt
	ERROR_ITEM_MGMT_TOO_MANY_OAID                         = "Có nhiều hơn 1 zalo OA đang liên kết với app id này"
	ERROR_ITEM_MGMT_TOO_MANY_APP_ID                       = "OAID này đã liên kết với 1 app id khác"
	ERROR_ITEM_MGMT_CANNOT_DELETE_CATE                    = "Đang tồn tại sản phẩm hoặc nhóm hàng thuộc về loại thực đơn này "
	ERROR_ITEM_MGMT_CANNOT_DELETE_SUB_CATE                = "Đang tồn tại sản phẩm thuộc về nhóm hàng đơn này "
	ERROR_ITEM_MGMT_ITEM_CODE_ALREADY_EXISTS              = "Mã sản phẩm hoặc tên sản phẩm đã tồn tại"
	ERROR_ITEM_MGMT_OA_ALREADY_EXISTS                     = "Thông tin OA cho app id đã tồn tại"
	ERROR_OA_ID_INVALID                                   = "OA ID không hợp lệ"
	ERROR_ITEM_MGMT_CANNOT_GET_OA_ZALO_INFO_BY_IDENTIFIER = "Số điện thoại nhận ZMS không hợp lệ (Hãy chắc chắn SĐT đã flow OA)"

	//for OA
	ERROR_OA_CANNOT_GET_OA_INFO = "Không thể lấy thông tin người dùng"

	ERROR_ITEM_MGMT_OUT_OF_WORKING = "Cửa hàng không hoạt động vào lúc này. Vui lòng quay lại sau"
	//For User Agreement
	ERROR_USER_AGREEMENT_PRODUCT_INVALID = "Product type is invalid"
	ERROR_USER_AGREEMENT_STATUS_INVALID  = "Status is invalid"

	//For merchant agreement
	ERROR_INVALID_START_TIME_MERCHANT_FEE = "Ngày bắt đầu hiệu lực phải nhỏ hơn hoặc bằng ngày bắt đầu tính phí của Agreement"
)

var (
	ErrorSystem                  = grpcStatus.Errorf(grpcCodes.Internal, ERROR_SYSTEM)
	ErrorNotFound                = grpcStatus.Errorf(grpcCodes.Internal, ERROR_NOT_FOUND)
	ErrorSQLGenerate             = grpcStatus.Errorf(grpcCodes.Internal, ERROR_SQL_GENERATE)
	ErrorUserExisted             = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_USER)
	ErrorEmailExisted            = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_EMAIL)
	ErrorMCEmailExisted          = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_MC_EMAIL)
	ErrorMCPhoneNoExisted        = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_MC_PHONE_NO)
	ErrorPhoneExisted            = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_PHONE)
	ErrorUnauthenticated         = grpcStatus.Errorf(grpcCodes.Unauthenticated, ERROR_REQUIRED_LOGIN)
	ErrorPermission              = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_PERMISSION_DENIED)
	ErrorPermissionInvalid       = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_PERMISSION_INVALID)
	ErrorInvalidData             = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_INVALID_DATA)
	ErrorInvalidDataSearch       = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_INVALID_DATA_SEARCH)
	ErrorAuthenticationFail      = grpcStatus.Errorf(grpcCodes.Unauthenticated, ERROR_AUTHENTICATION_FAIL)
	ErrorUpdateFail              = grpcStatus.Errorf(grpcCodes.ResourceExhausted, ERROR_UPDATE_FAIL)
	ErrorTokenExpired            = grpcStatus.Errorf(grpcCodes.Unauthenticated, ERROR_TOKEN_EXPIRED)
	ErrorCreateOTPRateLimit      = grpcStatus.Errorf(grpcCodes.ResourceExhausted, ERROR_CREATE_OTP_RATE_LIMIT)
	ErrorCheckOTPRateLimit       = grpcStatus.Errorf(grpcCodes.ResourceExhausted, ERROR_CHECK_OTP_RATE_LIMIT)
	ErrorForgotPasswordRateLimit = grpcStatus.Errorf(grpcCodes.ResourceExhausted, ERROR_FORGOT_PASS_RATE_LIMIT)
	ErrorLoginRateLimit          = grpcStatus.Errorf(grpcCodes.ResourceExhausted, ERROR_LOGIN_RATE_LIMIT)

	ErrorCreateBranchFail        = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_BRANCH_FAIL)
	ErrorUpdateBranchFail        = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_BRANCH_FAIL)
	ErrorDeleteBranchFail        = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_DELETE_BRANCH_FAIL)
	ErrorCreateStoreFail         = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_STORE_FAIL)
	ErrorUpdateStoreFail         = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_STORE_FAIL)
	ErrorDeleteStoreFail         = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_DELETE_STORE_FAIL)
	ErrorCreateCashierFail       = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_CASHIER_FAIL)
	ErrorUpdateCashierFail       = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_CASHIER_FAIL)
	ErrorDeleteCashierFail       = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_DELETE_CASHIER_FAIL)
	ErrorDepartmentExisted       = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_DEPARTMENT)
	ErrorDeleteStoreAlreadyTrans = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DELETE_STORE_ALREADY_TRANS)

	ErrorCreateSPFail   = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_SP_FAIL)
	ErrorUpdateSpFail   = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_SP_FAIL)
	ErrorCreateSaleFail = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_SALE_FAIL)
	ErrorUpdateSaleFail = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_SALE_FAIL)

	ErrorCreateUserFail        = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INSERT_USER_FAIL)
	ErrorUpdateUserFail        = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_UPDATE_USER_FAIL)
	ErrorInvalidOTP            = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_INVALID_OTP)
	ErrorActivateTokenUserFail = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_ACTIVATE_TOKEN_USER_FAIL)

	ErrorMerchantBlocked                = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_MERCHANT_BLOCKED)
	ErrorMerchantUpdateUserFail         = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_MERCHANT_UPDATE_USER_FAIL)
	ErrorMerchantUpdateUserStatusFail   = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_MERCHANT_UPDATE_USER_STATUS_FAIL)
	ErrorMerchantInvalid                = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_MERCHANT_INVALID)
	ErrorMerchantUpdateFail             = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MERCHANT_UPDATE_FAIL)
	ErrorMerchantExisted                = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_MERCHANT)
	ErrorMerchantNameExisted            = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_MERCHANT_NAME)
	ErrorMerchantProfileMissingBusiness = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MERCHANT_PROFILE_MISSING_BUSINESS)
	ErrorMerchantProfileMissingPayment  = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MERCHANT_PROFILE_MISSING_PAYMENT)

	ErrorInvalidOldPassword = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_INVALID_OLD_PASSWORD)
	ErrorDuplicatePassword  = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_DUPLICATE_PASSWORD)
	ErrorInvalidPassword    = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_INVALID_PASSWORD)

	ErrorAppExisted                   = grpcStatus.Errorf(grpcCodes.Internal, ERROR_DUPLICATE_APP)
	ErrorAppIdInvalid                 = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_APP_ID_INVALID)
	ErrorAlreadyFeeChargeDate         = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_ALREADY_FEE_CHARGE_DATE)
	ErrorAppDataInvalidWithZlp        = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_APP_DATA_INVALID_WITH_ZLP)
	ErrorAppInMethodInvalidWithMC     = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERR_APP_INTEGRATION_METHOD_INVALID_WITH_MC)
	ErrorAppInMethodInvalidBankStatus = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERR_APP_INTEGRATION_METHOD_INVALID_BANK_STATUS)
	ErrorAppCallbackUrlInvalid        = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_APP_CALLBACK_URL_INVALID)
	ErrorAppRedirectUrlInvalid        = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_APP_REDIRECT_URL_INVALID)
	ErrorAppIdNotHaveFaInfo           = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_APP_ID_NOT_HAVE_FA_INFO)
	ErrorFaCodeNotMapAccountCode      = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_FA_CODE_NOT_MAP_ACCOUNT_CODE)
	ErrorAppNotGrantMccCode           = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_APP_NOT_GRANT_MCC_CODE)
	ErrorMccCodeNotConfirm            = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MCC_CODE_NOT_CONFIRM)
	ErrorMccCodeAppDiffMerchant       = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MCC_CODE_APP_DIFF_MERCHANT)

	ErrorSpPhoneExisted        = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_SP_DUPLICATE_PHONE)
	ErrorAppBillingCyleExisted = grpcStatus.Errorf(grpcCodes.Internal, ERROR_DUPLICATE_APP_BILLING_CYCLE)
	ErrorUserInactive          = grpcStatus.Error(grpcCodes.PermissionDenied, ERROR_USER_INACTIVE)

	ErrorNameRoleExisted          = grpcStatus.Errorf(grpcCodes.Internal, ERROR_DUPLICATE_NAME_ROLE)
	ErrorUserRoleNotAllowedUpdate = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_USER_ROLE_NOT_ALLOWED_UPDATE)

	ErrorNotAllowSendMail = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_SEND_MAIL_REGISTER)

	ErrorMerchantAdminType = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_NOT_CHOOSE_TYPE_ADMIN)

	ErrorResetBilling    = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_RESET_BILLING)
	ErrorResetBillingFee = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_RESET_BILLING_FEE)
	ErrorCancelReconcile = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_CANCEL_RECONCILIATION)

	//For cashin
	ErrorCashinMerchantInvalid       = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_CASHIN_MERCHANT_INVALID)
	ErrorCashinMerchantBLocked       = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_CASHIN_MERCHANT_BLOCKED)
	ErrorCashinMerchantUpdateFail    = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_CASHIN_MERCHANT_UPDATE_FAIL)
	ErrorCashinMerchantExisted       = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_CASHIN_MERCHANT)
	ErrorCashinWalletMerchantInvalid = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_CASHIN_WALLET_MERCHANT_INVALID)

	//For wallet
	ErrorWalletInvalid        = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_WALLET_INVALID)
	ErrorWalletBLocked        = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_WALLET_BLOCKED)
	ErrorWalletUpdateFail     = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_WALLET_UPDATE_FAIL)
	ErrorWalletExisted        = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_WALLET)
	ErrorWalletMoneyNotEnough = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_WALLET_MONEY_NOT_ENOUGH)

	//For payment-info
	ErrorPaymentIdExisted            = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_PAYMENT_ID)
	ErrorPaymentChooseDefaultInvalid = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_PAYMENT_CHOOSE_DEFAULT_INVALID)

	//For product
	ErrorProductCodeExisted = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_PRODUCT_CODE)

	//For fa info
	ErrorFaCodeExisted         = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_FA_CODE)
	ErrorFaAppPaymentIdExisted = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_DUPLICATE_FA_APP_PAYMENT_ID)

	//For promotion config
	ErrorPromotionConfigGroupSaleInvalid = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_PROMOTION_CONFIG_GROUP_SALE_INVALID)

	//For Agreement
	ErrorAgreementInfoNotCreated         = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_AQUIRING_INFO_NOT_CREATED)
	ErrorBillingCycleNotCreated          = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_BILLING_CYCLE_NOT_CREATED)
	ErrorMerchantFeeNotCreated           = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MERCHANT_FEE_NOT_CREATED)
	ErrorAgreementCenterReferenceExisted = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_AGREEMENT_CENTER_DUPLICATE)
	ErrorAgreementSettleConfig           = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_AGREEMENT_SETTLE_CONFIG)

	//For Merchant User
	ErrorStoreCodeInvalid = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_MERCHANT_USER_STORE_CODE_INVALID)

	//For Taxi Integration
	ErrorTaxiIntegrationCallCreateOrderZLPFailed = grpcStatus.Errorf(grpcCodes.Canceled, ERROR_TAXI_INTEGRATION_CALL_CREATE_ORDER_ZLP_FAILED)
	ErrorTaxiIntegrationEMVCodeInvalid           = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_TAXI_INTEGRATION_EMV_CODE_INVALID)
	ErrorTaxiIntegrationEMVCodeAmountInvalid     = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_TAXI_INTEGRATION_EMV_AMOUNT_INVALID)
	ErrorTaxiIntegrationCheckCallbackInvalid     = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_TAXI_CHECK_CALLBACK_INVALID)

	//Define for external system
	ErrorExtUnknown            = grpcStatus.Errorf(grpcCodes.Unknown, "Unknown")
	ErrorExtInvalidArgument    = grpcStatus.Errorf(grpcCodes.InvalidArgument, "Invalid argument")
	ErrorExtNotFound           = grpcStatus.Errorf(grpcCodes.NotFound, "Find not found")
	ErrorExtAlreadyExists      = grpcStatus.Errorf(grpcCodes.AlreadyExists, "Already exists")
	ErrorExtPermissionDenied   = grpcStatus.Errorf(grpcCodes.PermissionDenied, "Permission denied")
	ErrorExtResourceExhausted  = grpcStatus.Errorf(grpcCodes.ResourceExhausted, "Resource exhausted")
	ErrorExtUnimplemented      = grpcStatus.Errorf(grpcCodes.Unimplemented, "Unimplemented")
	ErrorExtFailedPrecondition = grpcStatus.Errorf(grpcCodes.FailedPrecondition, "Failed precondition")
	ErrorExtInternal           = grpcStatus.Errorf(grpcCodes.Internal, "Internal errors")
	ErrorExtUnauthenticated    = grpcStatus.Errorf(grpcCodes.Unauthenticated, "Unauthenticated")

	//For User Agreement
	ErrorUserAgreementProductInvalid   = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_USER_AGREEMENT_PRODUCT_INVALID)
	ErrorUserAgreementStatusInvalid    = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_USER_AGREEMENT_STATUS_INVALID)
	ErrorAgreementCenterStatusInvalid  = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_USER_AGREEMENT_STATUS_INVALID)
	ErrorAgreementCenterProductInvalid = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_USER_AGREEMENT_PRODUCT_INVALID)

	ErrorItemMgmtTooManyOaID                = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_ITEM_MGMT_TOO_MANY_OAID)
	ErrorItemMgmtTooManyAppID               = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_ITEM_MGMT_TOO_MANY_APP_ID)
	ErrorItemMgmtCannotDeleteCate           = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_ITEM_MGMT_CANNOT_DELETE_CATE)
	ErrorItemMgmtCannotDeleteSubCate        = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_ITEM_MGMT_CANNOT_DELETE_SUB_CATE)
	ErrorItemMgmtItemCodeAlreadyExists      = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_ITEM_MGMT_ITEM_CODE_ALREADY_EXISTS)
	ErrorItemMgmtOAAlreadyExists            = grpcStatus.Errorf(grpcCodes.AlreadyExists, ERROR_ITEM_MGMT_OA_ALREADY_EXISTS)
	ErrorItemMgmtOAIdInvalid                = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_OA_ID_INVALID)
	ErrorItemMgmtCannotGetOAZaloUserByPhone = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_ITEM_MGMT_CANNOT_GET_OA_ZALO_INFO_BY_IDENTIFIER)
	ErrorItemMgmtOutOfWorking               = grpcStatus.Errorf(grpcCodes.Unavailable, ERROR_ITEM_MGMT_OUT_OF_WORKING)

	ErrorOACannotGetOAInfo = grpcStatus.Errorf(grpcCodes.FailedPrecondition, ERROR_OA_CANNOT_GET_OA_INFO)
	//For merchant agreement
	ErrorInvalidStartTimeMerchantFee = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_INVALID_START_TIME_MERCHANT_FEE)
)
