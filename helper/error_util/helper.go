package errorutil

import (
	"net/http"

	"google.golang.org/grpc/codes"
)

//HTTPStatusFromGRPCode Convert httpcode to grpc code
func HTTPStatusFromGRPCode(code int) codes.Code {
	switch code {
	case http.StatusOK:
		return codes.OK
	case http.StatusRequestTimeout:
		return codes.Canceled
	case http.StatusInternalServerError:
		return codes.Unknown
	case http.StatusBadRequest:
		return codes.InvalidArgument
	case http.StatusGatewayTimeout:
		return codes.DeadlineExceeded
	case http.StatusNotFound:
		return codes.NotFound
	case http.StatusConflict:
		return codes.AlreadyExists
	case http.StatusForbidden:
		return codes.PermissionDenied
	case http.StatusUnauthorized:
		return codes.Unauthenticated
	case http.StatusTooManyRequests:
		return codes.ResourceExhausted
	case http.StatusPreconditionFailed:
		return codes.FailedPrecondition
		/*case http.StatusConflict:
			return codes.Aborted
		case http.StatusBadRequest:
			return codes.OutOfRange
			case http.StatusInternalServerError:
		return codes.Internal
		case http.StatusInternalServerError:
		return codes.DataLoss*/
	case http.StatusNotImplemented:
		return codes.Unimplemented

	case http.StatusServiceUnavailable:
		return codes.Unavailable

	}
	return codes.Internal
}
