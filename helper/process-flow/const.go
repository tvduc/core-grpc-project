package flow

import (
	config "git.zapa.cloud/merchant-tools/helper/config"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
)

type ProcessFlowRegisterPair struct {
	FlowType pb.ProcessFlowType
	Register pb.ProcessFlowRegister
}

var (
	//Define flow begin: test -------------------------------------------
	//FLOW_MERCHANT_PROFILE_REGISTER_START : create flow
	FLOW_MERCHANT_PROFILE_REGISTER_START = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_MERCHANT_PROFILE_REGISTER,
		pb.ProcessFlowRegister{
			NextTopic: "first-topic",
		}}
	//FLOW_MERCHANT_PROFILE_REGISTER_FIRST : first step
	FLOW_MERCHANT_PROFILE_REGISTER_FIRST = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_MERCHANT_PROFILE_REGISTER,
		pb.ProcessFlowRegister{
			ReceiveTopic: "first-topic",
			NextTopic:    "second-topic",
		}}
	//FLOW_MERCHANT_PROFILE_REGISTER_SECOND : second step
	FLOW_MERCHANT_PROFILE_REGISTER_SECOND = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_MERCHANT_PROFILE_REGISTER,
		pb.ProcessFlowRegister{
			ReceiveTopic:  "second-topic",
			NextTopic:     "last-topic",
			PreviousTopic: "first-topic",
		}}
	//FLOW_MERCHANT_PROFILE_REGISTER_LAST : last step
	FLOW_MERCHANT_PROFILE_REGISTER_LAST = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_MERCHANT_PROFILE_REGISTER,
		pb.ProcessFlowRegister{
			ReceiveTopic:  "last-topic",
			PreviousTopic: "second-topic",
		}}
	//Define flow end: test -------------------------------------------

	//Define flow begin: Settlement -------------------------------------------
	//FLOW_SETTLEMENT_START : create flow
	FLOW_SETTLEMENT_START = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_SETTLEMENT,
		pb.ProcessFlowRegister{
			NextTopic: config.TransactionRecordTopic,
		}}
	//FLOW_SETTLEMENT_TRANSACTION_RECORD : first step
	FLOW_SETTLEMENT_TRANSACTION_RECORD = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_SETTLEMENT,
		pb.ProcessFlowRegister{
			ReceiveTopic: config.TransactionRecordTopic,
			NextTopic:    config.CloseSettlementPeriodTopic,
		}}
	//FLOW_CLOSE_SETTLEMENT_PERIOD
	FLOW_CLOSE_SETTLEMENT_PERIOD = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_SETTLEMENT,
		pb.ProcessFlowRegister{
			ReceiveTopic:  config.CloseSettlementPeriodTopic,
			NextTopic:     config.GeneratePayoutTopic,
			PreviousTopic: config.TransactionRecordTopic,
		}}
	//FLOW_GENETARE_PAYOUT
	FLOW_GENERARE_PAYOUT = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_SETTLEMENT,
		pb.ProcessFlowRegister{
			ReceiveTopic: config.GeneratePayoutTopic,
			// NextTopic:     config.GeneratePayoutTopic,
			PreviousTopic: config.CloseSettlementPeriodTopic,
		}}
	//Define flow end: Settlement -------------------------------------------

	//Define flow begin: Store registration -------------------------------------------
	//FLOW_STORE_REGISTRATION_START : create flow
	FLOW_STORE_REGISTRATION_START = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_STORE_REGISTRATION,
		pb.ProcessFlowRegister{
			NextTopic: config.BDStoreRegisConfirmationTopic,
		}}
	//FLOW_STORE_REGISTRATION_BD_CONFIRM : first step
	FLOW_STORE_REGISTRATION_BD_CONFIRM = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_STORE_REGISTRATION,
		pb.ProcessFlowRegister{
			ReceiveTopic: config.BDStoreRegisConfirmationTopic,
			NextTopic:    config.LegalStoreRegisConfirmationTopic,
		}}
	//FLOW_STORE_REGISTRATION_LEGAL_CONFIRM
	FLOW_STORE_REGISTRATION_LEGAL_CONFIRM = ProcessFlowRegisterPair{
		pb.ProcessFlowType_PFT_STORE_REGISTRATION,
		pb.ProcessFlowRegister{
			ReceiveTopic: config.LegalStoreRegisConfirmationTopic,
		}}
	//Define flow end: Store registration -------------------------------------------
)
