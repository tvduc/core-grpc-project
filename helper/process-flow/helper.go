package flow

import (
	"fmt"
	"os"

	commonHelper "git.zapa.cloud/merchant-tools/helper/common"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
)

var (
	servicePrefixName = os.Getenv("SERVICE_PREFIX_NAME")
)

func GenerateIdWithCache(cache *redis.Client, key string) (string, error) {
	currentDate := commonHelper.GetVNCurrentDateStringFormat("060102")
	redisKey := makeGenerateKey(key)
	id, err := cache.HIncrBy(redisKey, currentDate, 1).Result()
	if err != nil {
		glog.Infoln("GenerateIdWithCache: HIncrBy error: ", err)
		return "", err
	}

	idStr := fmt.Sprintf("%s%07d", currentDate, id)
	return idStr, nil
}

func makeGenerateKey(key string) string {
	if len(key) == 0 {
		return fmt.Sprintf("%sprocess-flow:genid", servicePrefixName)
	}

	return fmt.Sprintf("%sprocess-flow:genid:%s", servicePrefixName, key)
}
