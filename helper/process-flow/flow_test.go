package flow

import (
	"context"
	"flag"
	"fmt"
	"testing"
	"time"

	"git.zapa.cloud/merchant-tools/helper/config"
	wfPb "git.zapa.cloud/merchant-tools/protobuf/middleware/workflow"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes"
)

var (
	serviceConfig = config.ServiceBase{
		Kafka: config.Kafka{
			Addrs: []string{"127.0.0.1:9092"},
			Group: "process-flow-test",
		},
		Redis: config.Redis{
			Addr: "127.0.0.1:6379",
		},
		Database: config.Database{
			Addr:         "127.0.0.1:3306",
			User:         "root",
			Password:     "123456",
			Databasename: "merchant_tools",
		},
	}
	esConfig = "http://127.0.0.1:9200"

	NewEvent = &pb.ProcessFlowEvent{}

	FirstTopic  = "first-topic"
	SecondTopic = "second-topic"
	ThirdTopic  = "third-topic"
)

func init() {
	flag.Set("alsologtostderr", fmt.Sprintf("%t", true))
	NewEvent.Detail, _ = ptypes.MarshalAny(&wfPb.UserInfo{
		ApprovedBy: "haint3 test",
	})
}

type MerchantProfile struct {
}

type FirstHandler struct {
	received bool
}

func (h *FirstHandler) Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error) {

	fmt.Println("FirstHandler.Process: event = ", event.GetId())
	//Get detail
	userInfo := &wfPb.UserInfo{}
	err := ptypes.UnmarshalAny(event.GetDetail(), userInfo)
	if err != nil {
		return pb.ProcessFlowResponseType_PFRT_NONE, err
	}
	fmt.Println("FirstHandler.Process: event = ", event.GetId(), ", data = ", userInfo.GetApprovedBy())
	h.received = true
	return pb.ProcessFlowResponseType_PFRT_NEXT, nil
}

type SecondHandler struct {
	received bool
}

func (h *SecondHandler) Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error) {
	fmt.Println("SecondHandler.Process: event = ", event.GetId())
	//Get detail
	userInfo := &wfPb.UserInfo{}
	err := ptypes.UnmarshalAny(event.GetDetail(), userInfo)
	if err != nil {
		return pb.ProcessFlowResponseType_PFRT_NONE, err
	}
	fmt.Println("SecondHandler.Process: event = ", event.GetId(), ", data = ", userInfo.GetApprovedBy())
	h.received = true
	return pb.ProcessFlowResponseType_PFRT_NEXT, nil
}

type ThirdHandler struct {
	received bool
}

func (h *ThirdHandler) Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error) {
	//Do something ...
	h.received = true

	switch event.GetStatus() {
	case pb.ProcessFlowStatus_PFS_IN_PROGRESS:
		//Do activity flow
		event.Status = pb.ProcessFlowStatus_PFS_SUCCESS
	case pb.ProcessFlowStatus_PFS_FAILURE:
		//Do rollback flow
	}

	//Default: send to next step
	return pb.ProcessFlowResponseType_PFRT_NONE, nil
}

func TestStartFlow(t *testing.T) {
	ctx := context.Background()
	manager := NewProcessFlowManagerWithConfig(ctx, serviceConfig, esConfig)
	err := manager.Start(ctx)
	if err != nil {
		t.Error(err.Error())
	}
	//New flow
	manager.CreateFlowWithSpec(ctx, FLOW_MERCHANT_PROFILE_REGISTER_START, NewEvent)
	glog.Infoln("NewEvent id: ", NewEvent.GetId())
	time.Sleep(5 * time.Second)
}

func TestReceiveFirstStep(t *testing.T) {
	ctx := context.Background()

	handler := &FirstHandler{}
	err := NewProcessFlowManagerWithConfig(ctx, serviceConfig, esConfig).
		ListenFlowWithSpec(FLOW_MERCHANT_PROFILE_REGISTER_FIRST, handler).
		Start(ctx)
	if err != nil {
		t.Error(err.Error())
	}

	fmt.Println("Sleep 60 seconds to wait receive msg")
	time.Sleep(60 * time.Second)
	if !handler.received {
		t.Error("Don't receive message")
	}
}

func TestReceiveSecondStep(t *testing.T) {
	ctx := context.Background()
	handler := &SecondHandler{}
	err := NewProcessFlowManagerWithConfig(ctx, serviceConfig, esConfig).
		ListenFlowWithSpec(FLOW_MERCHANT_PROFILE_REGISTER_SECOND, handler).
		Start(ctx)
	if err != nil {
		t.Error(err.Error())
	}

	fmt.Println("Sleep 60 seconds to wait")
	time.Sleep(60 * time.Second)
	if !handler.received {
		t.Error("Don't receive message")
	}
}

func TestReceiveThirdStep(t *testing.T) {
	ctx := context.Background()
	handler := &ThirdHandler{}
	err := NewProcessFlowManagerWithConfig(ctx, serviceConfig, esConfig).
		ListenFlowWithSpec(FLOW_MERCHANT_PROFILE_REGISTER_LAST, handler).
		Start(ctx)
	if err != nil {
		t.Error(err.Error())
	}

	fmt.Println("Sleep 60 seconds to wait")
	time.Sleep(60 * time.Second)
	if !handler.received {
		t.Error("Don't receive message")
	}
}
