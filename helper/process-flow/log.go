package flow

import (
	"context"
	"errors"
	"fmt"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/kafka"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
)

//EventLogger ...
type EventLogger interface {
	Start(ctx context.Context) error
	Write(ctx context.Context, actionType pb.ProcessFlowActionType, event *pb.ProcessFlowEvent) error
}

//NewLogger ...
func NewLogger(ctx context.Context, cfg config.Kafka) EventLogger {
	return &KafkaLogger{
		kafkaConfig: cfg,
	}
}

//KafkaLogger ...
type KafkaLogger struct {
	logPublisher kafka.Writer
	kafkaConfig  config.Kafka
}

//Start ...
func (l *KafkaLogger) Start(ctx context.Context) error {
	if l == nil {
		return errors.New("KafkaLogger.Start: instance invalid")
	}

	if l.logPublisher != nil {
		return nil
	}

	fmt.Println("KafkaLogger.Start: ", l.kafkaConfig)
	l.logPublisher = kafka.CreateWriters(l.kafkaConfig)
	if l.logPublisher == nil {
		return errors.New("KafkaLogger.Start: create publisher null")
	}

	return nil
}

//Write ...
func (l *KafkaLogger) Write(ctx context.Context, actionType pb.ProcessFlowActionType, event *pb.ProcessFlowEvent) error {
	if l == nil || l.logPublisher == nil {
		glog.Errorln("KafkaLogger.Publish: instance invalid")
		return errors.New("KafkaLogger.Publish: instance invalid")
	}

	history := &pb.ProcessFlowHistory{
		EventId:     event.GetId(),
		FlowType:    event.GetType(),
		ActionType:  actionType,
		CreatedTime: event.GetCreatedTime(),
		UpdatedTime: event.GetUpdatedTime(),
		Status:      event.GetStatus(),
		Step:        event.GetStep(),
		ObjectId:    event.GetObjectId(),
		Event:       event,
	}

	buf, err := proto.Marshal(history)
	if err != nil {
		glog.Errorln("KafkaLogger.Write: marshal event error: ", err)
		return err
	}

	l.logPublisher.WriteRawByTopic(buf, config.ProcessFlowHistoryTopic)
	return nil
}
