# Process Flow
![Mô tả flow](images/process-flow.png)

# Desciption
1. protobuf **ProcessFlowEvent**: là 1 object event info bao gồm: id, flow type,.. + data (detail data do mỗi service tự quy định)
2. **ProcessHandler**: interface để các service implement khi muốn đăng ký nhận xử lý  event.

```golang
    type ProcessHandler interface {
        Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error)
    }
``` 

3. **ProcessFlowManager**: quản lý flow, read, write event. Có các func quan trọng: 
    - **AddFlow**: đăng ký nhận xử lý event.
    - **StartFlow**: bắt đầu tạo 1 flow.
    - **Start**: ProcessFlowManager bắt đầu work.
    - **GetCache**: trả về cache đang sử dụng (dùng cho các service muốn sử dụng khi cần)
4. func **NewProcessFlowManagerWithConfig**: tạo ra ProcessFlowManager

# Note
- Khi đăng ký 1 step của flow có 3 tham số: receive_topic (nhận), next_topice (gửi đi tiếp), previous_topic (rollback khi fail).
- 1 service có thể đăng ký nhiều flow.