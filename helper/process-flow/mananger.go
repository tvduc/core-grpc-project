package flow

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	cacheHelper "git.zapa.cloud/merchant-tools/helper/cache"
	"git.zapa.cloud/merchant-tools/helper/config"
	redisHelper "git.zapa.cloud/merchant-tools/helper/redis"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes"
)

type CreateEventCallback func(*pb.ProcessFlowEvent)

//ProcessHandlerFunc ...
type ProcessHandlerFunc func(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error)

//ProcessHandler ...
type ProcessHandler interface {
	Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error)
}

type defaultHandler struct {
	cb ProcessHandlerFunc
}

func (h *defaultHandler) Process(ctx context.Context, event *pb.ProcessFlowEvent) (pb.ProcessFlowResponseType, error) {
	return h.cb(ctx, event)
}

//RegisterInfo ...
type RegisterInfo struct {
	handler  ProcessHandler
	register pb.ProcessFlowRegister
}

//ProcessFlowManager ...
type ProcessFlowManager struct {
	timeout        time.Duration
	handlerMap     map[string]*RegisterInfo
	eventConsumer  EventConsumer
	eventPublisher EventPublisher
	logger         EventLogger
	storer         EventStorer
	redisClient    *redis.Client
}

//NewProcessFlowManager ...
func NewProcessFlowManager(srvCfg config.ServiceBase) *ProcessFlowManager {
	return &ProcessFlowManager{
		handlerMap:  make(map[string]*RegisterInfo),
		timeout:     0,
		redisClient: redisHelper.NewRedisClient(srvCfg.GetRedis()),
	}
}

//NewProcessFlowManagerWithConfig ...
func NewProcessFlowManagerWithConfig(ctx context.Context, srvCfg config.ServiceBase, esCfg string) *ProcessFlowManager {
	return NewProcessFlowManager(srvCfg).
		SetEventConsumer(NewEventConsumer(ctx, srvCfg.Kafka)).
		SetEventPublisher(NewEventPublisher(ctx, srvCfg.Kafka)).
		SetLogger(NewLogger(ctx, srvCfg.Kafka)).
		SetStorer(NewCacheStorer(ctx, srvCfg, esCfg))
}

//Start ...
func (s *ProcessFlowManager) Start(ctx context.Context) error {
	if s == nil {
		return errors.New("ProcessFlowManager.Start: instance is nil")
	}

	if s.eventConsumer == nil {
		return errors.New("ProcessFlowManager.Start: consumer is nil")
	}

	if s.eventPublisher == nil {
		return errors.New("ProcessFlowManager.Start: publisher is nil")
	}

	if s.storer == nil {
		return errors.New("ProcessFlowManager.Start: storer is nil")
	}

	if s.logger == nil {
		return errors.New("ProcessFlowManager.Start: logger is nil")
	}

	err := s.storer.Start(ctx)
	if err != nil {
		glog.Errorln("ProcessFlowManager.Start: storer start error ", err)
		return err
	}

	err = s.logger.Start(ctx)
	if err != nil {
		glog.Errorln("ProcessFlowManager.Start: logger start error ", err)
		return err
	}

	err = s.eventConsumer.Start(ctx)
	if err != nil {
		glog.Errorln("ProcessFlowManager.Start: consumer start error ", err)
		return err
	}

	err = s.eventPublisher.Start(ctx)
	if err != nil {
		glog.Errorln("ProcessFlowManager.Start: publisher start error ", err)
		return err
	}

	glog.Infoln("ProcessFlowManager: Start done")
	return nil
}

//SetEventConsumer ...
func (s *ProcessFlowManager) SetEventConsumer(consumer EventConsumer) *ProcessFlowManager {
	if s != nil && consumer != nil {
		s.eventConsumer = consumer
	}
	return s
}

//SetEventPublisher ...
func (s *ProcessFlowManager) SetEventPublisher(publisher EventPublisher) *ProcessFlowManager {
	if s != nil && publisher != nil {
		s.eventPublisher = publisher
	}
	return s
}

//SetLogger ...
func (s *ProcessFlowManager) SetLogger(logger EventLogger) *ProcessFlowManager {
	if s != nil && logger != nil {
		s.logger = logger
	}
	return s
}

//SetStorer ...
func (s *ProcessFlowManager) SetStorer(storer EventStorer) *ProcessFlowManager {
	if s != nil && storer != nil {
		s.storer = storer
	}
	return s
}

//SetTimeout ...
func (s *ProcessFlowManager) SetTimeout(timeout time.Duration) *ProcessFlowManager {
	if s != nil {
		s.timeout = timeout
	}
	return s
}

//CreateFlowWithCallback ...
func (s *ProcessFlowManager) CreateFlowWithCallback(ctx context.Context, registerInfo ProcessFlowRegisterPair, event *pb.ProcessFlowEvent, cb CreateEventCallback) error {
	if event == nil {
		return errors.New("ProcessFlowManager.CreateFlowWithCallback: event is null")
	}

	if len(registerInfo.Register.NextTopic) == 0 {
		return errors.New("ProcessFlowManager.CreateFlowWithCallback: register topic invalid")
	}

	if registerInfo.FlowType == pb.ProcessFlowType_PFT_NONE {
		return errors.New("ProcessFlowManager.CreateFlowWithCallback: register flow type invalid")
	}

	event.Type = registerInfo.FlowType
	return s.CreateFlow(ctx, registerInfo.Register.NextTopic, cb, event)
}

//CreateFlowWithSpec ...
func (s *ProcessFlowManager) CreateFlowWithSpec(ctx context.Context, registerInfo ProcessFlowRegisterPair, events ...*pb.ProcessFlowEvent) error {
	for _, event := range events {
		if event == nil {
			return errors.New("ProcessFlowManager.CreateFlowWithSpec: event is null")
		}

		if len(registerInfo.Register.NextTopic) == 0 {
			return errors.New("ProcessFlowManager.CreateFlowWithSpec: register topic invalid")
		}

		if registerInfo.FlowType == pb.ProcessFlowType_PFT_NONE {
			return errors.New("ProcessFlowManager.CreateFlowWithSpec: register flow type invalid")
		}

		event.Type = registerInfo.FlowType
	}
	return s.CreateFlow(ctx, registerInfo.Register.NextTopic, nil, events...)
}

//CreateFlow ...
func (s *ProcessFlowManager) CreateFlow(ctx context.Context, topic string, cb CreateEventCallback, events ...*pb.ProcessFlowEvent) error {
	if len(events) == 0 {
		glog.Errorln("ProcessFlowManager.CreateFlow: events is empty")
		return errors.New("ProcessFlowManager.CreateFlow: events is empty")
	}
	for _, event := range events {
		if event == nil {
			glog.Errorln("ProcessFlowManager.CreateFlow: event is null")
			return errors.New("ProcessFlowManager.CreateFlow: event is null")
		}
		event.Step = int32(pb.ProcessFlowStep_STEP_NONE)
		event.Status = pb.ProcessFlowStatus_PFS_START

		//Random uuid
		id, err := s.generateFlowId(ctx)
		if err != nil {
			return err
		}

		event.Id = id
		event.CreatedTime = ptypes.TimestampNow()

		if cb != nil {
			cb(event)
		}
	}

	err := s.storeEvent(ctx, pb.ProcessFlowActionType_AT_CREATE, events...)
	if err != nil {
		return err
	}

	return s.PublishEvent(ctx, topic, events...)
}

//PublishEvent ...
func (s *ProcessFlowManager) PublishEvent(ctx context.Context, topic string, events ...*pb.ProcessFlowEvent) error {
	if len(events) == 0 {
		return errors.New("ProcessFlowManager.PublishEvent: event is null")
	}
	for _, event := range events {
		if e := s.eventPublisher.Publish(topic, event); e != nil {
			return e
		}
	}
	return nil
}

//GetEvent ...
func (s *ProcessFlowManager) GetEvent(ctx context.Context, id string) (*pb.ProcessFlowEvent, error) {
	if s.storer == nil {
		return nil, errors.New("ProcessFlowManager.GetEvent: storer is null")
	}

	if len(id) == 0 {
		return nil, sql.ErrNoRows
	}

	return s.storer.Get(ctx, id)
}

//GetCache ...
func (s *ProcessFlowManager) GetCache() cacheHelper.Cache {
	if s == nil {
		return nil
	}

	if s.storer == nil {
		return nil
	}

	return s.storer.GetCache()
}

//ListenFlowWithSpec ...
func (s *ProcessFlowManager) ListenFlowWithFunc(registerInfo ProcessFlowRegisterPair, handlerFunc ProcessHandlerFunc) *ProcessFlowManager {
	register := registerInfo.Register
	return s.ListenFlow(register, &defaultHandler{cb: handlerFunc})
}

//ListenFlowWithSpec ...
func (s *ProcessFlowManager) ListenFlowWithSpec(registerInfo ProcessFlowRegisterPair, handler ProcessHandler) *ProcessFlowManager {
	register := registerInfo.Register
	return s.ListenFlow(register, handler)
}

//ListenFlow ...
func (s *ProcessFlowManager) ListenFlow(register pb.ProcessFlowRegister, handler ProcessHandler) *ProcessFlowManager {
	if len(register.GetReceiveTopic()) == 0 {
		return s
	}

	glog.Infoln("ProcessFlowManager.ListenFlow: topic", register.GetReceiveTopic())
	registerInfo := &RegisterInfo{
		register: register,
		handler:  handler,
	}

	if s.handlerMap != nil {
		s.handlerMap[register.GetReceiveTopic()] = registerInfo
	}

	if s.eventConsumer != nil {
		s.eventConsumer.AddEvent(register.GetReceiveTopic(), s.eventHandler)
	}

	return s
}

func (s *ProcessFlowManager) eventHandler(topic string, event *pb.ProcessFlowEvent) {
	ctx := context.Background()
	registerInfo, ok := s.handlerMap[topic]
	if !ok {
		return
	}

	if event == nil || len(event.GetId()) == 0 {
		return
	}

	buf, _ := json.Marshal(event)
	glog.Infoln("ProcessFlowManager.eventHandler: receive: ", string(buf))

	//Read event from cache
	cacheEvent, err := s.storer.Get(ctx, event.GetId())
	if err != nil {
		glog.Errorln("ProcessFlowManager.eventHandler: read event from cache error: ", err)
		return
	}

	//Check event id in cache
	if cacheEvent == nil || cacheEvent.GetId() != event.GetId() {
		glog.Errorln("ProcessFlowManager.eventHandler: event in cache invalid with queue kaka: event id = ", event.GetId())
		return
	}

	if s.timeout > 0 {
		eventTime, err := ptypes.Timestamp(cacheEvent.GetUpdatedTime())
		if err == nil {
			currentTime := time.Now()
			span := currentTime.Sub(eventTime)
			if span > s.timeout {
				glog.Infoln("eventLogHandler.eventHandler: event time invalid: eventTime =", eventTime, ", currentTime =", currentTime)
				return
			}
		}
	}

	event = cacheEvent
	//Change from start to in progress
	if event.GetStatus() == pb.ProcessFlowStatus_PFS_START {
		event.Status = pb.ProcessFlowStatus_PFS_IN_PROGRESS
	}

	event.Step = event.GetStep() + 1
	handler := registerInfo.handler
	glog.Infoln(fmt.Sprintf("ProcessFlowManager.eventHandler: receive Topic = %s, ExecId = %s, Type = %v, Status = %v",
		topic, event.GetId(), event.GetType(), event.GetStatus()))

	respType, err := handler.Process(ctx, event)
	if err != nil {
		glog.Errorln("ProcessFlowManager.eventHandler: handler process response error: ", err)
		return
	}

	s.storeEvent(ctx, pb.ProcessFlowActionType_AT_UPDATE, event)

	pushEvent := s.cloneHeaderEvent(ctx, event)
	switch respType {
	case pb.ProcessFlowResponseType_PFRT_NEXT:
		if len(registerInfo.register.GetNextTopic()) == 0 {
			glog.Errorln("ProcessFlowManager.eventHandler: send to next with empty topic")
			return
		}
		s.eventPublisher.Publish(registerInfo.register.GetNextTopic(), pushEvent)
	case pb.ProcessFlowResponseType_PFRT_PREVIOUS:
		if len(registerInfo.register.GetPreviousTopic()) == 0 {
			glog.Errorln("ProcessFlowManager.eventHandler: send to previous with empty topic")
			return
		}
		s.eventPublisher.Publish(registerInfo.register.GetPreviousTopic(), pushEvent)
	default:
	}

}

//Update event manual
func (s *ProcessFlowManager) UpdateEvent(ctx context.Context, events ...*pb.ProcessFlowEvent) error {
	return s.storeEvent(ctx, pb.ProcessFlowActionType_AT_MANUAL_UPDATE, events...)
}

//Storage and write log
func (s *ProcessFlowManager) storeEvent(ctx context.Context, actionType pb.ProcessFlowActionType, events ...*pb.ProcessFlowEvent) error {
	for _, event := range events {
		if event.UpdatedTime == nil {
			event.UpdatedTime = event.GetCreatedTime()
		} else {
			event.UpdatedTime = ptypes.TimestampNow()
		}
	}

	err := s.storer.MSet(ctx, events...)
	if err != nil {
		return err
	}
	//Flush cache data before send next
	s.GetCache().Flush(ctx)
	for _, event := range events {
		s.logger.Write(ctx, actionType, event)
	}
	return nil
}

func (s *ProcessFlowManager) cloneHeaderEvent(ctx context.Context, event *pb.ProcessFlowEvent) *pb.ProcessFlowEvent {
	return &pb.ProcessFlowEvent{
		Id:          event.GetId(),
		Type:        event.GetType(),
		CreatedTime: event.GetCreatedTime(),
		UpdatedTime: event.GetUpdatedTime(),
		Status:      event.GetStatus(),
		Step:        event.GetStep(),
	}
}

func (s *ProcessFlowManager) generateFlowId(ctx context.Context) (string, error) {
	return GenerateIdWithCache(s.redisClient, "")
}
