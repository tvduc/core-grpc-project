package flow

import (
	"context"
	"errors"
	"fmt"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/kafka"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
)

//EventHandler ...
type EventHandler func(topic string, event *pb.ProcessFlowEvent)

//EventConsumer ...
type EventConsumer interface {
	Start(ctx context.Context) error
	AddEvent(topic string, handler EventHandler)
}

//EventPublisher ...
type EventPublisher interface {
	Start(ctx context.Context) error
	Publish(topic string, event *pb.ProcessFlowEvent) error
}

//NewEventConsumer ...
func NewEventConsumer(ctx context.Context, cfg config.Kafka) EventConsumer {
	return &KafkaEventConsumer{
		kafkaConfig: cfg,
		handlerMap:  make(map[string]EventHandler),
	}
}

//NewEventPublisher ...
func NewEventPublisher(ctx context.Context, cfg config.Kafka) EventPublisher {
	return &KafkaEventPublisher{
		kafkaConfig: cfg,
	}
}

//KafkaEventConsumer ...
type KafkaEventConsumer struct {
	logConsumer kafka.Reader
	kafkaConfig config.Kafka
	handlerMap  map[string]EventHandler
}

//Start ...
func (c *KafkaEventConsumer) Start(ctx context.Context) error {
	if c == nil {
		return errors.New("KafkaEventConsumer.Start: instance invalid")
	}

	for k := range c.handlerMap {
		c.kafkaConfig.Topics = append(c.kafkaConfig.Topics, k)
	}

	glog.Infoln("KafkaEventConsumer: register topic: ", c.kafkaConfig.Topics)

	c.kafkaConfig.Group = servicePrefixName + "process-flow"
	c.logConsumer = kafka.CreateReaders(c.kafkaConfig)
	go c.logConsumer.ReadEx(func(tp string, val []byte) {
		glog.Infoln("KafkaEventConsumer: receive topic: ", tp)
		if handler, ok := c.handlerMap[tp]; ok {
			eventInfo := &pb.ProcessFlowEvent{}
			err := proto.Unmarshal(val, eventInfo)
			if err != nil {
				glog.Errorln("KafkaEventConsumer: Unmarshal data error: ", err)
				return
			}
			handler(tp, eventInfo)
		}
	})

	return nil
}

//AddEvent ...
func (c *KafkaEventConsumer) AddEvent(topic string, handler EventHandler) {
	if c == nil || c.handlerMap == nil {
		return
	}

	c.handlerMap[topic] = handler
}

//KafkaEventPublisher ...
type KafkaEventPublisher struct {
	logPublisher kafka.Writer
	kafkaConfig  config.Kafka
}

//Start ...
func (p *KafkaEventPublisher) Start(ctx context.Context) error {
	if p == nil {
		return errors.New("KafkaEventPublisher.Start: instance invalid")
	}

	if p.logPublisher != nil {
		return nil
	}

	fmt.Println("KafkaEventPublisher.Start: ", p.kafkaConfig)
	p.logPublisher = kafka.CreateWriters(p.kafkaConfig)
	if p.logPublisher == nil {
		return errors.New("KafkaEventPublisher.Start: create publisher null")
	}

	return nil
}

//Publish ...
func (p *KafkaEventPublisher) Publish(topic string, event *pb.ProcessFlowEvent) error {
	if p == nil || p.logPublisher == nil {
		return errors.New("KafkaEventPublisher.Publish: instance invalid")
	}

	buf, err := proto.Marshal(event)
	if err != nil {
		return err
	}

	p.logPublisher.WriteRawByTopic(buf, topic)
	return nil
}
