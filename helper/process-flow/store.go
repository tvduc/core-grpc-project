package flow

import (
	"context"
	"errors"
	"fmt"

	cacheHelper "git.zapa.cloud/merchant-tools/helper/cache"
	"git.zapa.cloud/merchant-tools/helper/config"
	pb "git.zapa.cloud/merchant-tools/protobuf/process-flow"
)

var (
	table          = config.ProcessFlowTable
	esIndex        = config.ProcessFlowESIndex
	esIndexSetting interface{}
)

//EventStorer ...
type EventStorer interface {
	Start(ctx context.Context) error
	Set(ctx context.Context, event *pb.ProcessFlowEvent) error
	MSet(ctx context.Context, event ...*pb.ProcessFlowEvent) error
	Get(ctx context.Context, id string) (*pb.ProcessFlowEvent, error)
	GetCache() cacheHelper.Cache
}

//NewCacheStorer ...
func NewCacheStorer(ctx context.Context, srvCfg config.ServiceBase, esCfg string) *CacheStorer {
	cache := cacheHelper.Open(&srvCfg.Redis, &srvCfg.Database, esCfg, &pb.ProcessFlowEvent{}, esIndexSetting, table, esIndex).Options(cacheHelper.Refresh(true))
	return &CacheStorer{
		cache: cache,
	}
}

//CacheStorer ...
type CacheStorer struct {
	cache cacheHelper.Cache
}

//Start ...
func (s *CacheStorer) Start(ctx context.Context) error {
	if s == nil {
		return errors.New("CacheStorer.Start: CacheStorer instance is null")
	}

	if s.cache == nil {
		return errors.New("CacheStorer.Start: cache is null")
	}

	return nil
}

//GetCache ...
func (s *CacheStorer) GetCache() cacheHelper.Cache {
	return s.cache
}

//Set ...
func (s *CacheStorer) Set(ctx context.Context, event *pb.ProcessFlowEvent) error {
	if s.cache == nil {
		return errors.New("CacheStorer.Set: cache is null")
	}

	key := makeCacheKey(event.GetId())
	return s.cache.Set(ctx, key, event)
}

//Set ...
func (s *CacheStorer) MSet(ctx context.Context, events ...*pb.ProcessFlowEvent) error {
	if s.cache == nil {
		return errors.New("CacheStorer.Set: cache is null")
	}
	keys := make([]string, len(events))
	values := make([]interface{}, len(events))
	for i, event := range events {
		keys[i] = makeCacheKey(event.GetId())
		values[i] = event
	}
	return s.cache.MSet(ctx, keys, values)
}

//Get ...
func (s *CacheStorer) Get(ctx context.Context, id string) (*pb.ProcessFlowEvent, error) {
	if s.cache == nil {
		return nil, errors.New("CacheStorer.Get: cache is null")
	}

	key := makeCacheKey(id)
	respEvent := &pb.ProcessFlowEvent{}
	err := s.cache.Get(ctx, respEvent, key)
	if err != nil {
		return nil, err
	}
	return respEvent, nil
}

func makeCacheKey(id string) string {
	return fmt.Sprintf("%s", id)
}
