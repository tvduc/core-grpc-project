package migrate

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/concurrency"
	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/database"
	"github.com/golang/glog"
	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"github.com/jmoiron/sqlx"
)

type Column struct {
	Field     string         `db:"Field"`
	Type      string         `db:"Type"`
	Null      string         `db:"Null"`
	Key       string         `db:"Key"`
	Default   sql.NullString `db:"Default"`
	Extra     string         `db:"Extra"`
	TableName string
}
type ColumnIndex struct {
	KeyName      string         `db:"Key_name"`
	ColumnName   string         `db:"Column_name"`
	Table        string         `db:"Table"`
	Non_unique   string         `db:"Non_unique"`
	Seq_in_index string         `db:"Seq_in_index"`
	Collation    string         `db:"Collation"`
	Cardinality  sql.NullString `db:"Cardinality"`
	Sub_part     sql.NullString `db:"Sub_part"`
	Packed       sql.NullString `db:"Packed"`

	Null          string `db:"Null"`
	Index_type    string `db:"Index_type"`
	Comment       string `db:"Comment"`
	Index_comment string `db:"Index_comment"`
}

func isStringInSlice(needle string, haystack []string) bool {
	for _, s := range haystack {
		if strings.HasPrefix(needle, s) {
			return true
		}
	}
	return false
}
func (column *Column) isString() bool {
	return isStringInSlice(column.Type, []string{
		"char",
		"varchar",
		"binary",
		"varbinary",
	})
}
func (column *Column) isText() bool {
	return isStringInSlice(column.Type, []string{
		"text",
		"blob",
	})
}
func (column *Column) isInteger() bool {
	return isStringInSlice(column.Type, []string{
		"tinyint",
		"smallint",
		"mediumint",
		"int",
		"bigint",
	})
}
func (column *Column) isFloat() bool {
	return isStringInSlice(column.Type, []string{
		"numeric",
		"decimal",
		"float",
		"real",
		"double precision",
		"double",
	})
}
func (column *Column) isTemporal() bool {
	return isStringInSlice(column.Type, []string{
		"time",
		"timestamp",
		"date",
		"datetime",
		"year",
	})
}
func (column *Column) isBool() bool {
	return isStringInSlice(column.Type, []string{
		"bit",
	})
}
func (column *Column) getKey(mapKey map[string]ColumnIndex) string {
	if column.Key == "PRI" {
		return ";primary_key"
	}

	index, ok := mapKey[column.Field]
	if ok {
		if index.Non_unique == "1" {
			return ";index:" + index.KeyName
		} else {
			return ";unique_index:" + index.KeyName
		}
	}

	return ""
}
func (column *Column) getNotNull() string {
	if column.Null == "NO" {
		if column.Key != "PRI" {
			return ";not null"
		}
	}
	return ""
}
func (column *Column) getType() string {
	if column.Extra == "auto_increment" && column.Key == "PRI" {
		return ""
	}
	return ";type:" + column.Type

}
func (column *Column) getDefault() string {
	if column.Default.Valid {
		if len(column.Default.String) > 0 {
			if column.isString() || column.isText() || (column.isTemporal() && column.Default.String != "CURRENT_TIMESTAMP") {
				return fmt.Sprintf(";default:'%s'", column.Default.String)
			}
			return ";default:" + column.Default.String
		} else {
			return ";default:''"
		}
	}
	return ""

}
func (column *Column) getExtra() string {
	if len(column.Extra) > 0 {
		return ";" + column.Extra
	}
	return ""
}

//GetGoType ...
func (column *Column) getGoType() (string, bool) {

	isTime := false
	goType := ""

	if column.isString() || column.isText() {
		goType = "string"
	} else if column.isInteger() {
		goType = "int"
	} else if column.isFloat() {
		goType = "float64"
	} else if column.isTemporal() {
		goType = "time.Time"
		isTime = true
	} else if column.isBool() {
		goType = "bool"
	} else {

		// TODO handle special data types
		switch column.Type {
		case "boolean":
			goType = "bool"
		default:
			goType = "sql.NullString"
			glog.Errorln("Unknow field ", column.TableName, column.Field, column.Type)
		}
	}

	return goType, isTime
}

func getGormInfo(db *sqlx.DB, table string) ([]string, error) {
	gormInfo := []string{}
	var columns []Column
	columnIdx := []ColumnIndex{}
	if err := concurrency.Do(func() error {
		return db.Select(&columns, fmt.Sprintf("DESCRIBE %s;", table))
	}, func() error {
		return db.Select(&columnIdx, fmt.Sprintf("show index from %s;", table))
	}); err != nil {
		return gormInfo, err
	}

	columnIdxMap := map[string]ColumnIndex{}
	for _, idx := range columnIdx {
		columnIdxMap[idx.ColumnName] = idx
	}

	for _, column := range columns {
		column.TableName = table
		fName := strcase.ToCamel(column.Field)
		if strings.HasSuffix(fName, "ID") {
			fName = strings.Replace(fName, "ID", "Id", 1)
		}
		//tag := "`gorm:" + `"Column:%s`
		tag := fmt.Sprintf("Column:%s", column.Field)
		tag += column.getType() + column.getNotNull() + column.getDefault() + column.getKey(columnIdxMap) +
			column.getExtra()
		gormInfo = append(gormInfo, tag)
	}

	return gormInfo, nil
}

func checkGormTag(m interface{}, gormChecks []string) bool {
	val := reflect.ValueOf(m)
	if val.Kind() == reflect.Ptr {
		if val.IsNil() {
			glog.Errorln("nil ...")
			return false
		}
		val = val.Elem()
	}
	vt := val.Type()
	if len(gormChecks) != vt.NumField() {
		//glog.Infoln("diff length")
		return false
	}
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if tag, ok := field.Tag.Lookup("gorm"); ok {
			if tag != gormChecks[i] {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

func DoMigrateModels(cfg config.Database, models ...interface{}) error {
	if len(models) <= 0 {
		return nil
	}
	now := time.Now()
	defer func() {
		glog.Infoln("DoMigrateModels in ", time.Since(now))
	}()
	dB := database.CreateSQLXDB(cfg)
	if dB == nil {
		return errors.New("Can not create DB")
	}
	//defer dB.Close()
	connectString := fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&charset=utf8",
		cfg.User, cfg.Password, cfg.Addr, cfg.Databasename)

	gormDB, err := gorm.Open("mysql", connectString)
	if err != nil {
		return err
	}
	//defer gormDB.Close()
	cc := concurrency.New()
	for _, _model := range models {
		model := _model
		if f, ok := model.(interface {
			TableName() string
		}); ok {
			tableName := f.TableName()
			cc.Add(func() error {
				if tags, e := getGormInfo(dB, tableName); e == nil {
					if !checkGormTag(model, tags) {
						count := 0
						if gormDB.Model(model).Count(&count).Error == nil && count == 0 {
							glog.Infoln("Drop empty table: ", tableName)
							if err := gormDB.DropTableIfExists(model).Error; err != nil {
								glog.Errorln("MigrateModel: drop table error: ", err)
							}
						}
						if err := gormDB.AutoMigrate(model).Error; err != nil {
							glog.Warningln(fmt.Sprintf("MigrateModel: AutoMigrate table %s error: %v", tableName, err))
						}
					}
				} else {
					if err := gormDB.AutoMigrate(model).Error; err != nil {
						glog.Warningln(fmt.Sprintf("MigrateModel: AutoMigrate new table %s error: %v", tableName, err))
					}
				}
				return nil
			})
		}
	}
	return cc.Do()
}
