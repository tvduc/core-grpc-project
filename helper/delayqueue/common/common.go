package common

import (
	"os"
	"strings"
)

var (
	servicePrefixName = os.Getenv("SERVICE_PREFIX_NAME")
)

//GetTopic ...
func GetTopic(topic string) string {
	if servicePrefixName != "" && strings.HasPrefix(topic, servicePrefixName) == false {
		return servicePrefixName + topic
	}
	return topic
}
