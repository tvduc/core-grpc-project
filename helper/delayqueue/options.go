package delayqueue

import (
	"os"
	"strings"
	"time"
)

const (
	KafkaQueue = iota
)

//Options ...
type Options struct {
	QueueType int
	Brokers   []string
	Group     string
	Oldest    bool
	Delay     time.Duration
}
type Option func(*Options)

//NewOption ...
func NewOption(setters ...Option) *Options {
	opts := &Options{
		Brokers:   strings.Split(os.Getenv("KAFKA_ADDR"), ","),
		Group:     "DefaultGroup",
		Oldest:    true,
		QueueType: KafkaQueue,
	}
	for _, setter := range setters {
		setter(opts)
	}
	return opts
}

//Brokers ...
func Brokers(broker string) Option {
	return func(arg *Options) {
		arg.Brokers = strings.Split(broker, ",")
	}
}

//Group ...
func Group(group string) Option {
	return func(arg *Options) {
		arg.Group = group
	}
}

//Offset ...
func Offset(oldest bool) Option {
	return func(arg *Options) {
		arg.Oldest = oldest
	}
}

//Delay ...
func Delay(delay time.Duration) Option {
	return func(arg *Options) {
		arg.Delay = delay
	}
}

//QueueType ...
func QueueType(queueType int) Option {
	return func(arg *Options) {
		arg.QueueType = queueType
	}
}
