package delayqueue

import (
	"git.zapa.cloud/merchant-tools/helper/delayqueue/kafka"
	"github.com/golang/glog"
)

//Writer ...
type Writer interface {
	Write(string, []byte)
}

//Reader ...
type Reader interface {
	Read(cb func(string, []byte), topics ...string)
}

//NewReader ...
func NewReader(opts ...Option) Reader {
	options := NewOption(opts...)
	switch options.QueueType {
	case KafkaQueue:
		r, _ := kafka.NewReader(options.Brokers, options.Group, options.Oldest,options.Delay).(Reader)
		return r
	default:
		glog.Errorln("Unknow Type ", options.QueueType)
		return nil
	}

}

//NewWriter ...
func NewWriter(opts ...Option) Writer {
	options := NewOption(opts...)
	switch options.QueueType {
	case KafkaQueue:
		w, _ := kafka.NewWriter(options.Brokers, options.Delay).(Writer)
		return w
	default:
		glog.Errorln("Unknow Type ", options.QueueType)
		return nil
	}

}
