package delayqueue

import (
	"fmt"
	"testing"
	"time"
)

var (
	brokers = "10.109.3.48:9092"
	topics  = "TestKafkaTopic1"
	data    = "Hello world"
)

func TestKafkaQueue(t *testing.T) {
	w := NewWriter(Brokers(brokers), Delay(5*time.Second))
	r := NewReader(Brokers(brokers))
	if w == nil || r == nil {
		t.Error("Create kafka fail", w, r)
		return
	}
	go func() {
		for i := 0; i < 20; i++ {
			fmt.Printf("Time Producer %v", time.Now())
			w.Write(topics, []byte(data))
			time.Sleep(1 * time.Second)
		}

	}()

	r.Read(func(topic string, msg []byte) {
		fmt.Printf("Time Consumer %v", time.Now())
		if topic != topics {
			t.Errorf("Invalid topic %s:%s", topic, topics)
		}
		if string(msg) != data {
			t.Errorf("Invalid data %s:%s", string(msg), data)
		}
		fmt.Println(topic, string(msg))

	}, topics)
}
