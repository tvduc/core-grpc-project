package kafka

import (
	"fmt"
	"time"

	"git.zapa.cloud/merchant-tools/helper/delayqueue/common"
	pb "git.zapa.cloud/merchant-tools/helper/delayqueue/proto"
	"git.zapa.cloud/merchant-tools/helper/graceful"
	"git.zapa.cloud/merchant-tools/helper/notify"
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
)

type writer struct {
	producer sarama.AsyncProducer
	delay    int64
	msg      *pb.DelayMessage
	stoped   bool
}

//NewWriter ...
func NewWriter(brokers []string, delay time.Duration) interface{} {

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
	config.Producer.Flush.Frequency = 200 * time.Millisecond // Flush batches every 200ms
	producer, err := sarama.NewAsyncProducer(brokers, config)
	if err != nil {
		glog.Errorln(err)
		return nil
	}
	go func() {
		for err := range producer.Errors() {
			notify.SendToTeams("err write kafka ", fmt.Sprintf("%v", err))
		}
	}()
	w := &writer{producer, int64(delay / time.Second), &pb.DelayMessage{}, false}
	graceful.Stop(0, func() {
		w.stoped = true
		producer.Close()
	})
	return w
}
func (w *writer) Write(topic string, msg []byte) {
	if w.stoped {
		glog.Errorln("Writer is stopped")
		return
	}
	graceful.AddProcess()
	w.msg.Expire = time.Now().Unix() + w.delay
	w.msg.Message = msg
	if data, err := proto.Marshal(w.msg); err != nil {
		glog.Errorln(err)
	} else {
		w.producer.Input() <- &sarama.ProducerMessage{

			Topic: common.GetTopic(topic),
			Value: sarama.ByteEncoder(data),
		}
	}
	graceful.DoneProcess()

}
