package kafka

import (
	"context"

	"git.zapa.cloud/merchant-tools/helper/notify"

	"sync"
	"time"

	"git.zapa.cloud/merchant-tools/helper/delayqueue/common"
	pb "git.zapa.cloud/merchant-tools/helper/delayqueue/proto"
	"git.zapa.cloud/merchant-tools/helper/graceful"
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
)

//Reader ...
type reader struct {
	client sarama.ConsumerGroup
}
type consumer struct {
	f   func(string, []byte)
	msg *pb.DelayMessage
}

func init() {
	//sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
}

//NewReader ...
func NewReader(brokers []string, group string, oldest bool, delay time.Duration) interface{} {
	config := sarama.NewConfig()
	config.Version = sarama.V0_10_2_1
	config.Consumer.Return.Errors = true
	config.ClientID = group
	//Fix Request exceeded the user-specified time limit in the request.
	if delay > 0 {
		config.Consumer.MaxProcessingTime = time.Second + delay
	}
	/*config.Consumer.MaxWaitTime = 10 * time.Second*/
	if oldest {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}

	client, err := sarama.NewConsumerGroup(brokers, group, config)
	if err != nil {
		glog.Errorln(err)
		return nil
	}
	// consume errors
	go func() {
		for err := range client.Errors() {
			glog.Errorln(err)
			notify.SendToTeams("Client Consumer", err.Error())
		}
	}()
	return &reader{client}
}

func (r *reader) Read(f func(string, []byte), topics ...string) {
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	wg.Add(1)
	graceful.Stop(0, func() {
		cancel()
		wg.Wait()
		if err := r.client.Close(); err != nil {
			glog.Errorln("Error closing client: ", err)
		}
	})
	c := &consumer{f, &pb.DelayMessage{}}
	for i, topic := range topics {
		topics[i] = common.GetTopic(topic)
	}
	for {
		if err := r.client.Consume(ctx, topics, c); err != nil {
			glog.Errorln(err)
			notify.SendToTeams("Kafka Consumer", err.Error())
			break
		}
		// check if context was cancelled, signaling that the consumer should stop
		if ctx.Err() != nil {
			glog.Infoln(ctx.Err())
			break
		}
	}
	wg.Done()

}
func (c *consumer) Setup(sarama.ConsumerGroupSession) error {
	glog.Errorln("Consumer Setup")
	return nil
}
func (c *consumer) Cleanup(sarama.ConsumerGroupSession) error {
	glog.Errorln("Consumer Cleanup")
	return nil
}
func (c *consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {

	for message := range claim.Messages() {
		c.msg.Reset()
		if err := proto.Unmarshal(message.Value, c.msg); err != nil {
			glog.Errorln(err)
		} else {
			dt := c.msg.GetExpire() - time.Now().Unix()

			if dt > 0 {
				time.Sleep(time.Duration(dt) * time.Second)
			}
			c.f(message.Topic, c.msg.GetMessage())
		}
		session.MarkMessage(message, "")
	}

	return nil
}
