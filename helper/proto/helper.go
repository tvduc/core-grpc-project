package proto

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/location"
	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
)

// GetCurrentDate : lấy ngày hiện tại theo kiểu Date
func GetCurrentDate() Date {
	return GetDateFromGivenTime(time.Now())
}

//GetDateFromGivenTime : chuyển time đầu vào thành kiểu Date
func GetDateFromGivenTime(time time.Time) Date {
	y, m, d := time.In(location.VNLocation).Date()
	return Date{
		Day:   int32(d),
		Month: int32(m),
		Year:  int32(y),
	}
}

//DateToProtoDate ...
func DateToProtoDate(ymd time.Time) *Date {
	transDate := Date{
		Day:   int32(ymd.Day()),
		Month: int32(ymd.Month()),
		Year:  int32(ymd.Year()),
	}
	return &transDate
}

//DateToTime chuyển đổi từ proto->Time
func DateToTime(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), int(date.GetDay()), 0, 0, 0, 0, time.UTC)
}

//DateUpperToTime chuyển đổi từ proto->Time
func DateUpperToTime(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), int(date.GetDay())+1, 0, 0, 0, 0, time.UTC)
}

//DateMonthToTime lấy month time từ Date
func DateMonthToTime(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), 1, 0, 0, 0, 0, time.UTC)
}

//DateMonthUpperToTime lấy month time từ Date
func DateMonthUpperToTime(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()+1), 1, 0, 0, 0, 0, time.UTC)
}

//trả về kiểu Date từ string có format yyyy-mm-dd
func StringToDate(dateStr string) *Date {
	if len(dateStr) > 0 {
		date := strings.Split(dateStr, "-")
		dd, _ := strconv.Atoi(date[2])
		mm, _ := strconv.Atoi(date[1])
		yyyy, _ := strconv.Atoi(date[0])
		return &Date{Year: int32(yyyy), Month: int32(mm), Day: int32(dd)}
	}
	return nil
}

//TimeStringToTime converts dateString in format string to Time
func TimeStringToTime(dateTime string) time.Time {
	year, _ := strconv.Atoi(dateTime[0:4])
	month, _ := strconv.Atoi(dateTime[5:7])
	day, _ := strconv.Atoi(dateTime[8:10])
	hour, _ := strconv.Atoi(dateTime[11:13])
	min, _ := strconv.Atoi(dateTime[14:16])
	sec, _ := strconv.Atoi(dateTime[17:19])
	return time.Date(year, time.Month(month), day, hour, min, sec, 0, time.Local)
}

//TimeToTimeStamp converts time to timestamp
func TimeToTimeStamp(t time.Time) *timestamp.Timestamp {
	ts, err := ptypes.TimestampProto(t)
	if err != nil {
		glog.Errorln(err)
	}
	return ts
}

func TimeStampToTime(ts *Timestamp) (time.Time, error) {
	if ts == nil {
		return time.Time{}, errors.New("TimeStamp is nil")
	}
	timeStampInternal := &timestamp.Timestamp{
		Seconds: ts.Seconds,
		Nanos:   ts.Nanos,
	}
	return ptypes.Timestamp(timeStampInternal)
}

//trả về kiểu Date từ string có format dd-mm-yyyy
func DDMMYYStringToDate(dateStr string) *Date {
	if len(dateStr) > 0 {
		date := strings.Split(dateStr, "-")
		dd, _ := strconv.Atoi(date[0])
		mm, _ := strconv.Atoi(date[1])
		yyyy, _ := strconv.Atoi(date[2])
		return &Date{Year: int32(yyyy), Month: int32(mm), Day: int32(dd)}
	}
	return nil
}

//Value mysql value Date
func (date Date) Value() (driver.Value, error) {
	return driver.Value(DateToTime(&date)), nil
}

//Scan mysql scan Date
func (date *Date) Scan(src interface{}) error {
	if date == nil {
		return errors.New("date is nil")
	}
	switch src.(type) {
	case time.Time:
	default:
		return errors.New("Incompatible type for Date")
	}
	*date = GetDateFromGivenTime(src.(time.Time))
	return nil

}

//Value mysql value timeOfDay
func (timeOfDay TimeOfDay) Value() (driver.Value, error) {
	return driver.Value(time.Date(0, 0, 0, (int)(timeOfDay.GetHours()), (int)(timeOfDay.GetMinutes()),
		(int)(timeOfDay.GetSeconds()), (int)(timeOfDay.GetNanos()), time.UTC)), nil
}

//Scan mysql scan TimeOfDay
func (timeOfDay *TimeOfDay) Scan(src interface{}) error {
	if timeOfDay == nil {
		return errors.New("timeOfDay is nil")
	}
	switch src.(type) {
	case time.Time:
	default:
		return errors.New("Incompatible type for Date")
	}
	tm := src.(time.Time)
	*timeOfDay = TimeOfDay{
		Hours:   (int32)(tm.Hour()),
		Minutes: (int32)(tm.Minute()),
		Seconds: (int32)(tm.Second()),
		Nanos:   (int32)(tm.Nanosecond()),
	}
	return nil

}

//Value mysql value Timestamp
func (timestamp Timestamp) Value() (driver.Value, error) {
	ts, err := GetTimestamp(&timestamp)
	return driver.Value(ts), err
}

//Scan mysql scan TimeStamp
func (timestamp *Timestamp) Scan(src interface{}) error {
	if timestamp == nil {
		return errors.New("Timestamp is nil")
	}
	switch src.(type) {
	case time.Time:
	default:
		return errors.New("Incompatible type for Timestamp")
	}

	ts, err := TimestampProto(src.(time.Time))
	if err != nil {
		return err
	}
	*timestamp = *ts
	return nil
}

// trả về kiểu Date từ string có dạng yymm
func ConvertYYMMStringToDate(dateStr string) *Date {
	date := &Date{}
	if len(dateStr) == 4 {
		dateBytes := []byte(dateStr)
		var year, month int
		yyyy := "20" + string(dateBytes[0:2])
		mm := string(dateBytes[2:4])
		year, _ = strconv.Atoi(yyyy)
		month, _ = strconv.Atoi(mm)
		date.Year = int32(year)
		date.Month = int32(month)
		date.Day = 1
	}
	return date
}

// trả về kiểu Date từ string có dạng yyyymmdd
func ConvertYYYYMMDDStringToDate(dateStr string) *Date {
	date := &Date{}
	if len(dateStr) == 8 {
		dateBytes := []byte(dateStr)
		var year, month, day int
		yyyy := string(dateBytes[0:4])
		mm := string(dateBytes[4:6])
		dd := string(dateBytes[6:8])
		year, _ = strconv.Atoi(yyyy)
		month, _ = strconv.Atoi(mm)
		day, _ = strconv.Atoi(dd)
		date.Year = int32(year)
		date.Month = int32(month)
		date.Day = int32(day)
	}
	return date
}

// trả về kiểu Date từ string có dạng yyyymm
func ConvertYYYYMMStringToDate(dateStr string) *Date {
	date := &Date{}
	if len(dateStr) == 6 {
		dateBytes := []byte(dateStr)
		var year, month int
		yyyy := string(dateBytes[0:4])
		mm := string(dateBytes[4:6])
		year, _ = strconv.Atoi(yyyy)
		month, _ = strconv.Atoi(mm)
		date.Year = int32(year)
		date.Month = int32(month)
		date.Day = 1
	}
	return date
}

// trả về kiểu Date từ string có dạng yymmdd
func ConvertYYMMDDStringToDate(dateStr string) *Date {
	date := &Date{}
	if len(dateStr) == 6 {
		dateBytes := []byte(dateStr)
		var year, month, day int
		yyyy := "20" + string(dateBytes[0:2])
		mm := string(dateBytes[2:4])
		dd := string(dateBytes[4:6])
		year, _ = strconv.Atoi(yyyy)
		month, _ = strconv.Atoi(mm)
		day, _ = strconv.Atoi(dd)
		date.Year = int32(year)
		date.Month = int32(month)
		date.Day = int32(day)
	}
	return date
}

func CovertStringToYYMMDD(dateStr string) (int32, error) {
	yyyyMMdd := strings.Replace(dateStr, "-", "", -1)
	if len(yyyyMMdd) == 8 {
		result, err := strconv.Atoi(string(yyyyMMdd[2:8]))
		if err != nil {
			return 0, err
		}
		return int32(result), nil
	}
	return 0, errors.New("incorrect input format")

}

//DateToTimeSearch chuyển đổi từ proto->Time
func DateToTimeSearch(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), int(date.GetDay()), 0, 0, 0, 0, location.VNLocation)
}

//DateUpperToTimeSearch chuyển đổi từ proto->Time
func DateUpperToTimeSearch(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), int(date.GetDay())+1, 0, 0, 0, 0, location.VNLocation)
}

//DateMonthToTimeSearch lấy month time từ Date
func DateMonthToTimeSearch(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()), 1, 0, 0, 0, 0, location.VNLocation)
}

//DateMonthUpperToTimeSearch lấy month time từ Date
func DateMonthUpperToTimeSearch(date *Date) time.Time {
	if date == nil {
		date = &Date{Year: 1970,
			Month: 1,
			Day:   1,
		}
		glog.Errorln("date is nil")
	}
	return time.Date(int(date.GetYear()), (time.Month)(date.GetMonth()+1), 1, 0, 0, 0, 0, location.VNLocation)
}

func DateCompare(date1, date2 Date) int64 {
	time1 := DateToTime(&date1).UnixNano()
	time2 := DateToTime(&date2).UnixNano()

	return time1 - time2
}

func DateToString(date *Date) string {
	return fmt.Sprintf("%04d-%02d-%02d", date.GetYear(), date.GetMonth(), date.GetDay())
}

//CompareProTime : return true if iTime < jTime
func CompareProTime(itime, jtime *timestamp.Timestamp) bool {
	if itime.GetSeconds() < jtime.GetSeconds() {
		return true
	} else if itime.GetSeconds() == jtime.GetSeconds() {
		return itime.GetNanos() < jtime.GetNanos()
	}
	return false

}

//CompareProTime : return true if iTime <= jTime
func CompareProTimeLte(itime, jtime *timestamp.Timestamp) bool {
	if itime.GetSeconds() < jtime.GetSeconds() {
		return true
	} else if itime.GetSeconds() == jtime.GetSeconds() && itime.GetNanos() == jtime.GetNanos() {
		return true
	}

	return false
}
