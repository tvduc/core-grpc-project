package database

import (
	"context"
	"database/sql"
	"fmt"

	"git.zapa.cloud/merchant-tools/helper/notify"
	tracing "git.zapa.cloud/merchant-tools/helper/opentracing"
	"github.com/jmoiron/sqlx"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var (
	mysqlTag = opentracing.Tag{Key: string(ext.Component), Value: "mySql"}
)

// Select using this DB.
// Any placeholder parameters are replaced with supplied args.
func Select(parentCtx context.Context, db *sqlx.DB, dest interface{}, query string, args ...interface{}) error {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	err := db.SelectContext(newCtx, dest, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Select ", fmt.Sprintf("%s:%v", query, err))
	}
	return err
}

// Get using this DB.
// Any placeholder parameters are replaced with supplied args.
// An error is returned if the result set is empty.
func Get(parentCtx context.Context, db *sqlx.DB, dest interface{}, query string, args ...interface{}) error {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	err := db.GetContext(newCtx, dest, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Get ", fmt.Sprintf("%s:%v", query, err))
	}
	return err
}

// Commit commits the transaction.
func Commit(parentCtx context.Context, tx *sql.Tx) error {
	_, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, "sqlCommit")
	err := tx.Commit()
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Commit", fmt.Sprintf("%v", err))
	}
	return err
}

// QueryRow executes a query that is expected to return at most one row.
// QueryRow always returns a non-nil value. Errors are deferred until
// Row's Scan method is called.
// If the query selects no rows, the *Row's Scan will return ErrNoRows.
// Otherwise, the *Row's Scan scans the first selected row and discards
// the rest.
func QueryRow(parentCtx context.Context, db *sqlx.DB, query string, args ...interface{}) *sql.Row {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	row := db.QueryRowContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, nil)

	return row
}

// Exec executes a query without returning any rows.
// The args are for any placeholder parameters in the query.
func Exec(parentCtx context.Context, db *sqlx.DB, query string, args ...interface{}) (sql.Result, error) {
	method := query
	if len(method) > 256 {
		method = method[:256]
	}
	if len(args) < 32 {
		method = fmt.Sprintf("%s-%v", method, args)
	}
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, method)
	res, err := db.ExecContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Exec ", fmt.Sprintf("%s:%v", query, err))
	}
	return res, err
}

// TxExec executes a query that doesn't return rows.
// For example: an INSERT and UPDATE.
func TxExec(parentCtx context.Context, tx *sql.Tx, query string, args ...interface{}) (sql.Result, error) {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	res, err := tx.ExecContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB TxExec ", fmt.Sprintf("%s:%v", query, err))
	}
	return res, err
}

// Query executes a query that returns rows, typically a SELECT.
// The args are for any placeholder parameters in the query.
func Query(parentCtx context.Context, db *sqlx.DB, query string, args ...interface{}) (*sql.Rows, error) {
	method := query
	if len(args) < 32 {
		method = fmt.Sprintf("%s-%v", query, args)
	}
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, method)
	res, err := db.QueryContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Query ", fmt.Sprintf("%s:%v", query, err))
	}
	return res, err
}

// Queryx queries the database and returns an *sqlx.Rows.
// Any placeholder parameters are replaced with supplied args.
func Queryx(parentCtx context.Context, db *sqlx.DB, query string, args ...interface{}) (*sqlx.Rows, error) {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	res, err := db.QueryxContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, err)
	if err != sql.ErrNoRows && err != nil {
		notify.SendToTeams("DB Queryx ", fmt.Sprintf("%s:%v", query, err))
	}
	return res, err
}

// QueryRowx queries the database and returns an *sqlx.Row.
// Any placeholder parameters are replaced with supplied args.
func QueryRowx(parentCtx context.Context, db *sqlx.DB, query string, args ...interface{}) *sqlx.Row {
	newCtx, clientSpan := tracing.NewClientSpanFromContext(parentCtx, mysqlTag, fmt.Sprintf("%s-%v", query, args))
	res := db.QueryRowxContext(newCtx, query, args...)
	tracing.FinishClientSpan(clientSpan, nil)

	return res
}
