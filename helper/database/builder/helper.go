package builder

import (
	sq "github.com/Masterminds/squirrel"
)

const (
	mysqlDriver   = "mysql"
	postgesDriver = "postgres"
)

//Select return sq.SelectBuilder from columns
func Select(driverName string, columns ...string) sq.SelectBuilder {
	switch driverName {
	case mysqlDriver:
		return sq.Select(columns...)
	case postgesDriver:
		return sq.StatementBuilder.PlaceholderFormat(sq.Dollar).Select(columns...)
	}
	return sq.Select(columns...)
}

// Insert returns a new InsertBuilder with the given table name.
func Insert(driverName, into string) sq.InsertBuilder {
	switch driverName {
	case mysqlDriver:
		return sq.Insert(into)
	case postgesDriver:
		return sq.StatementBuilder.PlaceholderFormat(sq.Dollar).Insert(into)
	}
	return sq.StatementBuilder.Insert(into)
}

// Update returns a new UpdateBuilder with the given table name.
//
// See UpdateBuilder.Table.
func Update(driverName, table string) sq.UpdateBuilder {
	switch driverName {
	case mysqlDriver:
		return sq.StatementBuilder.Update(table)
	case postgesDriver:
		return sq.StatementBuilder.PlaceholderFormat(sq.Dollar).Update(table)
	}
	return sq.StatementBuilder.Update(table)
}

// Delete returns a new DeleteBuilder with the given table name.
//
// See DeleteBuilder.Table.
func Delete(driverName, from string) sq.DeleteBuilder {
	switch driverName {
	case mysqlDriver:
		return sq.StatementBuilder.Delete(from)
	case postgesDriver:
		return sq.StatementBuilder.PlaceholderFormat(sq.Dollar).Delete(from)
	}
	return sq.StatementBuilder.Delete(from)
}
