package database

import (
	"context"
	"fmt"
	"strings"
	"time"

	codec "encoding/json"

	sq "github.com/Masterminds/squirrel"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/glog"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/core"
)

/*CreateSQLXDB tạo connection sqlx
Input: config
Output: trả về connection nếu thành công, ngược lại trả về null
*/
func CreateSQLXDB(dbconfig config.Database) *sqlx.DB {
	c, e := core.GetCache("CreateSQLXDB"+dbconfig.Addr+dbconfig.Databasename, func() (interface{}, error) {
		if len(dbconfig.Driver) == 0 {
			dbconfig.Driver = "mysql"
		}
		connectString := fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&charset=utf8",
			dbconfig.User, dbconfig.Password, dbconfig.Addr, dbconfig.Databasename)
		db, err := sqlx.Open(dbconfig.Driver, connectString)
		if err != nil {
			glog.Error(connectString, err)
			return nil, err
		}
		db.SetConnMaxLifetime(time.Duration(5) * time.Minute)
		return db, nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*sqlx.DB)
}

//SelectSQLEx là wrapper lấy danh sách kết quả từ query
func SelectSQLEx(ctx context.Context, dest interface{}, db *sqlx.DB, query string, args ...interface{}) error {
	rows, err := Query(ctx, db, query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	columns, err := rows.Columns()
	if err != nil {
		return err
	}
	if len(columns) != 1 {
		return fmt.Errorf("Number of columns %d not 1", len(columns))
	}
	var datas []string
	for rows.Next() {
		var data string
		if err = rows.Scan(&data); err != nil {
			return err
		}
		datas = append(datas, data)
	}
	jsondata := "[" + strings.Join(datas, ",") + "]"
	err = codec.Unmarshal([]byte(jsondata), dest)
	return err
}

//SelectSQLXEx là wrapper lấy danh sách kết quả từ query
func SelectSQLXEx(ctx context.Context, db *sqlx.DB, dest interface{}, query string, args ...interface{}) error {
	var datas []string

	if err := Select(ctx, db, &datas, query, args...); err != nil {
		glog.Errorln("SelectSQLXEx Select", err)
		return err
	}
	jsondata := "[" + strings.Join(datas, ",") + "]"
	return codec.Unmarshal([]byte(jsondata), dest)
}

//GetNumRowsSQLX là wrapper lấy số record trong query
func GetNumRowsSQLX(ctx context.Context, db *sqlx.DB, query string, args ...interface{}) (int32, error) {
	var count int
	err := QueryRow(ctx, db, query, args...).Scan(&count)
	return (int32)(count), err
}

//SQGetNumRowsSQLX là wrapper lấy số record trong query
func SQGetNumRowsSQLX(ctx context.Context, db *sqlx.DB, query *sq.SelectBuilder) (int32, error) {
	sql, args, err := query.ToSql()
	if err != nil {
		glog.Error(err)
		return 0, nil
	}
	return GetNumRowsSQLX(ctx, db, sql, args...)
}

//SQSelectSQLXEx wrapper sq for SelectSQLXEx
func SQSelectSQLXEx(ctx context.Context, db *sqlx.DB, dest interface{}, query *sq.SelectBuilder) error {
	sql, args, err := query.ToSql()
	if err != nil {
		glog.Error(err)
		return nil
	}
	glog.Info(sql)
	glog.Info(args)
	return SelectSQLXEx(ctx, db, dest, sql, args...)
}
