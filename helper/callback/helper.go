package callback

import (
	"encoding/json"
	"errors"
	topic "git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/kafka"
	callback_center "git.zapa.cloud/merchant-tools/protobuf/callback-center"
	"github.com/golang/glog"
	"github.com/google/go-querystring/query"
)

type CallbackClient interface {
	SendCallBackRequest(url string, method callback_center.HttpMethod, request interface{}, header map[string]string) error
}

type callbackHandler struct {
	writer kafka.Writer
}

func NewCallbackClient(writer kafka.Writer) CallbackClient {
	return &callbackHandler{
		writer: writer,
	}
}

//call this function to send a request to callback center
func (h *callbackHandler) SendCallBackRequest(url string, method callback_center.HttpMethod, request interface{}, header map[string]string) error {
	if h.writer == nil {
		return errors.New("writer can not be nil")
	}

	DTObject := &callback_center.CallBackRequest{
		Method: method,
		Header: header,
	}

	if method == callback_center.HttpMethod_MT_GET {
		data, err := query.Values(request)
		if err != nil {
			glog.Errorln(err)
			return err
		}

		url = url + "?" + data.Encode()
		DTObject.Url = url
	} else if method == callback_center.HttpMethod_MT_POST {
		data, err := json.Marshal(request)
		if err != nil {
			glog.Errorln(err)
			return err
		}
		DTObject.Url = url
		DTObject.Body = data
	} else {
		return nil
	}

	return h.writer.WriteByTopic(DTObject, topic.CallbackCenterTopic)
}