package aggregation

import (
	"strconv"
	"strings"
)

//SimpleBuilder ...
type SimpleBuilder struct {
	resp interface{}
	tags []string
}

var (
	mapCmdName = map[cmdType]string{}
)

func init() {
	for k, v := range mapCmd {
		mapCmdName[v] = k
	}
}

//NewBuilder ...
func NewBuilder() *SimpleBuilder {
	return &SimpleBuilder{}
}

//Encode ...
func (builder *SimpleBuilder) Encode() []string {
	return builder.tags
	//return strings.Join(builder.tags, ";")
}

//GetResponse ...
func (builder *SimpleBuilder) GetResponse() interface{} {
	return builder.resp
}

//Response ...
func (builder *SimpleBuilder) Response(resp interface{}) *SimpleBuilder {
	builder.resp = resp
	return builder
}

//Terms ...
func (builder *SimpleBuilder) Terms(field string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[termsCmd]+":"+field)
	return builder
}

//Count ...
func (builder *SimpleBuilder) Count() *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[countCmd])
	return builder
}

//Sum ...
func (builder *SimpleBuilder) Sum(fields ...string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[sumCmd]+":"+strings.Join(fields, ","))
	return builder
}

//Max ...
func (builder *SimpleBuilder) Max(fields ...string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[maxCmd]+":"+strings.Join(fields, ","))
	return builder
}

//Min ...
func (builder *SimpleBuilder) Min(fields ...string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[minCmd]+":"+strings.Join(fields, ","))
	return builder
}

//FilterNot ...
func (builder *SimpleBuilder) FilterNot(field string, values ...string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[filterCmd]+":"+field+"!="+strings.Join(values, ","))
	return builder
}

//Filter ...
func (builder *SimpleBuilder) Filter(field string, values ...string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[filterCmd]+":"+field+"="+strings.Join(values, ","))
	return builder
}

//Top ...
func (builder *SimpleBuilder) Top(n int, field ...string) *SimpleBuilder {
	tag := mapCmdName[topCmd] + ":" + strconv.Itoa(n)
	if len(field) == 1 && field[0] != "" {
		tag = tag + "," + field[0]
	}
	builder.tags = append(builder.tags, tag)
	return builder
}

//DateHisto ...
func (builder *SimpleBuilder) DateHisto(field string) *SimpleBuilder {
	builder.tags = append(builder.tags, mapCmdName[dateHistoCmd]+":"+field)
	return builder
}
