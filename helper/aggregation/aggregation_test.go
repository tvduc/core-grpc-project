package aggregation

import (
	"fmt"
	"reflect"
	"testing"
)

type Sum struct {
	Agg *ListAggregation1D
}

func TestMakeAggregation(t *testing.T) {
	sum := Sum{}
	fmt.Println(reflect.TypeOf(&sum.Agg))

	if !makeAggregation(reflect.ValueOf(&sum.Agg), reflect.TypeOf(ListAggregation1D{})) {
		t.Error("makeAggregation fail")
	}
	fmt.Println(sum)
}
