package aggregation

import (
	"git.zapa.cloud/merchant-tools/helper/proto"
)

type AggregationType int

const (
	AggregationRespNone AggregationType = iota
	AggregationResp1D
	AggregationResp2D
	AggregationResp3D
	AggregationResp4D
	AggregationResp5D
)

//AggregationResp ...
/*type AggregationResp interface {
	AddSubAgg(name string, count int64) AggregationResp
}*/

func (agg *ListAggregation) AddSubAgg(key string, count int64) proto.AggregationResp {

	return nil
}
func (agg *ListAggregation1D) AddSubAgg(key string, count int64) proto.AggregationResp {
	subAgg := &ListAggregation{Key: key, Count: count}
	agg.Aggs = append(agg.Aggs, subAgg)
	return subAgg
}
func (agg *ListAggregation2D) AddSubAgg(key string, count int64) proto.AggregationResp {
	subAgg := &ListAggregation1D{Key: key, Count: count}
	agg.Aggs = append(agg.Aggs, subAgg)
	return subAgg
}
func (agg *ListAggregation3D) AddSubAgg(key string, count int64) proto.AggregationResp {
	subAgg := &ListAggregation2D{Key: key, Count: count}
	agg.Aggs = append(agg.Aggs, subAgg)
	return subAgg
}
func (agg *ListAggregation4D) AddSubAgg(key string, count int64) proto.AggregationResp {
	subAgg := &ListAggregation3D{Key: key, Count: count}
	agg.Aggs = append(agg.Aggs, subAgg)
	return subAgg
}
func (agg *ListAggregation5D) AddSubAgg(key string, count int64) proto.AggregationResp {
	subAgg := &ListAggregation4D{Key: key, Count: count}
	agg.Aggs = append(agg.Aggs, subAgg)
	return subAgg
}
