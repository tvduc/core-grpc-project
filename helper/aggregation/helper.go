package aggregation

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	helperES "git.zapa.cloud/merchant-tools/helper/elastic"
	helperLocation "git.zapa.cloud/merchant-tools/helper/location"
	tracingHelper "git.zapa.cloud/merchant-tools/helper/opentracing"
	"git.zapa.cloud/merchant-tools/helper/proto"
	"github.com/golang/glog"
	"gopkg.in/olivere/elastic.v6"
)

type cmdType int

const (
	noneCmd cmdType = iota
	countCmd
	sumCmd
	termsCmd
	dateHistoCmd
	filterCmd
	topCmd
	maxCmd
	minCmd
)
const (
	countField  = "count:"
	sumField    = "sum:"
	aggTag      = "agg"
	countTag    = "_count"
	notEqualTag = "!="
	equalTag    = "="
	scriptTag   = "script:"
	maxField    = "max:"
	minField    = "min:"
	splitParam  = ","
	splitCmd    = ";"
)

var (
	mapCmd = map[string]cmdType{
		"count":   countCmd,     //count
		"sum":     sumCmd,       //sum:field1,field2,....
		"terms":   termsCmd,     //terms:field[,size]
		"dateHis": dateHistoCmd, //dateHis:field
		"filter":  filterCmd,    //filter:field=value
		"top":     topCmd,       //top:5[,field1]
		"max":     maxCmd,
		"min":     minCmd,
	}
	mapCmdToField = map[cmdType]string{
		countCmd: countField,
		sumCmd:   sumField,
		topCmd:   sumField,
		maxCmd:   maxField,
		minCmd:   minField,
	}
)

type treeCmd struct {
	aggs             map[string]elastic.Aggregation
	fromDate, toDate *proto.Date
	search           *elastic.SearchService
	iscript          int
}

func addAgg(aggs interface{}, name string, subAgg elastic.Aggregation) error {
	if search, ok := aggs.(*elastic.SearchService); ok {
		search.Aggregation(name, subAgg)
	} else if agg, ok := aggs.(*elastic.TermsAggregation); ok {
		agg.SubAggregation(name, subAgg)
	} else if agg, ok := aggs.(*elastic.FilterAggregation); ok {
		agg.SubAggregation(name, subAgg)
	} else if agg, ok := aggs.(*elastic.DateHistogramAggregation); ok {
		agg.SubAggregation(name, subAgg)
	} else {
		return fmt.Errorf("Unknow aggtype %s", name)
	}
	return nil

}
func parseQuery(query string) ([]string, cmdType, string, error) {
	var (
		err  error
		name string
	)
	params := strings.SplitN(query, ":", 2)
	cmd, ok := mapCmd[params[0]]
	if !ok {
		err = fmt.Errorf("%s unknow command %s", query, params[0])
	} else if cmd != countCmd && len(params) != 2 {
		err = fmt.Errorf("%s invalid param len %d", query, len(params))
	} else {
		if name, ok = mapCmdToField[cmd]; !ok {
			name = query
		}
	}
	return params, cmd, name, err
}

//parseFilterCmd return op,field, values
func parseFilterCmd(param string) (string, string, []string, error) {
	op := notEqualTag
	params := strings.SplitN(param, op, 2)
	if len(params) != 2 {
		op = equalTag
		params = strings.SplitN(param, op, 2)
	}
	if len(params) != 2 {
		return "", "", nil, fmt.Errorf("Invalid filter %s", param)
	}
	return op, params[0], strings.Split(params[1], splitParam), nil
}
func (tree treeCmd) addQuery(query, path string) (string, error) {
	params, cmd, name, err := parseQuery(query)
	if err != nil || cmd == countCmd {
		return path, err
	}
	newPath := path + ";" + name
	if _, ok := tree.aggs[newPath]; ok {
		return newPath, nil
	}

	var agg interface{}
	if path != "" {
		agg = tree.aggs[path]
	} else {
		agg = tree.search
	}
	var subAgg elastic.Aggregation
	switch cmd {
	case countCmd:
	case sumCmd: //sum:field1,field2,....
		for _, field := range strings.Split(params[1], splitParam) {
			sumAgg := elastic.NewSumAggregation().Missing(0)
			if strings.HasPrefix(field, scriptTag) {
				glog.Infoln("script", strings.TrimPrefix(field, scriptTag))
				script := elastic.NewScript(strings.TrimPrefix(field, scriptTag))
				sumAgg = sumAgg.Script(script).Missing(0)
				field = fmt.Sprintf("script%d", tree.iscript)
				tree.iscript++
			} else {
				sumAgg = sumAgg.Field(field)
			}
			if err := addAgg(agg, field, sumAgg); err != nil {
				return "", err
			}
		}
		return path, nil
	case maxCmd:
		for _, field := range strings.Split(params[1], splitParam) {
			maxAgg := elastic.NewMaxAggregation().Missing(0)
			if strings.HasPrefix(field, scriptTag) {
				script := elastic.NewScript(strings.TrimPrefix(field, scriptTag))
				maxAgg = maxAgg.Script(script)
			} else {
				maxAgg = maxAgg.Field(field)
			}
			if err := addAgg(agg, field, maxAgg); err != nil {
				return "", err
			}
		}
		return path, nil
	case minCmd:
		for _, field := range strings.Split(params[1], splitParam) {
			minAgg := elastic.NewMinAggregation().Missing(0)
			if strings.HasPrefix(field, scriptTag) {
				script := elastic.NewScript(strings.TrimPrefix(field, scriptTag))
				minAgg = minAgg.Script(script)
			} else {
				minAgg = minAgg.Field(field)
			}
			if err := addAgg(agg, field, minAgg); err != nil {
				return "", err
			}
		}
		return path, nil
	case termsCmd: //terms:field[,size,mindoc]
		p := strings.SplitN(params[1], splitParam, 3)
		switch len(p) {
		case 1:
			subAgg = elastic.NewTermsAggregation().Field(params[1]).Size(100)
		case 2, 3:
			size, err := strconv.Atoi(p[1])
			if err != nil {
				glog.Errorln(err)
				return "", err
			}
			termAgg := elastic.NewTermsAggregation().Field(p[0]).Size(size)
			if len(p) == 3 {
				size, err = strconv.Atoi(p[2])
				if err != nil {
					glog.Errorln(err)
					return "", err
				}
				termAgg = termAgg.MinDocCount(size)
			}
			subAgg = termAgg
		default:
			glog.Errorln("Invalid param term", params[1])
		}

	case filterCmd: //filter:field=value
		op, field, values, err := parseFilterCmd(params[1])
		if err != nil {
			return "", err
		}
		var query elastic.Query
		if len(values) == 1 {
			query = elastic.NewTermQuery(field, values[0])
		} else {
			query = elastic.NewTermsQuery(field, helperES.DoubleSlice(values)...)
		}
		if op == notEqualTag {
			query = elastic.NewBoolQuery().MustNot(query)
		}
		subAgg = elastic.NewFilterAggregation().Filter(query)
	case dateHistoCmd: //dateHis:field
		aggHis := elastic.NewDateHistogramAggregation().Field(params[1]).Interval("1d").TimeZone("+07:00")
		if tree.fromDate != nil && tree.toDate != nil {
			subAgg = aggHis.ExtendedBounds(proto.DateToTime(tree.fromDate), proto.DateToTime(tree.toDate))
		} else {
			subAgg = aggHis
		}
	case topCmd: //top:5[,field]
		params := strings.SplitN(params[1], splitParam, 2)
		if size, err := strconv.Atoi(params[0]); err == nil {
			sortAgg := elastic.NewBucketSortAggregation().Size(size)
			if len(params) == 1 {
				sortAgg.Sort("_count", false)
			} else {
				sortAgg.Sort(params[1], false)
			}
			if err := addAgg(agg, query, sortAgg); err != nil {
				return "", err
			}
			return path, nil
		} else {
			return "", err
		}

	}
	if subAgg != nil {
		if err := addAgg(agg, name, subAgg); err != nil {
			return "", nil
		}
		tree.aggs[newPath] = subAgg
	}

	return newPath, nil
}
func keyToDate(key float64) string {
	tm := time.Unix(0, int64(key)*int64(time.Millisecond)).In(helperLocation.VNLocation)
	return tm.Format("02-01-2006")
}
func setAggregation(value reflect.Value) bool {
	v := value.Interface()
	if agg, ok := v.(*proto.ListAggregation1D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(proto.ListAggregation1D)))
		}
	} else if agg, ok := v.(*proto.ListAggregation2D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(proto.ListAggregation2D)))
		}
	} else if agg, ok := v.(*proto.ListAggregation3D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(proto.ListAggregation3D)))
		}
	} else if agg, ok := v.(*proto.ListAggregation4D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(proto.ListAggregation4D)))
		}
	} else if agg, ok := v.(*proto.ListAggregation5D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(proto.ListAggregation5D)))
		}
	} else if agg, ok := v.(*ListAggregation1D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(ListAggregation1D)))
		}
	} else if agg, ok := v.(*ListAggregation2D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(ListAggregation2D)))
		}
	} else if agg, ok := v.(*ListAggregation3D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(ListAggregation3D)))
		}
	} else if agg, ok := v.(*ListAggregation4D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(ListAggregation4D)))
		}
	} else if agg, ok := v.(*ListAggregation5D); ok {
		if agg == nil {
			value.Set(reflect.ValueOf(new(ListAggregation5D)))
		}
	} else {
		return false
	}
	return true

}
func (tree treeCmd) addTag(param *cmdParam, req interface{}) error {
	var err error
	tags, value := param.tags, param.value
	path := ""
	glog.Infoln(tags)

	for _, query := range tags {
		if path, err = tree.addQuery(query, path); err != nil {
			glog.Errorln("addTag: addQuery", err)
			return err
		}
	}
	setAggregation(value)
	return err
}
func makeAggregation(val reflect.Value, kinds ...reflect.Type) bool {

	aggType := reflect.TypeOf(val.Interface())
	for _, kind := range kinds {
		if kind == aggType {
			val.Set(reflect.New(kind))
			return true
		}
	}
	return false
}
func extractQuery(aggs elastic.Aggregations, query []string, index int, count int64, aggResp proto.AggregationResp, fName string, value reflect.Value) error {
	if index >= len(query) {
		return nil
	}
	tag := query[index]
	params, cmd, name, err := parseQuery(tag)
	if err != nil {
		return err
	}

	switch cmd {
	case countCmd:
		if aggResp != nil {
			if resp, ok := aggResp.(*proto.ListAggregation); ok {
				resp.Count = count
			} else if resp, ok := aggResp.(*ListAggregation); ok {
				resp.Count = count
			} else {
				return fmt.Errorf("%s not ListAggregation", tag)
			}
		} else if _, ok := value.Interface().(int64); ok {
			value.SetInt(count)
		} else if v, ok := value.Interface().(*int64); ok {
			*v = count
		} else {
			glog.Errorln("Invalid ouput ", fName)
			return fmt.Errorf("Invalid ouput %s", fName)
		}
	case sumCmd, maxCmd, minCmd:
		fields := strings.Split(params[1], splitParam)
		sums := make([]int64, len(fields)+1)
		doubleSums := make([]float64, len(fields))
		sums[0] = count
		for i, field := range fields {
			if sum, ok := aggs.Sum(field); ok && sum.Value != nil {
				sums[i+1] = int64(*sum.Value)
				doubleSums[i] = *sum.Value
			}
		}
		if aggResp != nil {
			if resp, ok := aggResp.(*proto.ListAggregation); ok {
				resp.Sums = sums[1:]
			} else if resp, ok := aggResp.(*ListAggregation); ok {
				resp.Sums = doubleSums
			} else {
				return fmt.Errorf("%s not ListAggregation %v", tag, reflect.TypeOf(aggResp))
			}

		} else if _, ok := value.Interface().([]int64); ok {
			if value.CanSet() {
				value.Set(reflect.ValueOf(sums))
			} else {
				value = reflect.AppendSlice(value, reflect.ValueOf(sums))
			}
		} else if v, ok := value.Interface().(*[]int64); ok {
			*v = sums
		} else {
			glog.Errorln("Invalid ouput ", fName)
			return fmt.Errorf("Invalid ouput %s", fName)
		}

	case termsCmd:
		if agg, ok := aggs.Terms(name); ok {
			for _, bucket := range agg.Buckets {
				key := fmt.Sprintf("%v", bucket.Key)
				subAgg := aggResp.AddSubAgg(key, bucket.DocCount)
				if subAgg == nil {
					glog.Errorln("Invalid ouput ", fName)
					return fmt.Errorf("Invalid ouput %s", fName)
				}
				if err := extractQuery(bucket.Aggregations, query, index+1, bucket.DocCount, subAgg, fName, value); err != nil {
					return err
				}
			}

		} else {
			return fmt.Errorf("Not found terms %s", name)
		}
	case filterCmd:
		if agg, ok := aggs.Filter(name); ok {
			if err := extractQuery(agg.Aggregations, query, index+1, agg.DocCount, aggResp, fName, value); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("Not found filter %s", name)
		}
	case topCmd:
		if err := extractQuery(aggs, query, index+1, count, aggResp, fName, value); err != nil {
			return err
		}
	case dateHistoCmd:
		if agg, ok := aggs.DateHistogram(name); ok {
			for _, bucket := range agg.Buckets {
				key := keyToDate(bucket.Key) //fmt.Sprintf("%v", bucket.Key)
				subAgg := aggResp.AddSubAgg(key, bucket.DocCount)
				if subAgg == nil {
					glog.Errorln("Invalid ouput ", fName)
					return fmt.Errorf("Invalid ouput %s", fName)
				}
				if err := extractQuery(bucket.Aggregations, query, index+1, bucket.DocCount, subAgg, fName, value); err != nil {
					return err
				}
			}

		} else {
			return fmt.Errorf("Not found terms %s", name)
		}
	}
	return nil
}
func extractValue(aggs elastic.Aggregations, param *cmdParam, count int64) error {
	fName, value, tags := param.name, param.value, param.tags

	aggResp, _ := value.Interface().(proto.AggregationResp)
	return extractQuery(aggs, tags, 0, count, aggResp, fName, value)
}

/* Do example:
Use 1:
type DashBoardResponse struct {
	Test *google_type.ListAggregation1D `agg:"filter:zpTranstatus=2;terms:merchantID;top:3,amount;sum:amount"`
}
resp := &DashBoardResponse{}
aggHelper.Do(context.Background(), helperDashBoard.GetTransSearch(), req, resp)
or Use 2:
resp := &google_type.ListAggregation1D{}
aggHelper.Do(context.Background(), helperDashBoard.GetTransSearch(), req, nil,"filter:zpTranstatus=2;terms:merchantID;top:3,amount;sum:amount",resp)
or Use 3:
resp := &google_type.ListAggregation1D{}
builder := aggHelper.NewBuilder().Filter("zpTranstatus", "2").Terms("merchantID").Top(3, "amount").Sum("amount")
aggHelper.Do(context.Background(), helperDashBoard.GetTransSearch(), req, nil,builder,resp)

*/
type cmdParam struct {
	name  string
	tags  []string
	value reflect.Value
}

func Do(parentCtx context.Context, search *elastic.SearchService, req interface{}, resp interface{}, ext ...interface{}) error {

	if req != nil {
		if query, ok := req.(elastic.Query); ok {
			search = search.Query(query)
		} else {
			search = search.Query(helperES.BuildQuery(nil, req))
		}
	} else {
		search = search.Query(elastic.NewMatchAllQuery())
	}
	cmdParams, err := AddAgg(search, req, resp, ext...)
	if err != nil {
		return err
	}
	str, _ := json.Marshal(req)
	ctx, span := helperES.MakeSpanContext(parentCtx, string(str))
	searchResult, err := search.Size(0).Pretty(true).Do(ctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		helperES.DumpElasticError(err)
		glog.Errorln(err)
		return err
	}
	err = ExtractAgg(searchResult, cmdParams)
	return nil
}

//ExtractAgg ...
func ExtractAgg(searchResult *elastic.SearchResult, cmdParams interface{}) error {
	if cmdParams == nil {
		return nil
	}
	params, _ := cmdParams.([]*cmdParam)
	for _, param := range params {
		if err := extractValue(searchResult.Aggregations, param, searchResult.TotalHits()); err != nil {
			glog.Errorln(err)
			return err
		}
	}
	return nil
}

//AddAgg ...
func AddAgg(search *elastic.SearchService, req interface{}, resp interface{}, ext ...interface{}) (interface{}, error) {
	cmdParams := []*cmdParam{}

	tree := &treeCmd{
		search: search,
		aggs:   map[string]elastic.Aggregation{},
	}
	if reqDate, ok := req.(interface {
		GetFromDate() *proto.Date
		GetToDate() *proto.Date
	}); ok {
		tree.fromDate = reqDate.GetFromDate()
		tree.toDate = reqDate.GetToDate()
	}
	if resp != nil {
		res := reflect.ValueOf(resp)
		if res.Kind() != reflect.Ptr || res.IsNil() {
			return cmdParams, fmt.Errorf("Invalid resp %v", resp)
		}
		elem := res.Elem()
		vt := elem.Type()
		for i := 0; i < vt.NumField(); i++ {
			f := vt.Field(i)
			if tag, ok := f.Tag.Lookup(aggTag); ok {
				cmdParams = append(cmdParams, &cmdParam{f.Name, strings.Split(tag, splitCmd), elem.Field(i)})
			}
		}
	}
	for i := 1; i < len(ext); i += 2 {
		var tags []string
		if tag, ok := ext[i-1].(string); ok {
			tags = strings.Split(tag, splitCmd)
		} else {
			if builder, ok := ext[i-1].(*SimpleBuilder); ok {
				tags = builder.Encode()
			}
		}
		if len(tags) > 0 {
			value := reflect.ValueOf(ext[i])
			if value.Kind() != reflect.Ptr || value.IsNil() {
				return cmdParams, fmt.Errorf("Invalid ext %v", tags)
			}
			cmdParams = append(cmdParams, &cmdParam{"", tags, value})

		} else {
			glog.Errorf("ext %d is not string", i-1)
			return cmdParams, fmt.Errorf("ext %d is not string", i-1)
		}

	}
	for _, param := range cmdParams {
		if err := tree.addTag(param, req); err != nil {
			glog.Errorln(err)
			return cmdParams, err
		}
	}
	return cmdParams, nil
}

type esReq interface {
	BuildQuery(query *elastic.BoolQuery) *elastic.BoolQuery
}
type EsAgg interface {
	Encode() []string
	GetResponse() interface{}
}

/*DoAgg
resp := &google_type.ListAggregation1D{}
builder := aggHelper.NewBuilder().Filter("zpTranstatus", "2").Terms("merchantID").Top(3, "amount").Sum("amount")
builder = builder.Response(resp)
aggHelper.DoAgg(context.Background(), helperDashBoard.GetTransSearch(), req,builder)
*/
func DoAgg(parentCtx context.Context, search *elastic.SearchService, req interface{}, aggs ...EsAgg) error {
	var tag []byte
	if req != nil {
		if esreq, ok := req.(esReq); ok {
			tag, _ = json.Marshal(req)
			search = search.Query(esreq.BuildQuery(nil))
		} else if query, ok := req.(elastic.Query); ok {
			search = search.Query(query)
			src, _ := query.Source()
			tag, _ = json.Marshal(src)
		} else {
			glog.Errorln(req)
			return fmt.Errorf("Unknown req %v", reflect.TypeOf(req))
		}
	} else {
		tag = []byte("Match all")
		search = search.Query(elastic.NewMatchAllQuery())
	}
	for _, agg := range aggs {
		glog.Infoln(agg.Encode())
	}
	/*cmdParams, err := AddAgg(search, req, nil, agg, resp)
	if err != nil {
		return err
	}*/
	cmdParams := []*cmdParam{}

	tree := &treeCmd{
		search: search,
		aggs:   map[string]elastic.Aggregation{},
	}
	if reqDate, ok := req.(interface {
		GetFromDate() *proto.Date
		GetToDate() *proto.Date
	}); ok {
		tree.fromDate = reqDate.GetFromDate()
		tree.toDate = reqDate.GetToDate()
	}
	for _, agg := range aggs {
		tags := agg.Encode()
		if len(tags) > 0 {
			value := reflect.ValueOf(agg.GetResponse())
			if value.Kind() != reflect.Ptr || value.IsNil() {
				return fmt.Errorf("Invalid ext %v", tags)
			}
			cmdParams = append(cmdParams, &cmdParam{"", tags, value})

		}
	}
	for _, param := range cmdParams {
		if err := tree.addTag(param, req); err != nil {
			glog.Errorln(err)
			return err
		}
	}
	ctx, span := helperES.MakeSpanContext(parentCtx, "DoAgg "+string(tag))
	searchResult, err := search.Size(0).Pretty(true).Do(ctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		helperES.DumpElasticError(err)
		glog.Errorln(err)
		return err
	}
	err = ExtractAgg(searchResult, cmdParams)
	return nil
}
