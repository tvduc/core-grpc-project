package strings

import "sort"

func SearchStrings(a []string, x string) bool {
	if index := sort.SearchStrings(a, x); index < len(a) {
		return a[index] == x
	}
	return false
}

//return intersection of 2 slice of string
func Intersection(a []string, b []string) (inter []string) {
	if len(a) == 0 || len(b) == 0 {
		return
	}
	mapB := map[string]bool{}
	for _, bEle := range b {
		mapB[bEle] = true
	}
	for _, aEle := range a {
		if ok := mapB[aEle]; ok {
			inter = append(inter, aEle)
		}
	}
	return
}
