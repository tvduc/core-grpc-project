package role

var (
	//List all function code

	ROLE_MC_MANAGE_TRANSACTION                        	= "MC_MANAGE_TRANSACTION"                        //1
	ROLE_MC_VIEW_TRANSACTION                          	= "MC_VIEW_TRANSACTION"                          //2
	ROLE_MC_REFUND_TRANSACTION                        	= "MC_REFUND_TRANSACTION"                        //3
	ROLE_MC_EXPORT_EXCEL_TRANSACTION                  	= "MC_EXPORT_EXCEL_TRANSACTION"                  //4
	ROLE_MC_MANAGE_TRANSACTION_BRANCH                 	= "MC_MANAGE_TRANSACTION_BRANCH"                 //5
	ROLE_MC_VIEW_TRANSACTION_BRANCH                   	= "MC_VIEW_TRANSACTION_BRANCH"                   //6
	ROLE_MC_EXPORT_EXCEL_TRANSACTION_BRANCH           	= "MC_EXPORT_EXCEL_TRANSACTION_BRANCH"           //7
	ROLE_MC_MANAGE_TRANSACTION_REFUND                 	= "MC_MANAGE_TRANSACTION_REFUND"                 //8
	ROLE_MC_VIEW_TRANSACTION_REFUND                   	= "MC_VIEW_TRANSACTION_REFUND"                   //9
	ROLE_MC_EXPORT_EXCEL_TRANSACTION_REFUND           	= "MC_EXPORT_EXCEL_TRANSACTION_REFUND"           //10
	ROLE_MC_MANAGE_RECONCILE_BILLING_CYCLE            	= "MC_MANAGE_RECONCILE_BILLING_CYCLE"            //11
	ROLE_MC_VIEW_RECONCILE_BILLING_CYCLE              	= "MC_VIEW_RECONCILE_BILLING_CYCLE"              //12
	ROLE_MC_EXPORT_EXCEL_RECONCILE_BILLING_CYCLE      	= "MC_EXPORT_EXCEL_RECONCILE_BILLING_CYCLE"      //13
	ROLE_MC_MANAGE_RECONCILE_TRANSACTION              	= "MC_MANAGE_RECONCILE_TRANSACTION"              //14
	ROLE_MC_VIEW_RECONCILE_TRANSACTION                	= "MC_VIEW_RECONCILE_TRANSACTION"                //15
	ROLE_MC_EXPORT_EXCEL_RECONCILE_TRANSACTION        	= "MC_EXPORT_EXCEL_RECONCILE_TRANSACTION"        //16
	ROLE_MC_MANAGE_RECONCILE_REFUND_TRANSACTION       	= "MC_MANAGE_RECONCILE_REFUND_TRANSACTION"       //17
	ROLE_MC_VIEW_RECONCILE_REFUND_TRANSACTION         	= "MC_VIEW_RECONCILE_REFUND_TRANSACTION"         //18
	ROLE_MC_EXPORT_EXCEL_RECONCILE_REFUND_TRANSACTION 	= "MC_EXPORT_EXCEL_RECONCILE_REFUND_TRANSACTION" //19
	ROLE_MC_MANAGE_BILLING_CYCLE                      	= "MC_MANAGE_BILLING_CYCLE"                      //20
	ROLE_MC_VIEW_BILLING_CYCLE                        	= "MC_VIEW_BILLING_CYCLE"                        //21
	ROLE_MC_EXPORT_EXCEL_BILLING_CYCLE                	= "MC_EXPORT_EXCEL_BILLING_CYCLE"                //22
	ROLE_MC_MANAGE_MONTHLY_BILLING_FEE                	= "MC_MANAGE_MONTHLY_BILLING_FEE"                //23
	ROLE_MC_VIEW_MONTHLY_BILLING_FEE                  	= "MC_VIEW_MONTHLY_BILLING_FEE"                  //24
	ROLE_MC_CONFIRM_MONTHLY_BILLING_FEE               	= "MC_CONFIRM_MONTHLY_BILLING_FEE"               //500
	ROLE_MC_EXPORT_MONTHLY_BILLING_FEE        			= "MC_EXPORT_MONTHLY_BILLING_FEE"      			//501 // Xuất biên bản đối soát tháng
	ROLE_MC_EXPORT_EXCEL_MONTHLY_BILLING_FEE          	= "MC_EXPORT_EXCEL_MONTHLY_BILLING_FEE"          //25
	ROLE_MC_MANAGE_USER                               	= "MC_MANAGE_USER"                               //26
	ROLE_MC_VIEW_USER                                 	= "MC_VIEW_USER"                                 //27
	ROLE_MC_UPDATE_USER                               	= "MC_UPDATE_USER"                               //28
	ROLE_MC_GRANT_USER_ROLE                           	= "MC_GRANT_USER_ROLE"                           //29
	ROLE_MC_MANAGE_BRANCH                             	= "MC_MANAGE_BRANCH"                             //30
	ROLE_MC_VIEW_BRANCH                               	= "MC_VIEW_BRANCH"                               //31
	ROLE_MC_UPDATE_BRANCH                            	= "MC_UPDATE_BRANCH"                             //32
	ROLE_MC_SHOW_CASHIER_QRCODE                       	= "MC_SHOW_CASHIER_QRCODE"                       //33
	ROLE_MC_MANAGE_ROLE                               	= "MC_MANAGE_ROLE"                               //34
	ROLE_MC_VIEW_ROLE                                 	= "MC_VIEW_ROLE"                                 //35
	ROLE_MC_UPDATE_ROLE                               	= "MC_UPDATE_ROLE"                               //36
	ROLE_MC_GRANT_FUNCTION_ROLE                       	= "MC_GRANT_FUNCTION_ROLE"                       //37
	ROLE_MC_CREATE_BRANCH                             	= "MC_CREATE_BRANCH"                             //38

	MC_WORKFLOW        									= "MC_WORKFLOW"        //100
	MC_VIEW_WORKFLOW   									= "MC_VIEW_WORKFLOW"   //101 //Xem new merchant
	MC_CHANGE_WORKFLOW 									= "MC_CHANGE_WORKFLOW" //102 //Thay đổi new merchant
	MC_CREATE_WORKFLOW 									= "MC_CREATE_WORKFLOW" //103 //Tạo new merchant

	MC_DEPARTMENT 										= "MC_DEPARTMENT" //104 // Phòng ban merchant
	MC_ADMIN      										= "MC_ADMIN"      // 105 //Admin merchant

	ROLE_MC_REFUND_TRANSACTION_IN_BRANCH                = "ROLE_MC_REFUND_TRANSACTION_IN_BRANCH" //110 //Hoàn tiền trang quản lý giao dịch chi nhánh

	ROLE_MC_MANAGE_APP_INTEGRATION 						= "MC_MANAGE_APP_INTEGRATION" //200 //Quản lý app tích hợp
	ROLE_MC_VIEW_APP_INTEGRATION   						= "MC_VIEW_APP_INTEGRATION"   //201 //Xem danh sách app tích hợp
	ROLE_MC_UPDATE_APP_INTEGRATION                      = "MC_UPDATE_APP_INTEGRATION" //202 //Cập nhật thông tin app tích hợp

	ROLE_MC_MANAGE_DASHBOARD							= "MC_MANAGE_DASHBOARD"		  //210 //Quản lý phần tổng quan
	ROLE_MC_VIEW_DASHBOARD								= "MC_VIEW_DASHBOARD"		  //211 //Xem trang tổng quan

	ROLE_MC_MANAGE_ACC_CASHIN 							= "MC_MANAGE_ACC_CASHIN" //220 //Quản lý tài khoản cashin
	ROLE_MC_VIEW_ACC_CASHIN   							= "MC_VIEW_ACC_CASHIN"   //221 //Xem tài khoản cashin

	ROLE_MC_MANAGE_MERCHANT_WALLET_ZALOPAY 				= "MC_MANAGE_MERCHANT_WALLET_ZALOPAY" //505 //Quản lý tài khoản ví Zalopay
	ROLE_MC_VIEW_MERCHANT_WALLET_ZALOPAY   				= "MC_VIEW_MERCHANT_WALLET_ZALOPAY"   //506 //Xem tài khoản ví Zalopay
	ROLE_MC_ADD_MERCHANT_WALLET_ZALOPAY    				= "MC_ADD_MERCHANT_WALLET_ZALOPAY"    //507 //Thêm tài khoản ví Zalopay


	ROLE_MC_MANAGE_COLUMN_CONFIG						= "MC_MANAGE_COLUMN_CONFIG"  //520 //Quản lý hiển thị dữ liệu
	ROLE_MC_VIEW_COLUMN_CONFIG							= "MC_VIEW_COLUMN_CONFIG"    //521 //Xem danh sách hiển thị dữ liệu
	ROLE_MC_UPDATE_COLUMN_CONFIG						= "MC_UPDATE_COLUMN_CONFIG" //522 //Cài đặt hiển thị dữ liệu

	ROLE_MC_MANAGE_TOKEN                                = "MC_MANAGE_TOKEN" //530 //Quản lý token
	ROLE_MC_VIEW_LIST_LOG_TOKEN                         = "MC_VIEW_LIST_LOG_TOKEN" //531 //Xem thông tin liên kết token

	ROLE_MC_MANAGE_DETAIL_BILLING                       = "MC_MANAGE_DETAIL_BILLING" //540 //Quản lý chi tiết thanh toán
	ROLE_MC_VIEW_DETAIL_BILLING                         = "MC_VIEW_DETAIL_BILLING" //541 //Xem danh sách chi tiết thanh toán
	ROLE_MC_EXPORT_EXCEL_DETAIL_BILLING                 = "MC_EXPORT_EXCEL_DETAIL_BILLING" //542 //Xuất excel chi tiết thanh toán

	ROLE_MC_MANAGE_RECONCILE                            = "MC_MANAGE_RECONCILE" //550 //Quản lý đối soát tổng hợp
	ROLE_MC_VIEW_RECONCILE                              = "MC_VIEW_RECONCILE" //551 //Xem danh sách đối soát tổng hợp
	ROLE_MC_EXPORT_EXCEL_RECONCILE                      = "MC_EXPORT_EXCEL_RECONCILE" //552 //Xuất excel đối soát tổng hợp

	ROLE_MC_MANAGE_DETAIL_RECONCILE                     = "MC_MANAGE_DETAIL_RECONCILE" //560 //Quản lý chi tiết đối soát
	ROLE_MC_VIEW_DETAIL_RECONCILE                       = "MC_VIEW_DETAIL_RECONCILE" //561 //Xem danh sách chi tiết đối soát
	ROLE_MC_EXPORT_EXCEL_DETAIL_RECONCILE               = "MC_EXPORT_EXCEL_DETAIL_RECONCILE" //562 //Xuất excel chi tiết đối soát

	ROLE_MC_MANAGE_PRODUCT_ZALO_OA                      = "MC_MANAGE_PRODUCT_ZALO_OA" //570 //Quản lý sản phẩm Zalo OA
	ROLE_MC_VIEW_PRODUCT_ZALO_OA                        = "MC_VIEW_PRODUCT_ZALO_OA" //571 //Xem danh sách sản phẩm Zalo OA
	ROLE_MC_UPDATE_PRODUCT_ZALO_OA                      = "MC_UPDATE_PRODUCT_ZALO_OA" //572 //Tạo mới, cập nhật sản phẩm Zalo OA

	MC_MANAGE_MERCHANT_LIABILITY                    	= "MC_MANAGE_MERCHANT_LIABILITY" //580 //Quản lý thông tin công nợ
	MC_VIEW_MERCHANT_LIABILITY                      	= "MC_VIEW_MERCHANT_LIABILITY" //581 //Xem thông tin công nợ

	ROLE_MC_VIEW_USER_PROMOTION	 						= "MC_VIEW_USER_PROMOTION" //600 //Hiển thị thông tin User Promotion

	/////// Back Office
	AD_APPROVE_MERCHANT  								= "AD_APPROVE_MERCHANT"  //39
	AD_VIEW_WORKFLOW     								= "AD_VIEW_WORKFLOW"     //40 //Xem new merchant
	AD_CHANGE_WORKFLOW   								= "AD_CHANGE_WORKFLOW"   //41 //Thay đổi new merchant
	AD_DELETE_WORKFLOW   								= "AD_DELETE_WORKFLOW"   //42 //Xóa new merchant
	AD_VIEW_ALL_WORKFLOW 								= "AD_VIEW_ALL_WORKFLOW" //43 //Xem danh sách tất cả merchant

	AD_LIST_MERCHANT             						= "AD_LIST_MERCHANT"             //44
	AD_VIEW_LIST_MERCHANT        						= "AD_VIEW_LIST_MERCHANT"        //45 //Xem danh sách merchant
	AD_EXPORT_LIST_MERCHANT      						= "AD_EXPORT_LIST_MERCHANT"      //46 //Xuất excel danh sách merchant
	AD_CREATE_MERCHANT_ID        						= "AD_CREATE_MERCHANT_ID"        //47 //Tạo Merchant ID (trong mục danh sách merchant)
	AD_GRANT_MERCHANT_SP         						= "AD_GRANT_MERCHANT_SP"         //48 //Gán merchant vào SP (trong mục danh sách merchant)
	AD_APPROVE_GRANT_MERCHANT_SP 						= "AD_APPROVE_GRANT_MERCHANT_SP" //49 //Duyệt gán merchant vào SP (trong mục danh sách merchant)

	AD_LIST_APP      									= "AD_LIST_APP"      //50
	AD_VIEW_LIST_APP 									= "AD_VIEW_LIST_APP" //51 //Xem danh sách app
	AD_CREATE_APP    									= "AD_CREATE_APP"    //52 //Cập nhật ngày lauching, ngày set phí

	AD_MANAGE_MERCHANT_ACCOUNT    						= "AD_MANAGE_MERCHANT_ACCOUNT"    //53
	AD_VIEW_LIST_MERCHANT_ACCOUNT 						= "AD_VIEW_LIST_MERCHANT_ACCOUNT" //54 //Xem danh sách tài khoản merchant
	AD_EXPORT_MERCHANT_ACCOUNT    						= "AD_EXPORT_MERCHANT_ACCOUNT"    //55 //Xuất excel danh sách tài khoản merchant

	AD_LIST_SP           								= "AD_LIST_SP"           //56
	AD_VIEW_LIST_SP      								= "AD_VIEW_LIST_SP"      //57 //Xem danh sách SP
	AD_EXPORT_LIST_SP    								= "AD_EXPORT_LIST_SP"    //58 //Xuất excel danh sách SP
	AD_UPDATE_SP         								= "AD_UPDATE_SP"         //59 //Tạo mới/Cập nhật SP
	AD_APPROVE_SP        								= "AD_APPROVE_SP"        //60 //Duyệt SP
	AD_GRANT_SP_MC       								= "AD_GRANT_SP_MC"       //61 //Gán MC cho SP (trong mục danh sách SP)
	AD_RESET_PASSWORD_SP 								= "AD_RESET_PASSWORD_SP" //62 //Reset mật khẩu SP

	AD_LIST_SALE        								= "AD_LIST_SALE"        //63
	AD_GET_LIST_SALE    								= "AD_GET_LIST_SALE"    //64 //Xem danh sách sale
	AD_EXPORT_LIST_SALE 								= "AD_EXPORT_LIST_SALE" //65 //Xuất excel danh sách sale

	AD_STATISTIC         								= "AD_STATISTIC"         //66
	AD_MONTHLY_STATISTIC 								= "AD_MONTHLY_STATISTIC" //67 //Thống kê theo tháng
	AD_TRANSACTION_LOG   								= "AD_TRANSACTION_LOG"   //68 //Nhật ký giao dịch

	AD_MANAGE_RECONCILE 								= "AD_MANAGE_RECONCILE" //69
	//AD_RECONCILE          = "AD_RECONCILE"          //70 //Đối soát
	AD_INTERNAL_RECONCILE 								= "AD_INTERNAL_RECONCILE" //71 //Đối soát nội bộ
	AD_DETAIL_RECONCILE   								= "AD_DETAIL_RECONCILE"   //72 //Chi tiết đối soát

	AD_BILLING          								= "AD_BILLING"          //73
	AD_PERIODIC_BILLING 								= "AD_PERIODIC_BILLING" //74 //Xem thanh toán định kỳ
	AD_BILLING_FEE      								= "AD_BILLING_FEE"      //75 //Phí giao dịch

	AD_REPORT                   						= "AD_REPORT"                   //76
	AD_MONTHLY_MERCHANT_REPORT  						= "AD_MONTHLY_MERCHANT_REPORT"  //77 //Báo cáo tổng hợp merchant
	AD_MONTHLY_ONLINE_RECONCILE							= "AD_MONTHLY_ONLINE_RECONCILE" //78 //Biên bản đối soát online

	AD_MANAGE_BILLING 									= "AD_MANAGE_BILLING" //79
	AD_BILLING_CYCLE  									= "AD_BILLING_CYCLE"  //80 //Chu kỳ thanh toán
	//AD_MANAGE_BILLING_FEE = "AD_MANAGE_BILLING_FEE" //81 //Quản lý phí
	AD_MANAGE_JOB 										= "AD_MANAGE_JOB" //82 //Quản lý job

	AD_MANAGE_ADMIN     								= "AD_MANAGE_ADMIN"     //83
	AD_VIEW_LIST_ADMIN  								= "AD_VIEW_LIST_ADMIN"  //84 //Xem danh sách BO
	AD_GRANT_ADMIN_ROLE 								= "AD_GRANT_ADMIN_ROLE" //85 //Gán quyền BO
	AD_UPDATE_ADMIN     								= "AD_UPDATE_ADMIN"     //86 //Tạo mới/Cập nhật/Trạng thái BO

	AD_ROLE                								= "AD_ROLE"                //87
	AD_VIEW_LIST_ROLE      								= "AD_VIEW_LIST_ROLE"      //88 //Xem danh sách nhóm quyền
	AD_UPDATE_ROLE         								= "AD_UPDATE_ROLE"         //89 //Tạo mới/cập nhật/trạng thái nhóm quyền
	AD_GRANT_ROLE_FUNCTION 								= "AD_GRANT_ROLE_FUNCTION" //90 //Gán chức năng vào nhóm quyền

	AD_CREATE_MERCHANT_ACCOUNT 							= "AD_CREATE_MERCHANT_ACCOUNT" //91 //Tạo tài khoản merchant

	AD_DEPARTMENT       								= "AD_DEPARTMENT"       //92 //Phòng ban
	AD_DEPARTMENT_ADMIN 								= "AD_DEPARTMENT_ADMIN" //93 // BO Admin
	AD_DEPARTMENT_BD    								= "AD_DEPARTMENT_BD"    //94 // BO BD
	AD_DEPARTMENT_OP    								= "AD_DEPARTMENT_OP"    //95 // BO OP
	AD_DEPARTMENT_TS    								= "AD_DEPARTMENT_TS"    //96 // BO Thanh Sơn
	AD_DEPARTMENT_FA    								= "AD_DEPARTMENT_FA"    //97 // BO FA
	AD_DEPARTMENT_LEGAL 								= "AD_DEPARTMENT_LEGAL" //98 // BO Legal
	AD_DEPARTMENT_AM                                	= "AD_DEPARTMENT_AM" //99 // BO AM

	// quản lý app phí
	//AD_MANAGE_APP_FEE    = "AD_MANAGE_APP_FEE"    //600
	AD_VIEW_LIST_APP_FEE 								= "AD_VIEW_LIST_APP_FEE" //601 //Xem danh sách phí
	AD_UPDATE_APP_FEE    								= "AD_UPDATE_APP_FEE"    //602 //Tạo mới, cập nhật, gởi duyệt phí
	AD_CONFIRM_APP_FEE   								= "AD_CONFIRM_APP_FEE"   //603 //Duyệt phí

	// quản lý refund
	AD_MANAGE_REFUND    								= "AD_MANAGE_REFUND"    //605
	AD_VIEW_LIST_REFUND 								= "AD_VIEW_LIST_REFUND" //606 //Xem danh sách refund
	AD_REFUND           								= "AD_REFUND"           //607 //Refund
	AD_CANCEL_REFUND    								= "AD_CANCEL_REFUND"    //608 //Hủy refund

	// Thêm quyền xác nhận thanh toán định kỳ
	AD_CONFIRM_PERIODIC_BILLING_DATA 					= "AD_CONFIRM_PERIODIC_BILLING_DATA" //610 //xác nhận dữ liệu thanh toán định kỳ
	AD_CONFIRM_PERIODIC_BILLING_MC   					= "AD_CONFIRM_PERIODIC_BILLING_MC"   //611 //xác nhận merchant đã thanh toán
	AD_RESET_PERIODIC_BILLING       					= "AD_RESET_PERIODIC_BILLING"        //612 //Đặt lại thanh toán định kỳ
	AD_RESET_BILLING_FEE             					= "AD_RESET_BILLING_FEE"             //613 //Đặt lại phí thanh toán
	AD_BILLING_MAIL_MANUAL           					= "AD_BILLING_MAIL_MANUAL"           //614; //Gởi mail tạm ứng thanh toán

	AD_VIEW_LIST_RECONCILE 								= "AD_VIEW_LIST_RECONCILE" //615 //Xem danh sách đối soát
	AD_CONFIRM_RECONCILE   								= "AD_CONFIRM_RECONCILE"   //616 //Xác nhận đối soát
	AD_CANCEL_RECONCILE    								= "AD_CANCEL_RECONCILE"    //617 //Hủy xác nhận đối soát
	AD_VIEW_LIST_LOG_TOKEN                              = "AD_VIEW_LIST_LOG_TOKEN" //618 //Xem thông tin liên kết token

	AD_MANAGE_MIGRATE_MERCHANT    						= "AD_MANAGE_MIGRATE_MERCHANT"    //620 //Quản lý migrate merchant v1
	AD_VIEW_LIST_MIGRATE_MERCHANT 						= "AD_VIEW_LIST_MIGRATE_MERCHANT" //621 //Xem danh sách merchant v1
	AD_MIGRATE_MERCHANT           						= "AD_MIGRATE_MERCHANT"           //622 //Thực hiện migrate merchant v1

	AD_REFUND_LOG 										= "AD_REFUND_LOG" //626 //Nhật ký giao dịch hoàn tiền
	AD_FRONT_END_LOG                                	= "AD_FRONT_END_LOG" //627 // Dữ liệu log front end
	AD_TOP_TRANSACTION_MERCHANT                     	= "AD_TOP_TRANSACTION_MERCHANT" //628 // Doanh thu theo merchant
	AD_TOP_TRANSACTION_PROMOTION_MERCHANT           	= "AD_TOP_TRANSACTION_PROMOTION_MERCHANT" //629 // Doanh thu Promotion theo merchant

	AD_UPDATE_MERCHANT 									= "AD_UPDATE_MERCHANT" //630 //Cập nhật thông tin merchant

	AD_MANAGE_MERCHANT_WALLET_ZALOPAY 					= "AD_MANAGE_MERCHANT_WALLET_ZALOPAY" //635 // Quản lý tài khoản ví
	AD_VIEW_MERCHANT_WALLET_ZALOPAY   					= "AD_VIEW_MERCHANT_WALLET_ZALOPAY"   //636 // Xem tài khoản ví
	AD_ADD_MERCHANT_WALLET_ZALOPAY    					= "AD_ADD_MERCHANT_WALLET_ZALOPAY"    //637 // Thêm tài khoản ví
	AD_DELETE_MERCHANT_WALLET_ZALOPAY 					= "AD_DELETE_MERCHANT_WALLET_ZALOPAY" //638 // Xóa tài khoản ví
	AD_UPDATE_MERCHANT_WALLET_ZALOPAY 					= "AD_UPDATE_MERCHANT_WALLET_ZALOPAY" //639 // Sửa tài khoản ví

	AD_MANAGE_DEV_TOOL 									= "AD_MANAGE_DEV_TOOL" //640 // Quản lý devtool
	AD_VIEW_TRACE_LOG  									= "AD_VIEW_TRACE_LOG"  //641 // Trace log

	AD_MANAGE_OPERATION_LOG 							= "AD_MANAGE_OPERATION_LOG" //645 // Quản lý lịch sử thao tác
	AD_VIEW_OPERATION_LOG   							= "AD_VIEW_OPERATION_LOG"   //646 // Xem lịch sử thao tác

	AD_MANAGE_RECONCILE_CASHIN      					= "AD_MANAGE_RECONCILE_CASHIN"      //650 //Quản lý đối soát cashin
	AD_VIEW_RECONCILE_CASHIN        					= "AD_VIEW_RECONCILE_CASHIN"        //651 //Xem đối soát cashin
	AD_VIEW_RECONCILE_CASHIN_DETAIL 					= "AD_VIEW_RECONCILE_CASHIN_DETAIL" //652 //Xem chi tiết giao dịch cashin
	AD_DELETE_CASHIN                					= "AD_DELETE_CASHIN"                //653 //Xóa cashin
	AD_UPDATE_CASHIN                					= "AD_UPDATE_CASHIN"                //654 //Sửa cashin
	AD_ADD_CASHIN                   					= "AD_ADD_CASHIN"                   //655 //Thêm cashin
	AD_JOB_CASHIN                   					= "AD_JOB_CASHIN"                   //656 //Quản lý job cashin
	AD_VIEW_CASHIN                  					= "AD_VIEW_CASHIN"                  //657 //Xem cashin

	AD_MANAGE_ACC_CASHIN 								= "AD_MANAGE_ACC_CASHIN" //660 //Quản lý tài khoản cashin
	AD_VIEW_ACC_CASHIN   								= "AD_VIEW_ACC_CASHIN"   //661 //Xem tài khoản cashin
	AD_MAP_ACC_CASHIN_ZP 								= "AD_MAP_ACC_CASHIN_ZP" //662 //Map ví với tài khoản cashin
	AD_DELETE_ACC_CASHIN 								= "AD_DELETE_ACC_CASHIN" //663 //Xóa tài khoản cashin
	AD_UPDATE_ACC_CASHIN 								= "AD_UPDATE_ACC_CASHIN" //664 //Sửa tài khoản cashin
	AD_ADD_ACC_CASHIN    								= "AD_ADD_ACC_CASHIN"    //665 //Thêm tài khoản cashin

	AD_MANAGE_PAYMENT_INFO 								= "AD_MANAGE_PAYMENT_INFO" //670; //Quản lý thông tin thanh toán
	AD_VIEW_PAYMENT_INFO   								= "AD_VIEW_PAYMENT_INFO"   //671 //Xem thông tin thanh toán
	AD_UPDATE_PAYMENT_INFO 								= "AD_UPDATE_PAYMENT_INFO" //672  //Thêm, cập nhật thông tin thanh toán

	AD_CASHIN_TRANSACTION_LOG 							= "AD_CASHIN_TRANSACTION_LOG" //675 //Xem nhật ký giao dịch cashin

	AD_UPDATE_APP_INTEGRATION       					= "AD_UPDATE_APP_INTEGRATION"       //680 //Tạo, cập nhật app tích hợp
	AD_GRANT_APP_ZP_MERCHANT        					= "AD_GRANT_APP_ZP_MERCHANT"        //681 //Gán app ZaloPay vào Merchant
	AD_VIEW_APP_PAYMENT_METHOD                      	= "AD_VIEW_APP_PAYMENT_METHOD" //682 //Xem cấu hình phương thức thanh toán app
	AD_CONFIG_APP_PAYMENT_METHOD                    	= "AD_CONFIG_APP_PAYMENT_METHOD" //683 //Cấu hình phương thức thanh toán app
	AD_EXPORT_FILE_INTERNET_BANKING 					= "AD_EXPORT_FILE_INTERNET_BANKING" //685 //"Xuất file internet banking"

	AD_MANAGE_MERCHANT_BRANCH    						= "AD_MANAGE_MERCHANT_BRANCH"    //690 //Quản lý chi nhánh, cửa hàng, quầy của merchant
	AD_VIEW_MERCHANT_BRANCH      						= "AD_VIEW_MERCHANT_BRANCH"      //691 //Xem chi nhánh, cửa hàng, quầy
	AD_CREATE_MERCHANT_BRANCH    						= "AD_CREATE_MERCHANT_BRANCH"    //692 //Tạo chi nhánh, cửa hàng, quầy
	AD_UPDATE_MERCHANT_BRANCH    						= "AD_UPDATE_MERCHANT_BRANCH"    //693 //Cập nhật chi nhánh, cửa hàng, quầy
	AD_DELETE_MERCHANT_BRANCH    						= "AD_DELETE_MERCHANT_BRANCH"    //694 //Xoá chi nhánh, cửa hàng, quầy
	AD_GRANT_PAYMENT_FOR_CASHIER 						= "AD_GRANT_PAYMENT_FOR_CASHIER" //695 //Gán payment cho cashier

	AD_MANAGE_MERCHANT_CONTACT_INFO 					= "AD_MANAGE_MERCHANT_CONTACT_INFO" //700 //Quản lý thông tin liên hệ của merchant
	AD_VIEW_MERCHANT_CONTACT_INFO   					= "AD_VIEW_MERCHANT_CONTACT_INFO"   //701 //Xem danh sách thông tin liên hệ của merchant
	AD_CREATE_MERCHANT_CONTACT_INFO 					= "AD_CREATE_MERCHANT_CONTACT_INFO" //702 //Tạo thông tin liên hệ của merchant
	AD_UPDATE_MERCHANT_CONTACT_INFO 					= "AD_UPDATE_MERCHANT_CONTACT_INFO" //703 //Cập nhật thông tin liên hệ của merchant

	AD_MANAGE_PRODUCT  									= "AD_MANAGE_PRODUCT"  // 710 //Quản lý thông tin mã sản phẩm
	AD_VIEW_PRODUCT                                 	= "AD_VIEW_PRODUCT" // 711 //Xem danh sách thông tin mã sản phẩm
	AD_UPDATE_PRODUCT                               	= "AD_UPDATE_PRODUCT" //712 //Thêm, xóa, cập nhật thông tin mã sản phẩm

	AD_MANAGE_SOURCE_BANK                           	= "AD_MANAGE_SOURCE_BANK" //720 //Quản lý thông tin nguồn thanh toán
	AD_VIEW_SOURCE_BANK                             	= "AD_VIEW_SOURCE_BANK" //721 //Xem danh sách nguồn thanh toán
	AD_UPDATE_SOURCE_BANK                           	= "AD_UPDATE_SOURCE_BANK" //722 //Thêm, cập nhật nguồn thanh toán
	AD_GRANT_SOURCE_BANK                            	= "AD_GRANT_SOURCE_BANK" // 723 //Gán nguồn thanh toán

	AD_MANAGE_FA_INFO                               	= "AD_MANAGE_FA_INFO" //730 //Quản lý mã FA
	AD_VIEW_FA_INFO                                 	= "AD_VIEW_FA_INFO" //731 //Xem danh sách mã FA
	AD_CREATE_FA_INFO                               	= "AD_CREATE_FA_INFO" //732 //Tạo mã FA
	AD_UPDATE_FA_INFO                               	= "AD_UPDATE_FA_INFO" //733 //Cập nhật mã FA

	AD_MANAGE_PROMOTION_INFO                        	= "AD_MANAGE_PROMOTION_INFO" //740 //Quản lý promotion
	AD_VIEW_PROMOTION_LOG                           	= "AD_VIEW_PROMOTION_LOG" //741 //Xem promotion log
	AD_VIEW_PROMOTION_CONFIG                        	= "AD_VIEW_PROMOTION_CONFIG" //742 //Xem cấu hình promotion
	AD_UPDATE_PROMOTION_CONFIG                      	= "AD_UPDATE_PROMOTION_CONFIG" //743 //Cập nhật cấu hình promotion

	AD_MANAGE_APPROVE_PMR                           	= "AD_MANAGE_APPROVE_PMR" //750 //Quản lý duyệt PMR
	AD_VIEW_PMR_WORKFLOW                                = "AD_VIEW_PMR" //751 //Xem danh sách PMR
	AD_CHANGE_PMR_WORKFLOW                            	= "AD_CHANGE_STATUS_PMR" //752 //Duyệt, từ chối, hủy PMR
	AD_VIEW_HISTORY_PMR                             	= "AD_VIEW_HISTORY_PMR" //753 //Xem lịch sử thanh toán PMR
	AD_EXPORT_PAYOUT_ZAS                            	= "AD_EXPORT_PAYOUT_ZAS" //754 //Xuất file Payout ZAS

	AD_MANAGE_DEPARTMENT_PMR                        	= "AD_MANAGE_DEPARTMENT_PMR" //760 //Quản lý phòng ban PMR
	AD_DEPARTMENT_PMROP                            		= "AD_DEPARTMENT_OP_PMR" //761 //Operation (OP)
	AD_DEPARTMENT_PMROM                            		= "AD_DEPARTMENT_OM_PMR" //762 //Operation Manager (OM)
	AD_DEPARTMENT_PMRAR                            		= "AD_DEPARTMENT_AR_PMR" //763 //Account Receivable (AR)
	AD_DEPARTMENT_PMRAP                            		= "AD_DEPARTMENT_AP_PMR" //764 //Account Payable (AP)
	AD_DEPARTMENT_PMRCM                            		= "AD_DEPARTMENT_CM_PMR" //765 //Cash Management (CM)
	AD_DEPARTMENT_PMRGA                            		= "AD_DEPARTMENT_GA_PMR" //766 //General Accountant (GA)
	AD_DEPARTMENT_PMRAM                            		= "AD_DEPARTMENT_AM_PMR" //767 //Account Manager (AM)

	AD_MANAGE_MERCHANT_CONFIG                       	= "AD_MANAGE_MERCHANT_CONFIG" //770 //Cài đặt merchant
	AD_VIEW_AGREEMENT                               	= "AD_VIEW_AGREEMENT" //771 //Xem danh sách agreement
	AD_UPDATE_AGREEMENT                             	= "AD_UPDATE_AGREEMENT" //772 //Tạo mới, cập nhật, xóa, gửi duyệt agreement
	AD_CONFIRM_AGREEMENT                            	= "AD_CONFIRM_AGREEMENT" //773 //Duyệt thông tin agreement
	AD_VIEW_ACQUIRING                               	= "AD_VIEW_ACQUIRING" //774 //Xem cài đặt thông tin chung
	AD_UPDATE_ACQUIRING                             	= "AD_UPDATE_ACQUIRING" //775 //Cập nhật cài đặt thông tin chung
	AD_VIEW_BILLING_CYCLE                           	= "AD_VIEW_BILLING_CYCLE" //776 //Xem cài đặt chu kỳ thanh toán
	AD_UPDATE_BILLING_CYCLE                         	= "AD_UPDATE_BILLING_CYCLE" //777 //Cập nhật chu kỳ thanh toán
	AD_VIEW_MERCHANT_FEE                            	= "AD_VIEW_MERCHANT_FEE" //778 //Xem cài đặt phí merchant
	AD_UPDATE_MERCHANT_FEE                          	= "AD_UPDATE_MERCHANT_FEE" //779 //Cập nhật phí merchant

	AD_MANAGE_RECONCILE_V3                          	= "AD_MANAGE_RECONCILE_V3" //780 //Quản lý đối soát tổng hợp V3
	AD_VIEW_RECONCILE_V3                            	= "AD_VIEW_RECONCILE_V3" //781 //Xem danh sách đối soát tổng hợp
	AD_EXPORT_EXCEL_RECONCILE_V3                    	= "AD_EXPORT_EXCEL_RECONCILE_V3" //782 //Xuất excel đối soát tổng hợp

	AD_MANAGE_DETAIL_RECONCILE_V3                   	= "AD_MANAGE_DETAIL_RECONCILE_V3" //790 //Quản lý chi tiết đối soát V3
	AD_VIEW_DETAIL_RECONCILE_V3                     	= "AD_VIEW_DETAIL_RECONCILE_V3" //791 //Xem danh sách chi tiết đối soát
	AD_EXPORT_EXCEL_DETAIL_RECONCILE_V3             	= "AD_EXPORT_EXCEL_DETAIL_RECONCILE_V3" //792 //Xuất excel chi tiết đối soát

	AD_MANAGE_BILLING_DAILY_V3                      	= "AD_MANAGE_BILLING_DAILY_V3" //800 //Quản lý thanh toán daily V3
	AD_VIEW_BILLING_DAILY_V3                        	= "AD_VIEW_BILLING_DAILY_V3" //801 //Xem danh sách thanh toán daily
	AD_EXPORT_EXCEL_BILLING_DAILY_V3                	= "AD_EXPORT_EXCEL_BILLING_DAILY_V3" //802 //Xuất excel thanh toán daily
	AD_CONFIRM_CANCEL_BILLING_DAILY_V3              	= "AD_CONFIRM_CANCEL_BILLING_DAILY_V3" //803 //Xác nhận, hủy thanh toán daily

	AD_MANAGE_DETAIL_BILLING_V3                     	= "AD_MANAGE_DETAIL_BILLING_V3" //810 //Quản lý chi tiết thanh toán V3
	AD_VIEW_DETAIL_BILLING_V3                       	= "AD_VIEW_DETAIL_BILLING_V3" //811 //Xem danh sách chi tiết thanh toán
	AD_EXPORT_EXCEL_DETAIL_BILLING_V3               	= "AD_EXPORT_EXCEL_DETAIL_BILLING_V3" //812 //Xuất excel chi tiết thanh toán

	AD_MANAGE_PERIODIC_BILLING_V3                   	= "AD_MANAGE_PERIODIC_BILLING_V3" //820 //Quản lý thanh toán định kỳ V3
	AD_VIEW_PERIODIC_BILLING_V3                     	= "AD_VIEW_PERIODIC_BILLING_V3" //821 //Xem danh sách thanh toán định kỳ
	AD_EXPORT_EXCEL_PERIODIC_BILLING_V3             	= "AD_EXPORT_EXCEL_PERIODIC_BILLING_V3" //822 //Xuất excel thanh toán định kỳ
	AD_CONFIRM_PERIODIC_BILLING_V3                  	= "AD_CONFIRM_PERIODIC_BILLING_V3" //823 //Xác nhận thanh toán định kỳ
	AD_EXPORT_INTERNET_BANKING_PERIODIC_BILLING_V3  	= "AD_EXPORT_INTERNET_BANKING_PERIODIC_BILLING_V3" //824 //Xuất file internet banking

	AD_MANAGE_DOCUMENT_TOOL                         	= "AD_MANAGE_DOCUMENT_TOOL"// 830 //Quản lý tài liệu
	AD_VIEW_DOCUMENT_TOOL                           	= "AD_VIEW_DOCUMENT_TOOL" //831 //Xem danh sách tài liệu
	AD_UPDATE_DOCUMENT_TOOL                         	= "AD_UPDATE_DOCUMENT_TOOL" //832 //Cập nhật tài liệu
	AD_FA_CONFIRM_DOCUMENT_TOOL                     	= "AD_FA_CONFIRM_DOCUMENT_TOOL" //833 //FA duyệt, từ chối tài liệu
	AD_FA_LEADER_CONFIRM_DOCUMENT_TOOL              	= "AD_FA_LEADER_CONFIRM_DOCUMENT_TOOL" //834 //FA leader duyệt, từ chối tài liệu
	AD_LEGAL_CONFIRM_DOCUMENT_TOOL                  	= "AD_LEGAL_CONFIRM_DOCUMENT_TOOL" //835 //Legal duyệt, từ chối tài liệu

	AD_MANAGE_SETUP_MERCHANT                        	= "AD_MANAGE_SETUP_MERCHANT" //840 //Quản lý cài đặt merchant
	AD_VIEW_SETUP_MERCHANT                          	= "AD_VIEW_SETUP_MERCHANT" //841 //Xem danh sách cài đặt merchant
	AD_UPDATE_SETUP_MERCHANT                        	= "AD_UPDATE_SETUP_MERCHANT" //842 //Tạo, cập nhật, xóa cài đặt merchant

	AD_MANAGE_AGREEMENT_CENTER                      	= "AD_MANAGE_AGREEMENT_CENTER" //850 //Quản lý agreement center
	AD_VIEW_AGREEMENT_CENTER                        	= "AD_VIEW_AGREEMENT_CENTER" //851 //Xem danh sách agreement center
	AD_UPDATE_AGREEMENT_CENTER                      	= "AD_UPDATE_AGREEMENT_CENTER" //852 //Tạo mới,cập nhật agreement center
	AD_SEND_CONFIRM_AGREEMENT_CENTER                    = "AD_SEND_CONFIRM_AGREEMENT_CENTER" //853 //Gửi duyệt agreement center
	AD_UPDATE_STATUS_AGREEMENT_CENTER					= "AD_UPDATE_STATUS_AGREEMENT_CENTER" //854 //Cập nhật trạng thái agreement center

	AD_MANAGE_ZALO_OA                               	= "AD_MANAGE_ZALO_OA" //860 //Quản lý Zalo OA
	AD_VIEW_ZALO_OA                                 	= "AD_VIEW_ZALO_OA" //861 //Xem danh sách Zalo OA
	AD_UPDATE_ZALO_OA                               	= "AD_UPDATE_ZALO_OA" //862 //Tạo mới, cập nhật Zalo OA

	AD_DEPARTMENT_RISK                              	= "AD_DEPARTMENT_RISK" //870 //RISK
	AD_DEPARTMENT_COMPLIANCE                        	= "AD_DEPARTMENT_COMPLIANCE" //871 //COMPLIANCE

	AD_GRANT_MCC_TO_APP                             	= "AD_GRANT_MCC_TO_APP" //880 //Gán mcc code cho app
	AD_CONFIRM_MCC_TO_APP                           	= "AD_CONFIRM_MCC_TO_APP" //881 //Xác nhận gán mcc code cho app

	AD_MANAGE_MERCHANT_LIABILITY                    	= "AD_MANAGE_MERCHANT_LIABILITY" //890 //Quản lý thông tin công nợ
	AD_VIEW_MERCHANT_LIABILITY                      	= "AD_VIEW_MERCHANT_LIABILITY" //891 //Xem thông tin công nợ
	AD_CLEAR_MERCHANT_LIABILITY                     	= "AD_CLEAR_MERCHANT_LIABILITY" //892 //Xóa thông tin công nợ
	AD_VIEW_UN_SETTLE_REPORT                        	= "AD_VIEW_UN_SETTLE_REPORT" //893 //Xem merchant chưa thanh toán
	AD_VIEW_UN_SETTLE_TRANSACTION                   	= "AD_VIEW_UN_SETTLE_TRANSACTION" //894 //Xem chi tiết giao dịch chưa thanh toán
)