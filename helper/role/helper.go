package role

import (
	usersession "git.zapa.cloud/merchant-tools/protobuf/user-session"
	"github.com/qor/roles"
)

var (
	AnyonePermission = roles.
		Allow(roles.CRUD, roles.Anyone)

	MerchantAdminPermission = roles.
		Allow(roles.CRUD, usersession.UserType_MERCHANT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPPORT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPER_ADMIN.String())
	MerchantUserPermission = roles.
		Allow(roles.CRUD, usersession.UserType_MERCHANT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPPORT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPER_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_OPERATION.String())
	BackOfficeAdminPermission = roles.
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_USER.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_BD.String())
	BackOfficeOrMerchantAdminPermission = roles.
		Allow(roles.CRUD, usersession.UserType_MERCHANT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPPORT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPER_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_USER.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_BD.String())

	SalePermission = roles.
		Allow(roles.CRUD, usersession.UserType_SP_SALE.String())
	ServiceProviderPermission = roles.
		Allow(roles.CRUD, usersession.UserType_SP_ADMIN.String())
	ServiceProviderOrSalePermission = roles.
		Allow(roles.CRUD, usersession.UserType_SP_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_SP_SALE.String())
	SpReportPermission = roles.
		Allow(roles.CRUD, usersession.UserType_MERCHANT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_SP_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_SP_SALE.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_BD.String())
	ViewTransLogPermission = roles.
		Allow(roles.CRUD, usersession.UserType_MERCHANT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_OPERATION.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPPORT_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_MERCHANT_SUPER_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_SP_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_SP_SALE.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_ADMIN.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_OP.String()).
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_BD.String())
	BackOfficeBDPermission = roles.
		Allow(roles.CRUD, usersession.UserType_BACKOFFICE_BD.String())
)
