package crypto

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"
)

//Sha256Sum ...
func Sha256Sum(sep string, params ...string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(strings.Join(params, sep))))
}

//HmacSha256Sum ...
func HmacSha256Sum(key, sep string, params ...string) string {
	sig := hmac.New(sha256.New, []byte(key))
	sig.Write([]byte(strings.Join(params, sep)))
	return fmt.Sprintf("%x", sig.Sum(nil))
}

func Sha256(message string) string {
	s := sha256.New()
	s.Write([]byte(message))
	return hex.EncodeToString(s.Sum(nil))
}

func Md5(message string) string {
	s := md5.New()
	s.Write([]byte(message))
	return fmt.Sprintf("%x", s.Sum(nil))
}
