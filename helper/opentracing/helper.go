package opentracing

import (
	"context"
	"database/sql"
	"io"

	"git.zapa.cloud/merchant-tools/helper/env"

	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	jaeger "github.com/uber/jaeger-client-go"
	opConfig "github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics/prometheus"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/metadata"
)

// metadataTextMap extends a metadata.MD to be an opentracing textmap
type metadataTextMap metadata.MD

//Init ...
func Init(addr, serviceName string) error {
	opCfg := opConfig.Configuration{
		Reporter: &opConfig.ReporterConfig{
			LocalAgentHostPort: addr,
		},
	}
	var err error
	if env.IsProd() {
		sampler, _ := jaeger.NewProbabilisticSampler(0.5)
		_, err = opCfg.InitGlobalTracer(
			serviceName,
			opConfig.Logger(jaeger.StdLogger),
			opConfig.Sampler(sampler),
			opConfig.Metrics(prometheus.New()),
		)
	} else {
		_, err = opCfg.InitGlobalTracer(
			serviceName,
			opConfig.Logger(jaeger.StdLogger),
			opConfig.Sampler(jaeger.NewConstSampler(true)),
			opConfig.Metrics(prometheus.New()),
		)
	}

	if err != nil {
		glog.Errorln(err)

	}
	//defer closer.Close()
	return err
}

type clientSpanTagKey struct{}

//NewClientSpanFromContext ...
func NewClientSpanFromContext(ctx context.Context, tag opentracing.Tag, fullMethodName string) (context.Context, opentracing.Span) {
	tracer := opentracing.GlobalTracer()
	var parentSpanCtx opentracing.SpanContext
	if parent := opentracing.SpanFromContext(ctx); parent != nil {
		parentSpanCtx = parent.Context()
	}
	opts := []opentracing.StartSpanOption{
		opentracing.ChildOf(parentSpanCtx),
		ext.SpanKindRPCClient,
		tag,
	}
	if tagx := ctx.Value(clientSpanTagKey{}); tagx != nil {
		if opt, ok := tagx.(opentracing.StartSpanOption); ok {
			opts = append(opts, opt)
		}
	}
	clientSpan := tracer.StartSpan(fullMethodName, opts...)
	// Make sure we add this to the metadata of the call, so it gets propagated:
	md := metautils.ExtractOutgoing(ctx).Clone()
	if err := tracer.Inject(clientSpan.Context(), opentracing.HTTPHeaders, metadataTextMap(md)); err != nil {
		grpclog.Printf("grpc_opentracing: failed serializing trace information: %v", err)
	}
	ctxWithMetadata := md.ToOutgoing(ctx)
	return opentracing.ContextWithSpan(ctxWithMetadata, clientSpan), clientSpan
}

//FinishClientSpan ...
func FinishClientSpan(clientSpan opentracing.Span, err error) {
	if err != nil && err != io.EOF && err != sql.ErrNoRows && err != redis.Nil {
		ext.Error.Set(clientSpan, true)
		clientSpan.LogFields(log.String("event", "error"), log.String("message", err.Error()))
	}
	clientSpan.Finish()
}
