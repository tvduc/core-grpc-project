package copier

import (
	"errors"
	"reflect"
	"strings"
	"sync"

	"github.com/golang/glog"

	"git.zapa.cloud/merchant-tools/helper/concurrency"
)

type searchField struct {
	indexFrom []int
	indexTo   []int
}

var (
	deepFieldCache = map[string][]*searchField{}
	mux            = &sync.Mutex{}
)

type copier struct {
}

// Copy copy things
func Copy(toValue, fromValue interface{}) (err error) {
	c := &copier{}
	return c.copy(toValue, fromValue)
}
func setValue(from, to reflect.Value) bool {
	fType, tType := from.Type(), to.Type()
	if fType.AssignableTo(tType) {
		//glog.Infoln("Set AssignableTo", to.Type())
		to.Set(from)
		return true
	}
	if fType.ConvertibleTo(tType) {
		//glog.Infoln("Set Convert", to.Type())
		to.Set(from.Convert(tType))
		return true
	}
	return false
}
func (c *copier) copy(toValue, fromValue interface{}) (err error) {
	var (
		isSlice bool
		amount  = 1
		from    = reflect.Indirect(reflect.ValueOf(fromValue))
		to      = reflect.Indirect(reflect.ValueOf(toValue))
	)

	if !to.CanAddr() {
		return errors.New("copy to value is unaddressable")
	}

	// Return is from value is invalid
	if !from.IsValid() {
		return
	}

	// Just set it if possible to assign
	if setValue(from, to) {
		return
	}

	fromType := indirectType(from.Type())
	toType := indirectType(to.Type())

	if fromType.Kind() != reflect.Struct || toType.Kind() != reflect.Struct {
		return
	}

	if to.Kind() == reflect.Slice {
		isSlice = true
		if from.Kind() == reflect.Slice {
			amount = from.Len()
			to.Set(reflect.MakeSlice(to.Type(), amount, amount))
		}
	}
	extractValue := func(f, t int) error {
		var deepFields []*searchField
		for i := f; i < t; i++ {
			var source, dest reflect.Value
			if isSlice {
				// source
				if from.Kind() == reflect.Slice {
					source = reflect.Indirect(from.Index(i))
				} else {
					source = reflect.Indirect(from)
				}
				// dest
				dest = reflect.Indirect(reflect.New(toType).Elem())
			} else {
				source = reflect.Indirect(from)
				dest = reflect.Indirect(to)
			}
			if deepFields == nil {
				deepFields = c.deepFields(fromType, source, dest)
			}

			// Copy from field to field or method
			for _, field := range deepFields {
				if fromField := source.FieldByIndex(field.indexFrom); fromField.IsValid() {
					// has field
					if toField := dest.FieldByIndex(field.indexTo); toField.IsValid() {
						if toField.CanSet() {
							if !set(toField, fromField) {
								if err := c.copy(toField.Addr().Interface(), fromField.Interface()); err != nil {
									return err
								}
							}
						}
					}
				}
			}
			if isSlice {
				if dest.Addr().Type().AssignableTo(to.Type().Elem()) {
					to.Index(i).Set(dest.Addr())
				} else if dest.Type().AssignableTo(to.Type().Elem()) {
					to.Index(i).Set(dest)
				}

			}
		}
		return nil
	}
	if amount < 256 {
		return extractValue(0, amount)
	}
	//return extractValue(0, amount)
	cc := concurrency.New()
	n := 2
	_amount := 1 + (amount / n)
	for c := 0; c < n; c++ {
		f := c * _amount
		t := f + _amount
		if t > amount {
			t = amount
		}
		cc.Add(func() error {
			return extractValue(f, t)
		})
	}
	return cc.Do()
}
func (c *copier) deepFields(reflectType reflect.Type, source, dst reflect.Value) []*searchField {
	//key := fmt.Sprintf("%v:%v", source.Type(), dst.Type())
	key := source.Type().String() + ":" + dst.Type().String()
	mux.Lock()
	defer mux.Unlock()
	fields, ok := deepFieldCache[key]
	if ok {
		return fields
	}
	glog.Infoln("deepFields:", key)
	if reflectType = indirectType(reflectType); reflectType.Kind() == reflect.Struct {
		for i := 0; i < reflectType.NumField(); i++ {
			v := reflectType.Field(i)
			if v.Anonymous {
				fields = append(fields, c.deepFields(v.Type, source, dst)...)
			} else {
				if !strings.HasPrefix(v.Name, "XXX_") {
					if f, ok := dst.Type().FieldByName(v.Name); ok {
						fields = append(fields, &searchField{indexFrom: v.Index, indexTo: f.Index})
					}
				}
			}
		}
	}
	deepFieldCache[key] = fields

	return fields
}

func indirectType(reflectType reflect.Type) reflect.Type {
	for reflectType.Kind() == reflect.Ptr || reflectType.Kind() == reflect.Slice {
		reflectType = reflectType.Elem()
	}
	return reflectType
}
func set(to, from reflect.Value) bool {
	if from.IsValid() {
		if to.Kind() == reflect.Ptr {
			//set `to` to nil if from is nil
			if from.Kind() == reflect.Ptr && from.IsNil() {
				to.Set(reflect.Zero(to.Type()))
				return true
			}
			to.Set(reflect.New(to.Type().Elem()))
			to = to.Elem()
		}
		if setValue(from, to) {

		} else if from.Kind() == reflect.Ptr {
			return set(to, from.Elem())
		} else {
			return false
		}
	}
	return true
}
