package copier

import (
	"github.com/golang/glog"
	"github.com/json-iterator/go"
)

//It's use for improve speed time action copy data (process trial)
var json = jsoniter.ConfigCompatibleWithStandardLibrary

//JSONCopy ...
func JSONCopy(dst, src interface{}) (error) {
	bytes, err := json.Marshal(src)
	if err != nil {
		glog.Info("JSONCopy: err marshal json = ", err)
		return err
	}

	err = json.Unmarshal(bytes, dst)
	if err != nil {
		glog.Info("JSONCopy: err unmarshal json = ", err)
		return err
	}

	return nil
}
