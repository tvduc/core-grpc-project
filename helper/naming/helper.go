package naming

import (
	"context"
	"fmt"
	"time"

	"git.zapa.cloud/merchant-tools/helper/graceful"

	svc "git.zapa.cloud/merchant-tools/helper/consul"
	//svc "git.zapa.cloud/merchant-tools/helper/redis"
	"github.com/golang/glog"
	"google.golang.org/grpc/naming"
)

const (
	defaultFreq = time.Second * 3
)

type resolver struct {
	// frequency of polling the DNS server that the watchers created by this resolver will use.
	freq        time.Duration
	serviceName string
}

// NewResolver return ConsulResolver with service name
func NewResolver(serviceName string) naming.Resolver {
	return &resolver{serviceName: serviceName, freq: defaultFreq}
}
func (r *resolver) Resolve(target string) (naming.Watcher, error) {
	if r == nil || r.serviceName == "" {
		return nil, fmt.Errorf("Invalid resolver %v", r)
	}
	s := svc.NewStore(target, r.serviceName)
	if s == nil {
		return nil, fmt.Errorf("creat consul error: nil")
	}
	ctx, cancel := context.WithCancel(context.Background())
	return &watcher{
		r:      r,
		ctx:    ctx,
		cancel: cancel,
		t:      time.NewTimer(0),
		store:  s,
	}, nil
}
func Register(name string, host string, port int, target string, interval time.Duration, ttl int) error {
	s := svc.NewService(name, host, port, target, ttl)
	if s == nil {
		return fmt.Errorf("Empty consul service")
	}
	if err := s.Register(); err != nil {
		glog.Errorln(err)
		return err
	}
	//de-register if meet signhup
	graceful.Stop(time.Second, func() {
		if err := s.DeRegister(); err != nil {
			glog.Errorln(err)
		}
	})

	// routine to update ttl
	if interval > 0 {
		go func() {
			ticker := time.NewTicker(interval)
			for {
				<-ticker.C
				if err := s.UpdateTTL(); err != nil {
					glog.Errorln(err)
				}
			}
		}()
	}

	return nil
}
