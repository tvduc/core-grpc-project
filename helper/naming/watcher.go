package naming

import (
	"context"
	"errors"
	"time"

	"google.golang.org/grpc/naming"
)

var (
	errWatcherClose = errors.New("watcher has been closed")
)

type storeService interface {
	LookupSRV(string, bool) ([]string, error)
}
type watcher struct {
	r *resolver
	// The latest resolved address set
	curAddrs map[string]*naming.Update
	ctx      context.Context
	cancel   context.CancelFunc
	t        *time.Timer
	store    storeService
}

func (w *watcher) Next() ([]*naming.Update, error) {
	maxDuration, curDuration := time.Minute, w.r.freq
	if len(w.curAddrs) == 0 {
		curDuration = 0
	}
	w.t.Reset(curDuration)
	for {
		select {
		case <-w.ctx.Done():
			return nil, errWatcherClose
		case <-w.t.C:
		}

		result, _ := w.lookup()
		// Next lookup should happen after an interval defined by w.r.freq.
		if len(result) > 0 {
			return result, nil
		}
		//glog.Infoln("Empty")
		if len(w.curAddrs) == 0 {
			curDuration = time.Second
		} else {
			curDuration += w.r.freq
			if curDuration > maxDuration {
				curDuration = maxDuration
			}
		}
		w.t.Reset(curDuration)
	}
}
func (w *watcher) lookupSRV() (map[string]*naming.Update, error) {
	res, err := w.store.LookupSRV(w.r.serviceName, w.curAddrs == nil)
	if err != nil {
		return nil, err
	}
	newAddrs := make(map[string]*naming.Update)
	for _, addr := range res {
		newAddrs[addr] = &naming.Update{Addr: addr}
	}
	return newAddrs, nil

}
func (w *watcher) lookup() ([]*naming.Update, error) {
	newAddrs, err := w.lookupSRV()
	if err != nil {
		return nil, err
	}
	result := w.compileUpdate(newAddrs)
	w.curAddrs = newAddrs
	return result, nil
}

func (w *watcher) Close() {
	w.cancel()
}
func (w *watcher) compileUpdate(newAddrs map[string]*naming.Update) []*naming.Update {
	var res []*naming.Update
	for a, u := range w.curAddrs {
		if _, ok := newAddrs[a]; !ok {
			u.Op = naming.Delete
			res = append(res, u)
		}
	}
	for a, u := range newAddrs {
		if _, ok := w.curAddrs[a]; !ok {
			res = append(res, u)
		}
	}
	return res
}
