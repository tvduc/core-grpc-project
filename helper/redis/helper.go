package redis

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/notify"

	"github.com/golang/glog"

	"github.com/go-redis/redis"
	commonhelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/common"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/core"
)

//NewRedisClient ...
func NewRedisClient(cfg config.Redis) *redis.Client {
	c, e := core.GetCache("NewRedisClient"+cfg.Addr, func() (interface{}, error) {

		return redis.NewClient(&redis.Options{
			Addr:         cfg.Addr,
			Password:     cfg.Password, // no password set
			DB:           cfg.DB,       // use default DB
			ReadTimeout:  time.Minute,
			WriteTimeout: time.Minute,
		}), nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*redis.Client)
}

var (
	redisLimiter      *Limiter = nil
	servicePrefixName          = os.Getenv("SERVICE_PREFIX_NAME")
)

func NewRedisLimiter(cfg config.Redis) {
	if redisLimiter != nil {
		return
	}

	if len(cfg.Addr) == 0 {
		notify.SendToTeamsEx("New Redis Limiter Invalid", fmt.Sprintf("Config :%v", cfg))
		return
	}

	client := NewRedisClient(cfg)
	redisLimiter = NewLimiter(client)
}

func CheckLimitAllowMinute(key string, rateNumber int) (count int64, delay time.Duration, allow bool) {
	if redisLimiter == nil {
		return 0, 0, false
	}

	if rateNumber <= 0 {
		rateNumber = 5
	}

	rate, delay, allow := redisLimiter.AllowMinute(servicePrefixName+key, int64(rateNumber))
	if !allow {
		glog.Errorf("CheckLimitAllowMinute: key = %v, rate = %v, delay = %v, allow = %v\n", key, rate, delay, allow)
	}
	return rate, delay, allow
}

func CheckLimitAllowHour(key string, rateNumber int) (count int64, delay time.Duration, allow bool) {
	if redisLimiter == nil {
		return 0, 0, false
	}

	if rateNumber <= 0 {
		rateNumber = 10
	}

	rate, delay, allow := redisLimiter.AllowHour(servicePrefixName+key, int64(rateNumber))
	if !allow {
		glog.Errorf("CheckLimitAllowHour: key = %v, rate = %v, delay = %v, allow = %v\n", key, rate, delay, allow)
	}
	return rate, delay, allow
}

func ResetRateLimit(key string, dur time.Duration) error {
	if redisLimiter == nil {
		return nil
	}

	return redisLimiter.Reset(servicePrefixName+key, dur)
}

func GenerateIdWithCache(cache *redis.Client) (string, error) {
	currentDate := commonhelper.GetVNCurrentDateStringFormat("060102")
	redisKey := makeGenerateIdKey()
	id, err := cache.HIncrBy(redisKey, currentDate, 1).Result()
	if err != nil {
		glog.Infoln("GenerateIdWithCache: HIncrBy error: ", err)
		return "", err
	}

	idStr := fmt.Sprintf("%s%07d", currentDate, id)
	return idStr, nil
}

func makeGenerateIdKey() string {
	return fmt.Sprintf("%smerchant-tools:genid", servicePrefixName)
}
