package redis

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"

	"github.com/go-redis/redis"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
)

const (
	expireTm = 3 * 10
)

type serviceInfo struct {
	serviceAddr map[string][]string
	mutex       *sync.Mutex
}

var (
	cacheServiceInfo = &serviceInfo{
		serviceAddr: map[string][]string{},
		mutex:       &sync.Mutex{},
	}
	once             sync.Once
	errRedisEmpty    = errors.New("Empty Redis")
	channelNotifyDel = os.Getenv("SERVICE_PREFIX_NAME") + "Service-Notify-Del"
	channelNotifyAdd = os.Getenv("SERVICE_PREFIX_NAME") + "Service-Notify-Add"
)

//Store ...
type Store struct {
	r *redis.Client
}
type Service struct {
	name, info string
	r          *redis.Client
}

func (s *serviceInfo) getServiceAddr(name string) ([]string, bool) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	addr, ok := s.serviceAddr[name]
	return addr, ok
}
func (s *serviceInfo) setServiceAddr(name string, addr []string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.serviceAddr[name] = addr
}

//NewStore ...
func NewStore(target, serviceName string) *Store {
	s := &Store{r: NewRedisClient(config.Redis{Addr: target})}
	once.Do(func() {
		go func() {
			ch := s.r.Subscribe(channelNotifyAdd, channelNotifyDel).Channel()
			for msg := range ch {
				switch msg.Channel {
				case channelNotifyDel, channelNotifyAdd:
					glog.Infoln(msg.Channel, msg.Payload)
					name := msg.Payload
					if _, ok := cacheServiceInfo.getServiceAddr(name); ok {
						s.queryService(name)
					}
				}

			}
		}()

	})
	go s.queryUpdate(serviceName)
	return s
}
func (s *Store) queryUpdate(serviceName string) {
	ticker := time.NewTicker(5 * time.Second)
	for {
		<-ticker.C
		s.queryService(serviceName)

	}
}
func (s *Store) queryService(serviceName string) ([]string, error) {
	res, e := s.r.HGetAll(serviceName).Result()
	if e != nil {
		glog.Errorln(e)
		return nil, e
	}
	var (
		now           = time.Now().Unix()
		serviceExpire []string
		addr          []string
	)
	for k, v := range res {
		expire, e := strconv.ParseInt(v, 10, 64)
		if e != nil || now > expire {
			serviceExpire = append(serviceExpire, k)
		} else {
			addr = append(addr, k)
		}

	}
	if len(serviceExpire) > 0 {
		//notify.SendToTeamsEx("Service Idle", strings.Join(serviceExpire, ","))
		glog.Warningln("Service Idle", strings.Join(serviceExpire, ","))
		s.r.HDel(serviceName, serviceExpire...)
	}
	cacheServiceInfo.setServiceAddr(serviceName, addr)
	return addr, nil
}

//LookupSRV ...
func (s *Store) LookupSRV(serviceName string, all bool) ([]string, error) {

	addr, ok := cacheServiceInfo.getServiceAddr(serviceName)
	var err error
	glog.Infoln(addr, serviceName)
	if !ok {
		addr, err = s.queryService(serviceName)
		if err != nil {
			return nil, err
		}
	}
	return addr, nil
}

//NewService ...
func NewService(name string, host string, port int, target string, ttl int) *Service {
	return &Service{name: name, info: fmt.Sprintf("%s:%d", host, port), r: NewRedisClient(config.Redis{Addr: target})}
}

//Register ...
func (s *Service) Register() error {
	pipe := s.r.TxPipeline()
	pipe.HSet(s.name, s.info, strconv.FormatInt(time.Now().Unix()+expireTm, 10))
	pipe.Publish(channelNotifyAdd, s.name)
	_, e := pipe.Exec()
	return e
}

//UpdateTTL ...
func (s *Service) UpdateTTL() error {
	return s.r.HSet(s.name, s.info, strconv.FormatInt(time.Now().Unix()+expireTm, 10)).Err()
}

//DeRegister ...
func (s *Service) DeRegister() error {
	pipe := s.r.TxPipeline()
	pipe.HDel(s.name, s.info)
	pipe.Publish(channelNotifyDel, s.name)
	_, e := pipe.Exec()
	return e
}
