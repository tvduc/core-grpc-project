package http

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	tracing "git.zapa.cloud/merchant-tools/helper/opentracing"
	"github.com/golang/glog"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var (
	httpTag = opentracing.Tag{Key: string(ext.Component), Value: "httpTrace"}
)

//NewHTTPClientCtx ...
func NewHTTPClientCtx(ctx context.Context, url string) (context.Context, opentracing.Span) {
	return tracing.NewClientSpanFromContext(ctx, httpTag, url)
}

type GetParameCB func() (queryParams *url.Values, appendUrl string)
type PutParameCB func() (requestBody interface{}, queryParams *url.Values, appendUrl string)
type PostParameCB func() (requestBody interface{}, queryParams *url.Values, appendUrl string)

func GetHttpClient() *http.Client {
	return http.DefaultClient
}

func SendHttpGet(getUrl string, paramCB GetParameCB) ([]byte, error) {
	values, appendUrl := paramCB()
	getUrl += appendUrl + "?" + values.Encode()
	glog.Info("SendHttpGet: url = ", getUrl)

	client := GetHttpClient()
	request, err := http.NewRequest("GET", getUrl, nil)
	httpResp, err := client.Do(request)
	if err != nil {
		glog.Errorln("SendHttpGet: error: ", err)
		return nil, err
	}
	defer httpResp.Body.Close()
	if httpResp.StatusCode != http.StatusOK {
		glog.Errorln("SendHttpGet response http status: ", httpResp.Status)
		return nil, fmt.Errorf(httpResp.Status)
	}

	body, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		glog.Errorln("SendHttpGet read all data from body error: ", err)
		return nil, err
	}
	return body, nil
}

func SendHttpPost(postUrl, contentType string, paramCB PostParameCB) ([]byte, error) {
	dataBody, queryParams, appendUrl := paramCB()
	var rawData []byte
	var err error
	if f, ok := dataBody.(interface{ Encode() string }); ok {
		rawData = []byte(f.Encode())
	} else {
		rawData, err = json.Marshal(dataBody)
		if err != nil {
			glog.Errorln("SendHttpPost: marshal object error: ", err)
			return nil, err
		}
	}

	postUrl += appendUrl
	if queryParams != nil && len(*queryParams) > 0 {
		if strings.HasSuffix(postUrl, "?") {
			postUrl += queryParams.Encode()
		} else {
			postUrl += "?" + queryParams.Encode()
		}
	}

	glog.Infoln("SendHttpPost: url = ", postUrl, ", body data = ", string(rawData))
	client := GetHttpClient()
	request, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(rawData))
	request.Header.Set("Content-Type", contentType)
	httpResp, err := client.Do(request)
	if err != nil {
		glog.Errorln("SendHttpPost: error: ", err)
		return nil, err
	}
	defer httpResp.Body.Close()
	if httpResp.StatusCode != http.StatusOK {
		glog.Errorln("SendHttpPost: status code = ", httpResp.StatusCode)
		return nil, fmt.Errorf(httpResp.Status)
	}

	glog.Info("SendHttpPost httpResp = ", httpResp)

	body, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		glog.Errorln("SendHttpPost read all data from body error: ", err)
		return nil, err
	}
	glog.Info("SendHttpPost: body response = ", string(body))
	return body, nil
}

func SendHttpFormPost(postUrl string, paramCB PostParameCB) ([]byte, error) {
	return SendHttpPost(postUrl, "application/x-www-form-urlencoded", paramCB)
}

func SendHttpJsonPost(postUrl string, paramCB PostParameCB) ([]byte, error) {
	return SendHttpPost(postUrl, "application/json", paramCB)
}

func SendHttpJsonPut(putUrl string, paramCB PutParameCB) ([]byte, error) {
	data, queryParams, appendUrl := paramCB()
	rawData, err := json.Marshal(data)
	if err != nil {
		glog.Errorln("sendHttpJsonPut: marshal object error: ", data, err)
		return nil, err
	}

	putUrl += appendUrl
	if queryParams != nil && len(*queryParams) > 0 {
		if strings.HasSuffix(putUrl, "?") {
			putUrl += queryParams.Encode()
		} else {
			putUrl += "?" + queryParams.Encode()
		}
	}

	glog.Info("sendHttpJsonPut: url = ", putUrl, ", json = ", string(rawData))
	client := GetHttpClient()
	request, err := http.NewRequest("PUT", putUrl, bytes.NewBuffer(rawData))
	request.Header.Set("Content-Type", "application/json; charset=utf-8")
	httpResp, err := client.Do(request)
	if err != nil {
		glog.Errorln("sendHttpJsonPut: error: ", err)
		return nil, err
	}
	defer httpResp.Body.Close()
	if httpResp.StatusCode != http.StatusOK {
		glog.Errorln("sendHttpJsonPut: status code = ", httpResp.StatusCode)
		return nil, fmt.Errorf(httpResp.Status)
	}

	glog.Info("sendHttpJsonPut httpResp = ", httpResp)

	body, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		glog.Errorln("sendHttpJsonPut read all data from body error: ", err)
		return nil, err
	}
	glog.Info("sendHttpJsonPut: body response = ", string(body))
	return body, nil
}
