package elastic

import (
	"encoding/json"
	"reflect"

	"github.com/golang/glog"
	google_protobuf "github.com/golang/protobuf/ptypes/timestamp"
)

var (
	mappings = map[string]interface{}{
		"sum": map[string]string{"type": "long"},
		"=":   map[string]string{"type": "keyword"},
		"mt":  map[string]string{"type": "text"},
		"match": map[string]interface{}{"type": "keyword", "fields": map[string]interface{}{
			"search": map[string]string{"type": "text"}}},
		"*%*": map[string]interface{}{"type": "keyword",
			"fields": map[string]interface{}{
				"search":         map[string]string{"type": "text", "analyzer": "search", "search_analyzer": "search"},
				"search_reverse": map[string]string{"type": "text", "analyzer": "search_reverse", "search_analyzer": "search_reverse"}}},
		"*%": map[string]interface{}{"type": "keyword",
			"fields": map[string]interface{}{"search": map[string]string{"type": "text", "analyzer": "search", "search_analyzer": "search"}}}, //`{"search":{"type":"text","analyzer":"token_analyzer","search_analyzer":"token_search"}}`},
		"%*": map[string]interface{}{"type": "keyword",
			"fields": map[string]interface{}{"search_reverse": map[string]string{"type": "text", "analyzer": "search_reverse", "search_analyzer": "search_reverse"}}},
	}
	properties = "properties"
	//SearchAnalyzer ...
	SearchAnalyzer = parseAnalyzer()
	int32Type      = reflect.TypeOf(int32(0))
	dateType       = reflect.TypeOf(&google_protobuf.Timestamp{})
)

func parseAnalyzer() interface{} {
	var analyzer map[string]interface{}
	json.Unmarshal([]byte(`{
		"analyzer": {
		  "search": {
			"tokenizer": "standard",
			"filter": [
			  "lowercase",
			  "asciifolding",
			  "short_length"
			]
		  },
		  "search_reverse": {
			"tokenizer": "standard",
			"filter": [
			  "lowercase",
			  "reverse",
			  "asciifolding",
			  "short_length"
			]
		  }
		},
		"filter": {
		  "short_length": {
			"type": "length",
			"min": 3
		  }
		}
	  }`), &analyzer)
	return analyzer
}

func makeKeyMap(m map[string]interface{}, key string) map[string]interface{} {
	if t, ok := m[key]; ok {
		if t, ok := t.(map[string]interface{}); ok {
			return t
		}
	}
	t := map[string]interface{}{}
	m[key] = t
	return t
}
func esMapping(vt reflect.Type, mappingEs map[string]interface{}) {
	if vt.Kind() == reflect.Ptr {
		vt = vt.Elem()
	}
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		tagEs, _ := field.Tag.Lookup("es")
		if tagEs == "" {
			continue
		}
		tagQuery, _ := field.Tag.Lookup("query")
		if field.Type == dateType {
			mappingEs[tagEs] = map[string]string{"type": "date"}
			continue
		}
		t, _ := field.Tag.Lookup("type")
		if t != "" {
			mappingEs[tagEs] = map[string]string{"type": t}
			continue
		}

		if tagQuery != "" && field.Type.Kind() != reflect.Ptr && field.Type.Kind() != reflect.Struct {

			mappingEs[tagEs], _ = mappings[tagQuery]
			continue
		}
		switch field.Type.Kind() {
		case reflect.Ptr, reflect.Struct:
			esMapping(field.Type, makeKeyMap(makeKeyMap(mappingEs, tagEs), "properties"))
			//glog.Infoln(field)
		case reflect.Int32:
			if field.Type != int32Type {
				mappingEs[tagEs] = map[string]string{"type": "keyword"}
			} else {
				mappingEs[tagEs] = map[string]string{"type": "integer"}
			}
		case reflect.Int64:
			mappingEs[tagEs] = map[string]string{"type": "long"}
		case reflect.String:
			mappingEs[tagEs] = map[string]string{"type": "keyword"}
		case reflect.Bool:
			mappingEs[tagEs] = map[string]string{"type": "boolean"}
		case reflect.Float32:
			mappingEs[tagEs] = map[string]string{"type": "float"}
		case reflect.Float64:
			mappingEs[tagEs] = map[string]string{"type": "double"}

		default:
			glog.Errorln("Unknown Type ", tagEs, field.Type)
		}

	}
}

//GetEsMapping ...
func GetEsMapping(req interface{}) map[string]interface{} {
	hasEsMappingMethod := false
	mappingEs := map[string]interface{}{}
	if f, ok := req.(interface {
		EsMapping(mpEs map[string]interface{})
	}); ok {
		f.EsMapping(mappingEs)
		hasEsMappingMethod = true
	} else {

		esMapping(reflect.TypeOf(req), mappingEs)
	}
	src, _ := json.Marshal(mappingEs)
	glog.Infoln(string(src))
	glog.Infoln("hasEsMappingMethod: ", hasEsMappingMethod)
	return map[string]interface{}{EsType: map[string]interface{}{properties: mappingEs}}
}
