package elastic

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"git.zapa.cloud/merchant-tools/helper/env"

	"io"

	"git.zapa.cloud/merchant-tools/helper/concurrency"
	"git.zapa.cloud/merchant-tools/helper/core"
	errorutil "git.zapa.cloud/merchant-tools/helper/error_util"
	tracingHelper "git.zapa.cloud/merchant-tools/helper/opentracing"
	"git.zapa.cloud/merchant-tools/helper/proto"
	"github.com/golang/glog"
	jsoniter "github.com/json-iterator/go"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gopkg.in/olivere/elastic.v6"
)

var (
	//EsType ...
	EsType           = "merchant-tools"
	elasticSearchTag = opentracing.Tag{Key: string(ext.Component), Value: "elasticSearch"}
	scriptTag        = "script:"
	iJson            = jsoniter.ConfigCompatibleWithStandardLibrary
)

type esScrollService func() *elastic.ScrollService
type jsonDecode struct {
}

func (d *jsonDecode) Decode(data []byte, v interface{}) error {
	return iJson.Unmarshal(data, v)
}

//New ...
func New(addr string, sniff bool) *elastic.Client {
	if len(addr) == 0 {
		if env.IsProd() {
			addr = "http://10.50.49.6:9200"
		} else {
			addr = "http://10.109.3.47:9200"
		}
	}
	c, e := core.GetCache("NewEs"+addr, func() (interface{}, error) {
		return elastic.NewClient(elastic.SetDecoder(new(jsonDecode)), elastic.SetURL(strings.Split(addr, ",")...), elastic.SetSniff(sniff))
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*elastic.Client)
}

//Init ...
func Init(c *elastic.Client, index, cfg string, settings ...string) error {
	exists, err := c.IndexExists(index).Do(context.Background())
	if err != nil {
		glog.Error(err)
		return err
	}
	if !exists {
		// Create a new index.
		_, err := c.CreateIndex(index).Body(cfg).Do(context.Background())
		if err != nil {
			glog.Error(err)
			DumpElasticError(err)
			return err
		}
	}
	for _, setting := range settings {

		_, err := c.IndexPutSettings().Index(index).BodyString(setting).Do(context.Background())
		if err != nil {
			glog.Errorln("IndexPutSettings: ", err)
			DumpElasticError(err)
			return err
		}
	}
	return nil
}

//DoSearch ...
func DoSearch(ctx context.Context, svc *elastic.SearchService) (*elastic.SearchResult, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoSearch")
	res, err := svc.Do(newctx)
	tracingHelper.FinishClientSpan(span, err)
	DumpElasticError(err)
	return res, err
}

//DoBulk ...
func DoBulk(ctx context.Context, svc *elastic.BulkService) (*elastic.BulkResponse, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoBulk")
	res, err := svc.Do(newctx)
	tracingHelper.FinishClientSpan(span, err)
	DumpElasticError(err)
	return res, err
}

//DoIndex ...
func DoIndex(ctx context.Context, svc *elastic.IndexService) (*elastic.IndexResponse, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoIndex")
	res, err := svc.Do(newctx)
	tracingHelper.FinishClientSpan(span, err)
	DumpElasticError(err)
	return res, err
}

//DoBulkProcessorAdd ...
func DoBulkProcessorAdd(ctx context.Context, bulk *elastic.BulkProcessor, request elastic.BulkableRequest) error {
	if bulk == nil {
		return nil
	}
	//_, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoBulkProcessor")
	bulk.Add(request)
	//tracingHelper.FinishClientSpan(span, nil)
	return nil
}

//DoAll ...
func DoAll(ctx context.Context, scroll esScrollService) ([]string, error) {
	const (
		nSlice = 5
	)
	keys := []string{}
	mtx := &sync.Mutex{}
	c := concurrency.New()
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoAll")
	for i := 0; i < nSlice; i++ {
		sliceQuery := elastic.NewSliceQuery().Id(i).Max(nSlice)
		svc := scroll().Size(1024).Slice(sliceQuery).FetchSource(false)
		c.Add(func() error {
			for {
				res, err := svc.Do(newctx)
				if err == io.EOF {
					break
				}
				if err != nil {
					DumpElasticError(err)
					return err
				}
				mtx.Lock()
				for _, hits := range res.Hits.Hits {
					keys = append(keys, hits.Id)
				}
				mtx.Unlock()

			}
			return nil
		})
	}

	err := c.Do()
	tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		DumpElasticError(err)
		return nil, err
	}

	return keys, nil

}

//DoCount ...
func DoCount(ctx context.Context, svc *elastic.CountService) (int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoCount")
	res, err := svc.Do(newctx)
	tracingHelper.FinishClientSpan(span, err)
	DumpElasticError(err)
	return res, err
}

//DoTermsAggregation ...
func DoTermsAggregation(ctx context.Context, svc *elastic.SearchService, field string) (map[string]int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoTermsAggregation")
	agg := elastic.NewTermsAggregation().Field(field)
	const aggKey = "ermsAggregation"
	svc = svc.Aggregation(aggKey, agg).Pretty(true).Size(0)
	aggeResult, err := svc.Do(newctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		DumpElasticError(err)
		return nil, err
	}
	result := map[string]int64{}
	if aggRes, ok := aggeResult.Aggregations.Terms(aggKey); ok {
		for _, bulk := range aggRes.Buckets {
			result[fmt.Sprintf("%v", bulk.Key)] = bulk.DocCount
		}

	}
	return result, nil

}

//DoSummary ...
func DoSummary(ctx context.Context, svc *elastic.SearchService, fields ...string) ([]float64, int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoSummary")
	sumRes := []float64{}
	fieldNames := []string{}

	for _, field := range fields {
		if strings.HasPrefix(field, scriptTag) {
			script := elastic.NewScript(strings.TrimPrefix(field, scriptTag))
			glog.Infoln("Script: ", script)
			agg := elastic.NewSumAggregation().Script(script).Missing(0)
			field = fmt.Sprintf("%s_%d", scriptTag, len(fieldNames))
			svc = svc.Aggregation(field, agg)
		} else {
			agg := elastic.NewSumAggregation().Field(field).Missing(0)
			svc = svc.Aggregation(field, agg)
		}
		fieldNames = append(fieldNames, field)
	}
	svc = svc.Size(0).Pretty(true)
	searchResult, err := svc.Do(newctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		glog.Errorln(err)
		DumpElasticError(err)
		return sumRes, 0, err

	}
	agg := searchResult.Aggregations
	for _, field := range fieldNames {
		if res, found := agg.Sum(field); found {
			sumRes = append(sumRes, *res.Value)
		} else {
			sumRes = append(sumRes, 0.0)
		}
	}
	return sumRes, searchResult.TotalHits(), err
}

//DoSummaryDistinc ...
func DoSummaryDistinc(ctx context.Context, svc *elastic.SearchService, fields []string, disticFields []string, groupField string) ([]float64, []float64, int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoSummaryDistinc")
	sumRes := []float64{}
	sumDistincRes := []float64{}
	aggName := "unique_twitter"

	number, err := DoCardinality(ctx, svc, groupField)
	if err != nil {
		return sumRes, sumDistincRes, 0, err
	}

	if number == 0 {
		number = 999
	} else {
		number += 1000
	}
	groupAgg := elastic.NewTermsAggregation().Field(groupField).Size(int(number))

	for i, field := range disticFields {
		agg := elastic.NewMaxAggregation().Field(field).Missing(0)
		subName := fmt.Sprintf("%d", i+1)
		groupAgg = groupAgg.SubAggregation(subName, agg)
		svc = svc.Aggregation(subName, elastic.NewSumBucketAggregation().BucketsPath(aggName+">"+subName))
	}

	svc = svc.Aggregation(aggName, groupAgg)
	fieldNames := []string{}

	for _, field := range fields {
		if strings.HasPrefix(field, scriptTag) {
			script := elastic.NewScript(strings.TrimPrefix(field, scriptTag))
			glog.Infoln("Script: ", script)
			agg := elastic.NewSumAggregation().Script(script).Missing(0)
			field = fmt.Sprintf("%s_%d", scriptTag, len(fieldNames))
			svc = svc.Aggregation(field, agg)
		} else {
			agg := elastic.NewSumAggregation().Field(field).Missing(0)
			svc = svc.Aggregation(field, agg)
		}
		fieldNames = append(fieldNames, field)
	}
	/*for _, field := range fields {
		agg := elastic.NewSumAggregation().Field(field).Missing(0)
		svc = svc.Aggregation(field, agg)
	}*/
	svc = svc.Size(0).Pretty(true)
	searchResult, err := svc.Do(newctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		DumpElasticError(err)
		return sumRes, sumDistincRes, 0, err

	}
	agg := searchResult.Aggregations
	for _, field := range fieldNames {
		if res, found := agg.Sum(field); found {
			sumRes = append(sumRes, *res.Value)
		} else {
			sumRes = append(sumRes, 0.0)
		}
	}

	for i, _ := range disticFields {
		aggName := fmt.Sprintf("%d", i+1)
		if res, found := agg.SumBucket(aggName); found {
			sumDistincRes = append(sumDistincRes, *res.Value)
		} else {
			sumDistincRes = append(sumDistincRes, 0.0)
		}
	}
	return sumRes, sumDistincRes, searchResult.TotalHits(), nil
}

//DoCardinality ...
//
func DoCardinality(ctx context.Context, svc *elastic.SearchService, field string) (int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoCardinality")

	agg := elastic.NewCardinalityAggregation().Field(field).Missing(0)
	svc = svc.Aggregation("cardinality", agg)

	svc = svc.Size(0).Pretty(true)
	searchResult, err := svc.Do(newctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		DumpElasticError(err)
		return 0, err
	}
	aggs := searchResult.Aggregations

	if res, found := aggs.Cardinality("cardinality"); found {
		return int64(*res.Value), nil
	}
	return 0, errorutil.ErrorSystem
}

//DoMaxMin ...
func DoMaxMin(ctx context.Context, svc *elastic.SearchService, isMax bool, fields ...string) ([]float64, int64, error) {
	newctx, span := tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, "DoMaxMin")
	sumRes := []float64{}

	for _, field := range fields {
		if isMax {
			svc = svc.Aggregation(field, elastic.NewMaxAggregation().Field(field))
		} else {
			svc = svc.Aggregation(field, elastic.NewMinAggregation().Field(field))
		}

	}
	svc = svc.Size(0).Pretty(true)
	searchResult, err := svc.Do(newctx)
	defer tracingHelper.FinishClientSpan(span, err)
	if err != nil {
		DumpElasticError(err)
		return sumRes, 0, err

	}
	agg := searchResult.Aggregations
	for _, field := range fields {
		if res, found := agg.Sum(field); found == true && res.Value != nil {
			sumRes = append(sumRes, *res.Value)
		} else {
			sumRes = append(sumRes, 0.0)
		}
	}
	return sumRes, searchResult.TotalHits(), err
}
func DoubleSlice(s interface{}) []interface{} {
	v := reflect.ValueOf(s)
	items := make([]interface{}, v.Len())
	for i := 0; i < v.Len(); i++ {
		items[i] = v.Index(i).Interface()
	}
	return items
}
func isEnumAll(vv interface{}) bool {
	type enumInterface interface {
		EnumDescriptor() ([]byte, []int)
	}
	if _, ok := vv.(enumInterface); ok {
		return fmt.Sprintf("%d", vv) == "-1"
	}
	return false
}
func convertDateTimeSearch(vv interface{}, op string) interface{} {
	if date, ok := vv.(*proto.Date); ok {
		switch op {
		case "<":
			return proto.DateUpperToTimeSearch(date).UnixNano() / int64(time.Millisecond)
		default:
			return proto.DateToTimeSearch(date).UnixNano() / int64(time.Millisecond)

		}
	}
	return vv

}

//BuildQuery ...
func BuildQuery(query *elastic.BoolQuery, req interface{}) *elastic.BoolQuery {
	hasBuildQueryMethod := false
	if f, ok := req.(interface {
		BuildQuery(query *elastic.BoolQuery) *elastic.BoolQuery
	}); ok {
		query = f.BuildQuery(query)
		hasBuildQueryMethod = true
	} else {
		if query == nil {
			query = elastic.NewBoolQuery()
		}
		r := &rangeQuery{
			mapQuery: map[string]*elastic.RangeQuery{},
		}
		rangeDateSearch := &mapRangeDateSearch{mapRangeDateSearch: map[string]*rangeDateSearch{}}
		val := reflect.ValueOf(req)
		if val.Kind() == reflect.Ptr {
			if val.IsNil() {
				glog.Errorln("nil ...")
				return query
			}
			val = val.Elem()
		}
		bHasSearchPrefix, disableRangeFilter, searchPhone := false, false, false
		vt := val.Type()
		for i := 0; i < vt.NumField(); i++ {
			field := vt.Field(i)
			if tag, ok := field.Tag.Lookup("query"); ok {
				params := strings.Split(tag, ",")
				if len(params) != 2 {
					glog.Warningln(tag, len(params))
				}
				v := val.FieldByName(field.Name)
				zero := reflect.Zero(v.Type()).Interface()
				vv := v.Interface()
				if reflect.DeepEqual(vv, zero) {
					//glog.Infoln(params[0], "Empty")
					continue
				}
				switch params[1] {
				case "*%*":
					fields := strings.Split(params[0], ";")
					if len(fields) < 2 {
						bHasSearchPrefix = true
						text := fmt.Sprintf("%v", vv)
						if !disableRangeFilter && len(text) >= 8 {
							disableRangeFilter = true
						}
						if params[0] == "userInfo.phoneNumber" {
							searchPhone = true
						}
						//Workaround for ESALE
						if params[0] == "appTransId" && strings.Contains(text, "-") && strings.HasPrefix(text, "M") {
							disableRangeFilter = false
							query = query.Must(elastic.NewWildcardQuery(params[0], "??????"+text))
						} else {
							query = query.Must(elastic.NewMultiMatchQuery(
								vv, params[0]+".search", params[0]+".search_reverse").MaxExpansions(1024).Slop(2).Type("phrase_prefix"))
						}
					} else {
						fieldsSearch := make([]string, 2*len(fields))
						for i, field := range fields {
							fieldsSearch[2*i] = field + ".search"
							fieldsSearch[2*i+1] = field + ".search_reverse"
						}
						query = query.Must(elastic.NewMultiMatchQuery(
							vv, fieldsSearch...).MaxExpansions(1024).Slop(2).Type("phrase_prefix"))
					}
				case "*%":
					query = query.Must(elastic.NewMatchPhrasePrefixQuery(
						fmt.Sprintf("%s.search", params[0]),
						vv,
					).MaxExpansions(1024).Slop(2))
				case "%*":
					query = query.Must(elastic.NewMatchPhrasePrefixQuery(
						fmt.Sprintf("%s.search_reverse", params[0]),
						vv,
					).MaxExpansions(1024).Slop(2))

				case "*.*": //Wildcard
					s := fmt.Sprintf("%v", vv)
					if !strings.Contains(s, "*") {
						s = "*" + s + "*"
					}
					query = query.Must(elastic.NewWildcardQuery(params[0], s))
				case "*.": //Wildcard
					query = query.Must(elastic.NewWildcardQuery(params[0], fmt.Sprintf("*%v", vv)))
				case ".*": //Wildcard
					query = query.Must(elastic.NewWildcardQuery(params[0], fmt.Sprintf("%v*", vv)))
				case "=": //Term
					if reflect.TypeOf(vv).Kind() == reflect.Slice {
						query = query.Filter(elastic.NewTermsQuery(params[0], DoubleSlice(vv)...).Boost(2))
					} else if isEnumAll(vv) {
						glog.Infoln(params[0], "Is enum all")
					} else {
						comp := convertDateTimeSearch(vv, params[1])
						query = query.Filter(elastic.NewTermQuery(params[0], comp).Boost(2))
					}
				case "mt":
					query = query.Must(elastic.NewMatchQuery(params[0], vv))
				case "match":
					query = query.Must(elastic.NewMatchQuery(params[0]+".search", vv).MinimumShouldMatch("3<90%"))
				case ">=":
					glog.Infoln(params[0], vv)
					if !rangeDateSearch.addFrom(params[0], vv) {
						query = query.Must(r.NewRangeQuery(params[0]).Gte(vv))
					}
				case "<=":
					glog.Infoln(params[0], vv)
					if !rangeDateSearch.addTo(params[0], vv) {
						query = query.Must(r.NewRangeQuery(params[0]).Lte(vv))
					}
				case ">":
					glog.Infoln(params[0], vv)
					if !rangeDateSearch.addFrom(params[0], vv) {
						query = query.Must(r.NewRangeQuery(params[0]).Gt(vv))
					}
				case "<":
					glog.Infoln(params[0], vv)
					if !rangeDateSearch.addTo(params[0], vv) {
						query = query.Must(r.NewRangeQuery(params[0]).Lt(vv))
					}
				case "!=":
					glog.Infoln(params[0], vv)
					if reflect.TypeOf(vv).Kind() == reflect.Slice {
						query = query.MustNot(elastic.NewTermsQuery(params[0], DoubleSlice(vv)...))
					} else {
						comp := convertDateTimeSearch(vv, params[1])
						query = query.MustNot(elastic.NewTermQuery(params[0], comp))
					}
				default:
					glog.Warningln("Unknow ", params[1])

				}

				//glog.Infoln(es)
			}
		}
		if !disableRangeFilter || searchPhone {
			for k, v := range rangeDateSearch.mapRangeDateSearch {
				glog.Infoln(k, v)
				f, t := v.from, v.to
				if f != nil && t != nil {
					if !searchPhone && bHasSearchPrefix && f.Day+7 > t.Day && f.Month == t.Month && f.Year == t.Year {
						tm := time.Date(int(f.Year), time.Month(f.Month), int(f.Day), 0, 0, 0, 0, time.UTC).Add(-7 * 24 * time.Hour)
						f.Year, f.Month, f.Day = int32(tm.Year()), int32(tm.Month()), int32(tm.Day())
					}
					query = query.Filter(elastic.NewRangeQuery(k).Gte(dateToStringSearch(f)).Lte(dateToStringSearch(t)).TimeZone("+07:00").Boost(3))
				} else {
					glog.Errorln("Invalid ", k)
				}

			}
		}
	}
	glog.Infoln("hasBuildQueryMethod", hasBuildQueryMethod)
	src, _ := query.Source()
	str, _ := json.Marshal(src)
	glog.Infoln(string(str))
	return query
}

type rangeQuery struct {
	mapQuery map[string]*elastic.RangeQuery
}

func (r *rangeQuery) NewRangeQuery(name string) *elastic.RangeQuery {
	if q, ok := r.mapQuery[name]; ok {
		return q
	}
	q := elastic.NewRangeQuery(name)
	r.mapQuery[name] = q
	return q
}

type rangeDateSearch struct {
	from, to *proto.Date
}
type mapRangeDateSearch struct {
	mapRangeDateSearch map[string]*rangeDateSearch
}

func dateToStringSearch(date *proto.Date) string {
	return fmt.Sprintf("%04d-%02d-%02d", date.GetYear(), date.GetMonth(), date.GetDay())
}
func (r *mapRangeDateSearch) addFrom(name string, vv interface{}) bool {
	if from, ok := vv.(*proto.Date); ok {
		if q, ok := r.mapRangeDateSearch[name]; ok {
			q.from = from
		} else {
			r.mapRangeDateSearch[name] = &rangeDateSearch{from: from}
		}
		return true
	}
	return false
}
func (r *mapRangeDateSearch) addTo(name string, vv interface{}) bool {
	if to, ok := vv.(*proto.Date); ok {
		if q, ok := r.mapRangeDateSearch[name]; ok {
			q.to = to
		} else {
			r.mapRangeDateSearch[name] = &rangeDateSearch{to: to}
		}
		return true
	}
	return false
}

//MakeSpanContext ...
func MakeSpanContext(ctx context.Context, tag string) (context.Context, opentracing.Span) {
	return tracingHelper.NewClientSpanFromContext(ctx, elasticSearchTag, tag)
}

//ESRangeSearchDate ...
func ESRangeSearchDate(field string, from, to time.Time) *elastic.RangeQuery {
	f := from.UnixNano() / int64(time.Millisecond)
	t := (to.UnixNano() + 24*int64(time.Hour)) / int64(time.Millisecond)
	return elastic.NewRangeQuery(field).Gte(f).Lt(t)
}

type pagingReq interface {
	GetPageIndex() int32
	GetPageSize() int32
}

//BuildSearchPaging ...
func BuildSearchPaging(svc *elastic.SearchService, req pagingReq) *elastic.SearchService {
	idx := int(req.GetPageIndex())
	size := int(req.GetPageSize())
	if idx > 0 && size > 0 {
		svc = svc.From((idx - 1) * size).Size(size)
	}
	if f, ok := req.(interface {
		GetShowSummary() bool
	}); ok {
		if !f.GetShowSummary() {
			//svc = svc.TrackTotalHits(false)
		}
	}
	return svc
}

//DumpElasticError ...
func DumpElasticError(err error) {
	if err != nil {
		if e, ok := err.(*elastic.Error); ok {
			glog.Errorln("Elastic error ", e.Status, e.Details)
		}
	}
}
