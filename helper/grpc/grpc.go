package grpc

import (
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	config "gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/graceful"

	"github.com/golang/glog"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/notify"

	authen "gitlab.com/tvduc/product-management/core-grpc-project/helper/authentication"
	net_helper "gitlab.com/tvduc/product-management/core-grpc-project/helper/net"

	//grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	//"github.com/wothing/wonaming/consul"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/core"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/registry"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	grpcCodes "google.golang.org/grpc/codes"
	"google.golang.org/grpc/health"
	healthgrpc "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	grpcStatus "google.golang.org/grpc/status"
)

var (
	connCache    = core.NewPersitentCache()
	BuildVersion = "[190718]"
	healthPort   = ":12345"
)

type callBackGrpc func(context.Context, *grpc.Server)

//StartServer ..
func StartServer(serviceName string, cfg config.Service, cb callBackGrpc, opts ...grpc.UnaryServerInterceptor) {
	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()
	if err := config.Init(serviceName, cfg); err != nil {
		glog.Errorln("Init fail ", err)
	}
	glog.Infoln("Config ", cfg)
	intercepts := []grpc.UnaryServerInterceptor{grpc_opentracing.UnaryServerInterceptor(),
		authen.UnaryServerInterceptor(),
		grpc_validator.UnaryServerInterceptor()}
	if len(opts) > 0 {
		intercepts = append(intercepts, opts...)
	}

	grpcServer := grpc.NewServer(grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
		grpc_opentracing.StreamServerInterceptor(),
		authen.StreamServerInterceptor(),
		grpc_validator.StreamServerInterceptor(),
	)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(intercepts...)), grpc.MaxRecvMsgSize(128<<20))
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer func() {
		cancel()
		if err := recover(); err != nil {
			// notify.SendToTeamsEx("Panic", fmt.Sprintf("%v", err))
			time.Sleep(time.Second)

		}
	}()

	cb(ctx, grpcServer)
	//sc.RegisterOrderServer(grpcServer, sc.NewServiceHandler(ctx, &config))

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GetPort()))
	if err != nil {
		glog.Fatalf("failed to listen: %v", err)
	}
	host, _ := net_helper.ExtractRealIP("")
	_, port, _ := net.SplitHostPort(lis.Addr().String())

	strPort, _ := strconv.Atoi(port)
	/*if err := naming.Register(serviceName, host, strPort, cfg.GetConsul(), time.Second*10, 15); err != nil {
		glog.Error(err)
	}*/
	if err := registry.Register(serviceName, host, strPort, 15); err != nil {
		glog.Error(err)
	}
	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		glog.Fatalf("failed to serve: %v", err)
	}
	go func() {
		s := grpc.NewServer()
		healthgrpc.RegisterHealthServer(s, health.NewServer())
		lis, err := net.Listen("tcp", healthPort)
		if err != nil {
			glog.Errorln(err)
			return
		}
		if err = s.Serve(lis); err != nil {
			glog.Errorln(err)
			return
		}
	}()
	graceful.ShutDown()
}
func traceUnaryClientInterceptor() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		tm := time.Now()
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err != nil {
			if st, ok := grpcStatus.FromError(err); ok {
				switch st.Code() {
				case grpcCodes.Unauthenticated, grpcCodes.Canceled, grpcCodes.NotFound:
				default:
					notify.SendToTeams(method, fmt.Sprintf("%v:%v", req, err))
				}
			} else {
				str, _ := json.Marshal(req)
				notify.SendToTeams(method, fmt.Sprintf("%s:%v", string(str), err))
			}
		} else if time.Since(tm) > 300*time.Millisecond {
			str, _ := json.Marshal(req)
			notify.SendToTeams(method, fmt.Sprintf("%s:%v", string(str), time.Since(tm)))
		}
		return err
	}
}

var (
	whitelist = []string{
		"/storage_service.",
		"/fileserver.",
	}
	tracingOpt = grpc_opentracing.WithFilterFunc(func(ctx context.Context, method string) bool {
		for _, s := range whitelist {
			if strings.Contains(method, s) {
				return false
			}
		}
		return true
	})
)

//UnaryClientErrInterceptor ...
func UnaryClientErrInterceptor() grpc.UnaryClientInterceptor {

	return func(ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err != nil && strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
			err = status.Error(codes.NotFound, "Not found")
		}
		return err
	}
}

//CreateGrpcConn ...
func CreateGrpcConn(ctx context.Context, consulAdd, serviceName string, opts ...grpc.UnaryClientInterceptor) *grpc.ClientConn {

	c, e := connCache.Get("CreateGrpcConn"+consulAdd+serviceName, func() (interface{}, error) {
		r := registry.NewResolver(serviceName)
		b := grpc.RoundRobin(r)
		// interceptors := []grpc.UnaryClientInterceptor{prometheus.UnaryClientInterceptor(), grpc_opentracing.UnaryClientInterceptor(tracingOpt)}
		interceptors := []grpc.UnaryClientInterceptor{grpc_opentracing.UnaryClientInterceptor(tracingOpt)}
		interceptors = append(interceptors, opts...)
		interceptors = append(interceptors, traceUnaryClientInterceptor())
		intercept := grpc_middleware.ChainUnaryClient(interceptors...)

		opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBalancer(b),
			grpc.WithUnaryInterceptor(intercept),
			grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor()),
			grpc.WithMaxMsgSize(128 << 20),
		}
		conn, err := grpc.Dial(serviceName, opts...)
		if err != nil {
			return nil, err
		}
		defer func() {
			if err != nil {
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
				return
			}
			go func() {
				<-ctx.Done()
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
			}()
		}()
		return conn, nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*grpc.ClientConn)
}

//CreateProxyGrpcConn ...
func CreateProxyGrpcConn(ctx context.Context, consulAdd, serviceName string, codec grpc.Codec) *grpc.ClientConn {
	c, e := connCache.Get("CreateProxyGrpcConn"+consulAdd+serviceName, func() (interface{}, error) {
		r := registry.NewResolver(serviceName)
		b := grpc.RoundRobin(r)
		intercept := grpc_middleware.ChainUnaryClient(grpc_opentracing.UnaryClientInterceptor(), traceUnaryClientInterceptor())
		opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBalancer(b),
			grpc.WithUnaryInterceptor(intercept),
			grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor()),
			grpc.WithMaxMsgSize(128 << 20),
			grpc.WithCodec(codec),
		}
		conn, err := grpc.Dial(serviceName, opts...)
		if err != nil {
			return nil, err
		}
		defer func() {
			if err != nil {
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
				return
			}
			go func() {
				<-ctx.Done()
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
			}()
		}()
		return conn, nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*grpc.ClientConn)
}

//CreateGrpcBlockConn create connection block
func CreateGrpcBlockConn(ctx context.Context, consulAdd, serviceName string) *grpc.ClientConn {
	c, e := connCache.Get("CreateGrpcBlockConn"+consulAdd+serviceName, func() (interface{}, error) {
		r := registry.NewResolver(serviceName)
		b := grpc.RoundRobin(r)
		opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock(), grpc.WithBalancer(b),
			grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor()),
			grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor())}
		conn, err := grpc.Dial(serviceName, opts...)
		if err != nil {
			return nil, err
		}
		defer func() {
			if err != nil {
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
				return
			}
			go func() {
				<-ctx.Done()
				if cerr := conn.Close(); cerr != nil {
					glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
				}
			}()
		}()
		return conn, nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(*grpc.ClientConn)
}
