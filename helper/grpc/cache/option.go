package cache

import (
	"strings"
	"time"
)

type Kind int

const (
	NormalType Kind = iota
	ExpireType
)

type Options struct {
	SizeMax       int
	Expire        time.Duration
	Topics        []string
	Type          Kind
	FilterMethods map[string]bool
}
type Option func(*Options)

func NewOption(setters ...Option) *Options {
	opts := &Options{
		SizeMax: 1024 * 1024,
		Expire:  15 * time.Minute,
		Type:    NormalType,
		//FilterMethods: make(map[string]bool),
	}
	for _, setter := range setters {
		setter(opts)
	}
	return opts
}
func SizeMax(sizeMax int) Option {
	return func(arg *Options) {
		arg.SizeMax = sizeMax
	}
}
func Expire(expire time.Duration) Option {
	return func(arg *Options) {
		arg.Expire = expire
	}
}
func Topics(topics ...string) Option {
	return func(arg *Options) {
		arg.Topics = topics
	}
}
func Type(t Kind) Option {
	return func(arg *Options) {
		arg.Type = t
	}
}
func FilterMethods(methods ...string) Option {
	return func(arg *Options) {
		if arg.FilterMethods == nil {
			arg.FilterMethods = make(map[string]bool)
		}
		for _, method := range methods {
			arg.FilterMethods[strings.ToLower(method)] = true
		}
		//arg.Topics = topics
	}
}
