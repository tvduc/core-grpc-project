package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"database/sql"

	"git.zapa.cloud/merchant-tools/helper/copier"
	"git.zapa.cloud/merchant-tools/helper/notify"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

func UnaryClientInterceptor(setters ...Option) grpc.UnaryClientInterceptor {

	cacheRequest := NewCache(NewOption(setters...))

	return func(ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		var reqData []byte

		if pbMsg, ok := req.(proto.Message); ok {
			reqData, _ = proto.Marshal(pbMsg)
		} else {
			reqData, _ = json.Marshal(req)
		}
		keyCache := method + string(reqData)
		item := cacheRequest.Get(keyCache)
		if item.IsValid() {
			if item.Data() != nil {
				return copier.Copy(reply, item.Data())
			} else {
				return sql.ErrNoRows
			}
		}

		// Calls the invoker to execute RPC
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err == nil {
			cacheRequest.Set(keyCache, reply)
		} else if grpc.Code(err) == codes.Unavailable {
			notify.SendToTeams(method, fmt.Sprintf("%s:%v", string(reqData), err))
			if item.Data() != nil {
				err = copier.Copy(reply, item.Data())
			} else {
				err = sql.ErrNoRows
			}
		} else if err.Error() == sql.ErrNoRows.Error() {
			cacheRequest.Set(keyCache, nil)
			err = sql.ErrNoRows
		}
		glog.Infoln(method, req, err)
		return err
	}
}

func UnaryServerInterceptor(setters ...Option) grpc.UnaryServerInterceptor {

	opts := NewOption(setters...)
	cache := NewCache(opts)
	filterFunc := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo) bool {
		method := info.FullMethod
		if opts.FilterMethods == nil {
			//Filter per handler
			if f, ok := info.Server.(interface {
				CacheMethods(context.Context, interface{}, string) bool
			}); ok {
				return f.CacheMethods(ctx, req, method)
			}
			return false
		}

		if idx := strings.LastIndexByte(method, '/') + 1; idx > 0 && idx < len(method) {
			return opts.FilterMethods[strings.ToLower(method[idx:])]
		}
		return false
	}
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		method := info.FullMethod
		if filterFunc(ctx, req, info) {
			reqData, _ := json.Marshal(req)
			keyCache := method + string(reqData)
			item := cache.Get(keyCache)
			if item.IsValid() && item.Data() != nil {
				return item.Data(), nil
			}
			resp, err := handler(ctx, req)
			if err == nil {
				cache.Set(keyCache, resp)
			}
			return resp, err

		}
		return handler(ctx, req)
	}
}
