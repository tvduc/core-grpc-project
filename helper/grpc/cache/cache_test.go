package cache

import (
	"testing"
	"time"
)

var (
	keyTest   = "keyTest"
	valueTest = "valueTest"
	// valueNil  = interface{}
)

func TestSimpleCache(t *testing.T) {
	c := NewCache(NewOption(Type(NormalType)))
	if c == nil {
		t.Error("Create Cache fail")
		return
	}
	item := c.Get(keyTest)
	if item.IsValid() || item.Data() != nil {
		t.Error("Get ", keyTest)
	}
	c.Set(keyTest, valueTest)
	item = c.Get(keyTest)
	if item.Data() == nil {
		t.Error("Data", keyTest)
	} else if item.Data().(string) != valueTest {
		t.Error("Value ", item.Data().(string))
	}
	if item.IsValid() {
		t.Error("IsValid ", keyTest)
	}

	// c.Set(keyTest, valueNil)
	item = c.Get(keyTest)
	if item.Data() != nil {
		t.Error("Data", keyTest)
	}
	if item.IsValid() {
		t.Error("IsValid ", keyTest)
	}

}
func TestExpireCache(t *testing.T) {
	c := NewCache(NewOption(Type(ExpireType), Expire(2*time.Second)))
	if c == nil {
		t.Error("Create Cache fail")
		return
	}
	item := c.Get(keyTest)
	if item.IsValid() || item.Data() != nil {
		t.Error("Get ", keyTest)
	}
	c.Set(keyTest, valueTest)
	item = c.Get(keyTest)
	if item.Data() == nil {
		t.Error("Data", keyTest)
	} else if item.Data().(string) != valueTest {
		t.Error("Value ", item.Data().(string))
	}
	if !item.IsValid() {
		t.Error("IsValid ", keyTest)
	}
	time.Sleep(2 * time.Second)
	item = c.Get(keyTest)
	if item.Data() == nil {
		t.Error("Data", keyTest)
	} else if item.Data().(string) != valueTest {
		t.Error("Value ", item.Data().(string))
	}
	if item.IsValid() {
		t.Error("IsValid ", keyTest)
	}

}
