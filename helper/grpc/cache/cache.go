package cache

import (
	"sync"
	"time"

	"sync/atomic"

	"github.com/golang/glog"

	kafka "git.zapa.cloud/merchant-tools/helper/kafka/shopify"
)

type Item interface {
	Data() interface{}
	IsValid() bool
}

var (
	dummyItem = &normalItem{}
)

type Cache interface {
	Get(key interface{}) Item
	Set(key, value interface{})
	Reset()
}
type createItem func(val interface{}) Item
type localCache struct {
	m          sync.Map
	createItem createItem
}

func (this *localCache) Get(key interface{}) Item {
	if this == nil {
		return dummyItem
	}
	if val, ok := this.m.Load(key); ok {
		return val.(Item)
	}
	return dummyItem
}
func (this *localCache) Set(key, value interface{}) {
	if this != nil {
		this.m.Store(key, this.createItem(value))
	}
}
func (this *localCache) Reset() {
	this.m.Range(func(k, v interface{}) bool {
		this.m.Delete(k)
		return true
	})
}

type normalItem struct {
	data interface{}
}

func (this *normalItem) Data() interface{} {
	if this != nil {
		return this.data
	}
	return nil
}
func (this *normalItem) IsValid() bool {
	return false
}

type expireItem struct {
	data        interface{}
	expire      time.Time
	expireCount int64
}

func (this *expireItem) Data() interface{} {
	if this != nil {
		return this.data
	}
	return nil
}
func (this *expireItem) IsValid() bool {
	if this != nil {
		if this.expire.After(time.Now()) {
			return true
		}
		return atomic.AddInt64(&this.expireCount, 1) != 1
	}
	return false
}

//NewCache ...
func NewCache(opts *Options) Cache {

	c := new(localCache)

	switch opts.Type {
	case NormalType:
		c.createItem = func(val interface{}) Item {
			return &normalItem{val}
		}
	case ExpireType:
		c.createItem = func(val interface{}) Item {
			return &expireItem{val, time.Now().Add(opts.Expire), 0}
		}
	default:
		glog.Errorln("Unknow type ", opts.Type)
		return nil
	}
	if len(opts.Topics) > 0 {
		glog.Infoln(opts.Topics)
		go kafka.NewConsumer(opts.Topics, func(topic string, data []byte) {
			glog.Infoln("Update ", topic)
			c.Reset()

		})

	}
	return c

}
