package concurrency

type concurrencyFunc func() error

//Concurrency ...
type Concurrency struct {
	f []concurrencyFunc
}

//New ...
func New() *Concurrency {
	return &Concurrency{}
}

//Add ...
func (cc *Concurrency) Add(f ...concurrencyFunc) *Concurrency {
	cc.f = append(cc.f, f...)
	return cc
}

//Do ...
func (cc *Concurrency) Do() error {
	return Do(cc.f...)
}

//Do ...
func Do(ccf ...concurrencyFunc) error {
	const (
		maxGoRoutine = 256
	)
	len := len(ccf)
	if len == 0 {
		return nil
	} else if len == 1 {
		return ccf[0]()
	}
	ec := make(chan error, len)
	if len < maxGoRoutine {
		for _, f := range ccf {
			go func(f concurrencyFunc) { ec <- f() }(f)
		}
	} else {
		c := make(chan concurrencyFunc, len)
		for _, f := range ccf {
			c <- f
		}
		for i := 0; i < maxGoRoutine; i++ {
			go func() {
				for {
					select {
					case f := <-c:
						ec <- f()
					default:
						return
					}
				}
			}()
		}

	}
	for e := range ec {
		if e != nil {
			return e
		}
		if len--; len == 0 {
			return nil
		}
	}

	return nil

}
