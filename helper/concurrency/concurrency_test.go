package concurrency

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestDoMax(t *testing.T) {
	var (
		cnt      int
		mtx      sync.Mutex
		numberCC = 1024000
	)
	c := New()
	for i := 0; i < numberCC; i++ {
		c.Add(func() error {
			time.Sleep(time.Millisecond)
			mtx.Lock()
			cnt = cnt + 1
			fmt.Println(cnt)
			mtx.Unlock()
			return nil
		})
	}
	c.Do()
	if cnt != numberCC {
		t.Errorf("cnt:%d,numberCC:%d", cnt, numberCC)
	}
}
