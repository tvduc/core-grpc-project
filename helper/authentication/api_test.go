package authentication

import (
	"context"
	"testing"

	pbApp "git.zapa.cloud/merchant-tools/protobuf/middleware/private-api/private-merchant-app"
	pbZas "git.zapa.cloud/merchant-tools/protobuf/middleware/private-api/private-merchant-zas"
)

var (
	keyMd5      = "rx71cvhIFKC0yIareOKb1BMNitVV9Ug8"
	saltKey     = "abcxyz"
	sigParams   = []string{"abc", "123"}
	privateKey  = "3d8517edb520256580233f56745da891"
	reqApp      = &pbApp.GetAppInfoRequest{}
	reqZas      = &pbZas.GetMerchantZIDRequest{}
	reqMerchant = &pbApp.GetMerchantInfoRequest{}
)

func BenchmarkZasValidate(b *testing.B) {
	h := &MiddlewareServiceAPIKeyAuth{}
	ctx := context.Background()
	reqZas.Sig = keyMd5 + ";456789"
	for n := 0; n < b.N; n++ {
		h.AuthorizeAPIKeyValidate(ctx, reqZas, "abc")
	}
}
func BenchmarkAppValidate(b *testing.B) {
	h := &MiddlewareServiceAPIKeyAuth{}
	ctx := context.Background()
	reqApp.Sig = keyMd5 + ";456789"
	for n := 0; n < b.N; n++ {
		h.AuthorizeAPIKeyValidate(ctx, reqApp, "abc")
	}
}
func BenchmarkMerchantValidate(b *testing.B) {
	h := &MiddlewareServiceAPIKeyAuth{}
	ctx := context.Background()
	reqMerchant.Sig = keyMd5 + ";456789"
	for n := 0; n < b.N; n++ {
		h.AuthorizeAPIKeyValidate(ctx, reqMerchant, "abc")
	}
}
