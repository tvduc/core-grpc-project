package authentication

import (
	
	"context"
	"errors"
	"fmt"
	"strings"

	"git.zapa.cloud/merchant-tools/helper/notify"

	"git.zapa.cloud/merchant-tools/helper/redis"

	//helperCrypto "git.zapa.cloud/merchant-tools/helper/crypto"
	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpcCodes "google.golang.org/grpc/codes"
	grpcStatus "google.golang.org/grpc/status"
)

type MethodList []string
type RateLimitMethod struct {
	Name                string
	RateNumberPerMinute int
}
type RateLimitMethodList []RateLimitMethod

type MiddlewareServiceAPIKeyAuth struct {
	MethodWhiteList  		MethodList
	RateLimitMethods 		RateLimitMethodList
	SalKey           		string
	RateLimitNumberDefault  int
}

func (h *MiddlewareServiceAPIKeyAuth) AuthorizeAPIKeyValidate(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error) {
	for _, method := range h.MethodWhiteList {
		if isMethodName(fullMethodName, method) == true {
			return ctx, nil
		}
	}
	var (
		apiKey string
		err    error
	)
	if f, ok := req.(interface {
		GetSig() string
	}); ok && f.GetSig() != "" {
		apiKey = f.GetSig()
	} else {
		apiKey, err = grpc_auth.AuthFromMD(ctx, "bearer")
		if err != nil {
			return ctx, err
		}
	}
	params := strings.SplitN(apiKey, ";", 2)
	if len(params) != 2 {
		glog.Errorln("Incorrect len bear: ", params)
		return ctx, grpcStatus.Errorf(grpcCodes.InvalidArgument, "Incorrect len bear: %d", len(params))
	}

	_, err = h.RateLimitValidate(ctx, fullMethodName, params[0])
	if err != nil {
		glog.Errorln("MiddlewareServiceAPIKeyAuth.RateLimitValidate: error = ", err)
		return nil, err
	}

	sigParams, err := getSigValues(req)
	if err != nil {
		return ctx, err
	}
	/*privateKey := fmt.Sprintf("%x", md5.Sum([]byte(params[0]+h.SalKey)))
	if params[1] != helperCrypto.HmacSha256Sum(privateKey, "|", sigParams...) {
		glog.Errorln(fmt.Sprintf("InCorrect checksum: privateKey = %s, publicKey = %s, reqHash = %s, sigParams = %s",
			privateKey, params[0], params[1], strings.Join(sigParams, "|")))
		return ctx, grpcStatus.Errorf(grpcCodes.InvalidArgument, "InCorrect checksum")
	}*/
	if !validSig(params[0],h.SalKey,params[1],sigParams...){
		/*glog.Errorln(fmt.Sprintf("InCorrect checksum: privateKey = %s, publicKey = %s, reqHash = %s, sigParams = %s",
			privateKey, params[0], params[1], strings.Join(sigParams, "|")))*/
		return ctx, grpcStatus.Errorf(grpcCodes.InvalidArgument, "InCorrect checksum")
	}
	return ctx, nil
}

func (h *MiddlewareServiceAPIKeyAuth) RateLimitValidate(ctx context.Context, fullMethodName, keyId string) (context.Context, error) {
	methodName := fullMethodName
	rateLimitNumber := h.RateLimitNumberDefault
	for _, limitMethod := range h.RateLimitMethods {
		if isMethodName(fullMethodName, limitMethod.Name) == true {
			if limitMethod.RateNumberPerMinute > 0 {
				methodName = limitMethod.Name
				rateLimitNumber = limitMethod.RateNumberPerMinute
				break
			}
		}
	}

	if rateLimitNumber > 0 {
		count, delay, ok := redis.CheckLimitAllowMinute("MiddlewareServiceAPIKeyAuth:"+methodName+":"+keyId, rateLimitNumber)
		if !ok {
			//Check if redis limit not yet ready
			if count == 0 && delay == 0 {
				errText := "MiddlewareServiceAPIKeyAuth.RateLimitValidate: redis limiter not yet ready"
				notify.SendToTeams("Middleware Rate Limit Validate", errText)
				glog.Errorln(errText)
				return ctx, nil
			}

			errText := fmt.Sprintf("Rate limit validate: method = %s, account = %s, count = %v, delay = %v", methodName, keyId, count, delay)
			glog.Errorln(errText)
			return nil, errors.New(errText)
		}
	}
	return ctx, nil
}
