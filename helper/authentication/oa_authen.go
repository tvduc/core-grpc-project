package authentication

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	errorutil "git.zapa.cloud/merchant-tools/helper/error_util"
	oa_service "git.zapa.cloud/merchant-tools/protobuf/oa-service"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	md "google.golang.org/grpc/metadata"
	"strings"
)

const (
	oaTokenDataHeader = "oatokendata"
	oaTokenDataKey  = userSessionType(oaTokenDataHeader)
)

type MiddlewareOAAPIServiceAuth struct {
	MethodWhiteList MethodList
	AuthenCli       oa_service.OAServiceClient
}

//authorize oa api
func (h *MiddlewareOAAPIServiceAuth) AuthorizeOAApi(ctx context.Context, fullMethodName string) (context.Context, error) {
	for _, method := range h.MethodWhiteList {
		if isMethodName(fullMethodName, method) == true {
			return ctx, nil
		}
	}
	if h.AuthenCli == nil {
		return ctx, errorutil.ErrorSystem
	}

	token, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return ctx, err
	}

	tokenData, err := h.AuthenCli.GetTokenData(ctx, &oa_service.GetTokenDataRequest{
		Token: token,
	})
	data, err := json.Marshal(tokenData)
	if err != nil {
		return nil, errorutil.ErrorTokenExpired
	}

	ctx = md.AppendToOutgoingContext(ctx, oaTokenDataHeader, string(data[:]))
	//Support for GetUserSessionFromCtx2
	ctx = context.WithValue(ctx, oaTokenDataKey, tokenData)
	if err != nil { //fake data
		return nil, errorutil.ErrorTokenExpired
	}

	return ctx, nil
}

func AuthorizeWebhookCallBack(req interface{}, oaSecretKey string) error {

	var xzEventSignature, timeStamp, appId, originalBody string

	if f, ok := req.(interface {
		GetXZeventSignature() string
		GetTimestamp() string
		GetAppId() string
		GetOriginalBody() string
	}); ok && f.GetXZeventSignature() != "" && f.GetTimestamp() != "" && f.GetAppId() != "" && f.GetOriginalBody() != "" {
		xzEventSignature = f.GetXZeventSignature()
		timeStamp = f.GetTimestamp()
		appId = f.GetAppId()
		originalBody = f.GetOriginalBody()
	} else {
		return errorutil.ErrorAuthenticationFail
	}

	hasher := sha256.New()
	hasher.Write([]byte(appId + originalBody + timeStamp + oaSecretKey))
	if strings.HasSuffix(xzEventSignature, hex.EncodeToString(hasher.Sum(nil))) {
		return nil
	}

	return errorutil.ErrorAuthenticationFail
}

func GetOATokenDataFromCtx(ctx context.Context) (*oa_service.TokenData, error) {
	tokenData, ok := ctx.Value(oaTokenDataKey).(*oa_service.TokenData)
	if !ok {
		val, ok := metautils.ExtractIncoming(ctx)[oaTokenDataHeader]
		if !ok || len(val) == 0 {
			return nil, grpc.Errorf(codes.Unauthenticated, "Request unauthenticated with "+oaTokenDataHeader)

		}
		tokenData = &oa_service.TokenData{}
		if err := json.Unmarshal([]byte(val[0]), tokenData); err != nil {
			return nil, grpc.Errorf(codes.Unauthenticated, err.Error())
		}
	}
	return tokenData, nil
}
