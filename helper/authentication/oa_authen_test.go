package authentication

import (
	api_mep_oa "git.zapa.cloud/merchant-tools/protobuf/middleware/oa-webhook"
	"testing"
)

func OARequestValidationTest(b *testing.B) {
	h := &MiddlewareOAAPIServiceAuth{
		MapOASecretKey: map[string]string{
			"310639567074746165":"R4mZ6yQ1TnKT3Lji3rjN",
		},
	}
	req := &api_mep_oa.ReceivedSendMessageCallBackRequest{
		EventName: "oa_send_text",
		AppId:     "310639567074746165",
		Sender: &api_mep_oa.ReceivedSendMessageCallBackRequest_Sender{
			Id: "4447976214969282536",
		},
		Recipient: &api_mep_oa.ReceivedSendMessageCallBackRequest_Recipient{
			Id: "6465583283138181621",
		},
		Message: &api_mep_oa.ReceivedSendMessageCallBackRequest_Message{
			Text:  "Bạn đang tra cứu, đợi trong giây lát",
			MsgId: "81e6133fd972a92df062",
		},
		Timestamp:        "1582701820001",
		UserIdByApp:      "719184549140294099",
		XZeventSignature: "mac=eeee5b75e63f16dd8b41038de68b29c0216b83c774b14a046f67db9391034773",
		OriginalBody:     `{"event_name":"oa_send_text","app_id":"310639567074746165","sender":{"id":"4447976214969282536"},"recipient":{"id":"6465583283138181621"},"message":{"text":"Bạn đang tra cứu, đợi trong giây lát","msg_id":"81e6133fd972a92df062"},"timestamp":"1582701820001","user_id_by_app":"719184549140294099"}`,
	}

	for n := 0; n < b.N; n++ {
		h.AuthorizeOAApi(req, "ReceivedSendMessageCallBack")
	}

}
