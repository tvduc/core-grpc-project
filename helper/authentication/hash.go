package authentication

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash"
	"strings"
	"sync"
)

var (
	hashPool = map[string]*sync.Pool{}
	mu       = sync.RWMutex{}
)

func getHashPool(key, saltKey string) *sync.Pool {
	mu.RLock()
	if p, ok := hashPool[key]; ok {
		mu.RUnlock()
		return p
	}
	mu.RUnlock()
	mu.Lock()
	p := &sync.Pool{New: func() interface{} {
		return hmac.New(sha256.New, []byte(fmt.Sprintf("%x", md5.Sum([]byte(key+saltKey)))))
	}}
	hashPool[key] = p
	mu.Unlock()
	return p

}
func validSig(key, saltKey, sig string, params ...string) bool {
	pool := getHashPool(key, saltKey)
	h, _ := pool.Get().(hash.Hash)
	if h == nil {
		return false
	}
	h.Reset()
	h.Write([]byte(strings.Join(params, "|")))
	v := sig == hex.EncodeToString(h.Sum(nil)) //fmt.Sprintf("%x", h.Sum(nil))
	pool.Put(h)
	return v

}
