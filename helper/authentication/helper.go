package authentication

import (
	"context"
	"encoding/json"
	"strings"

	"git.zapa.cloud/merchant-tools/helper/error_util"
	"git.zapa.cloud/merchant-tools/protobuf/user-session"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/qor/roles"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	md "google.golang.org/grpc/metadata"
)

type userSessionType string

const (
	userSessionHeader = "usersession"
	userSessionKey    = userSessionType(userSessionHeader)
)

type MiddlewareServiceAuth struct {
	AuthenCli usersession.UserSessionServiceClient
	//API Danh sach API can check Permission <method name, function code>
	APIFunctionCodes map[string]FunctionCodeList
}
type FunctionCodeList []string

func (functionCodeList FunctionCodeList) hasPermission(userSession *usersession.UserSession) bool {
	for _, functionCode := range functionCodeList {
		_, ok := userSession.GetPermissions()[functionCode]
		if ok {
			return true
		}
	}
	return false
}

type ServiceReqValidate struct {
}

func (s *ServiceReqValidate) ReqValidate(ctx context.Context, req interface{}, fullMethod string) (context.Context, error) {
	return ctx, nil
}

//ServiceAuth :
type ServiceAuth struct {
	AuthenCli usersession.UserSessionServiceClient
}
type PermissionList []*roles.Permission

//ServicePermission ...
type ServicePermission struct {
	//Danh sach cac API không cần check
	AllowAPIs []string
	//API Danh sach API can check Permission
	APIs map[string]PermissionList
	//Danh sách các Permission cần check
	Permissions PermissionList
}

func isMethodName(fullMethodName, methodName string) bool {
	return strings.HasSuffix(fullMethodName, "/"+methodName)
}
func (permissions PermissionList) hasPermission(mode roles.PermissionMode, userType string) bool {
	for _, permission := range permissions {
		if permission.HasPermission(roles.Read, userType) {
			return true
		}

	}
	return false
}

//PermissionValidate : ...
func (s *ServicePermission) PermissionValidate(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error) {
	//check AllowAPIS
	for _, api := range s.AllowAPIs {
		if isMethodName(fullMethodName, api) == true {
			if userSession, _ := GetUserSessionFromCtx(ctx); userSession != nil {
				ctx = context.WithValue(ctx, userSessionKey, userSession)
			}
			return ctx, nil
		}
	}
	//Check API
	for api, permissions := range s.APIs {
		if isMethodName(fullMethodName, api) == true {
			userSession, err := GetUserSessionFromCtx(ctx)
			if err == nil {
				if permissions.hasPermission(roles.Read, userSession.GetUserType().String()) {
					return context.WithValue(ctx, userSessionKey, userSession), nil
				}
			}
			return nil, errorutil.ErrorPermission
		}
	}
	//check permissions
	if len(s.Permissions) > 0 {
		userSession, err := GetUserSessionFromCtx(ctx)
		if err == nil {
			if s.Permissions.hasPermission(roles.Read, userSession.GetUserType().String()) {
				return context.WithValue(ctx, userSessionKey, userSession), nil
			}

		}
		return nil, errorutil.ErrorPermission
	}
	return ctx, nil

}
func getToken(ctx context.Context, req interface{}) (string, error) {
	if f, ok := req.(interface {
		GetBearer() string
	}); ok {
		token := f.GetBearer()
		if len(token) > 0 {
			return token, nil
		}
	}
	return grpc_auth.AuthFromMD(ctx, "bearer")
}

//AuthorizeValidate : Override hàm AuthenFunc
func (h *ServiceAuth) AuthorizeValidate(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error) {

	token, err := getToken(ctx, req) //grpc_auth.AuthFromMD(ctx, "bearer")
	var userSession *usersession.UserSession
	if err == nil {
		userSession, err = h.AuthenCli.GetUserSession(ctx, &usersession.GetUserSessionRequest{Token: token})
	}
	if err != nil { //fake data
		return nil, errorutil.ErrorTokenExpired
	}
	data, err := json.Marshal(userSession)
	if err != nil {
		return nil, err
	}
	ctx = md.AppendToOutgoingContext(ctx, userSessionHeader, string(data[:]))

	//Support for GetUserSessionFromCtx2
	ctx = context.WithValue(ctx, userSessionKey, userSession)

	return ctx, nil
}

//GetUserSessionFromCtx Lấy thông tin usersession từ context:
// Return lỗi nếu như không thể decode được context
//Return thông tin usersesion nếu thành công
func GetUserSessionFromCtx(ctx context.Context) (*usersession.UserSession, error) {
	val, ok := metautils.ExtractIncoming(ctx)[userSessionHeader]
	if !ok || len(val) == 0 {
		return nil, grpc.Errorf(codes.Unauthenticated, "Request unauthenticated with "+userSessionHeader)

	}
	userSession := &usersession.UserSession{}
	if err := json.Unmarshal([]byte(val[0]), userSession); err != nil {
		return nil, grpc.Errorf(codes.Unauthenticated, err.Error())
	}

	userSession = GetSystemType(userSession)

	return userSession, nil
}

//GetUserSessionFromCtx2 Lấy thông tin usersession từ context:
//Return lỗi nếu như không tồn tại trong context
//Return thông tin usersesion nếu thành công
func GetUserSessionFromCtx2(ctx context.Context) (*usersession.UserSession, error) {
	userSession, ok := ctx.Value(userSessionKey).(*usersession.UserSession)
	if !ok {
		return GetUserSessionFromCtx(ctx)
	}
	return userSession, nil
}

//GetPermission ...
func GetPermission(ctx context.Context, permission *roles.Permission) (*usersession.UserSession, error) {
	userSession, err := GetUserSessionFromCtx(ctx)
	if err != nil {
		return nil, err
	}
	if permission.HasPermission(roles.Read, userSession.GetUserType().String()) {
		return userSession, nil
	}
	return nil, errorutil.ErrorPermission
}

//AuthorizeValidate ....
func (s *MiddlewareServiceAuth) AuthorizeValidate(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error) {
	//glog.Info("MiddlewareServiceAuth AuthFuncOverride: full method name = ", fullMethodName)

	token, err := getToken(ctx, req) //grpc_auth.AuthFromMD(ctx, "bearer")
	var userSession *usersession.UserSession
	if err == nil {
		userSession, err = s.AuthenCli.GetUserSession(ctx, &usersession.GetUserSessionRequest{Token: token})
	}
	if err != nil { //fake data
		return nil, errorutil.ErrorTokenExpired
	}
	data, err := json.Marshal(userSession)
	if err != nil {
		return nil, err
	}

	ctx = md.AppendToOutgoingContext(ctx, userSessionHeader, string(data[:]))
	//Support for GetUserSessionFromCtx2
	ctx = context.WithValue(ctx, userSessionKey, userSession)

	for api, functionCode := range s.APIFunctionCodes {
		if isMethodName(fullMethodName, api) == true {
			ok := functionCode.hasPermission(userSession)
			if ok {
				return ctx, nil
			}
			return nil, errorutil.ErrorPermission
		}
	}

	return ctx, nil
}

func GetSystemType(userSession *usersession.UserSession) *usersession.UserSession {
	switch userSession.GetUserType() {
	case usersession.UserType_MERCHANT_SUPPORT_ADMIN, usersession.UserType_MERCHANT_SUPER_ADMIN,
		usersession.UserType_MERCHANT_ADMIN, usersession.UserType_MERCHANT_OPERATION:
		userSession.SystemType = usersession.SystemType_SYSTEM_MERCHANT
	case usersession.UserType_BACKOFFICE_ADMIN, usersession.UserType_BACKOFFICE_BD,
		usersession.UserType_BACKOFFICE_RISK, usersession.UserType_BACKOFFICE_LEGAL,
		usersession.UserType_BACKOFFICE_HOBD, usersession.UserType_BACKOFFICE_PH,
		usersession.UserType_BACKOFFICE_OP, usersession.UserType_BACKOFFICE_FA,
		usersession.UserType_BACKOFFICE_USER:
		userSession.SystemType = usersession.SystemType_SYSTEM_BACKOFFICE
	default:
		userSession.SystemType = usersession.SystemType_SYSTEM_NONE
	}

	return userSession
}

// UnaryServerInterceptor returns a new unary server interceptors that performs per-request auth.
func UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx := ctx
		var err error
		if f, ok := info.Server.(interface {
			AuthorizeValidate(context.Context, interface{}, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.AuthorizeValidate(ctx, req, info.FullMethod); err != nil {
				return nil, err
			}
		}
		if f, ok := info.Server.(interface {
			PermissionValidate(context.Context, interface{}, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.PermissionValidate(newCtx, req, info.FullMethod); err != nil {
				return nil, err
			}
		}
		if f, ok := info.Server.(interface {
			AuthorizeAPIKeyValidate(context.Context, interface{}, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.AuthorizeAPIKeyValidate(newCtx, req, info.FullMethod); err != nil {
				return nil, err
			}
		}

		if f, ok := info.Server.(interface {
			ReqValidate(context.Context, interface{}, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.ReqValidate(newCtx, req, info.FullMethod); err != nil {
				return nil, err
			}
		}

		//authorize oa api
		if f, ok := info.Server.(interface {
			AuthorizeOAApi(context.Context, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.AuthorizeOAApi(newCtx, info.FullMethod); err != nil {
				return nil, err
			}
		}

		return handler(newCtx, req)
	}
}

// StreamServerInterceptor returns a new unary server interceptors that performs per-request auth.
func StreamServerInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx := stream.Context()
		var err error
		if f, ok := srv.(interface {
			AuthorizeValidate(context.Context, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.AuthorizeValidate(newCtx, info.FullMethod); err != nil {
				return err
			}
		}
		if f, ok := srv.(interface {
			PermissionValidate(context.Context, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.PermissionValidate(newCtx, info.FullMethod); err != nil {
				return err
			}
		}
		if f, ok := srv.(interface {
			AuthorizeAPIKeyValidate(context.Context, string) (context.Context, error)
		}); ok {
			if newCtx, err = f.AuthorizeAPIKeyValidate(newCtx, info.FullMethod); err != nil {
				return err
			}
		}

		wrapped := grpc_middleware.WrapServerStream(stream)
		/*
			if f, ok := srv.(ValidateReq); ok {
				if newCtx, err = f.ReqValidate(newCtx, wrapped,info.FullMethod); err != nil {
					return err
				}
			}*/

		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}
