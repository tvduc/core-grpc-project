package authentication

import (
	"fmt"
	"reflect"
	"sync"
)

var (
	fieldTbls = map[reflect.Type][]int{}
	fieldMu   sync.RWMutex
)

func getFields(req interface{}) []int {
	t := reflect.TypeOf(req)
	fieldMu.RLock()
	if f, ok := fieldTbls[t]; ok {
		fieldMu.RUnlock()
		return f
	}
	fieldMu.RUnlock()
	fieldMu.Lock()
	defer fieldMu.Unlock()
	fields := []int{}
	val := reflect.Indirect(reflect.ValueOf(req))
	vt := val.Type()
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if _, ok := field.Tag.Lookup("sig"); ok {
			fields = append(fields, i)
		}
	}
	fieldTbls[t] = fields
	return fields

}
func getSigValues(req interface{}) ([]string, error) {
	var (
		sigs []string
		ok   bool
	)
	val := reflect.Indirect(reflect.ValueOf(req))
	fields := getFields(req)
	if len(fields) > 0 {
		sigs = make([]string, len(fields))
	}
	for ii, i := range fields {
		v := val.Field(i).Interface()
		if sigs[ii], ok = v.(string); !ok {
			sigs[ii] = fmt.Sprintf("%v", v)
		}
	}
	return sigs, nil
}
