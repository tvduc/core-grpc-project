package kafka

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"git.zapa.cloud/merchant-tools/helper/concurrency"
	"git.zapa.cloud/merchant-tools/helper/notify"
	"github.com/golang/glog"
)

var (
	servicePrefixName = os.Getenv("SERVICE_PREFIX_NAME")
	//KafkaAddress ...
	KafkaAddress = getAddress()

	//For check kafka error => send mail
	once                sync.Once
	kafkaErrorList      = concurrency.NewConcurrentSlice(20)
	kafkaErrorPefixName = servicePrefixName
)

//GetTopic ...
func GetTopic(topic string) string {
	if servicePrefixName != "" && strings.HasPrefix(topic, servicePrefixName) == false {
		return servicePrefixName + topic
	}
	return topic
}
func getAddress() []string {
	addr := os.Getenv("KAFKA_ADDR")
	if len(addr) > 0 {
		glog.Infoln(addr)
		return strings.Split(addr, ",")
	}
	return nil
}

func SetKafkaErrorConfig(prefixName string) {
	kafkaErrorPefixName = strings.Replace(prefixName, ".", "", -1)
}

func addKafkaError(address []string, err error) {
	glog.Infoln("addKafkaError: " + strings.Join(address, ",") + ": " + err.Error())
	kafkaErrorList.Append(strings.Join(address, ",") + ": " + err.Error())
}

func processKafkaError() {
	once.Do(func() {
		go func() {
			signals := make(chan os.Signal, 1)
			signal.Notify(signals, os.Interrupt)

			tick := time.Tick(5 * time.Minute)
			for {
				select {
				case <-tick:
					errText := kafkaErrorList.ToStringAndClear()
					if len(errText) > 0 {
						sendKafkaAlert(errText)
					}
				case <-signals:
					glog.Infoln("receive signal interrupt")
					return
				}
			}
		}()
	})
}

func sendKafkaAlert(errText string) {
	subject := "Thông báo lỗi hệ thống kafka"
	if len(kafkaErrorPefixName) > 0 {
		subject = fmt.Sprintf("[%s] %s", kafkaErrorPefixName, subject)
	}

	notify.SendToTeams(subject, errText)
	glog.Infoln("sendKafkaAlert: " + errText)
}
