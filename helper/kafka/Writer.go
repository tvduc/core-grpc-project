package kafka

import (
	"encoding/json"
	"fmt"
	"time"

	"git.zapa.cloud/merchant-tools/helper/notify"

	"github.com/golang/glog"

	"github.com/Shopify/sarama"

	"git.zapa.cloud/merchant-tools/helper/config"
	kafka "github.com/Shopify/sarama"
)

//Writer ...
type Writer interface {
	WriteRaw([]byte)
	Write(kafka.Encoder)
	WriteRawByTopic([]byte, string)
	Close()
	WriteByTopic(interface{}, string) error
}
type writer struct {
	topics          []string
	producer        kafka.AsyncProducer
	maxMessageBytes int
}

func SyncWrite(cfg config.Kafka, topic string, v []byte) error {

	cfgKafka := kafka.NewConfig()
	cfgKafka.Producer.RequiredAcks = kafka.WaitForLocal
	cfgKafka.Producer.Return.Successes = true
	if len(KafkaAddress) > 0 {
		cfg.Addrs = KafkaAddress
	}
	producer, err := kafka.NewSyncProducer(cfg.Addrs, cfgKafka)
	if err != nil {
		return err
	}
	_, _, err = producer.SendMessage(&kafka.ProducerMessage{
		Topic: GetTopic(topic),
		Value: sarama.ByteEncoder(v),
	})
	return err

}

//SyncWriteAndClose ...
func SyncWriteAndClose(cfg config.Kafka, topic string, v []byte) error {
	if len(KafkaAddress) > 0 {
		cfg.Addrs = KafkaAddress
	}
	cfgKafka := kafka.NewConfig()
	cfgKafka.Producer.RequiredAcks = kafka.WaitForLocal
	cfgKafka.Producer.Return.Successes = true
	if cfg.MaxMessageBytes > 0 {
		cfgKafka.Producer.MaxMessageBytes = cfg.MaxMessageBytes
	} else {
		cfgKafka.Producer.MaxMessageBytes = 64 * 1000000
	}

	if cfg.Compress {
		cfgKafka.Producer.Compression = kafka.CompressionGZIP
	}

	producer, err := kafka.NewSyncProducer(cfg.Addrs, cfgKafka)
	if err != nil {
		return err
	}
	defer producer.Close()
	_, _, err = producer.SendMessage(&kafka.ProducerMessage{
		Topic: GetTopic(topic),
		Value: sarama.ByteEncoder(v),
	})
	return err

}

//CreateWriters ....
func CreateWriters(cfg config.Kafka) Writer {
	return createWriter(cfg.Addrs, cfg.Topics, cfg.MaxMessageBytes, cfg.Compress)
}

//CreateWriter ....
func createWriter(addrs, topics []string, maxMessageBytes int, compress bool) Writer {

	if len(KafkaAddress) > 0 {
		addrs = KafkaAddress
	}
	cfg := kafka.NewConfig()
	cfg.Producer.RequiredAcks = kafka.WaitForLocal
	cfg.Producer.Flush.Frequency = 50 * time.Millisecond
	if maxMessageBytes > 0 {
		cfg.Producer.MaxMessageBytes = maxMessageBytes
	}
	if compress {
		cfg.Producer.Compression = kafka.CompressionGZIP
	}

	producer, err := kafka.NewAsyncProducer(addrs, cfg)
	if err != nil {
		glog.Errorln(addrs, topics, err)

		return nil
	}
	go func() {

		for err := range producer.Errors() {
			notify.SendToTeams("err write kafka ", fmt.Sprintf("%v", err))

			//glog.Error("Failed to write entry:", err)
		}
	}()
	return &writer{topics: topics,
		producer:        producer,
		maxMessageBytes: cfg.Producer.MaxMessageBytes}

}
func (w *writer) Write(v kafka.Encoder) {
	if w == nil {
		glog.Errorln("Write is nil")
		return
	}
	for _, topic := range w.topics {
		w.producer.Input() <- &kafka.ProducerMessage{
			Topic: GetTopic(topic),
			Value: v,
		}
	}
}
func (w *writer) Close() {
	w.producer.AsyncClose()
}
func (w *writer) WriteRaw(v []byte) {
	if w == nil {
		glog.Errorln("Write is nil")
		return
	}
	for _, topic := range w.topics {

		w.producer.Input() <- &kafka.ProducerMessage{
			Topic: GetTopic(topic),
			Value: sarama.ByteEncoder(v),
		}
		n := len(v)
		if n > warningMessageSize || n >= w.maxMessageBytes {
			notify.SendToTeams(topic, fmt.Sprintf("%v:%v", n, w.maxMessageBytes))

		}
	}

}

const (
	warningMessageSize = 1024 * 1024
)

func (w *writer) WriteRawByTopic(v []byte, topicName string) {
	if w == nil {
		glog.Errorln("Write is nil")
		return
	}
	w.producer.Input() <- &kafka.ProducerMessage{
		Topic: GetTopic(topicName),
		Value: sarama.ByteEncoder(v),
	}
	n := len(v)
	if n > warningMessageSize || n >= w.maxMessageBytes {
		notify.SendToTeams(topicName, fmt.Sprintf("%v:%v", n, w.maxMessageBytes))

	}
}

func (w *writer) WriteByTopic(v interface{}, topicName string) error {
	if data, err := json.Marshal(v); err == nil {
		w.producer.Input() <- &kafka.ProducerMessage{
			Topic: GetTopic(topicName),
			Value: sarama.ByteEncoder(data),
		}
		n := len(data)
		if n > warningMessageSize || n >= w.maxMessageBytes {
			notify.SendToTeams(topicName, fmt.Sprintf("%v:%v", n, w.maxMessageBytes))

		}
		return nil
	} else {
		return err
	}

}
