package kafka

import (
	"time"

	"git.zapa.cloud/merchant-tools/helper/concurrency"
	"git.zapa.cloud/merchant-tools/helper/graceful"

	"git.zapa.cloud/merchant-tools/helper/config"
	"github.com/Shopify/sarama"
	kafka "github.com/bsm/sarama-cluster"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
)

//ReadCB ...
type ReadCB func([]byte)
type ReadExCB func(string, []byte)
type ReadCBWithErr func(string, []byte) error

//Reader ...
type Reader interface {
	Read(cb ReadCB)
	ReadEx(cb ReadExCB)
	ReadWithErr(cb ReadCBWithErr, q *redis.Client)
}
type reader struct {
	consumer *kafka.Consumer
	topics   []string
	group    string
}

//CreateReaders ...
func CreateReaders(cfg config.Kafka) Reader {
	processKafkaError()
	config := kafka.NewConfig()
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	if cfg.Newest {
		config.Consumer.Offsets.Initial = sarama.OffsetNewest
	} else {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}
	if len(KafkaAddress) > 0 {
		cfg.Addrs = KafkaAddress
	}
	topics := []string{}
	for _, topic := range cfg.Topics {
		topics = append(topics, GetTopic(topic))
	}
	c, err := kafka.NewConsumer(cfg.Addrs, cfg.Group, topics, config)

	if err != nil {
		glog.Errorln(err)
		return &reader{}
	}
	// consume errors
	go func() {
		for err := range c.Errors() {
			glog.Errorln(err)
			addKafkaError(cfg.Addrs, err)
		}
	}()

	// consume notifications
	go func() {
		for ntf := range c.Notifications() {
			glog.Infoln(ntf)
		}
	}()
	return &reader{consumer: c, topics: topics, group: cfg.Group}
}

func (r *reader) Read(cb ReadCB) {
	if r.consumer == nil {
		glog.Errorln("Consumer is nil")
		return
	}
	stop := make(chan bool, 1)
	consumer := r.consumer
	graceful.Stop(0, func() {
		stop <- true
	})
	for {
		select {
		case msg, ok := <-consumer.Messages():
			if ok {
				graceful.AddProcess()
				cb(msg.Value)
				//fmt.Fprintf(os.Stdout, "%s/%d/%d\t%s\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Key, msg.Value)
				consumer.MarkOffset(msg, "") // mark message as processed
				graceful.DoneProcess()

			}
		case <-stop:
			graceful.ShutDown()
			return
		}
	}

}

func (r *reader) ReadEx(cb ReadExCB) {
	if r.consumer == nil {
		glog.Errorln("Consumer is nil")
		return
	}
	stop := make(chan bool, 1)
	consumer := r.consumer
	graceful.Stop(0, func() {
		stop <- true
	})
	for {
		select {
		case msg, ok := <-consumer.Messages():
			if ok {
				graceful.AddProcess()
				cb(msg.Topic, msg.Value)
				//fmt.Fprintf(os.Stdout, "%s/%d/%d\t%s\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Key, msg.Value)
				consumer.MarkOffset(msg, "") // mark message as processed
				graceful.DoneProcess()

			}
		case <-stop:
			graceful.ShutDown()
			return
		}
	}
}
func (r *reader) retryMessage(cb ReadCBWithErr, q *redis.Client) {
	cc := concurrency.New()
	for _, topic := range r.topics {
		_topic := topic
		key := r.group + topic
		cc.Add(func() error {
			datas, err := q.LRange(key, 0, -1).Result()
			if err != nil {
				glog.Errorln(err)
				return nil
			}
			if len(datas) <= 0 {
				return nil
			}
			q.Del(key)
			glog.Infoln("Restore ", len(datas))
			for _, data := range datas {
				if err = cb(_topic, []byte(data)); err != nil {
					glog.Errorln(_topic, err)
				}
			}
			return nil

		})
	}
	cc.Do()

}
func (r *reader) ReadWithErr(cb ReadCBWithErr, q *redis.Client) {
	if r.consumer == nil {
		glog.Errorln("Consumer is nil")
		return
	}
	done := make(chan bool, 1)
	if q != nil {
		r.retryMessage(cb, q)
		go func() {
			for {
				select {
				case <-done:
					return
				case <-time.After(time.Hour):
					{
						r.retryMessage(cb, q)
					}
				}

			}
		}()
	}

	const nWoker = 64
	consumer := r.consumer
	messages := consumer.Messages()
	for i := 0; i < nWoker; i++ {
		go func() {
			for msg := range messages {
				graceful.AddProcess()
				if err := cb(msg.Topic, msg.Value); err != nil {
					glog.Errorln(msg.Topic, err)
					key := r.group + msg.Topic
					if q != nil {
						if n, err := q.LLen(key).Result(); err == nil && n < 4096 {
							q.LPush(key, msg.Value)
						} else {
							glog.Errorln("Size queue ", n)
						}
					}
				}
				consumer.MarkOffset(msg, "") // mark message as processed
				graceful.DoneProcess()
			}

		}()
	}
	graceful.Stop(0, func() {
		done <- true
		consumer.Close()
	})
	graceful.ShutDown()
}
