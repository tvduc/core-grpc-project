package shopify

import (
	"os"
	"strings"
	"sync"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/graceful"
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
)

func NewConsumer(topics []string, f func(topic string, data []byte)) {
	addrs := config.GlobalConfig.Kafka.Addrs //strings.Split(os.Getenv("KAFKA_ADDR"), ",")
	if len(addrs) == 0 {
		addrs = strings.Split(os.Getenv("KAFKA_ADDR"), ",")
	}
	if len(addrs) == 0 {
		glog.Errorln("Kafka empty addr")
		return
	}

	glog.Infoln("Kafka Addr", addrs)
	config := sarama.NewConfig()
	c, err := sarama.NewConsumer(addrs, config)
	if err != nil {
		glog.Errorln(err)
		return
	}
	pcList := []sarama.PartitionConsumer{}
	wg := sync.WaitGroup{}
	for _, topic := range topics {
		partitionList, err := c.Partitions(topic)
		if err != nil {
			glog.Errorln(topic, err)
			continue
		}
		for _, partition := range partitionList {
			glog.Infoln("Kafka Start ", topic, partition)
			pc, err := c.ConsumePartition(topic, partition, sarama.OffsetNewest)
			if err != nil {
				glog.Errorln(err)
				continue
			}
			pcList = append(pcList, pc)
			wg.Add(1)
			go func(pc sarama.PartitionConsumer, topic string) {
				for message := range pc.Messages() {
					f(topic, message.Value)
				}
				wg.Done()
			}(pc, topic)

		}
	}
	graceful.Stop(0, func() {
		for _, pc := range pcList {
			pc.AsyncClose()
		}
		wg.Wait()
		c.Close()

	})
}
