+ cho phép query/store các trường với giá trị là zero(0, false, "")
    - query
        cái cũ : int32 transaction_amount  = 1 [(es_proto.query) = {terms:"transaction_amount"}];
        cái mới : int32 transaction_amount = 1 [(es_proto.query_zero) ={query_zero:"transaction_amount"},
                                                (es_proto.query) = {terms:"transaction_amount"}];
        
    - store 
        cái cũ : int32   transaction_amount         = 1 [(es_proto.es) = {es:"transaction_amount"}];
        cái mới: int32   transaction_amount         = 1 [(es_proto.es) = {store_zero_value:"transaction_amount"}];


+ Dùng terms_zero  để query các doc với giá trị field là nil

+ Note:
    Nếu muốn query giá trị transaction_amount = 0 thì 
            + thêm tag query_zero
            + trường transaction_amount phải đánh kiểu store_zero_value
            + fefault truyền lên luôn bằng zero(0, false)
            + nếu muốn query all như cũ  ko truyền lên default thì ko dùng tag query_zero



run test:   
    nếu có lỗi /go.mod: reading https://sum.golang.org/lookup/git.zapa.cloud/merchant-tools/protobuf@v0.0.0-20191129035950-e146ee358739: 410 Gone => fix: export GOSUMDB=off, xoá file go.mod
    