package test

import (
	"encoding/json"
	reflect "reflect"
	"testing"

	"git.zapa.cloud/merchant-tools/helper/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	git_zapa_cloud_merchant_tools_helper_search_elastic "gopkg.in/olivere/elastic.v6"
)

func covertToJSON(resp *git_zapa_cloud_merchant_tools_helper_search_elastic.BoolQuery) string {
	src, err := resp.Source()
	if err != nil {
		return ""
	}
	queryJSON, err := json.Marshal(src)
	if err != nil {
		return ""
	}
	// fmt.Println(string(queryJSON))
	return string(queryJSON)
}

func TestIntoRangeDate(t *testing.T) {

	req := &ListCITransactionsRequest{
		PointDate: &proto.Date{Year: 2019, Month: 9, Day: 26},
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":[{"range":{"start_time":{"from":null,"include_lower":true,"include_upper":true,"to":1569430800000}}},{"range":{"end_time":{"from":1569430800000,"include_lower":true,"include_upper":true,"to":null}}}],"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestMatchPhrasePrefix(t *testing.T) {

	req := &ListCITransactionsRequest{
		MTransId: "9833276237343",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must":{"multi_match":{"fields":["mTransId.search","mTransId.search_reverse"],"max_expansions":1024,"query":"9833276237343","slop":2,"tie_breaker":0,"type":"phrase_prefix"}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestTimeStamp(t *testing.T) {
	req := &ListCITransactionsRequest{
		CreateTime: &timestamp.Timestamp{Seconds: 60},
		EndTime:    &timestamp.Timestamp{Seconds: 70},
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":{"range":{"reqTimdestamp":{"from":60000,"include_lower":true,"include_upper":true,"to":70000}}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}

	req1 := &ListCITransactionsRequest{
		CreateTime: &timestamp.Timestamp{Seconds: 60},
	}
	resp1 := req1.BuildQuery(nil)

	got1 := covertToJSON(resp1)
	want1 := `{"bool":{"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want1, got1) {
		t.Fatalf("expected: %v, got: %v", want1, got1)
	}

}
func TestGteAndLt(t *testing.T) {

	req := &ListCITransactionsRequest{
		FromDate: &proto.Date{Year: 2019, Month: 9, Day: 23},
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}

	//  test 2
	req1 := &ListCITransactionsRequest{
		FromDate: &proto.Date{Year: 2019, Month: 9, Day: 23},
		ToDate:   &proto.Date{Year: 2019, Month: 9, Day: 26},
	}
	resp1 := req1.BuildQuery(nil)

	got1 := covertToJSON(resp1)
	want1 := `{"bool":{"filter":{"range":{"reqDate":{"from":"2019-09-23","include_lower":true,"include_upper":true,"time_zone":"+07:00","to":"2019-09-26"}}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want1, got1) {
		t.Fatalf("expected: %v, got: %v", want1, got1)
	}
	//  test 3
	req2 := &ListCITransactionsRequest{
		MTransId: "9833279999", // len>8
		FromDate: &proto.Date{Year: 2019, Month: 9, Day: 23},
		ToDate:   &proto.Date{Year: 2019, Month: 9, Day: 26},
	}
	resp2 := req2.BuildQuery(nil)

	got2 := covertToJSON(resp2)
	want2 := `{"bool":{"must":{"multi_match":{"fields":["mTransId.search","mTransId.search_reverse"],"max_expansions":1024,"query":"9833279999","slop":2,"tie_breaker":0,"type":"phrase_prefix"}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want2, got2) {
		t.Fatalf("expected: %v, got: %v", want2, got2)
	}
}

func TestTerms(t *testing.T) {

	req := &ListCITransactionsRequest{
		MId: "123456789",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":{"term":{"mId":"123456789"}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
	// test 2
	req1 := &ListCITransactionsRequest{
		TransStatus: 1,
	}
	resp1 := req1.BuildQuery(nil)

	got1 := covertToJSON(resp1)
	want1 := `{"bool":{"filter":{"term":{"transStatus":1}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want1, got1) {
		t.Fatalf("expected: %v, got: %v", want1, got1)
	}
}

func TestMatch(t *testing.T) {

	req := &ListCITransactionsRequest{
		Description: "test tag query:mt",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must":{"match":{"description":{"query":"test tag query:mt"}}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestNotTerms(t *testing.T) {

	req := &ListCITransactionsRequest{
		AppId: "98766555",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must_not":[{"term":{"appId":"98766555"}},{"exists":{"field":"query_term_zero"}}]}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestMultMatchPhrasePrefix(t *testing.T) {

	req := &ListCITransactionsRequest{
		Others: "others Field",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must":{"multi_match":{"fields":["receiver.search","receiver.search_reverse","description.search","description.search_reverse"],"max_expansions":1024,"query":"others Field","slop":2,"tie_breaker":0,"type":"phrase_prefix"}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}
func TestZpTranID(t *testing.T) {
	req := &ListCITransactionsRequest{
		ZpTransId: "9876556789",
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"must":{"multi_match":{"fields":["zpTransId.search","zpTransId.search_reverse"],"max_expansions":1024,"query":"9876556789","slop":2,"tie_breaker":0,"type":"phrase_prefix"}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestRepeated(t *testing.T) {
	req := &ListCITransactionsRequest{
		TranIds: []string{"abc", "cbd"},
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":{"terms":{"tran_id":["abc","cbd"]}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}
func TestEnumType(t *testing.T) {
	req := &ListCITransactionsRequest{
		TransStatus: 2,
	}
	resp := req.BuildQuery(nil)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":{"term":{"transStatus":2}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}
func TestNull(t *testing.T) {
	tables := []struct {
		input *ListCITransactionsRequest
	}{
		{&ListCITransactionsRequest{
			MTransId: "",
		},
		},
		{&ListCITransactionsRequest{
			ZpTransId: "",
		},
		},
		{&ListCITransactionsRequest{
			FromDate: nil,
		},
		},
		{&ListCITransactionsRequest{
			ToDate: nil,
		},
		},
		{&ListCITransactionsRequest{
			TransStatus: 0,
		},
		},
		{&ListCITransactionsRequest{
			MerchantId: "",
		},
		},
		{&ListCITransactionsRequest{
			Others: "",
		},
		},
		{&ListCITransactionsRequest{
			Description: "",
		},
		},
		{&ListCITransactionsRequest{
			AppId: "",
		},
		},
		{&ListCITransactionsRequest{
			AppUser: "",
		},
		},
	}
	for _, table := range tables {
		resp := table.input.BuildQuery(nil)
		got := covertToJSON(resp)
		want := `{"bool":{"must_not":{"exists":{"field":"query_term_zero"}}}}`
		if !reflect.DeepEqual(want, got) {
			t.Fatalf("expected: %v, got: %v", want, got)
		}
	}
}

func TestExistInput(t *testing.T) {
	req := &ListCITransactionsRequest{}
	boolQuery := git_zapa_cloud_merchant_tools_helper_search_elastic.NewBoolQuery()
	boolQuery = boolQuery.Filter(git_zapa_cloud_merchant_tools_helper_search_elastic.NewTermsQuery("transStatus", 4))
	resp := req.BuildQuery(boolQuery)

	got := covertToJSON(resp)
	want := `{"bool":{"filter":{"terms":{"transStatus":[4]}},"must_not":{"exists":{"field":"query_term_zero"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestTermsZero(t *testing.T) {
	tables := []struct {
		input  *TermsZeroRequest
		output string
	}{
		{
			&TermsZeroRequest{
				ConfirmStatus: TermsZeroRequest_ConfirmStatus(-1),
			},
			`{"bool":{"must_not":[{"exists":{"field":"bool_type"}},{"exists":{"field":"int_type"}},{"exists":{"field":"string_type"}},{"exists":{"field":"arr_type"}}]}}`,
		},
		{
			&TermsZeroRequest{},
			`{"bool":{"must_not":[{"exists":{"field":"bool_type"}},{"exists":{"field":"int_type"}},{"exists":{"field":"string_type"}},{"exists":{"field":"arr_type"}},{"exists":{"field":"confirm_status"}}]}}`,
		},
		{
			&TermsZeroRequest{
				BoolType: true,
			},
			`{"bool":{"must":{"term":{"bool_type":true}},"must_not":[{"exists":{"field":"int_type"}},{"exists":{"field":"string_type"}},{"exists":{"field":"arr_type"}},{"exists":{"field":"confirm_status"}}]}}`,
		},
		{
			&TermsZeroRequest{
				BoolType:   true,
				StringType: "StringType",
			},
			`{"bool":{"must":[{"term":{"bool_type":true}},{"term":{"string_type":"StringType"}}],"must_not":[{"exists":{"field":"int_type"}},{"exists":{"field":"arr_type"}},{"exists":{"field":"confirm_status"}}]}}`,
		},
	}
	for _, table := range tables {
		resp := table.input.BuildQuery(nil)
		got := covertToJSON(resp)
		want := table.output
		if !reflect.DeepEqual(want, got) {
			t.Fatalf("expected: %v, got: %v", want, got)
		}
	}
}
