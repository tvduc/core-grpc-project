// Code generated by protoc-gen-go. DO NOT EDIT.
// source: buildquery.proto

package test

import (
	fmt "fmt"
	_ "git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es/protobuf"
	proto1 "git.zapa.cloud/merchant-tools/helper/proto"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TransStatus int32

const (
	TransStatus_TS_UNSPECIFIED TransStatus = 0
	TransStatus_TS_FAIL        TransStatus = 1
	TransStatus_TS_SUCCESSFUL  TransStatus = 2
	TransStatus_TS_ALL         TransStatus = 3
	TransStatus_TS_NEW         TransStatus = 4
)

var TransStatus_name = map[int32]string{
	0: "TS_UNSPECIFIED",
	1: "TS_FAIL",
	2: "TS_SUCCESSFUL",
	3: "TS_ALL",
	4: "TS_NEW",
}

var TransStatus_value = map[string]int32{
	"TS_UNSPECIFIED": 0,
	"TS_FAIL":        1,
	"TS_SUCCESSFUL":  2,
	"TS_ALL":         3,
	"TS_NEW":         4,
}

func (x TransStatus) String() string {
	return proto.EnumName(TransStatus_name, int32(x))
}

func (TransStatus) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_16afcc3b6a3c36cf, []int{0}
}

type TermsZeroRequest_ConfirmStatus int32

const (
	//Không xác định
	TermsZeroRequest_DSS_UNSPECIFIED TermsZeroRequest_ConfirmStatus = 0
	//Chưa xác nhận
	TermsZeroRequest_DSS_NOT_CONFIRMED TermsZeroRequest_ConfirmStatus = 1
	//Đã xác nhận
	TermsZeroRequest_DSS_CONFIRMED TermsZeroRequest_ConfirmStatus = 2
	// Tất cả trạng thái
	TermsZeroRequest_DSS_NONE TermsZeroRequest_ConfirmStatus = -1
)

var TermsZeroRequest_ConfirmStatus_name = map[int32]string{
	0:  "DSS_UNSPECIFIED",
	1:  "DSS_NOT_CONFIRMED",
	2:  "DSS_CONFIRMED",
	-1: "DSS_NONE",
}

var TermsZeroRequest_ConfirmStatus_value = map[string]int32{
	"DSS_UNSPECIFIED":   0,
	"DSS_NOT_CONFIRMED": 1,
	"DSS_CONFIRMED":     2,
	"DSS_NONE":          -1,
}

func (x TermsZeroRequest_ConfirmStatus) String() string {
	return proto.EnumName(TermsZeroRequest_ConfirmStatus_name, int32(x))
}

func (TermsZeroRequest_ConfirmStatus) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_16afcc3b6a3c36cf, []int{1, 0}
}

type ListCITransactionsRequest struct {
	// @inject_tag: query:"mTransId,*%*"
	MTransId string `protobuf:"bytes,1,opt,name=m_trans_id,json=mTransId,proto3" json:"m_trans_id,omitempty"`
	// @inject_tag: query:"zpTransId,*%*"
	ZpTransId string `protobuf:"bytes,2,opt,name=zp_trans_id,json=zpTransId,proto3" json:"zp_trans_id,omitempty"`
	// @inject_tag: query:"reqDate,>="
	FromDate *proto1.Date `protobuf:"bytes,3,opt,name=from_date,json=fromDate,proto3" json:"from_date,omitempty"`
	// @inject_tag: query:"reqDate,<"
	ToDate *proto1.Date `protobuf:"bytes,4,opt,name=to_date,json=toDate,proto3" json:"to_date,omitempty"`
	// @inject_tag: query:"transStatus,="
	TransStatus TransStatus `protobuf:"varint,5,opt,name=trans_status,json=transStatus,proto3,enum=test.TransStatus" json:"trans_status,omitempty"`
	// @inject_tag: query:"merchantId,="
	MerchantId string `protobuf:"bytes,6,opt,name=merchant_id,json=merchantId,proto3" json:"merchant_id,omitempty"`
	// @inject_tag: query:"mId,="
	MId string `protobuf:"bytes,7,opt,name=m_id,json=mId,proto3" json:"m_id,omitempty"`
	// summary
	ShowSummary   bool  `protobuf:"varint,8,opt,name=show_summary,json=showSummary,proto3" json:"show_summary,omitempty"`
	PageIndex     int32 `protobuf:"varint,9,opt,name=page_index,json=pageIndex,proto3" json:"page_index,omitempty"`
	PageSize      int32 `protobuf:"varint,10,opt,name=page_size,json=pageSize,proto3" json:"page_size,omitempty"`
	ShowStatistic bool  `protobuf:"varint,11,opt,name=show_statistic,json=showStatistic,proto3" json:"show_statistic,omitempty"`
	// @inject_tag: query:"receiver;description,*%*"
	Others string `protobuf:"bytes,12,opt,name=others,proto3" json:"others,omitempty"`
	// @inject_tag: query:"description,mt"
	Description string `protobuf:"bytes,13,opt,name=description,proto3" json:"description,omitempty"`
	AppId       string `protobuf:"bytes,14,opt,name=app_id,json=appId,proto3" json:"app_id,omitempty"`
	// @inject_tag: query:"appUser,match"
	AppUser string `protobuf:"bytes,15,opt,name=app_user,json=appUser,proto3" json:"app_user,omitempty"`
	// @inject_tag: query:"tran_id,="
	TranIds    []string             `protobuf:"bytes,16,rep,name=tran_ids,json=tranIds,proto3" json:"tran_ids,omitempty"`
	PointDate  *proto1.Date         `protobuf:"bytes,17,opt,name=pointDate,proto3" json:"pointDate,omitempty"`
	CreateTime *timestamp.Timestamp `protobuf:"bytes,18,opt,name=create_time,json=createTime,proto3" json:"create_time,omitempty"`
	EndTime    *timestamp.Timestamp `protobuf:"bytes,19,opt,name=end_time,json=endTime,proto3" json:"end_time,omitempty"`
	AppIds     []string             `protobuf:"bytes,20,rep,name=app_ids,json=appIds,proto3" json:"app_ids,omitempty"`
	// summary
	TestQueryZero        int32    `protobuf:"varint,21,opt,name=test_query_zero,json=testQueryZero,proto3" json:"test_query_zero,omitempty"`
	QueryTermZero        bool     `protobuf:"varint,22,opt,name=query_term_zero,json=queryTermZero,proto3" json:"query_term_zero,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ListCITransactionsRequest) Reset()         { *m = ListCITransactionsRequest{} }
func (m *ListCITransactionsRequest) String() string { return proto.CompactTextString(m) }
func (*ListCITransactionsRequest) ProtoMessage()    {}
func (*ListCITransactionsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_16afcc3b6a3c36cf, []int{0}
}

func (m *ListCITransactionsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListCITransactionsRequest.Unmarshal(m, b)
}
func (m *ListCITransactionsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListCITransactionsRequest.Marshal(b, m, deterministic)
}
func (m *ListCITransactionsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListCITransactionsRequest.Merge(m, src)
}
func (m *ListCITransactionsRequest) XXX_Size() int {
	return xxx_messageInfo_ListCITransactionsRequest.Size(m)
}
func (m *ListCITransactionsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListCITransactionsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListCITransactionsRequest proto.InternalMessageInfo

func (m *ListCITransactionsRequest) GetMTransId() string {
	if m != nil {
		return m.MTransId
	}
	return ""
}

func (m *ListCITransactionsRequest) GetZpTransId() string {
	if m != nil {
		return m.ZpTransId
	}
	return ""
}

func (m *ListCITransactionsRequest) GetFromDate() *proto1.Date {
	if m != nil {
		return m.FromDate
	}
	return nil
}

func (m *ListCITransactionsRequest) GetToDate() *proto1.Date {
	if m != nil {
		return m.ToDate
	}
	return nil
}

func (m *ListCITransactionsRequest) GetTransStatus() TransStatus {
	if m != nil {
		return m.TransStatus
	}
	return TransStatus_TS_UNSPECIFIED
}

func (m *ListCITransactionsRequest) GetMerchantId() string {
	if m != nil {
		return m.MerchantId
	}
	return ""
}

func (m *ListCITransactionsRequest) GetMId() string {
	if m != nil {
		return m.MId
	}
	return ""
}

func (m *ListCITransactionsRequest) GetShowSummary() bool {
	if m != nil {
		return m.ShowSummary
	}
	return false
}

func (m *ListCITransactionsRequest) GetPageIndex() int32 {
	if m != nil {
		return m.PageIndex
	}
	return 0
}

func (m *ListCITransactionsRequest) GetPageSize() int32 {
	if m != nil {
		return m.PageSize
	}
	return 0
}

func (m *ListCITransactionsRequest) GetShowStatistic() bool {
	if m != nil {
		return m.ShowStatistic
	}
	return false
}

func (m *ListCITransactionsRequest) GetOthers() string {
	if m != nil {
		return m.Others
	}
	return ""
}

func (m *ListCITransactionsRequest) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *ListCITransactionsRequest) GetAppId() string {
	if m != nil {
		return m.AppId
	}
	return ""
}

func (m *ListCITransactionsRequest) GetAppUser() string {
	if m != nil {
		return m.AppUser
	}
	return ""
}

func (m *ListCITransactionsRequest) GetTranIds() []string {
	if m != nil {
		return m.TranIds
	}
	return nil
}

func (m *ListCITransactionsRequest) GetPointDate() *proto1.Date {
	if m != nil {
		return m.PointDate
	}
	return nil
}

func (m *ListCITransactionsRequest) GetCreateTime() *timestamp.Timestamp {
	if m != nil {
		return m.CreateTime
	}
	return nil
}

func (m *ListCITransactionsRequest) GetEndTime() *timestamp.Timestamp {
	if m != nil {
		return m.EndTime
	}
	return nil
}

func (m *ListCITransactionsRequest) GetAppIds() []string {
	if m != nil {
		return m.AppIds
	}
	return nil
}

func (m *ListCITransactionsRequest) GetTestQueryZero() int32 {
	if m != nil {
		return m.TestQueryZero
	}
	return 0
}

func (m *ListCITransactionsRequest) GetQueryTermZero() bool {
	if m != nil {
		return m.QueryTermZero
	}
	return false
}

type TermsZeroRequest struct {
	BoolType             bool                           `protobuf:"varint,1,opt,name=bool_type,json=boolType,proto3" json:"bool_type,omitempty"`
	IntType              int64                          `protobuf:"varint,2,opt,name=int_type,json=intType,proto3" json:"int_type,omitempty"`
	StringType           string                         `protobuf:"bytes,3,opt,name=string_type,json=stringType,proto3" json:"string_type,omitempty"`
	ArrType              []string                       `protobuf:"bytes,4,rep,name=arr_type,json=arrType,proto3" json:"arr_type,omitempty"`
	ConfirmStatus        TermsZeroRequest_ConfirmStatus `protobuf:"varint,5,opt,name=confirm_status,json=confirmStatus,proto3,enum=test.TermsZeroRequest_ConfirmStatus" json:"confirm_status,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                       `json:"-"`
	XXX_unrecognized     []byte                         `json:"-"`
	XXX_sizecache        int32                          `json:"-"`
}

func (m *TermsZeroRequest) Reset()         { *m = TermsZeroRequest{} }
func (m *TermsZeroRequest) String() string { return proto.CompactTextString(m) }
func (*TermsZeroRequest) ProtoMessage()    {}
func (*TermsZeroRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_16afcc3b6a3c36cf, []int{1}
}

func (m *TermsZeroRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TermsZeroRequest.Unmarshal(m, b)
}
func (m *TermsZeroRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TermsZeroRequest.Marshal(b, m, deterministic)
}
func (m *TermsZeroRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TermsZeroRequest.Merge(m, src)
}
func (m *TermsZeroRequest) XXX_Size() int {
	return xxx_messageInfo_TermsZeroRequest.Size(m)
}
func (m *TermsZeroRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_TermsZeroRequest.DiscardUnknown(m)
}

var xxx_messageInfo_TermsZeroRequest proto.InternalMessageInfo

func (m *TermsZeroRequest) GetBoolType() bool {
	if m != nil {
		return m.BoolType
	}
	return false
}

func (m *TermsZeroRequest) GetIntType() int64 {
	if m != nil {
		return m.IntType
	}
	return 0
}

func (m *TermsZeroRequest) GetStringType() string {
	if m != nil {
		return m.StringType
	}
	return ""
}

func (m *TermsZeroRequest) GetArrType() []string {
	if m != nil {
		return m.ArrType
	}
	return nil
}

func (m *TermsZeroRequest) GetConfirmStatus() TermsZeroRequest_ConfirmStatus {
	if m != nil {
		return m.ConfirmStatus
	}
	return TermsZeroRequest_DSS_UNSPECIFIED
}

func init() {
	proto.RegisterEnum("test.TransStatus", TransStatus_name, TransStatus_value)
	proto.RegisterEnum("test.TermsZeroRequest_ConfirmStatus", TermsZeroRequest_ConfirmStatus_name, TermsZeroRequest_ConfirmStatus_value)
	proto.RegisterType((*ListCITransactionsRequest)(nil), "test.ListCITransactionsRequest")
	proto.RegisterType((*TermsZeroRequest)(nil), "test.TermsZeroRequest")
}

func init() { proto.RegisterFile("buildquery.proto", fileDescriptor_16afcc3b6a3c36cf) }

var fileDescriptor_16afcc3b6a3c36cf = []byte{
	// 993 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x54, 0x4f, 0x6f, 0xe3, 0x44,
	0x14, 0xc7, 0x9b, 0x36, 0x76, 0x9e, 0x9b, 0xd4, 0x99, 0xfe, 0x91, 0x29, 0x20, 0x4c, 0x05, 0x28,
	0x5a, 0xd1, 0x44, 0xb4, 0xe2, 0x40, 0x57, 0x8b, 0xb4, 0x49, 0x53, 0x64, 0xb6, 0x74, 0xe9, 0x38,
	0x15, 0x82, 0x8b, 0xe5, 0xc6, 0xd3, 0xd4, 0x52, 0xec, 0x71, 0x67, 0x26, 0x40, 0x73, 0xe4, 0x06,
	0xdf, 0x80, 0x2b, 0xc7, 0x7e, 0x10, 0x1f, 0xfc, 0xa5, 0x40, 0x33, 0x13, 0x37, 0xd9, 0x48, 0xa0,
	0xcd, 0x21, 0x7e, 0xef, 0xf7, 0x7e, 0xbf, 0x37, 0x33, 0x6f, 0xde, 0x3c, 0x70, 0x6e, 0x66, 0xc9,
	0x34, 0xbe, 0x9f, 0x11, 0xf6, 0xd0, 0xcd, 0x19, 0x15, 0x14, 0x6d, 0x08, 0xc2, 0xc5, 0xc1, 0xd5,
	0x24, 0x11, 0xdd, 0x79, 0x94, 0x47, 0xdd, 0xf1, 0x94, 0xce, 0xe2, 0x5e, 0x4a, 0xd8, 0xf8, 0x2e,
	0xca, 0xc4, 0x91, 0xa0, 0x74, 0xca, 0x7b, 0x77, 0x64, 0x9a, 0x13, 0xd6, 0x9b, 0xd0, 0xa3, 0x09,
	0xc9, 0x08, 0x8b, 0x04, 0x65, 0x3d, 0x25, 0x1f, 0x4b, 0xe0, 0x88, 0x70, 0xed, 0xdd, 0xcc, 0x6e,
	0x7b, 0x84, 0xeb, 0xc4, 0x07, 0x5f, 0xbd, 0x53, 0x4a, 0xc5, 0xed, 0x89, 0x87, 0x9c, 0x2c, 0x64,
	0x1f, 0x4f, 0x28, 0x9d, 0x4c, 0xc9, 0x32, 0xa1, 0x48, 0x52, 0xc2, 0x45, 0x94, 0xe6, 0x9a, 0x70,
	0xf8, 0x47, 0x03, 0xde, 0xbf, 0x48, 0xb8, 0x18, 0xf8, 0x23, 0x16, 0x65, 0x3c, 0x1a, 0x8b, 0x84,
	0x66, 0x1c, 0x93, 0xfb, 0x19, 0xe1, 0x02, 0x7d, 0x01, 0x90, 0x86, 0x42, 0x06, 0xc2, 0x24, 0x76,
	0x0d, 0xcf, 0xe8, 0x34, 0xfa, 0xad, 0xb2, 0xf0, 0x00, 0xac, 0x54, 0xf1, 0xfd, 0x18, 0x3f, 0x59,
	0xa8, 0x07, 0xf6, 0x3c, 0x5f, 0xd2, 0x9f, 0x29, 0xfa, 0x76, 0x59, 0x78, 0x36, 0x34, 0xe6, 0x79,
	0xc5, 0x5f, 0x9a, 0xe8, 0x25, 0x34, 0x6e, 0x19, 0x4d, 0xc3, 0x38, 0x12, 0xc4, 0xad, 0x79, 0x46,
	0xc7, 0x3e, 0x6e, 0x77, 0xf5, 0x8e, 0xbb, 0xea, 0x10, 0x67, 0x91, 0x20, 0xfd, 0x66, 0x59, 0x78,
	0x0d, 0x6c, 0x32, 0x72, 0x2f, 0x5d, 0x6c, 0x49, 0x89, 0xb4, 0xd0, 0xd7, 0x60, 0x0a, 0xaa, 0xc5,
	0x1b, 0xff, 0x2b, 0x66, 0x4f, 0xe2, 0xba, 0xa0, 0x4a, 0xfa, 0x2d, 0x6c, 0xe9, 0x7d, 0x72, 0x11,
	0x89, 0x19, 0x77, 0x37, 0x3d, 0xa3, 0xd3, 0x3a, 0x6e, 0x77, 0xe5, 0xf5, 0x75, 0xd5, 0xf6, 0x02,
	0x15, 0xe8, 0xb7, 0xcb, 0xc2, 0x6b, 0x9e, 0xda, 0x62, 0x09, 0xe1, 0x55, 0x07, 0x7d, 0x09, 0x76,
	0x75, 0x15, 0xf2, 0xcc, 0x75, 0x75, 0x66, 0xa7, 0x2c, 0xbc, 0xad, 0x53, 0xa8, 0x02, 0x7e, 0x8c,
	0x57, 0x6c, 0xf4, 0x21, 0x6c, 0xa4, 0x92, 0x6b, 0x2a, 0x6e, 0xa3, 0x2c, 0xbc, 0xcd, 0xd3, 0x5a,
	0xea, 0xc7, 0x58, 0xfe, 0xa1, 0x4f, 0x60, 0x8b, 0xdf, 0xd1, 0x5f, 0x43, 0x3e, 0x4b, 0xd3, 0x88,
	0x3d, 0xb8, 0x96, 0x67, 0x74, 0x2c, 0x6c, 0x4b, 0x2c, 0xd0, 0x10, 0xfa, 0x08, 0x20, 0x8f, 0x26,
	0x24, 0x4c, 0xb2, 0x98, 0xfc, 0xe6, 0x36, 0x3c, 0xa3, 0xb3, 0x89, 0x1b, 0x12, 0xf1, 0x25, 0x80,
	0x3e, 0x00, 0xe5, 0x84, 0x3c, 0x99, 0x13, 0x17, 0x54, 0xd4, 0x92, 0x40, 0x90, 0xcc, 0x09, 0xfa,
	0x0c, 0x5a, 0x3a, 0xbd, 0x88, 0x44, 0xc2, 0x45, 0x32, 0x76, 0x6d, 0xb5, 0x40, 0x53, 0x2d, 0x50,
	0x81, 0xe8, 0x18, 0xea, 0x54, 0xdc, 0x11, 0xc6, 0xdd, 0x2d, 0xb5, 0xcb, 0x83, 0xb2, 0xf0, 0xf6,
	0x61, 0x97, 0x91, 0x31, 0x49, 0x7e, 0x21, 0xec, 0x45, 0x4c, 0xf8, 0x98, 0x25, 0xb9, 0x6c, 0x16,
	0xbc, 0x60, 0xa2, 0x13, 0xb0, 0x57, 0x60, 0xb7, 0xa9, 0x84, 0xaa, 0x7e, 0xfd, 0xd5, 0x00, 0x5e,
	0x75, 0xd0, 0x21, 0xd4, 0xa3, 0x3c, 0x97, 0xe5, 0x68, 0x29, 0xbe, 0x5d, 0x16, 0x9e, 0x39, 0xdf,
	0x8c, 0xf2, 0xdc, 0x8f, 0xb1, 0xfe, 0xa0, 0x0e, 0x58, 0x92, 0x33, 0xe3, 0x84, 0xb9, 0xdb, 0x8a,
	0xa5, 0x6e, 0xf5, 0x3b, 0x33, 0xca, 0xf3, 0x6b, 0x4e, 0x18, 0xae, 0x0c, 0xc9, 0x94, 0x97, 0x13,
	0x26, 0x31, 0x77, 0x1d, 0xaf, 0x56, 0x31, 0x4f, 0xcd, 0x05, 0x8a, 0x95, 0xe1, 0xc7, 0x1c, 0xbd,
	0x86, 0x46, 0x4e, 0x93, 0x4c, 0xc8, 0x6e, 0x70, 0xdb, 0xff, 0xd5, 0x3d, 0xea, 0xd8, 0xbf, 0x1b,
	0x3b, 0x5c, 0x44, 0x4c, 0x84, 0xf2, 0xed, 0xbc, 0x20, 0x59, 0xac, 0x0c, 0xbc, 0xd4, 0xa3, 0x2b,
	0xb0, 0xc7, 0x8c, 0x44, 0x82, 0xa8, 0x88, 0x8b, 0x54, 0xba, 0x83, 0x2a, 0x5d, 0xf5, 0xf6, 0xba,
	0xa3, 0xea, 0xed, 0xf5, 0x77, 0xcb, 0xc2, 0x73, 0xfe, 0x34, 0x9a, 0x8c, 0xdc, 0x8f, 0x92, 0x34,
	0xd6, 0x28, 0x06, 0x9d, 0x44, 0xd2, 0xd0, 0x6b, 0xb0, 0xaa, 0x95, 0xdc, 0x9d, 0x77, 0xcb, 0xf7,
	0xf7, 0x7a, 0x3e, 0x93, 0x64, 0xb1, 0x4a, 0xf6, 0x39, 0x98, 0xba, 0xc8, 0xdc, 0xdd, 0x5d, 0x56,
	0x65, 0x5e, 0x81, 0xb8, 0xae, 0xea, 0xcc, 0xd1, 0x4b, 0xd8, 0x96, 0x0f, 0x20, 0x54, 0x13, 0x2d,
	0x9c, 0x13, 0x46, 0xdd, 0x3d, 0xd9, 0x3f, 0xfd, 0xbd, 0xb2, 0xf0, 0xda, 0xa7, 0xeb, 0x41, 0xdc,
	0x94, 0xc0, 0x95, 0xf4, 0x7f, 0x26, 0x8c, 0xa2, 0x6f, 0x60, 0x5b, 0x07, 0x05, 0x61, 0xa9, 0x96,
	0xef, 0xcb, 0xe6, 0xea, 0xef, 0x97, 0x85, 0x87, 0x1e, 0x8d, 0xf5, 0x28, 0x6e, 0x2a, 0x60, 0x44,
	0x58, 0x2a, 0xf5, 0x87, 0x7f, 0xd5, 0xc0, 0x91, 0x0e, 0x97, 0x5e, 0x35, 0x82, 0x8e, 0xa0, 0x71,
	0x43, 0xe9, 0x34, 0x94, 0x97, 0xa2, 0x26, 0x90, 0xa5, 0x9f, 0xd7, 0xa3, 0xb1, 0xc4, 0xb1, 0x25,
	0xcd, 0xd1, 0x43, 0x4e, 0xd0, 0x73, 0xb0, 0x92, 0x4c, 0x68, 0xb6, 0x1c, 0x40, 0x35, 0x3d, 0x80,
	0x1e, 0x8d, 0x27, 0x18, 0x9b, 0x49, 0x26, 0x14, 0xf7, 0x04, 0x6c, 0x2e, 0x58, 0x92, 0x4d, 0x34,
	0xbd, 0xa6, 0x5a, 0x0b, 0x95, 0x85, 0xd7, 0x7a, 0x34, 0x56, 0x23, 0x18, 0xb4, 0x53, 0x2d, 0x10,
	0x31, 0xa6, 0x15, 0x1b, 0xaa, 0x98, 0xd5, 0x02, 0x15, 0x8c, 0xcd, 0x88, 0x31, 0xc5, 0xbd, 0x81,
	0xd6, 0x98, 0x66, 0xb7, 0x09, 0x4b, 0xdf, 0x9e, 0x33, 0x9f, 0x2e, 0xe6, 0xcc, 0xda, 0x59, 0xbb,
	0x03, 0x4d, 0x5e, 0x8c, 0x1e, 0x55, 0xf4, 0x47, 0x63, 0x2d, 0x05, 0x6e, 0x8e, 0x57, 0x59, 0x87,
	0x04, 0x9a, 0x6f, 0xc9, 0xd0, 0x0e, 0x6c, 0x9f, 0x05, 0x41, 0x78, 0x7d, 0x19, 0xfc, 0x30, 0x1c,
	0xf8, 0xe7, 0xfe, 0xf0, 0xcc, 0x79, 0x0f, 0xed, 0x41, 0x5b, 0x82, 0x97, 0x6f, 0x46, 0xe1, 0xe0,
	0xcd, 0xe5, 0xb9, 0x8f, 0xbf, 0x1f, 0x9e, 0x39, 0x06, 0x6a, 0x43, 0x53, 0xc2, 0x4b, 0xe8, 0x19,
	0xda, 0x03, 0x4b, 0x33, 0x2f, 0x87, 0xce, 0x3f, 0xd5, 0xcf, 0x78, 0xfe, 0x13, 0xd8, 0x2b, 0x63,
	0x11, 0x21, 0x68, 0x8d, 0xd6, 0xd7, 0xb0, 0xc1, 0x1c, 0x05, 0xe1, 0xf9, 0x2b, 0xff, 0x42, 0x67,
	0x1e, 0x05, 0x61, 0x70, 0x3d, 0x18, 0x0c, 0x83, 0xe0, 0xfc, 0xfa, 0xc2, 0x79, 0x86, 0x00, 0xea,
	0xa3, 0x20, 0x7c, 0x75, 0x71, 0xe1, 0xd4, 0x16, 0xf6, 0xe5, 0xf0, 0x47, 0x67, 0xe3, 0xa6, 0xae,
	0x1a, 0xfa, 0xe4, 0xdf, 0x00, 0x00, 0x00, 0xff, 0xff, 0xf2, 0xf8, 0x8d, 0x7b, 0x4e, 0x07, 0x00,
	0x00,
}
