package test

import (
	"encoding/json"
	fmt "fmt"
	reflect "reflect"
	"testing"

	"git.zapa.cloud/merchant-tools/helper/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
)

func TestAllTypeMapping(t *testing.T) {
	esMap := map[string]interface{}{}
	trans := TransactionMessage3{
		CreateTime:        &timestamp.Timestamp{Seconds: 60},
		ToDate:            &proto.Date{Year: 2019, Month: 9, Day: 23},
		TransChargeStatus: 12,
		UserInfo: &TransactionMessage3_UserInfo{
			Email: "tvduc@gmail.com",
			Phone: "0364859473",
		},
	}
	trans.EsMapping(esMap)
	src, _ := json.Marshal(esMap)
	// fmt.Println(string(src))
	got := string(src)
	want := `{"disCountAmount":{"type":"long"},"itemCount":{"type":"integer"},"merchantName":{"type":"keyword"},"reqDate":{"type":"date"},"reqTimdestamp":{"type":"date"},"transChargeStatus":{"type":"keyword"},"userInfo":{"properties":{"address":{"properties":{"thon":{"type":"keyword"}}},"birthday":{"type":"date"},"email":{"type":"keyword"},"phoneNumber":{"type":"keyword"}}}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}

}

func TestProductInfo(t *testing.T) {
	apps := []*AppInfo{}
	esMap := map[string]interface{}{}
	productInfo := ProductInfo{
		ProductCode: "abcxyz",
		Apps:        apps,
	}
	productInfo.EsMapping(esMap)
	src, _ := json.Marshal(esMap)
	fmt.Println(string(src))
	got := string(src)
	want := `{"apps.name":{"fields":{"search":{"analyzer":"search","search_analyzer":"search","type":"text"},"search_reverse":{"analyzer":"search_reverse","search_analyzer":"search_reverse","type":"text"}},"type":"keyword"},"apps.publisher":{"type":"keyword"},"apps.status":{"type":"keyword"},"product_code":{"type":"keyword"}}`
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}
