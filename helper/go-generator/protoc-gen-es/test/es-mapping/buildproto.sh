#!/bin/bash
protoc -I/usr/local/include -I. \
  -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  -I${GOPATH}/src/git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es \
  --go_out=plugins=grpc:. --grpc-gateway_out=logtostderr=true:. \
  --es_out="lang=go:." \
  *.proto 
  

