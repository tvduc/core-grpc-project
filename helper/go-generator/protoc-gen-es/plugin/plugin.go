package plugin

import (
	"strings"

	"git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es/helper"
	esProto "git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es/protobuf"
	"github.com/gogo/protobuf/proto"
	"github.com/gogo/protobuf/protoc-gen-gogo/descriptor"
	"github.com/gogo/protobuf/protoc-gen-gogo/generator"
	"github.com/golang/glog"
)

type pkgImport struct {
	name     string
	isImport bool
}
type es struct {
	*generator.Generator
	// querierPkg generator.Single
	glogPkg, elasticPkg, reflectPkg, timePkg, pbTimePkg, ptypesPkg, helperPkg, stringsPkg generator.GoImportPath
	packageName                                                                           map[generator.GoImportPath]*pkgImport
}

// NewToEs ...
func NewToEs() generator.Plugin {

	return &es{
		// query: query,
		glogPkg:     "github.com/golang/glog",
		elasticPkg:  "gopkg.in/olivere/elastic.v6",
		reflectPkg:  "reflect",
		timePkg:     "time",
		pbTimePkg:   "github.com/golang/protobuf/ptypes/timestamp",
		ptypesPkg:   "github.com/golang/protobuf/ptypes",
		helperPkg:   "git.zapa.cloud/merchant-tools/helper/go-generator/protoc-gen-es/helper",
		stringsPkg:  "strings",
		packageName: make(map[generator.GoImportPath]*pkgImport),
	}
}
func (c *es) GenerateImports(file *generator.FileDescriptor) {
	for _, pkg := range c.packageName {
		if !pkg.isImport {
			pkg.isImport = true
			//c.Generator.PrintImport(generator.GoPackageName(pkg.name), generator.GoImportPath(path))
		}
	}
	//log.Println(c.packageName)

}
func (c *es) Name() string {
	return "es"
}
func (c *es) getPacketName(path generator.GoImportPath) string {
	pkg, ok := c.packageName[path]
	if !ok {
		pkg = &pkgImport{string(c.Generator.AddImport(path)), false}
		c.packageName[path] = pkg
	}

	return pkg.name
}

func (c *es) Init(g *generator.Generator) {
	c.Generator = g

}

func (c *es) Generate(file *generator.FileDescriptor) {
	//c.PluginImports = generator.NewPluginImports(c.Generator)
	//generator.RegisterUniquePackageName("proto", nil)
	//c.Generator.AddImport("github.com/gogo/protobuf/proto")
	if strings.Contains(file.GetName(), "/") {
		return
	}
	for _, msg := range file.Messages() {
		if msg.DescriptorProto.GetOptions().GetMapEntry() {
			continue
		}
		c.generateProto3Message(file, msg)
	}

}
func (c *es) generateEsMapping(file *generator.FileDescriptor, message *generator.Descriptor, ccTypeName string) {
	once := false
	genfunc := func() {
		if once {
			return
		}
		c.P(`func (this *`, ccTypeName, `) EsMapping(mappingEs map[string]interface{}) {`)
		c.In()
		once = true
	}

	for _, field := range message.Field {
		fieldExten := c.getFieldEsIfAny(field)
		fv := c.getFieldQueryIfAny(field)
		if fieldExten == nil {
			continue
		}

		genfunc()
		fieldName := c.GetOneOfFieldName(message, field)

		if fv != nil {
			if fv.GetMatchPhrasePrefix() != "" {
				c.P(`mappingEs["`, fv.GetMatchPhrasePrefix(), `"] = map[string]interface{}{"type": "keyword",`)

				c.P(`"fields": map[string]interface{}{`)
				c.P(`"search":         map[string]string{"type": "text", "analyzer": "search", "search_analyzer": "search"},`)
				c.P(`"search_reverse": map[string]string{"type": "text", "analyzer": "search_reverse", "search_analyzer": "search_reverse"}}}`)
			} else if fv.GetMatchPhrasePrefixLeft() != "" {
				c.P(`mappingEs["`, fv.GetMatchPhrasePrefixLeft(), `"] = map[string]interface{}{"type": "keyword",`)
				c.P(`"fields": map[string]interface{}{"search": map[string]string{"type": "text", "analyzer": "search", "search_analyzer": "search"}}} `)
			} else if fv.GetMatchPhrasePrefixRight() != "" {
				c.P(`mappingEs["`, fv.GetMatchPhrasePrefixRight(), `"] = map[string]interface{}{"type": "keyword",`)
				c.P(`"fields": map[string]interface{}{"search_reverse": map[string]string{"type": "text", "analyzer": "search_reverse", "search_analyzer": "search_reverse"}}}`)
			} else if fv.GetTerms() != "" { //Term'
				c.P(`mappingEs["`, fv.GetTerms(), `"] = map[string]string{"type": "keyword"}`)
			} else if fv.GetMatch() != "" {
				c.P(`mappingEs["`, fv.GetMatch(), `"] = map[string]string{"type": "text"}`)
			} else if fv.GetMatchPrefix() != "" {
				c.P(`mappingEs["`, fv.GetMatchPrefix(), `"] = map[string]interface{}{"type": "keyword", "fields": map[string]interface{}{`)
				c.P(`"search": map[string]string{"type": "text"}}}`)
			} else {
				c.P(c.getPacketName(c.glogPkg), `.Warningln("Unknow ")`)
			}
			continue
		}

		if field.IsMessage() {
			if field.IsRepeated() {
				if fieldExten.GetEs() != "" {
					c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil(this.`, fieldName, `){`)
					typeName := helper.GetTypeName(field.GetTypeName())
					c.P(`tmpMapingEs := map[string]interface{}{}`)
					c.P(typeName, ` := &`, typeName, `{}`)
					c.P(`this.`, fieldName, ` = append(this.`, fieldName, `, `, typeName, `)`)
					c.P(`this.Get`, fieldName, `()[0].EsMapping(tmpMapingEs)`)
					c.P(`for key := range tmpMapingEs {`)
					c.P(`mappingEs["`, fieldExten.GetEs(), `."+key] = tmpMapingEs[key]`)
					c.P(`}`)
					c.P(`}`)
				}
			} else {
				if fieldExten.GetEs() != "" {
					c.P(`this.Get`, fieldName, `().EsMapping(`, c.getPacketName(c.helperPkg), `.MakeKeyEsMap(`, c.getPacketName(c.helperPkg), `.MakeKeyEsMap(mappingEs, "`, fieldExten.GetEs(), `"), "properties"))`)
				} else if fieldExten.GetTimestampEs() != "" {
					c.P(`mappingEs["`, fieldExten.GetTimestampEs(), `"] = map[string]string{"type": "date"}`)

				} else if fieldExten.GetDateEs() != "" {
					c.P(`mappingEs["`, fieldExten.GetDateEs(), `"] = map[string]string{"type": "date"}`)
				}
			}
		} else if field.IsEnum() {
			if fieldExten.GetEs() == "" {
				glog.Errorln("MAPPING ERROR: tag must not empty ", field.GetName())
				c.P(`err = "MAPPING ERROR: tag must not empty, field: `, field.GetName(), `"`)
			} else {
				c.P(`mappingEs["`, fieldExten.GetEs(), `"] = map[string]string{"type": "keyword"}`)
			}
		} else if field.IsRepeated() {
			if fieldExten.GetEs() == "" {
				glog.Errorln("MAPPING ERROR: tag must not empty ", field.GetName())
				c.P(`err = "MAPPING ERROR: tag must not empty, field: `, field.GetName(), `"`)
			} else {
				c.P(`mappingEs["`, fieldExten.GetEs(), `"] = map[string]string{"type": "keyword"}`)
			}
		} else {
			tag := ""
			if fieldExten.GetEs() != "" {
				tag = fieldExten.GetEs()
			} else {
				tag = fieldExten.GetStoreZeroValue()
			}
			if tag == "" {
				glog.Errorln("MAPPING ERROR: tag must not empty ", field.GetName())
				c.P(`err = "MAPPING ERROR: tag must not empty, field: `, field.GetName(), `"`)
			}
			switch *(field.Type) {
			case descriptor.FieldDescriptorProto_TYPE_BOOL:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "boolean"}`)
			case descriptor.FieldDescriptorProto_TYPE_INT32:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "integer"}`)
			case descriptor.FieldDescriptorProto_TYPE_INT64:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "long"}`)
			case descriptor.FieldDescriptorProto_TYPE_STRING:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "keyword"}`)
			case descriptor.FieldDescriptorProto_TYPE_FLOAT:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "float"}`)
			case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
				c.P(`mappingEs["`, tag, `"] = map[string]string{"type": "double"}`)
			default:
				glog.Errorln("Unknown Type ", tag, field.Type.String())
			}
		}
	}
	if once {
		c.P(`}`)
	}
}
func (c *es) generateBuildQuery(file *generator.FileDescriptor, message *generator.Descriptor, ccTypeName string) {
	once := false
	genfunc := func() {
		if once {
			return
		}
		once = true
		c.P(`func (this *`, ccTypeName, `) BuildQuery(query *`, c.getPacketName(c.elasticPkg), `.BoolQuery) *`, c.getPacketName(c.elasticPkg), `.BoolQuery {`)
		c.In()
		c.P(`if query == nil {`)
		c.P(`query = `, c.getPacketName(c.elasticPkg), `.NewBoolQuery()`)
		c.P(`}`)
	}

	once1 := false
	rangeDateSearchDeclar := func() {
		if !once1 {
			c.P(`rangeDateSearch := &`, c.getPacketName(c.helperPkg), `.MapRangeSearch{MapRangeDateSearch: map[string]*`, c.getPacketName(c.helperPkg), `.RangeDateSearch{}}`)
			c.P(`rangeTmstampSearch := &`, c.getPacketName(c.helperPkg), `.MapRangeSearch{MapRangeTmStampSearch: map[string]*`, c.getPacketName(c.helperPkg), `.RangeTmstampSearch{}}`)
			c.P(`bHasSearchPrefix, disableRangeFilter, searchPhone := false, false, false`)
		}
		once1 = true
	}
	once2 := false
	for _, field := range message.Field {
		isRepeated := false
		FieldQueryZeroValue := c.getFieldQueryZeroValue(field)
		fieldQuerier := c.getFieldQueryIfAny(field)
		if fieldQuerier == nil {
			continue
		}
		genfunc()
		rangeDateSearchDeclar()

		fieldName := c.GetOneOfFieldName(message, field)
		variableName := "this." + fieldName
		if field.IsRepeated() {
			isRepeated = true
		}
		c.generateQuerier(variableName, ccTypeName, fieldQuerier, FieldQueryZeroValue, isRepeated, &once2)
	}
	if once {
		c.P(`if !disableRangeFilter || searchPhone {`)
		c.P(`for k, v := range rangeDateSearch.MapRangeDateSearch {`)
		c.P(c.getPacketName(c.glogPkg), `.Infoln(k, v)`)
		c.P(`f, t := v.From, v.To`)
		c.P(`if f != nil && t != nil {`)
		c.P(`if !searchPhone && bHasSearchPrefix && f.Day+7 > t.Day && f.Month == t.Month && f.Year == t.Year {`)
		c.P(`tm := `, c.getPacketName(c.timePkg), `.Date(int(f.Year), `, c.getPacketName(c.timePkg), `.Month(f.Month), int(f.Day), 0, 0, 0, 0, `, c.getPacketName(c.timePkg), `.UTC).Add(-7 * 24 * `, c.getPacketName(c.timePkg), `.Hour)`)
		c.P(`f.Year, f.Month, f.Day = int32(tm.Year()), int32(tm.Month()), int32(tm.Day())`)
		c.P(`}`)
		c.P(`query = query.Filter(`, c.getPacketName(c.elasticPkg), `.NewRangeQuery(k).Gte(`, c.getPacketName(c.helperPkg), `.DateToStringSearch(f)).Lte(`, c.getPacketName(c.helperPkg), `.DateToStringSearch(t)).TimeZone("+07:00"))`)
		c.P(`} else {`)
		c.P(c.getPacketName(c.glogPkg), `.Errorln("Invalid ", k)`)
		c.P(`}`)
		c.P(`}`)

		c.P(`for k, v := range rangeTmstampSearch.MapRangeTmStampSearch {`)
		c.P(c.getPacketName(c.glogPkg), `.Infoln(k, v)`)
		c.P(`f, t := v.From, v.To`)
		c.P(`if f != 0 && t != 0 {`)
		c.P(`query = query.Filter(elastic.NewRangeQuery(k).Gte(f).Lte(t))`)
		c.P(`} else {`)
		c.P(c.getPacketName(c.glogPkg), `.Errorln("Invalid ", k)`)
		c.P(`}`)
		c.P(`}`)
		c.P(`}`)

		c.P(`return query`)
		c.Out()
		c.P(`}`)
	}
}
func (c *es) generateEsStore(file *generator.FileDescriptor, message *generator.Descriptor, ccTypeName string) {
	once := false
	genfunc := func() {
		if once {
			return
		}
		once = true

		c.P(`func (this *`, ccTypeName, `) GetEsMap(esMap *map[string]interface{}) {`)
		c.In()
	}

	for _, field := range message.Field {
		fieldEs := c.getFieldEsIfAny(field)
		if fieldEs == nil {
			continue
		}
		genfunc()
		fieldName := c.GetOneOfFieldName(message, field)
		variableName := "this." + fieldName
		if field.IsMessage() {
			isRepeated := false
			if field.IsRepeated() {
				isRepeated = true
			}
			c.generatePtrAndStructEs(variableName, ccTypeName, fieldName, fieldEs, isRepeated)
		} else {
			c.generateFieldEs(variableName, ccTypeName, fieldEs)
		}
	}
	if once {
		c.P(`}`)
	}
}
func (c *es) generateProto3Message(file *generator.FileDescriptor, message *generator.Descriptor) {
	ccTypeName := generator.CamelCaseSlice(message.TypeName())
	c.generateEsMapping(file, message, ccTypeName)
	c.generateBuildQuery(file, message, ccTypeName)
	c.generateEsStore(file, message, ccTypeName)

}

func (c *es) generatePtrAndStructEs(variableName string, ccTypeName string, fieldName string, fv *esProto.FieldEs, isRepeated bool) {
	if fv.GetEs() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		if isRepeated {
			c.P(`esMapArr := map[string][]interface{}{}`)
			c.P(`	for _, v := range this.Get`, fieldName, `() {`)
			c.P(`	tpmEsMap := map[string]interface{}{}`)
			c.P(`	v.GetEsMap(&tpmEsMap)`)
			c.P(`	for key := range tpmEsMap {`)
			c.P(`		if _, ok := esMapArr[key]; ok {`)
			c.P(`			esMapArr[key] = append(esMapArr[key], tpmEsMap[key])`)
			c.P(`		} else {`)
			c.P(`			esMapArr[key] = []interface{}{tpmEsMap[key]}`)
			c.P(`		}`)
			c.P(`	}`)
			c.P(`}`)
			c.P(`for key := range esMapArr {`)
			c.P(`	(*esMap)["`, fv.GetEs(), `."+key] = esMapArr[key]`)
			c.P(`}`)
		} else {
			c.P(`this.Get`, fieldName, `().GetEsMap(`, c.getPacketName(c.helperPkg), `.MakeKeyMap(esMap, "`, fv.GetEs(), `"))`)
		}
		c.P(`}`)
	} else if fv.GetTimestampEs() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if ts, ok := `, c.getPacketName(c.helperPkg), `.CheckTimestampType(`, variableName, `); ok {`)
		c.P(`if ts != nil {`)
		c.P(`tm, err := `, c.getPacketName(c.ptypesPkg), `.Timestamp(ts)`)
		c.P(`if err != nil {`)
		c.P(c.getPacketName(c.glogPkg), `.Errorln(err)`)
		c.P(`} else {`)
		c.P(`(*esMap)["`, fv.GetTimestampEs(), `"] = tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)`)
		c.P(`}`)
		c.P(`}`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetDateEs() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if date, ok := `, c.getPacketName(c.helperPkg), `.CheckDateType(`, variableName, `); ok {`)
		c.P(`if date != nil {`)
		c.P(`tm := `, c.getPacketName(c.helperPkg), `.DateToTimeSearch(date)`)
		c.P(`(*esMap)["`, fv.GetDateEs(), `"] = tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)`)
		c.P(`}`)
		c.P(`}`)
		c.P(`}`)
	}
}

func (c *es) generateFieldEs(variableName string, ccTypeName string, fv *esProto.FieldEs) {
	if fv.GetEs() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`(*esMap)["`, fv.GetEs(), `"] = `, variableName)
		c.P(`}`)

	} else {
		if fv.GetStoreZeroValue() != "" {
			c.P(`if !`, c.getPacketName(c.helperPkg), `.IsDefaultZeroValue( `+variableName+`){`)
			c.P(`(*esMap)["`, fv.GetStoreZeroValue(), `"] = `, variableName)
			c.P(`}`)
		}
	}
}

func (c *es) getFieldEsIfAny(field *descriptor.FieldDescriptorProto) *esProto.FieldEs {
	if field.Options != nil {
		v, err := proto.GetExtension(field.Options, esProto.E_Es)
		if err == nil && v.(*esProto.FieldEs) != nil {
			return (v.(*esProto.FieldEs))
		}
	}
	return nil
}

func (c *es) getFieldQueryIfAny(field *descriptor.FieldDescriptorProto) *esProto.FieldQuery {
	if field.Options != nil {
		v, err := proto.GetExtension(field.Options, esProto.E_Query)
		if err == nil && v.(*esProto.FieldQuery) != nil {
			return (v.(*esProto.FieldQuery))
		}
	}
	return nil
}

func (c *es) getFieldQueryZeroValue(field *descriptor.FieldDescriptorProto) *esProto.FieldZeroValueQuery {
	if field.Options != nil {
		v, err := proto.GetExtension(field.Options, esProto.E_QueryZero)
		if err == nil && v.(*esProto.FieldZeroValueQuery) != nil {
			return (v.(*esProto.FieldZeroValueQuery))
		}
	}
	return nil
}

func (c *es) generateQuerier(variableName string, ccTypeName string, fv *esProto.FieldQuery, eZerovalue *esProto.FieldZeroValueQuery, isRepeated bool, once2 *bool) {
	rangeQueryDeclar := func() {
		if !*once2 {
			c.P(`r := &`, c.getPacketName(c.helperPkg), `.RangeQuery{`)
			c.P(`MapQuery: map[string]*`, c.getPacketName(c.elasticPkg), `.RangeQuery{},`)
			c.P(`}`)
		}
		*once2 = true

	}

	checkNilFuncName := ""
	if eZerovalue.GetQueryZero() != "" {
		checkNilFuncName = "IsDefaultZeroValue"
	} else {
		checkNilFuncName = "IsNil"
	}
	if fv.GetMatchPhrasePrefix() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)

		c.P(`fields := `, c.getPacketName(c.stringsPkg), `.Split("`, fv.GetMatchPhrasePrefix(), `", ";")`)
		c.P(`if len(fields) < 2 {`)
		c.P(`bHasSearchPrefix = true`)
		c.P(`if !disableRangeFilter && len(fmt.Sprintf("%v",` + variableName + `)) >= 8 {`)
		c.P(`disableRangeFilter = true`)
		c.P(`}`)
		c.P(`if "` + fv.GetMatchPhrasePrefix() + `" == "userInfo.phoneNumber" {`)
		c.P(`searchPhone = true`)
		c.P(`}`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMultiMatchQuery(`)
		c.P(variableName + `, "` + fv.GetMatchPhrasePrefix() + `.search", "` + fv.GetMatchPhrasePrefix() + `.search_reverse").MaxExpansions(1024).Slop(2).Type("phrase_prefix"))`)
		c.P(`} else {`)
		c.P(`fieldsSearch := make([]string, 2*len(fields))`)
		c.P(`for i, field := range fields {`)
		c.P(`fieldsSearch[2*i] = field+ ".search"`)
		c.P(`fieldsSearch[2*i+1] = field+".search_reverse"`)
		c.P(`}`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMultiMatchQuery(`)
		c.P(variableName, `, fieldsSearch...).MaxExpansions(1024).Slop(2).Type("phrase_prefix"))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetMatchPhrasePrefixLeft() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMatchPhrasePrefixQuery(`)
		c.P(`fmt.Sprintf("%s.search", "` + fv.GetMatchPhrasePrefixLeft() + `"),`)
		c.P(variableName + `,).MaxExpansions(1024).Slop(2))`)
		c.P(`}`)
	} else if fv.GetMatchPhrasePrefixRight() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMatchPhrasePrefixQuery(`)
		c.P(`fmt.Sprintf("%s.search_reverse", "` + fv.GetMatchPhrasePrefixRight() + `"),`)
		c.P(variableName + `,).MaxExpansions(1024).Slop(2)))`)
		c.P(`}`)
	} else if fv.GetWildcard() != "" { //Wildcard
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`s := fmt.Sprintf("%v", ` + variableName + `)`)
		c.P(`if !`, c.getPacketName(c.stringsPkg), `.Contains(s, "*") {`)
		c.P(`	s = "*" + s + "*"`)
		c.P(`}`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewWildcardQuery(`+fv.GetWildcard()+`, s))`)
		c.P(`}`)
	} else if fv.GetWildcardLeft() != "" { //Wildcard
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewWildcardQuery(`+fv.GetWildcardLeft()+`, fmt.Sprintf("*%v", `+variableName+`)))`)
		c.P(`}`)
	} else if fv.GetWildcardRight() != "" { //Wildcard
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewWildcardQuery(`+fv.GetWildcardRight()+`, fmt.Sprintf("%v*", `+variableName+`)))`)
		c.P(`}`)
	} else if fv.GetTerms() != "" { //Term'
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		var variableNameRepeated string
		if isRepeated {
			variableNameRepeated = c.getPacketName(c.helperPkg) + `.DoubleSlice(` + variableName + `)`
			c.P(`query = query.Filter(`, c.getPacketName(c.elasticPkg), `.NewTermsQuery("`+fv.GetTerms()+`",`+variableNameRepeated+`...))`)
		} else {
			c.P(`if `, c.getPacketName(c.helperPkg), `.IsEnumAll(`, variableName, `) {`)
			c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTerms()+`", "Is enum all")`)
			c.P(`} else{`)
			c.P(`comp := `, c.getPacketName(c.helperPkg), `.ConvertDateTimeSearch(`+variableName+`,"=")`)
			c.P(`query = query.Filter(`, c.getPacketName(c.elasticPkg), `.NewTermQuery("`+fv.GetTerms()+`",comp))`)
			c.P(`}`)
		}
		c.P(`}`)
	} else if fv.GetMatch() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMatchQuery("`+fv.GetMatch()+`",`+variableName+`))`)
		c.P(`}`)
		// query = query.Must(elastic.NewMatchQuery(params[0], vv))
	} else if fv.GetMatchPrefix() != "" {
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewMatchQuery("`+fv.GetMatchPrefix()+`.search",`+variableName+`).MinimumShouldMatch("3<90%"))`)
		c.P(`}`)
	} else if fv.GetGte() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetGte()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`if !rangeDateSearch.AddFrom("` + fv.GetGte() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetGte() + `").Gte(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetLte() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetLte()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`if !rangeDateSearch.AddTo("` + fv.GetLte() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetLte() + `").Lte(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetGt() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetGt()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`if !rangeDateSearch.AddFrom("` + fv.GetGt() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetGt() + `").Gt(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetLt() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetLt()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		c.P(`if !rangeDateSearch.AddTo("` + fv.GetLt() + `", ` + variableName + `) {`)
		c.P(`	query = query.Must(r.NewRangeQuery("` + fv.GetLt() + `").Lt(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetNotTerms() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetNotTerms()+`",`+variableName+`)`)
		c.P(`if !`, c.getPacketName(c.helperPkg), `.`, checkNilFuncName, `( `+variableName+`){`)
		var variableNameRepeated string
		if isRepeated {
			variableNameRepeated = c.getPacketName(c.helperPkg) + `.DoubleSlice(` + variableName + `)`
			c.P(`query = query.MustNot(`, c.getPacketName(c.elasticPkg), `.NewTermsQuery("`+fv.GetNotTerms()+`",`+variableNameRepeated+`...))`)
		} else {
			c.P(`comp := `, c.getPacketName(c.helperPkg), `.ConvertDateTimeSearch(`+variableName+`,"!=")`)
			c.P(`query = query.MustNot(`, c.getPacketName(c.elasticPkg), `.NewTermQuery("`+fv.GetNotTerms()+`",comp))`)
		}
		c.P(`}`)
	} else if fv.GetIntoRangeDate() != "" {
		tag := strings.Split(fv.GetIntoRangeDate(), ";")
		if len(tag) != 2 {
			c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetIntoRangeDate()+`", "lenth !=2")`)
		} else {
			c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
			c.P(`tm := `, c.getPacketName(c.helperPkg), `.DateToTimeSearch(`, variableName, `)`)
			c.P(`query = query.Filter(`, c.getPacketName(c.elasticPkg), `.NewRangeQuery("`, tag[0], `").Lte(tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)))`)
			c.P(`query = query.Filter(`, c.getPacketName(c.elasticPkg), `.NewRangeQuery("`, tag[1], `").Gte(tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)))`)
			c.P(`}`)
		}
	} else if fv.GetTmstampGte() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTmstampGte()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if !rangeTmstampSearch.AddFrom("` + fv.GetTmstampGte() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetTmstampGte() + `").Gte(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetTmstampLte() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTmstampLte()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if !rangeTmstampSearch.AddTo("` + fv.GetTmstampLte() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetTmstampLte() + `").Lte(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetTmstampGt() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTmstampGt()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if !rangeTmstampSearch.AddFrom("` + fv.GetTmstampGt() + `", ` + variableName + `) {`)
		c.P(`query = query.Must(r.NewRangeQuery("` + fv.GetTmstampGt() + `").Gt(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetTmstampLt() != "" {
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTmstampLt()+`",`+variableName+`)`)
		rangeQueryDeclar()
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if !rangeTmstampSearch.AddTo("` + fv.GetTmstampLt() + `", ` + variableName + `) {`)
		c.P(`	query = query.Must(r.NewRangeQuery("` + fv.GetTmstampLt() + `").Lt(` + variableName + `))`)
		c.P(`}`)
		c.P(`}`)
	} else if fv.GetTermsZero() != "" { //TermsZero'
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTermsZero()+`",`+variableName+`)`)
		c.P(`if `, c.getPacketName(c.helperPkg), `.IsZero( `+variableName+`){`)
		c.P(`query = query.MustNot(`, c.getPacketName(c.elasticPkg), `.NewExistsQuery("`+fv.GetTermsZero()+`"))`)
		c.P(`} else{`)
		c.P(`if !`, c.getPacketName(c.helperPkg), `.IsNil( `+variableName+`){`)
		c.P(`if `, c.getPacketName(c.helperPkg), `.IsEnumAll(`, variableName, `) {`)
		// Nếu truyền vào là -1 thì lấy hết: vừa active(1), inactive(0)
		c.P(c.getPacketName(c.glogPkg), `.Infoln("`+fv.GetTerms()+`", "Is enum all")`)
		c.P(`} else{`)
		c.P(`query = query.Must(`, c.getPacketName(c.elasticPkg), `.NewTermQuery("`+fv.GetTermsZero()+`",`+variableName+`))`)
		c.P(`}`)
		c.P(`}`)
		c.P(`}`)
	} else {

		c.P(c.getPacketName(c.glogPkg), `.Warningln("Unknow ")`)
	}

}
