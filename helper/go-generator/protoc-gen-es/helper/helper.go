package helper

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	"git.zapa.cloud/merchant-tools/helper/proto"
	"github.com/golang/glog"
	ptypes "github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	elasticPkg "gopkg.in/olivere/elastic.v6"
)

// ConvertDateTimeSearch ...
func ConvertDateTimeSearch(vv interface{}, op string) interface{} {
	if date, ok := vv.(*proto.Date); ok {
		switch op {
		case "<":
			return proto.DateUpperToTimeSearch(date).UnixNano() / int64(time.Millisecond)
		default:
			return proto.DateToTimeSearch(date).UnixNano() / int64(time.Millisecond)
		}
	}
	return vv
}

// IsEnumAll ...
func IsEnumAll(vv interface{}) bool {
	type enumInterface interface {
		EnumDescriptor() ([]byte, []int)
	}
	if _, ok := vv.(enumInterface); ok {
		return fmt.Sprintf("%d", vv) == "-1"
	}
	return false
}

// IsZeroEnum ...
func IsZeroEnum(vv interface{}) bool {
	type enumInterface interface {
		EnumDescriptor() ([]byte, []int)
	}
	if _, ok := vv.(enumInterface); ok {
		return fmt.Sprintf("%d", vv) == "1000"
	}
	return false
}

// DateToStringSearch ...
func DateToStringSearch(date *proto.Date) string {
	return fmt.Sprintf("%04d-%02d-%02d", date.GetYear(), date.GetMonth(), date.GetDay())
}

// IsNil ...
func IsNil(field interface{}) bool {
	return field == nil || reflect.ValueOf(field).IsZero()
	/*zero := reflect.Zero(reflect.TypeOf(field)).Interface()
	if reflect.DeepEqual(field, zero) {
		return true
	}
	return false*/
}

// IsDefaultZeroValue ...
func IsDefaultZeroValue(field interface{}) bool {
	return field == nil
}

// IsZero ...
func IsZero(field interface{}) bool {
	if field != nil {
		if reflect.ValueOf(field).IsZero() {
			return true
		}
	}
	return false
}

// DoubleSlice ...
func DoubleSlice(s interface{}) []interface{} {
	v := reflect.ValueOf(s)
	items := make([]interface{}, v.Len())
	for i := 0; i < v.Len(); i++ {
		items[i] = v.Index(i).Interface()
	}
	return items
}

// RangeQuery ...
type RangeQuery struct {
	MapQuery map[string]*elasticPkg.RangeQuery
}

// NewRangeQuery ...
func (r *RangeQuery) NewRangeQuery(name string) *elasticPkg.RangeQuery {
	if q, ok := r.MapQuery[name]; ok {
		return q
	}
	q := elasticPkg.NewRangeQuery(name)
	r.MapQuery[name] = q
	return q
}

// MapRangeDateSearch ...
type MapRangeSearch struct {
	MapRangeDateSearch    map[string]*RangeDateSearch
	MapRangeTmStampSearch map[string]*RangeTmstampSearch
}

// RangeDateSearch ...
type RangeDateSearch struct {
	From, To *proto.Date
}

type RangeTmstampSearch struct {
	From, To int64
}

// AddFrom ...
func (r *MapRangeSearch) AddFrom(name string, vv interface{}) bool {
	if from, ok := vv.(*proto.Date); ok {
		if q, ok := r.MapRangeDateSearch[name]; ok {
			q.From = from
		} else {
			r.MapRangeDateSearch[name] = &RangeDateSearch{From: from}
		}
		return true
	} else if from, ok := vv.(*timestamp.Timestamp); ok {
		tm, err := ptypes.Timestamp(from)
		if err != nil {
			glog.Errorln(err)
		}
		if q, ok := r.MapRangeTmStampSearch[name]; ok {
			q.From = tm.UnixNano() / int64(time.Millisecond)
		} else {
			r.MapRangeTmStampSearch[name] = &RangeTmstampSearch{From: tm.UnixNano() / int64(time.Millisecond)} //tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)`)
		}
		return true
	}
	return false
}

// AddTo ...
func (r *MapRangeSearch) AddTo(name string, vv interface{}) bool {
	if to, ok := vv.(*proto.Date); ok {
		if q, ok := r.MapRangeDateSearch[name]; ok {
			q.To = to
		} else {
			r.MapRangeDateSearch[name] = &RangeDateSearch{To: to}
		}
		return true
	} else if to, ok := vv.(*timestamp.Timestamp); ok {
		tm, err := ptypes.Timestamp(to)
		if err != nil {
			glog.Errorln(err)
		}
		if q, ok := r.MapRangeTmStampSearch[name]; ok {
			q.To = tm.UnixNano() / int64(time.Millisecond)
		} else {
			r.MapRangeTmStampSearch[name] = &RangeTmstampSearch{To: tm.UnixNano() / int64(time.Millisecond)} //tm.UnixNano() / int64(`, c.getPacketName(c.timePkg), `.Millisecond)`)
		}
		return true
	}
	return false
}

// MakeKeyEsMap ...
func MakeKeyEsMap(m map[string]interface{}, key string) map[string]interface{} {
	if t, ok := m[key]; ok {
		if t, ok := t.(map[string]interface{}); ok {
			return t
		}
	}
	t := map[string]interface{}{}
	m[key] = t
	return t
}

// MakeKeyMap ...
func MakeKeyMap(m *map[string]interface{}, key string) *map[string]interface{} {
	if t, ok := (*m)[key]; ok {
		if t, ok := t.(*map[string]interface{}); ok {
			return t
		}
	}
	t := &map[string]interface{}{}
	(*m)[key] = t
	return t
}

// // IsNil ...
// func IsNil(field interface{}) bool {
// 	zero := reflect.Zero(reflect.TypeOf(field)).Interface()
// 	if reflect.DeepEqual(field, zero) {
// 		return true
// 	}
// 	return false
// }

// CheckTimestampType ...
func CheckTimestampType(field interface{}) (*timestamp.Timestamp, bool) {
	if ts, ok := field.(*timestamp.Timestamp); ok {
		return ts, true
	}
	if ts, ok := field.(*proto.Timestamp); ok {
		return &timestamp.Timestamp{Seconds: ts.Seconds, Nanos: ts.Nanos}, true
	}
	return nil, false
}

// CheckDateType ...
func CheckDateType(field interface{}) (*proto.Date, bool) {
	if date, ok := field.(*proto.Date); ok {
		return date, true
	}
	return nil, false
}

// CheckDateType ...
func DateToTimeSearch(date *proto.Date) time.Time {
	return proto.DateToTimeSearch(date)
}

// GetTypeName ...
func GetTypeName(name string) string {
	typeNames := strings.Split(name, ".")
	return typeNames[len(typeNames)-1]
}
