package config

import (
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

//FormatNumber ...
func FormatNumber(input interface{}) string {
	p := message.NewPrinter(language.English)
	return p.Sprintf("%v", number.Decimal(input))
}

//FormatPercent ...
func FormatPercent(input interface{}) string {
	p := message.NewPrinter(language.English)
	return p.Sprintf("%v", number.Percent(input))
}
