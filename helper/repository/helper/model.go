package repositoryHelper

import (
	"encoding/json"
	"time"

	protoHelper "git.zapa.cloud/merchant-tools/helper/proto"
	appPb "git.zapa.cloud/merchant-tools/protobuf/merchant-manage"
	"github.com/golang/glog"
)

func ConvertAppInfoToShortInfo(appInfo *appPb.AppInfo) *appPb.AppShortInfo {
	if appInfo == nil {
		return nil
	}

	UnMarshalExtendInfo(appInfo)
	shortInfo := &appPb.AppShortInfo{
		Id:            appInfo.GetId(),
		Name:          appInfo.GetName(),
		Image:         appInfo.GetLogo(),
		Key1:          appInfo.GetMacKey(),
		Key2:          appInfo.GetCallbackKey(),
		RsaPrivateKey: appInfo.GetRsaPrivateKey(),
		RsaPublicKey:  appInfo.GetRsaPublicKey(),
		NotifyUrl:     appInfo.GetExtendInfoObject().GetNotifyUrl(),
		CallbackUrl:   appInfo.GetExtendInfoObject().GetCallbackUrl(),
	}
	return shortInfo
}

func UnMarshalExtendInfo(appInfo *appPb.AppInfo) {
	if appInfo == nil || len(appInfo.GetExtendInfo()) == 0 {
		return
	}

	appExtendInfo := &appPb.AppExtendInfo{}
	err := json.Unmarshal([]byte(appInfo.GetExtendInfo()), appExtendInfo)
	if err == nil {
		appInfo.ExtendInfoObject = appExtendInfo
	} else {
		glog.Warning("UnMarshalExtendInfo: unmarshal extend info error: ", err)
	}
}

func MarshalExtendInfo(appInfo *appPb.AppInfo) {
	if appInfo == nil || appInfo.GetExtendInfoObject() == nil {
		return
	}

	buf, err := json.Marshal(appInfo.GetExtendInfoObject())
	if err != nil {
		glog.Warning("MarshalExtendInfo: marshal extend info object error: ", err)
		return
	}

	appInfo.ExtendInfo = string(buf)
}

func CheckAppFeeChargeDateStatus(app *appPb.AppInfo) int {
	if app.GetFeeChargeDate() == nil {
		return -1
	}

	feeChargeTime, err := protoHelper.TimeStampToTime(app.GetFeeChargeDate())
	if err != nil {
		glog.Error("GetMerchantProfileStatistic: err convert time feeChargeDate = ", err)
		return -1
	}

	feeChargeTime = feeChargeTime.Add(time.Hour * time.Duration(7))
	if feeChargeTime.Year() <= 1970 {
		return -1
	}
	return 1
}
