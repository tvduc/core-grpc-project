package repository

import (
	"context"
	"database/sql"

	sq "github.com/Masterminds/squirrel"
)

type DatabaseType int

const (
	DatabaseNone    DatabaseType = 0
	RDBMSOnly       DatabaseType = 1
	CacheOnly       DatabaseType = 2
	CacheReadPrefer DatabaseType = 3
)

type SelectCallBack func(builder sq.SelectBuilder) (sq.SelectBuilder, []string)
type UpdateCallBack func(builder sq.UpdateBuilder) sq.UpdateBuilder
type InsertCallBack func(builder sq.InsertBuilder) sq.InsertBuilder
type DeleteCallBack func(builder sq.DeleteBuilder) sq.DeleteBuilder
type TransactionCallBack func(args ...interface{}) (string, []interface{}, []interface{}, error) // Return: execSql, execArgs, nextArgs, error
type ResultCallBack func(result sql.Result, err error) error
type AllowListCacheCallBack func() (hashKey string, hashFields []string, setKey []string, allow bool)
type PreProcessObjectCallBack func(dest interface{}) (err error)
type CloneShortObjectCallBack func(model interface{}) (dest interface{}, err error)

type BaseRequester interface {
	GetContext() context.Context
	SetContext(context.Context)
	GetId() string
	GetDBType() DatabaseType
	SetDBType(DatabaseType)
	GetCacheKeyName() string
	SetCacheKeyName(string)
	GetCacheShortKeyName() string
	SetCacheShortKeyName(string)
	GetTableName() string
	SetTableName(string)
	GetColumnKeyName() string
	SetColumnKeyName(string)
	GetColumns() []string
	SetColumns([]string)
	GetSelectCB() SelectCallBack
	SetSelectCB(SelectCallBack)
	GetUpdateCB() UpdateCallBack
	SetUpdateCB(UpdateCallBack)
	GetInsertCB() InsertCallBack
	SetInsertCB(InsertCallBack)
	GetDeleteCB() DeleteCallBack
	SetDeleteCB(DeleteCallBack)
	GetResultCB() ResultCallBack
	GetTransactionCBs() []TransactionCallBack
	GetAllowListCacheCB() AllowListCacheCallBack
	GetPreProcessObjectCB() PreProcessObjectCallBack
	GetCloneShortObjectCB() CloneShortObjectCallBack
}

type BaseRequest struct {
	Ctx                context.Context
	Id                 string
	DBType             DatabaseType
	CacheKeyName       string
	CacheShortKeyName  string
	TableName          string
	ColumnKeyName      string
	Columns            []string
	SelectCB           SelectCallBack
	UpdateCB           UpdateCallBack
	InsertCB           InsertCallBack
	DeleteCB           DeleteCallBack
	ResultCB           ResultCallBack
	TransactionCBs     []TransactionCallBack
	AllowListCacheCB   AllowListCacheCallBack
	PreProcessObjectCB PreProcessObjectCallBack
	CloneShortObjectCB CloneShortObjectCallBack
}

func (req *BaseRequest) GetContext() context.Context {
	return req.Ctx
}

func (req *BaseRequest) SetContext(ctx context.Context) {
	req.Ctx = ctx
}

func (req *BaseRequest) GetId() string {
	return req.Id
}

func (req *BaseRequest) GetDBType() DatabaseType {
	return req.DBType
}

func (req *BaseRequest) SetDBType(dbType DatabaseType) {
	req.DBType = dbType
}

func (req *BaseRequest) GetCacheKeyName() string {
	return req.CacheKeyName
}

func (req *BaseRequest) SetCacheKeyName(cacheKeyName string) {
	req.CacheKeyName = cacheKeyName
}

func (req *BaseRequest) GetCacheShortKeyName() string {
	return req.CacheShortKeyName
}

func (req *BaseRequest) SetCacheShortKeyName(cacheShortKeyName string) {
	req.CacheShortKeyName = cacheShortKeyName
}

func (req *BaseRequest) GetTableName() string {
	return req.TableName
}

func (req *BaseRequest) SetTableName(tableName string) {
	req.TableName = tableName
}

func (req *BaseRequest) GetColumnKeyName() string {
	return req.ColumnKeyName
}

func (req *BaseRequest) SetColumnKeyName(columnKeyName string) {
	req.ColumnKeyName = columnKeyName
}

func (req *BaseRequest) GetColumns() []string {
	return req.Columns
}

func (req *BaseRequest) SetColumns(columns []string) {
	req.Columns = columns
}

func (req *BaseRequest) GetSelectCB() SelectCallBack {
	return req.SelectCB
}

func (req *BaseRequest) SetSelectCB(cb SelectCallBack) {
	req.SelectCB = cb
}

func (req *BaseRequest) GetUpdateCB() UpdateCallBack {
	return req.UpdateCB
}

func (req *BaseRequest) SetUpdateCB(cb UpdateCallBack) {
	req.UpdateCB = cb
}

func (req *BaseRequest) GetInsertCB() InsertCallBack {
	return req.InsertCB
}

func (req *BaseRequest) SetInsertCB(cb InsertCallBack) {
	req.InsertCB = cb
}

func (req *BaseRequest) GetDeleteCB() DeleteCallBack {
	return req.DeleteCB
}

func (req *BaseRequest) SetDeleteCB(cb DeleteCallBack) {
	req.DeleteCB = cb
}

func (req *BaseRequest) GetResultCB() ResultCallBack {
	return req.ResultCB
}

func (req *BaseRequest) GetTransactionCBs() []TransactionCallBack {
	return req.TransactionCBs
}

func (req *BaseRequest) GetAllowListCacheCB() AllowListCacheCallBack {
	return req.AllowListCacheCB
}

func (req *BaseRequest) GetPreProcessObjectCB() PreProcessObjectCallBack {
	return req.PreProcessObjectCB
}

func (req *BaseRequest) GetCloneShortObjectCB() CloneShortObjectCallBack {
	return req.CloneShortObjectCB
}

type BaseRepository interface {
	GetById(interface{}, BaseRequester) error
	Get(interface{}, BaseRequester) error
	Lists(interface{}, BaseRequester) error
	Insert(interface{}, BaseRequester) error
	Update(interface{}, BaseRequester) error
	UpdateStatus(interface{}, BaseRequester) error
	Delete(BaseRequester) error
}
