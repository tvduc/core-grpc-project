package repository

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strings"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/error_util"
	tracing "git.zapa.cloud/merchant-tools/helper/opentracing"
	redisHelper "git.zapa.cloud/merchant-tools/helper/redis"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/iancoleman/strcase"
	jsoniter "github.com/json-iterator/go"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var (
	json     = jsoniter.ConfigCompatibleWithStandardLibrary
	redisTag = opentracing.Tag{Key: string(ext.Component), Value: "Redis"}
)

type CacheRepository struct {
	redisClient *redis.Client
}

func NewCacheRepository(ctx context.Context, cfg config.Redis) *CacheRepository {
	return &CacheRepository{
		redisClient: redisHelper.NewRedisClient(cfg),
	}
}

func (r *CacheRepository) GetById(dest interface{}, req BaseRequester) error {
	return getObjectByIdFromCache(dest, r.redisClient, req)
}

func (r *CacheRepository) Get(interface{}, BaseRequester) error {
	//Don't implement. Support get by id only
	return errorutil.ErrorNotFound
}

func (r *CacheRepository) Lists(dest interface{}, req BaseRequester) error {

	if req.GetAllowListCacheCB() == nil {
		return errorutil.ErrorNotFound
	}

	hashKey, hashFields, setKeys, ok := req.GetAllowListCacheCB()()
	if !ok {
		return errorutil.ErrorNotFound
	}

	if len(hashKey) > 0 {
		req.SetCacheKeyName(hashKey)
	}

	return listObjectFromCache(dest, r.redisClient, req, hashFields, setKeys)
}

func (r *CacheRepository) Insert(m interface{}, req BaseRequester) error {
	return updateObjectToCache(m, r.redisClient, req)
}

func (r *CacheRepository) Update(m interface{}, req BaseRequester) error {
	return updateObjectToCache(m, r.redisClient, req)
}

func (r *CacheRepository) UpdateStatus(m interface{}, req BaseRequester) error {
	//Don't implement
	return errorutil.ErrorNotFound
}

func (r *CacheRepository) Delete(req BaseRequester) error {
	return deleteObjectInCache(r.redisClient, req)
}

func (r *CacheRepository) Clear(req BaseRequester) error {
	return clearCacheKey(r.redisClient, req)
}

func (r *CacheRepository) RemoveRelation(req BaseRequester) error {
	return clearCacheKey(r.redisClient, req)
}

var (
	script = redis.NewScript(`
		local key = KEYS[1]..':'
		local keys = {}
		local results = {}
		for i, k in ipairs(ARGV) do
			table.insert(keys,key..k)
		end
		local ret = redis.call("SUNION",unpack(keys))
		if table.getn(ret)  == 0 then
			return results
		end
		return redis.call("HMGET",KEYS[1],unpack(ret))
		`)
)

func listObjectFromCache(dest interface{}, redisClient *redis.Client, req BaseRequester, hashFields, setKeys []string) (respErr error) {
	if redisClient == nil || req == nil {
		glog.Errorln("ListObjectFromCache: redis client instance or CacheRepoRequest is nil")
		return errorutil.ErrorInvalidData
	}

	if len(req.GetCacheKeyName()) == 0 {
		glog.Errorln("ListObjectFromCache: CacheKeyName is empty")
		return errorutil.ErrorInvalidData
	}

	key, _, isHash := parseKey(req)
	if !isHash {
		return errorutil.ErrorNotFound
	}

	parentCtx := req.GetContext()
	_, clientSpan := tracing.NewClientSpanFromContext(parentCtx, redisTag, fmt.Sprintf("HashKey = %v, Fields = %v, SetKeys = %v", key, hashFields, setKeys))
	defer func() {
		tracing.FinishClientSpan(clientSpan, respErr)
	}()

	if len(setKeys) == 0 {
		if len(hashFields) == 0 {
			dataList, err := redisClient.HVals(key).Result()
			if err == redis.Nil {
				glog.Errorln("ListObjectFromCache: redis get error nil. key = ", key)
				return errorutil.ErrorNotFound
			}
			if err != nil {
				glog.Errorln("ListObjectFromCache: redis call get error: ", err)
				return err
			}
			convertHashValuesToList(dest, dataList)
		} else {
			dataList, err := redisClient.HMGet(key, hashFields...).Result()
			if err == redis.Nil {
				glog.Errorln("ListObjectFromCache: redis get error nil. key = ", key)
				return errorutil.ErrorNotFound
			}
			if err != nil {
				glog.Errorln("ListObjectFromCache: redis call get error: ", err)
				return err
			}

			convertHashValuesToListEx(dest, dataList)
		}

	} else {
		/*unionKeys := make([]string, len(setKeys))
		for i, setKey := range setKeys {
			unionKeys[i] = fmt.Sprintf("%s:%s", key, setKey)
		}
		ids, err := redisClient.SUnion(unionKeys...).Result()
		if err != nil && err != redis.Nil {
			glog.Errorln(err)
			return errorutil.ErrorNotFound
		}
		glog.Infoln(ids)


		if len(ids) == 0 {
			return nil
		}

		dataList, err := redisClient.HMGet(key, ids...).Result()
		if err == redis.Nil {
			glog.Errorln("ListObjectFromCache: redis get error nil. key = ", key)
			return errorutil.ErrorNotFound
		}*/
		keys := make([]interface{}, len(setKeys))
		for i, k := range setKeys {
			keys[i] = k
		}
		ret, e := script.Run(redisClient, []string{key}, keys...).Result()
		if e != nil {
			glog.Errorln(e)
			return e
		}
		dataList := ret.([]interface{})

		convertHashValuesToListEx(dest, dataList)
	}

	return nil
}

func getObjectByIdFromCache(dest interface{}, redisClient *redis.Client, req BaseRequester) error {
	//glog.Infoln("getObjectByIdFromCache: idKey = ", idKey)
	if redisClient == nil || req == nil {
		glog.Errorln("getObjectByIdFromCache: redis client instance or BaseRequest is nil")
		return errorutil.ErrorInvalidData
	}

	if len(req.GetCacheKeyName()) == 0 {
		glog.Errorln("getObjectByIdFromCache: CacheKeyName is empty")
		return errorutil.ErrorInvalidData
	}

	val := []byte{}
	var err error
	key, field, isHash := parseKey(req)
	if isHash {
		if len(field) == 0 {
			return errorutil.ErrorNotFound
		}
		val, err = redisClient.HGet(key, field).Bytes()
	} else {
		val, err = redisClient.Get(key).Bytes()
	}

	if err == redis.Nil {
		glog.Errorln("getObjectByIdFromCache: redis get error nil. key = ", req.GetCacheKeyName())
		return errorutil.ErrorNotFound
	}

	if err != nil {
		glog.Errorln("getObjectByIdFromCache: redis call get error: ", err)
		return err
	}

	err = json.Unmarshal(val, dest)
	if err != nil {
		glog.Errorln("getObjectByIdFromCache: Unmarshal error : ", err)
		return err
	}

	if req.GetPreProcessObjectCB() != nil {
		err = req.GetPreProcessObjectCB()(dest)
	}

	return nil
}

func updateObjectToCache(m interface{}, redisClient *redis.Client, req BaseRequester) error {
	//glog.Infoln("updateObjectToCache: value = ", value)
	if redisClient == nil || req == nil {
		glog.Errorln("updateObjectToCache: redis client instance or BaseRequest is nil")
		return errorutil.ErrorInvalidData
	}

	if m == nil {
		glog.Errorln("updateObjectToCache: value is nil")
		return errorutil.ErrorInvalidData
	}

	if len(req.GetCacheKeyName()) == 0 {
		glog.Errorln("updateObjectToCache: CacheKeyName is empty")
		return errorutil.ErrorInvalidData
	}

	bytes, err := json.Marshal(m)
	if err != nil {
		glog.Errorln("updateObjectToCache: Unmarshal error : ", err)
		return err
	}

	key, field, isHash := parseKey(req)
	if isHash {
		err = redisClient.HSet(key, field, bytes).Err()
		if err != nil {
			glog.Errorln("updateObjectToCache: HSet key error = ", err)
			return err
		}

		if req.GetCloneShortObjectCB() != nil && len(req.GetCacheShortKeyName()) > 0 {
			newModel, err := req.GetCloneShortObjectCB()(m)
			if err != nil {
				glog.Errorln("updateObjectToCache: Clone new object error: ", err)
				return err
			}

			newBytes, err := json.Marshal(newModel)
			if err != nil {
				glog.Errorln("updateObjectToCache: Unmarshal new model error : ", err)
				return err
			}

			err = redisClient.HSet(req.GetCacheShortKeyName(), field, newBytes).Err()
			if err != nil {
				glog.Errorln("updateObjectToCache: Hset short key error = ", err)
				return err
			}
		}

		err = addIdToSet(m, redisClient, key, field)
		if err != nil {
			glog.Errorln("updateObjectToCache: add key to set list error = ", err)
			return err
		}
	} else {
		err = redisClient.Set(key, bytes, 0).Err()
		if err != nil {
			glog.Errorln("updateObjectToCache: Set key error = ", err)
			return err
		}
	}

	return nil
}

func getTagValues(m interface{}) (map[string][]string, error) {
	setVals := make(map[string][]string)
	val := reflect.ValueOf(m)
	if val.Kind() == reflect.Ptr {
		if val.IsNil() {
			glog.Errorln("getTagValues: input is null")
			return setVals, errors.New("getTagValues: input is null")
		}
		val = val.Elem()
	}

	if val.Kind() != reflect.Struct && val.Kind() != reflect.Map {
		glog.Infoln("getTagValues invalid type: ", val.Kind(), ", m = ", m)
		return setVals, nil
	}

	vt := val.Type()
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if tagValue, ok := field.Tag.Lookup("hset"); ok {
			ft := field.Type
			if ft.Kind() == reflect.Ptr {
				if val.Field(i).IsNil() {
					continue
				}
				ft = ft.Elem()
			}

			switch ft.Kind() {
			case reflect.Slice, reflect.Array:
				ft = ft.Elem()
				if ft.Kind() == reflect.Ptr {
					ft = ft.Elem()
				}
				if len(tagValue) > 1 {
					itemField, itemOk := ft.FieldByName(tagValue)
					if !itemOk {
						itemField, itemOk = ft.FieldByName(strcase.ToCamel(tagValue))
					}

					if itemOk {
						arrValue := val.FieldByName(field.Name)
						for j := 0; j < arrValue.Len(); j++ {
							itemValue := arrValue.Index(j)
							if itemValue.Kind() == reflect.Ptr {
								itemValue = itemValue.Elem()
							}
							if itemValue.FieldByName(itemField.Name).CanInterface() {
								key := strcase.ToSnake(field.Name) + ":" + strcase.ToSnake(itemField.Name)
								setVals[key] = append(setVals[key], fmt.Sprintf("%v", itemValue.FieldByName(itemField.Name).Interface()))
							}
						}
					}
				}
			case reflect.Struct, reflect.Map:
				if ft.Kind() == reflect.Ptr {
					ft = ft.Elem()
				}
				if len(tagValue) > 1 {
					subField, itemOk := ft.FieldByName(tagValue)
					if !itemOk {
						subField, itemOk = ft.FieldByName(strcase.ToCamel(tagValue))
					}

					if itemOk {
						subValue := val.FieldByName(field.Name)
						if subValue.Kind() == reflect.Ptr {
							subValue = subValue.Elem()
						}
						if subValue.FieldByName(subField.Name).CanInterface() {
							setVals[strcase.ToSnake(field.Name)+":"+strcase.ToSnake(subField.Name)] = []string{fmt.Sprintf("%v", subValue.FieldByName(subField.Name).Interface())}
						}
					}
				}
			default:
				if val.FieldByName(field.Name).CanInterface() {
					setVals[strcase.ToSnake(field.Name)] = []string{fmt.Sprintf("%v", val.FieldByName(field.Name).Interface())}
				}
			}

		}
	}
	return setVals, nil
}

func addIdToSet(m interface{}, redisClient *redis.Client, hashKey, hashField string) error {
	if redisClient == nil {
		glog.Errorln("addIdToSet: redis client instance or BaseRequest is nil")
		return errorutil.ErrorInvalidData
	}

	if m == nil {
		glog.Errorln("addIdToSet: value is nil")
		return errorutil.ErrorInvalidData
	}

	setMap, err := getTagValues(m)
	if err != nil {
		glog.Errorln("addIdToSet: redis set value error: ", err)
		return err
	}

	for k, v := range setMap {
		for _, itemVal := range v {
			setKey := fmt.Sprintf("%s:%s:%s", hashKey, k, itemVal)
			//Add hash field (id) into set
			redisClient.SAdd(setKey, hashField)
			redisClient.SAdd(hashKey+":allkeys", setKey)
		}
	}

	return nil
}

func clearCacheKey(redisClient *redis.Client, req BaseRequester) error {
	if len(req.GetCacheKeyName()) == 0 {
		return errorutil.ErrorInvalidData
	}

	redisClient.Del(req.GetCacheKeyName())
	if len(req.GetCacheShortKeyName()) > 0 {
		redisClient.Del(req.GetCacheShortKeyName())
	}

	setVals, err := redisClient.SMembers(fmt.Sprintf("%s:allkeys", req.GetCacheKeyName())).Result()
	if err == redis.Nil {
		return nil
	}
	if err != nil {
		glog.Errorln("ListObjectFromCache: redis get list from set value: ", err)
		return errorutil.ErrorNotFound
	}

	redisClient.Del(setVals...)

	return nil
}

func deleteObjectInCache(redisClient *redis.Client, req BaseRequester) error {
	if redisClient == nil || req == nil {
		glog.Errorln("deleteObjectInCache: redis client instance or BaseRequest is nil")
		return errorutil.ErrorInvalidData
	}

	if len(req.GetCacheKeyName()) == 0 {
		glog.Errorln("deleteObjectInCache: id key is empty")
		return errorutil.ErrorInvalidData
	}

	key, field, isHash := parseKey(req)
	glog.Infoln("deleteObjectInCache: key, field, isHash =  ", key, field, isHash)
	if isHash {
		if len(field) == 0 {
			glog.Errorln("deleteObjectInCache: delete with empty field in hash")
			return errorutil.ErrorInvalidData
		}
		err := redisClient.HDel(key, field).Err()
		if err != nil {
			glog.Errorln("deleteObjectInCache: delete key ", key, ", field ", field, ", error ", err)
			return err
		}

		if len(req.GetCacheShortKeyName()) > 0 {
			err := redisClient.HDel(req.GetCacheShortKeyName(), field).Err()
			if err != nil {
				glog.Errorln("deleteObjectInCache: delete short key error: ", err)
			}
		}
		return nil
	} else {
		err := redisClient.Del(key).Err()
		if err != nil {
			glog.Errorln("deleteObjectInCache: redis delete by key error: ", err)
			return err
		}
	}

	return nil
}

func parseKey(req BaseRequester) (key string, field string, isHash bool) {
	id := req.GetId()
	cacheKey := req.GetCacheKeyName()

	if strings.HasSuffix(cacheKey, ":") {
		if len(id) == 0 {
			return "", "", false
		}

		return cacheKey + id, "", false
	}

	if strings.HasSuffix(cacheKey, ":hash") {
		return cacheKey, id, true
	}

	if !strings.Contains(cacheKey, ":hash:") {
		return cacheKey, "", false
	}

	items := strings.Split(cacheKey, ":hash:")
	if len(items) != 2 {
		return cacheKey, "", false
	}

	items[0] = items[0] + ":hash"
	return items[0], items[1], true
}

func convertHashValuesToListEx(dest interface{}, dataList []interface{}) {
	//dataListEx := make([]string, len(dataList))
	dataListEx := []string{}
	for _, v := range dataList {
		data, ok := v.(string)
		if !ok || data == "null" {
			continue
		}
		dataListEx = append(dataListEx, data)
		//dataListEx[i] = data
	}

	convertHashValuesToList(dest, dataListEx)
}

func convertHashValuesToList(dest interface{}, dataList []string) {

	destValue := reflect.ValueOf(dest)
	if destValue.Kind() != reflect.Ptr {
		glog.Errorln("convertHashValuesToList: must pass a pointer, not a value, to StructScan destination")
		return
	}

	if destValue.IsNil() {
		glog.Errorln("convertHashValuesToList: dest interface is nil")
		return
	}

	fullStr := fmt.Sprintf("[%s]", strings.Join(dataList, ","))
	err := json.Unmarshal([]byte(fullStr), dest)
	if err != nil {
		glog.Errorln("convertHashValuesToList: err: ", err)
	}
}
