package repository

import (
	"context"
	"reflect"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/error_util"
	"github.com/golang/glog"
)

type GeneralRepository struct {
	cacheRepo        *CacheRepository
	mysqlRepo        *MysqlRepository
	DefaultRequester BaseRequester
}

type GeneralRepositoryOption struct {
	TableName         string
	Columns           []string
	DBType            DatabaseType
	ColumnKeyName     string
	CacheKeyName      string
	CacheShortKeyName string
}

func GetDatabaseType(dbTypeDefault DatabaseType, options []interface{}) DatabaseType {
	for _, option := range options {
		dbType, ok := option.(DatabaseType)
		if ok {
			return dbType
		}
	}
	return dbTypeDefault
}

func GetColumns(columnsDefault []string, options []interface{}) []string {
	for _, option := range options {
		columns, ok := option.([]string)
		if ok {
			return columns
		}
	}
	return columnsDefault
}

func NewGeneralRepository(ctx context.Context, cfg *config.ServiceBase, opt GeneralRepositoryOption) *GeneralRepository {
	if opt.DBType == DatabaseNone {
		opt.DBType = RDBMSOnly
	}

	return &GeneralRepository{
		DefaultRequester: &BaseRequest{
			DBType:            opt.DBType,
			Columns:           opt.Columns,
			TableName:         opt.TableName,
			ColumnKeyName:     opt.ColumnKeyName,
			CacheKeyName:      opt.CacheKeyName,
			CacheShortKeyName: opt.CacheShortKeyName,
		},
		cacheRepo: NewCacheRepository(ctx, cfg.Redis),
		mysqlRepo: NewMysqlRepository(ctx, cfg.Database),
	}
}

func (r *GeneralRepository) GetCacheRepo() *CacheRepository {
	if r == nil {
		return nil
	}
	return r.cacheRepo
}

func (r *GeneralRepository) GetMysqlRepo() *MysqlRepository {
	if r == nil {
		return nil
	}
	return r.mysqlRepo
}

func (r *GeneralRepository) GetDatabaseType(options []interface{}) DatabaseType {
	return GetDatabaseType(r.DefaultRequester.GetDBType(), options)
}

func (r *GeneralRepository) GetColumns(options []interface{}) []string {
	return GetColumns(r.DefaultRequester.GetColumns(), options)
}

func (r *GeneralRepository) SetupDefaultCallOption(req BaseRequester) error {
	if r == nil || req == nil {
		return errorutil.ErrorInvalidData
	}

	if req.GetDBType() == DatabaseNone {
		req.SetDBType(r.DefaultRequester.GetDBType())
	}

	if len(req.GetTableName()) == 0 {
		req.SetTableName(r.DefaultRequester.GetTableName())
	}

	if len(req.GetColumns()) == 0 {
		req.SetColumns(r.DefaultRequester.GetColumns())
	}

	if len(req.GetColumnKeyName()) == 0 {
		req.SetColumnKeyName(r.DefaultRequester.GetColumnKeyName())
	}

	if len(req.GetCacheKeyName()) == 0 {
		req.SetCacheKeyName(r.DefaultRequester.GetCacheKeyName())
	}

	if len(req.GetCacheShortKeyName()) == 0 {
		req.SetCacheShortKeyName(r.DefaultRequester.GetCacheShortKeyName())
	}

	return nil
}

func (r *GeneralRepository) GetById(dest interface{}, req BaseRequester) error {
	if r == nil || len(req.GetId()) == 0 {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.GetDBType() {
	case RDBMSOnly:
		return r.mysqlRepo.GetById(dest, req)
	case CacheOnly:
		return r.cacheRepo.GetById(dest, req)
	case CacheReadPrefer:
		err := r.cacheRepo.GetById(dest, req)
		if err == nil {
			return nil
		}
		err = r.mysqlRepo.GetById(dest, req)
		if err != nil {
			return err
		}

		err = r.cacheRepo.Update(dest, req)
		if err != nil {
			glog.Errorln("GeneralRepository.GetById: update to cache error: ", err)
		}
		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Get(dest interface{}, req *BaseRequest) error {
	if r == nil || req == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.Get(dest, req)
	case CacheOnly:
		return r.cacheRepo.Get(dest, req)
	case CacheReadPrefer:
		err := r.cacheRepo.Get(dest, req)
		if err == nil {
			return nil
		}
		err = r.mysqlRepo.Get(dest, req)
		if err != nil {
			return err
		}

		err = r.cacheRepo.Update(dest, req)
		if err != nil {
			glog.Errorln("GeneralRepository.Get: update to cache error: ", err)
		}

		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Lists(dest interface{}, req *BaseRequest) error {
	if r == nil || req == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.Lists(dest, req)
	case CacheOnly:
		return r.cacheRepo.Lists(dest, req)
	case CacheReadPrefer:
		err := r.cacheRepo.Lists(dest, req)
		if err == nil {
			return nil
		}
		err = r.mysqlRepo.Lists(dest, req)
		if err != nil {
			return err
		}
		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Insert(m interface{}, req *BaseRequest) error {
	if r == nil || m == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.Insert(m, req)
	case CacheOnly:
		return r.cacheRepo.Insert(m, req)
	case CacheReadPrefer:
		err := r.mysqlRepo.Insert(m, req)
		if err != nil {
			return err
		}

		req.Ctx = context.Background()
		go r.cacheRepo.Insert(m, req)
		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Update(m interface{}, req *BaseRequest) error {
	if r == nil || m == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.Update(m, req)
	case CacheOnly:
		return r.cacheRepo.Update(m, req)
	case CacheReadPrefer:
		err := r.mysqlRepo.Update(m, req)
		if err != nil {
			glog.Errorln("GeneralRepository.Update: mysql update error: ", err)
			return err
		}
		req.Ctx = context.Background()
		go func() {
			t := reflect.TypeOf(m)
			if t.Kind() == reflect.Ptr {
				t = t.Elem()
			}
			v := reflect.New(t)
			if v.CanInterface() {
				newM := v.Interface()
				err := r.mysqlRepo.GetById(newM, req)
				if err != nil {
					glog.Errorln("GeneralRepository.Update: call mysqlRepo.GetById to update cache error: ", err, ", object name: ", t.Name())
					return
				}
				r.cacheRepo.Update(newM, req)
			}

		}()
		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) UpdateStatus(m interface{}, req *BaseRequest) error {
	if r == nil || m == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.UpdateStatus(m, req)
	case CacheOnly:
		return r.cacheRepo.UpdateStatus(m, req)
	case CacheReadPrefer:
		err := r.mysqlRepo.UpdateStatus(m, req)
		if err != nil {
			return err
		}
		req.Ctx = context.Background()
		go func() {
			t := reflect.TypeOf(m)
			if t.Kind() == reflect.Ptr {
				t = t.Elem()
			}
			v := reflect.New(t)
			if v.CanInterface() {
				newM := v.Interface()
				err := r.mysqlRepo.GetById(newM, req)
				if err != nil {
					glog.Errorln("GeneralRepository.UpdateStatus: call mysqlRepo.GetById to update cache error: ", err, ", object name: ", t.Name())
					return
				}
				r.cacheRepo.Update(newM, req)
			}

		}()

		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Delete(id string, req *BaseRequest) error {
	if r == nil {
		return errorutil.ErrorInvalidData
	}

	if len(id) == 0 {
		return nil
	}

	r.SetupDefaultCallOption(req)
	switch req.DBType {
	case RDBMSOnly:
		return r.mysqlRepo.Delete(req)
	case CacheOnly:
		return r.cacheRepo.Delete(req)
	case CacheReadPrefer:
		err := r.mysqlRepo.Delete(req)
		if err != nil {
			return err
		}
		req.Ctx = context.Background()
		go r.cacheRepo.Delete(req)
		return nil
	}

	return errorutil.ErrorInvalidData
}

func (r *GeneralRepository) Clear(req *BaseRequest) error {
	if r == nil {
		return errorutil.ErrorInvalidData
	}

	r.SetupDefaultCallOption(req)
	if len(req.GetCacheKeyName()) == 0 {
		glog.Errorln("GeneralRepository: Clear with cache key name empty")
		return errorutil.ErrorInvalidData
	}
	switch req.DBType {
	case CacheOnly:
		return r.cacheRepo.Clear(req)
	}

	glog.Infoln("GeneralRepository: Clear with dbType is not support")
	return errorutil.ErrorInvalidData
}
