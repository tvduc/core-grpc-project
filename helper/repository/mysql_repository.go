package repository

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"

	"git.zapa.cloud/merchant-tools/helper/config"
	"git.zapa.cloud/merchant-tools/helper/database"
	sql_builder "git.zapa.cloud/merchant-tools/helper/database/builder"
	"git.zapa.cloud/merchant-tools/helper/error_util"
	sq "github.com/Masterminds/squirrel"
	"github.com/golang/glog"
	"github.com/jmoiron/sqlx"
)

type MysqlRepository struct {
	mysqlDB *sqlx.DB
}

func NewMysqlRepository(ctx context.Context, cfg config.Database) *MysqlRepository {
	return &MysqlRepository{
		mysqlDB: database.CreateSQLXDB(cfg),
	}
}

func (r *MysqlRepository) GetMysqlDB() *sqlx.DB {
	return r.mysqlDB
}

func (r *MysqlRepository) GetById(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	if req.GetSelectCB() == nil {
		req.SetSelectCB(func(builder sq.SelectBuilder) (sq.SelectBuilder, []string) {
			builder = builder.Where(sq.Eq{req.GetColumnKeyName(): req.GetId()})
			return builder, []string{}
		})
	}

	return databaseQueryHelper(dest, r.mysqlDB, req)
}

func (r *MysqlRepository) Get(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	err := databaseQueryHelper(dest, r.mysqlDB, req)
	if err != nil {
		glog.Errorln("MysqlRepository: query error: ", err)
		return err
	}

	return nil
}

func (r *MysqlRepository) Lists(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	return databaseQueryHelper(dest, r.mysqlDB, req)
}

func (r *MysqlRepository) Insert(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	_, err := databaseInsertHelper(r.mysqlDB, req)
	return err
}

func (r *MysqlRepository) Update(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	_, err := databaseUpdateHelper(r.mysqlDB, req)
	return err
}

func (r *MysqlRepository) UpdateStatus(dest interface{}, req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	_, err := databaseUpdateHelper(r.mysqlDB, req)
	return err
}

func (r *MysqlRepository) Delete(req BaseRequester) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	if req.GetDeleteCB() == nil {
		if len(req.GetId()) == 0 {
			glog.Errorln("MysqlRepository: Delete Id value is empty, invalid")
			return errorutil.ErrorInvalidData
		}
		if len(req.GetColumnKeyName()) == 0 {
			glog.Errorln("MysqlRepository: Delete column key name is empty, invalid")
			return errorutil.ErrorInvalidData
		}

		req.SetDeleteCB(func(builder sq.DeleteBuilder) sq.DeleteBuilder {
			builder = builder.Where(sq.Eq{req.GetColumnKeyName(): req.GetId()})
			return builder
		})
	}

	return databaseDeleteHelper(r.mysqlDB, req)
}

func (r *MysqlRepository) TransactionExec(req *BaseRequest) error {
	if r == nil || r.mysqlDB == nil {
		glog.Errorln("MysqlRepository or mysql connection is null")
		return errorutil.ErrorSystem
	}

	return databaseTransactionExecHelper(r.mysqlDB, req)
}

func DatabaseQueryHelperEx(dest interface{}, db *sqlx.DB, req *BaseRequest) error {
	return databaseQueryHelper(dest, db, req)
}

func databaseQueryHelper(dest interface{}, db *sqlx.DB, req BaseRequester) error {
	if db == nil || req == nil || req.GetContext() == nil || dest == nil {
		glog.Errorln("databaseQueryHelper: db, or BaseRequest, or context, or dest is null")
		return errorutil.ErrorInvalidData
	}

	if req.GetSelectCB() == nil {
		glog.Errorln("databaseQueryHelper: select callback is null")
		return errorutil.ErrorInvalidData
	}

	sqlBuilder := sql_builder.Select(db.DriverName()).From(req.GetTableName())
	sqlBuilder, selectColumns := req.GetSelectCB()(sqlBuilder)
	if len(selectColumns) == 0 {
		selectColumns = req.GetColumns()
	}

	if len(selectColumns) == 0 {
		glog.Errorln("databaseQueryHelper: select columns list empty")
		return errorutil.ErrorInvalidData
	}

	sqlBuilder = sqlBuilder.Columns(selectColumns...)
	sqlQuery, args, err := sqlBuilder.ToSql()
	if err != nil {
		glog.Error("databaseQueryHelper error generate sql: ", err)
		return err
	}

	logText := fmt.Sprintf("sql: %s, args: %s", sqlQuery, args)
	//glog.Infof("databaseQueryHelper: %s\n", logText)
	destType := reflect.TypeOf(dest)
	if destType.Kind() == reflect.Ptr {
		destType = destType.Elem()
	}

	switch destType.Kind() {
	case reflect.Slice, reflect.Array:
		err = database.Select(req.GetContext(), db, dest, sqlQuery, args...)
		if err != nil {
			glog.Errorf("databaseQueryHelper: %s, exec select error: %s\n", logText, err)
			return err
		}
	case reflect.Struct, reflect.Map:
		err = database.Get(req.GetContext(), db, dest, sqlQuery, args...)
		if err != nil {
			glog.Errorf("databaseQueryHelper: %s, exec get error: %s\n", logText, err)
			return err
		}
	default:
		err = database.QueryRow(req.GetContext(), db, sqlQuery, args...).Scan(dest)
		if err != nil {
			glog.Errorf("databaseQueryHelper: %s, exec query row error: %s\n", logText, err)
			return err
		}
	}

	if req.GetPreProcessObjectCB() != nil {
		err = req.GetPreProcessObjectCB()(dest)
		if err != nil {
			glog.Errorln("databaseQueryHelper: call preprocess object error: ", err)
			return err
		}
	}

	return nil
}

func databaseInsertHelper(db *sqlx.DB, req BaseRequester) (int64, error) {
	if db == nil || req == nil || req.GetContext() == nil {
		glog.Errorln("databaseInsertHelper: db or BaseRequest or context is null")
		return 0, errorutil.ErrorInvalidData
	}

	if req.GetInsertCB() == nil {
		glog.Errorln("databaseInsertHelper: insert callback is null")
		return 0, errorutil.ErrorInvalidData
	}

	insertBuilder := sql_builder.Insert(db.DriverName(), req.GetTableName())
	execSql, args, err := req.GetInsertCB()(insertBuilder).ToSql()
	if err != nil {
		glog.Error("databaseInsertHelper: error generate sql: ", err)
		return 0, err
	}

	glog.Info("databaseInsertHelper sql: ", execSql, " args: ", args)
	rs, err := database.Exec(req.GetContext(), db, execSql, args...)
	if req.GetResultCB() != nil {
		err = req.GetResultCB()(rs, err)
	}

	if err != nil {
		glog.Error("databaseInsertHelper: exec insert error: ", err)
		return 0, err
	}

	return rs.LastInsertId()
}

func databaseUpdateHelper(db *sqlx.DB, req BaseRequester) (int64, error) {
	if db == nil || req == nil || req.GetContext() == nil {
		glog.Errorln("databaseUpdateHelper: db or BaseRequest or context is null")
		return 0, errorutil.ErrorInvalidData
	}

	if req.GetUpdateCB() == nil {
		glog.Errorln("databaseUpdateHelper: update callback is null")
		return 0, errorutil.ErrorInvalidData
	}

	updateBuilder := sql_builder.Update(db.DriverName(), req.GetTableName())
	execSql, args, err := req.GetUpdateCB()(updateBuilder).ToSql()
	if err != nil {
		glog.Error("databaseUpdateHelper: error generate sql: ", err)
		return 0, err
	}

	glog.Info("databaseUpdateHelper sql: ", execSql, " args: ", args)
	rs, err := database.Exec(req.GetContext(), db, execSql, args...)
	if req.GetResultCB() != nil {
		err = req.GetResultCB()(rs, err)
	}

	if err != nil {
		glog.Error("databaseUpdateHelper: exec update error: ", err)
		return 0, err
	}

	return rs.RowsAffected()
}

func databaseTransactionExecHelper(db *sqlx.DB, req BaseRequester) error {
	if db == nil || req == nil || req.GetContext() == nil {
		glog.Errorln("databaseTransactionHelper: db or BaseRequest or context is null")
		return errorutil.ErrorInvalidData
	}

	if len(req.GetTransactionCBs()) == 0 {
		glog.Errorln("databaseTransactionHelper: transaction callback list is empty")
		return errorutil.ErrorInvalidData
	}

	tx, err := db.Begin()
	if err != nil {
		glog.Error("databaseTransactionHelper error begin transaction: ", err)
		return errorutil.ErrorSystem
	}

	cbArgs := []interface{}{}
	for _, cb := range req.GetTransactionCBs() {
		if cb != nil {
			execSql, execArgs, nextArgs, err := cb(cbArgs...)
			rs, err := database.TxExec(req.GetContext(), tx, execSql, execArgs...)
			if err != nil {
				tx.Rollback()
				return err
			}
			cbArgs = nextArgs
			if cbArgs == nil {
				cbArgs = []interface{}{}
			}
			cbArgs = append(cbArgs, rs)
		}
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func databaseDeleteHelper(db *sqlx.DB, req BaseRequester) error {
	if db == nil || req == nil || req.GetContext() == nil {
		glog.Errorln("databaseDeleteHelper: db or BaseRequest or context is null")
		return errorutil.ErrorInvalidData
	}

	if req.GetDeleteCB() == nil {
		glog.Errorln("databaseDeleteHelper: insert callback is null")
		return errorutil.ErrorInvalidData
	}

	deleteBuilder := sql_builder.Delete(db.DriverName(), req.GetTableName())
	execSql, args, err := req.GetDeleteCB()(deleteBuilder).ToSql()
	if err != nil {
		glog.Error("databaseDeleteHelper: error generate sql: ", err)
		return err
	}

	glog.Info("databaseDeleteHelper sql: ", execSql, " args: ", args)
	_, err = database.Exec(req.GetContext(), db, execSql, args...)
	if err != nil {
		glog.Error("databaseDeleteHelper: exec delete error: ", err)
		return err
	}

	return nil
}

func GetResultFromArgs(args ...interface{}) (sql.Result, error) {
	for _, arg := range args {
		rs, ok := arg.(sql.Result)
		if ok {
			return rs, nil
		}
	}
	return nil, errorutil.ErrorNotFound
}
