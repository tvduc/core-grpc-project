package report

import (
	"context"
	"fmt"
	"reflect"

	"github.com/golang/glog"

	"git.zapa.cloud/merchant-tools/helper/aggregation"
	"git.zapa.cloud/merchant-tools/helper/cache"
	"git.zapa.cloud/merchant-tools/helper/concurrency"
	esHelper "git.zapa.cloud/merchant-tools/helper/elastic"
	"gopkg.in/olivere/elastic.v6"
)

type sortDataExcel func()

func IsExcelRequest(req interface{}) bool {
	type Req interface {
		GetPageIndex() int32
		GetPageSize() int32
	}
	if fn, ok := req.(Req); ok {
		return fn.GetPageIndex() == 1 && fn.GetPageSize() >= 500
	}
	return false
}
func getPageIndexRequest(req interface{}) int {
	type Req interface {
		GetPageIndex() int32
	}
	if fn, ok := req.(Req); ok {
		return int(fn.GetPageIndex())
	}
	return 0
}
func setVal(v reflect.Value, val float64) {
	switch v.Kind() {
	case reflect.Int, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Int8:
		v.SetInt(int64(val))
	case reflect.Float32, reflect.Float64:
		v.SetFloat(val)
	}
}

//GetReport ...
func GetReport(ctx context.Context, req interface{}, cache cache.Cache, esDetail cache.EsQueryFn, esExcel cache.EsScrollQuery, querySum *elastic.BoolQuery, detail, summary interface{}, sortFn sortDataExcel) (int64, error) {
	var (
		totalRecord int64
	)
	cc := concurrency.New()
	valueSum := reflect.ValueOf(summary)
	fn, useSum := req.(interface {
		GetShowSummary() bool
	})
	if useSum {
		useSum = fn.GetShowSummary()
	} else { //force true if do not have GetShowSummary
		useSum = true
	}
	if useSum && valueSum.Kind() == reflect.Ptr && !valueSum.IsNil() && getPageIndexRequest(req) == 1 {
		//extract sumfield
		val := valueSum.Elem()
		vt := val.Type()
		sumFields := []string{}
		//sumNames := []string{}
		sumVals := []reflect.Value{}
		for i := 0; i < vt.NumField(); i++ {
			f := vt.Field(i)
			if tag, ok := f.Tag.Lookup("sum"); ok {
				v := val.Field(i)
				if v.IsValid() && v.IsZero() {
					sumFields = append(sumFields, tag)
					sumVals = append(sumVals, v)
				} else {
					glog.Infoln("Ignore calculate", f.Name)
				}

			}
		}
		if len(sumFields) > 0 {
			cc.Add(func() error {
				aggSum := &aggregation.ListAggregation{}
				if err := aggregation.DoAgg(ctx, cache.Search(), querySum, aggregation.NewBuilder().Sum(sumFields...).Response(aggSum)); err != nil {
					glog.Errorln(err)
					return err
				}
				sum := aggSum.GetSums()
				if len(sum) != len(sumVals) {
					err := fmt.Errorf("Invalid field %d:%d", len(sum), len(sumVals))
					return err
				}
				for i, v := range sumVals {
					setVal(v, sum[i])
				}
				return nil
			})
		}

	}
	cc.Add(func() error {
		var err error
		if IsExcelRequest(req) && esExcel != nil {
			totalRecord, err = cache.QueryAll(ctx, esExcel, detail)
			if err == nil && sortFn != nil {
				sortFn()
			}
		} else {
			totalRecord, err = cache.QueryPaging(ctx, esDetail, detail)
		}
		return err
	})
	if err := cc.Do(); err != nil {
		glog.Errorln(err)
		return 0, err
	}
	return totalRecord, nil

}

//GetReportUsingSumDistinc ...
func GetReportUsingSumDistinc(ctx context.Context, req interface{}, groudField string, cache cache.Cache, esDetail cache.EsQueryFn, esExcel cache.EsScrollQuery, querySum *elastic.BoolQuery, detail, summary interface{}, sortFn sortDataExcel) (int64, error) {
	var (
		totalRecord int64
	)
	cc := concurrency.New()
	valueSum := reflect.ValueOf(summary)
	fn, useSum := req.(interface {
		GetShowSummary() bool
	})
	if useSum {
		useSum = fn.GetShowSummary()
	} else { //force true if do not have GetShowSummary
		useSum = true
	}
	if useSum && valueSum.Kind() == reflect.Ptr && !valueSum.IsNil() && getPageIndexRequest(req) == 1 {
		//extract sumfield
		val := valueSum.Elem()
		vt := val.Type()
		sumFields := []string{}
		sumVals := []reflect.Value{}
		sumDistincFields := []string{}
		sumDistincVals := []reflect.Value{}
		for i := 0; i < vt.NumField(); i++ {
			f := vt.Field(i)
			v := val.Field(i)
			if sumDistinTag, ok := f.Tag.Lookup("sum_distinc"); ok {
				sumDistincFields = append(sumDistincFields, sumDistinTag)
				sumDistincVals = append(sumDistincVals, v)
			} else if sumTag, ok := f.Tag.Lookup("sum"); ok {
				if v.IsZero() {
					sumFields = append(sumFields, sumTag)
					sumVals = append(sumVals, v)
				} else {
					glog.Infoln("Ignore calculate", f.Name)
				}
			}
		}
		glog.Infoln(sumFields)
		glog.Infoln(sumDistincFields)

		cc.Add(func() error {
			sumFields = append(sumFields, sumDistincFields...)
			var (
				aggs          = []aggregation.EsAgg{}
				sumAgg        = &aggregation.ListAggregation{}
				sumAggDistinc = &aggregation.ListAggregation1D{}
			)
			if len(sumFields) > 0 {
				aggs = append(aggs, aggregation.NewBuilder().Sum(sumFields...).Response(sumAgg))
			}
			if len(sumDistincFields) > 0 {
				aggs = append(aggs, aggregation.NewBuilder().Terms(fmt.Sprintf("%s,1024,2", groudField)).Max(sumDistincFields...).Response(sumAggDistinc))
			}
			if err := aggregation.DoAgg(ctx, cache.Search().Size(0), querySum, aggs...); err != nil {
				glog.Errorln(err)
				return err
			}
			sums := sumAgg.GetSums()
			i := 0
			for _, v := range sumVals {
				setVal(v, sums[i])
				i++
			}
			if len(sumAggDistinc.GetAggs()) >= 1024 {
				glog.Errorln("sum aggs more than 1024")
			}

			for ii, v := range sumDistincVals {
				s := sums[i]
				for _, agg := range sumAggDistinc.GetAggs() {
					s = s - float64(agg.GetCount()-1)*agg.GetSums()[ii]
				}
				glog.Infoln(int64(s), int64(sums[i]))
				setVal(v, s)
				i++

			}
			return nil
		})
	}

	cc.Add(func() error {
		var err error
		if IsExcelRequest(req) {
			totalRecord, err = cache.QueryAll(ctx, esExcel, detail)
			if err == nil && sortFn != nil {
				sortFn()
			}
		} else {
			totalRecord, err = cache.QueryPaging(ctx, esDetail, detail)
		}
		return err
	})
	if err := cc.Do(); err != nil {
		glog.Errorln(err)
		return 0, err
	}
	return totalRecord, nil

}

//GetSumReport ...
func GetSumReport(ctx context.Context, cache cache.Cache, querySum *elastic.BoolQuery, summary interface{}) error {
	valueSum := reflect.ValueOf(summary)
	if valueSum.Kind() != reflect.Ptr || valueSum.IsNil() {
		return fmt.Errorf("Invalid summary")
	}
	//extract sumfield
	val := valueSum.Elem()
	vt := val.Type()
	sumFields := []string{}
	sumVals := []reflect.Value{}
	//sumNames := []string{}
	for i := 0; i < vt.NumField(); i++ {
		f := vt.Field(i)
		if tag, ok := f.Tag.Lookup("sum"); ok {
			v := val.Field(i)
			if v.IsValid() && v.IsZero() {
				sumFields = append(sumFields, tag)
				sumVals = append(sumVals, v)
			} else {
				glog.Infoln("Ignore calculate", f.Name)
			}

		}
	}
	if len(sumFields) > 0 {
		aggSum := &aggregation.ListAggregation{}
		if err := aggregation.DoAgg(ctx, cache.Search(), querySum, aggregation.NewBuilder().Sum(sumFields...).Response(aggSum)); err != nil {
			glog.Errorln(err)
			return err
		}
		sum := aggSum.GetSums()
		if len(sum) != len(sumVals) {
			err := fmt.Errorf("Invalid field %d:%d", len(sum), len(sumVals))
			return err
		}
		for i, v := range sumVals {
			setVal(v, sum[i])
		}
	}
	return nil

}

type PagingReq interface {
	GetPageIndex() int32
	GetPageSize() int32
}

func ESQuery(ctx context.Context, c cache.Cache, queryFn cache.EsQueryFn, req, resp interface{}) (int64, error) {
	return ESQueryEx(ctx, c, queryFn, nil, req, resp)
}

func ESQueryEx(ctx context.Context, c cache.Cache, queryFn cache.EsQueryFn, bquery *elastic.BoolQuery, req, resp interface{}) (int64, error) {

	query := esHelper.BuildQuery(bquery, req)
	var idx, size int
	if preq, ok := req.(PagingReq); ok {
		idx = int(preq.GetPageIndex())
		size = int(preq.GetPageSize())
	}

	if idx > 0 && size > 0 {
		//Return paging
		return c.QueryPaging(ctx, func(svc *elastic.SearchService) *elastic.SearchService {
			svc = svc.Query(query).From((idx - 1) * size).Size(size)
			if queryFn != nil {
				svc = queryFn(svc)
			}
			return svc
		}, resp)
	}

	//Return all: no sort
	return c.QueryAll(ctx, func(svc *elastic.ScrollService) *elastic.ScrollService {
		svc.Query(query)
		return svc
	}, resp)
}
