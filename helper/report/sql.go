package report

import (
	"context"

	"git.zapa.cloud/merchant-tools/helper/database"
	builder "git.zapa.cloud/merchant-tools/helper/database/builder"
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

/*PaginateData  hàm phân trang :
đầu vào: số trang, số dòng trên 1 trang, tổng số dòng của data
đầu ra: index bắt đầu và index kết thúc
*/
func PaginateData(pageNumber int32, pageSize int32, dataLength int32) (int32, int32) {
	start := (pageNumber - 1) * pageSize
	if start+pageSize >= dataLength {
		return start, dataLength
	} else {
		return start, start + pageSize
	}
}

/*CountTotalPages đếm tổng số trang :
đầu vào: tổng số dòng của data, số dòng trên 1 trang
đầu ra: tổng số trang
*/
func CountTotalPages(totalRecord int32, pageSize int32) int32 {
	if pageSize <= 0 {
		return -1
	}
	return totalRecord + (pageSize-1)/pageSize
}

/*GetReport :
Đầu vào:
	dest: danh sách kết quả report response
	db: connection của DB
	field: là tên trường của dest
	table: bảng chứa field
	pageIndex: trang
	pageSize: số record trên mỗi trang
Đầu ra:
	Lỗi nếu có lỗi xuất hiện

*/

//GetReportX giống GetReport nhưng sử dung sqlx
func GetReportX(ctx context.Context, db *sqlx.DB, dest interface{}, field, table string, pageIndex, pageSize int32, conditionCB func(sq.SelectBuilder) sq.SelectBuilder) error {

	query := builder.Select(db.DriverName(), "COALESCE("+field+",'{}')").From(table)
	query = conditionCB(query).Offset(uint64((pageIndex - 1) * pageSize)).Limit(uint64(pageSize))
	return database.SQSelectSQLXEx(ctx, db, dest, &query)
}

/*SQLXGetReportSummary : tương tự SQLGetReportSummary
 */
func SQLXGetReportSummary(ctx context.Context, db *sqlx.DB, dest interface{}, field []string, table string, conditionCB func(sq.SelectBuilder) sq.SelectBuilder) error {
	query := builder.Select(db.DriverName(), field...).From(table)
	sql, args, err := conditionCB(query).ToSql()
	if err != nil {
		return err
	}
	return database.Get(ctx, db, dest, sql, args...)
}

/*SQLXGetReportSummaryList : tương tự SQLGetReportSummary
 */
func SQLXGetReportSummaryList(ctx context.Context, db *sqlx.DB, dest interface{}, field []string, table string, conditionCB func(sq.SelectBuilder) sq.SelectBuilder) error {
	query := builder.Select(db.DriverName(), field...).From(table)
	sql, args, err := conditionCB(query).ToSql()
	if err != nil {
		return err
	}
	return database.Select(ctx, db, dest, sql, args...)
}

func SQLXGetReportList(ctx context.Context, db *sqlx.DB, dest interface{}, field []string, table string, pageIndex, pageSize int32, conditionCB func(sq.SelectBuilder) sq.SelectBuilder) error {
	query := builder.Select(db.DriverName(), field...).From(table)
	sql, args, err := conditionCB(query).Offset(uint64((pageIndex - 1) * pageSize)).Limit(uint64(pageSize)).ToSql()
	if err != nil {
		return err
	}
	return database.Select(ctx, db, dest, sql, args...)
}
