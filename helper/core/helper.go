package core

import (
	"sync"
)

type PersitentCache interface {
	Get(key string, get func() (interface{}, error)) (interface{}, error)
}
type cache struct {
	cacheTbl map[string]interface{}
	mtx      *sync.Mutex
}

var (
	globalCache = NewPersitentCache()
)

//NewPersitentCache ...
func NewPersitentCache() PersitentCache {
	return &cache{
		cacheTbl: map[string]interface{}{},
		mtx:      &sync.Mutex{},
	}
}
func (c *cache) Get(key string, get func() (interface{}, error)) (interface{}, error) {
	var err error
	c.mtx.Lock()
	defer c.mtx.Unlock()
	res, ok := c.cacheTbl[key]
	if !ok {
		res, err = get()
		if err != nil {
			return nil, err
		}
		c.cacheTbl[key] = res
	}
	return res, nil
}

//GetCache ...
func GetCache(key string, get func() (interface{}, error)) (interface{}, error) {
	return globalCache.Get(key, get)
}
