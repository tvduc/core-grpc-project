# Hướng dẫn sử dụng `gen tool`

1- Gen tool nằm trong thư mục helper/generate

2- Gen service `helloworld` `./gen.sh helloworld` sẽ tạo ra:

```
    * merchant-tools/'helloworld' chứa main.go cơ bản để chạy service, handler/handler.go để nhận và thực hiện các logic service

    * protobuf/'helloworld' chứa file proto.proto định nghĩa cho protobuf

```

3- Vào thư mục protbuf/`helloworld` sửa file proto.proto, chạy `./buildproto.sh` để tạo ra các file go service của grpc

4- Vào merchant-tools/`helloworld`/handler/handler.go implement các interface được định nghĩa trong file proto

5- Vào merchant-tools/`helloworld` chạy `go run main.go` để chạy instance service

# Chạy service API-Gateway: `go run api-gateway/main.go`


