package batch

import (
	"context"
	"fmt"
	"time"

	"github.com/golang/glog"

	"git.zapa.cloud/merchant-tools/helper/graceful"
	tracingHelper "git.zapa.cloud/merchant-tools/helper/opentracing"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var (
	processorTag = opentracing.Tag{Key: string(ext.Component), Value: "processor"}
)

type updateFn func(context.Context, []interface{}, int) error

const (
	defaultBatchSize = 1024
	defaultInterval  = 2 * time.Second
	defaultName      = "BatchProcessor"
)

//Processor ...
type Processor struct {
	c         chan interface{}
	size      int
	name      string
	interval  time.Duration
	flushC    chan context.Context
	flushAckC chan struct{}
}

//NewProcessor ...
func NewProcessor() *Processor {
	return &Processor{
		size:      defaultBatchSize,
		name:      defaultName,
		interval:  defaultInterval,
		flushC:    make(chan context.Context),
		flushAckC: make(chan struct{}),
	}
}

//Name ...
func (p *Processor) Name(name string) *Processor {
	p.name = name
	return p
}

//Size ...
func (p *Processor) Size(size int) *Processor {
	p.size = size
	return p
}

//Interval ...
func (p *Processor) Interval(interval time.Duration) *Processor {
	p.interval = interval
	return p
}

//Start ...
func (p *Processor) Start(fn updateFn) *Processor {
	p.c = make(chan interface{}, p.size)
	go p.start(fn)
	return p
}
func (p *Processor) start(fn updateFn) {
	store := make([]interface{}, p.size)
	id := 0
	ticker := time.NewTicker(p.interval)
	stop := make(chan bool, 1)
	process := func(ctx context.Context) {
		if id <= 0 {
			return
		}
		if ctx == nil {
			ctx = context.Background()
		}
		ctx, span := tracingHelper.NewClientSpanFromContext(ctx, processorTag, fmt.Sprintf("%s processor %d", p.name, id))
		err := fn(ctx, store, id)
		tracingHelper.FinishClientSpan(span, err)
		if err != nil {
			glog.Errorln(err)
		} else {
			id = 0
		}
	}
	graceful.Stop(time.Second, func() {
		stop <- true
		process(nil)
		glog.Infoln("Stop patch", p.name)
	})
	for {
		select {
		case <-ticker.C:
			process(nil)
		case ctx := <-p.flushC:
			process(ctx)
			p.flushAckC <- struct{}{}
		case <-stop:
			//process()
			break
		case c := <-p.c:
			if id < p.size {
				store[id] = c
				id++
			} else {
				glog.Errorln("invalid index ", id, p.size)
			}
			if id >= p.size {
				process(nil)
			}

		}
	}
}

//Add ...
func (p *Processor) Add(item interface{}) *Processor {
	if p.c != nil {
		p.c <- item
	}
	return p
}

//Flush() ...
func (p *Processor) Flush(ctx context.Context) {
	if p == nil {
		return
	}
	p.flushC <- ctx
	<-p.flushAckC // wait for completion
}
