package cache

//Options ...
type Options struct {
	Refresh bool
}

//Option ...
type Option func(*Options)

//NewOption ...
func NewOption(setters ...Option) *Options {
	opts := &Options{
		Refresh: false,
	}
	for _, setter := range setters {
		setter(opts)
	}
	return opts
}

//Refresh ...
func Refresh(refresh bool) Option {
	return func(arg *Options) {
		arg.Refresh = refresh
	}
}
