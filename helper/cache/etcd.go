package cache

// import (
// 	"context"
// 	"database/sql"
// 	"encoding/json"
// 	"fmt"
// 	"reflect"
// 	"sync"

// 	"git.zapa.cloud/merchant-tools/helper/concurrency"
// 	"git.zapa.cloud/merchant-tools/helper/copier"
// 	"git.zapa.cloud/merchant-tools/helper/env"
// 	"github.com/golang/glog"
// 	etcd "go.etcd.io/etcd/clientv3"
// 	es "gopkg.in/olivere/elastic.v6"
// )

// type changeCallback func(kind int, key string, v ...interface{})

// type etcdCache struct {
// 	esSearch
// 	localCache sync.Map
// 	cli        *etcd.Client
// }
// type storeCache struct {
// 	v   interface{}
// 	buf []byte
// }

// func (c *etcdCache) makeObject(data []byte) interface{} {
// 	v := reflect.New(c.dataType).Interface()
// 	if err := json.Unmarshal(data, v); err != nil {
// 		glog.Errorln(err)
// 	}
// 	return v
// }
// func newEtcdCache(search *es.Client, cli *etcd.Client, tables ...string) *etcdCache {
// 	c := &etcdCache{cli: cli}
// 	c.toEs = GetESMap
// 	c.start(search, func(ctx context.Context, value interface{}, key ...string) error {
// 		return c.MGet(ctx, value, key...)
// 	}, tables...)
// 	c.get = c.Get
// 	c.refresh = true

// 	return c
// }
// func (c *etcdCache) Options(opts ...Option) Cache {
// 	option := NewOption(opts...)
// 	c.refresh = option.Refresh
// 	return c
// }
// func (c *etcdCache) set(k string, v interface{}, f changeCallback) {
// 	if f != nil {
// 		v1, _ := c.localCache.Load(k)
// 		f(1, k, v, v1)
// 	}
// 	buf, err := protoEncode(v)
// 	if err != nil {
// 		glog.Errorln(err)
// 	}
// 	c.localCache.Store(k, &storeCache{v, buf})

// }
// func (c *etcdCache) del(k string, f changeCallback) {
// 	if f != nil {
// 		if v, ok := c.localCache.Load(k); ok {
// 			if item, ok := v.(*storeCache); ok {
// 				f(0, k, item.v)
// 			}

// 		}

// 	}
// 	c.localCache.Delete(k)
// }
// func (c *etcdCache) init(f changeCallback) {
// 	cli := c.cli
// 	prefix := c.makeKVKey("")
// 	l := len(prefix)
// 	resp, err := cli.Get(context.Background(), prefix, etcd.WithPrefix())
// 	if err == nil {
// 		for _, ev := range resp.Kvs {
// 			k := ev.Key
// 			if len(k) > l {
// 				c.set(string(k[l:]), c.makeObject(ev.Value), f)
// 			}
// 		}
// 	}
// 	go func() {
// 		rch := cli.Watch(context.Background(), prefix, etcd.WithPrefix())
// 		for wresp := range rch {
// 			for _, ev := range wresp.Events {
// 				if len(ev.Kv.Key) <= l {
// 					glog.Errorln(ev.Kv.Key)
// 					continue
// 				}
// 				k := string(ev.Kv.Key[l:])
// 				switch ev.Type {
// 				case etcd.EventTypePut:
// 					c.set(k, c.makeObject(ev.Kv.Value), f)
// 				case etcd.EventTypeDelete:
// 					c.del(k, f)

// 				}
// 			}

// 		}
// 	}()
// }
// func (c *etcdCache) ResetData() {
// 	if env.IsProd() {
// 		glog.Warningln("Invalid ...")
// 		return
// 	}
// 	ids, ctx := []string{}, context.Background()
// 	allItems := c.GetAll()
// 	for id := range allItems {
// 		ids = append(ids, id)
// 	}
// 	glog.Infoln(ids)
// 	if len(ids) > 0 {
// 		if e := c.Del(ctx, ids...); e != nil {
// 			glog.Errorln(e)
// 		}
// 	}
// }
// func (c *etcdCache) GetInCache(ctx context.Context, value interface{}, key string) error {
// 	return c.Get(ctx, value, key)
// }
// func (c *etcdCache) MGet(ctx context.Context, resp interface{}, keys ...string) error {
// 	if len(keys) == 0 {
// 		return nil
// 	}
// 	c.once.Do(func() {
// 		c.dataType = getDataType(resp)
// 	})
// 	n := len(keys)
// 	values := reflect.MakeSlice(reflect.SliceOf(reflect.PtrTo(c.dataType)), n, n)

// 	for i, key := range keys {
// 		/*if v, ok := c.localCache.Load(key); ok {
// 			values.Index(i).Set(reflect.ValueOf(v))
// 		} else {
// 			values.Index(i).Set(reflect.New(c.dataType))
// 		}*/
// 		item := reflect.New(c.dataType)
// 		if err := c.Get(ctx, item.Interface(), key); err != nil && err != sql.ErrNoRows {
// 			glog.Errorln(err)
// 			return err
// 		}
// 		values.Index(i).Set(item)
// 	}
// 	return copier.Copy(resp, values.Interface())
// }
// func (c *etcdCache) Get(ctx context.Context, value interface{}, key string) error {
// 	if v, ok := c.localCache.Load(key); ok {
// 		if item, ok := v.(*storeCache); ok {
// 			if item.buf != nil {
// 				return protoDecode(string(item.buf), value)
// 			}
// 			return copier.Copy(value, item.v)

// 		}
// 		//return copier.Copy(value, v)
// 	}
// 	return sql.ErrNoRows

// }
// func (c *etcdCache) MSet(ctx context.Context, keys []string, values []interface{}) error {
// 	var (
// 		err error
// 	)
// 	n := len(keys)
// 	if len(keys) != len(values) {
// 		err = fmt.Errorf("Invalid size %d:%d", len(keys), len(values))
// 		glog.Errorln(err)
// 		return err
// 	}
// 	if n == 0 {
// 		return nil
// 	}

// 	cc := concurrency.New()
// 	//Es update
// 	if bulkProcessor != nil && c.search != nil {
// 		cc.Add(func() error {
// 			c.updateMulti(ctx, keys, values)
// 			return nil
// 		})
// 	}
// 	for _i := 0; _i < n; _i++ {
// 		i := _i
// 		cc.Add(func() error {
// 			v, err := json.Marshal(values[i])
// 			if err != nil {
// 				glog.Errorln(err)
// 				return err
// 			}
// 			_, err = c.cli.Put(ctx, c.makeKVKey(keys[i]), string(v))
// 			if err != nil {
// 				glog.Errorln(err)
// 			} else {
// 				buf, err := protoEncode(values[i])
// 				if err != nil {
// 					glog.Errorln(err)
// 				}
// 				c.localCache.Store(keys[i], &storeCache{values[i], buf})
// 				//c.localCache.Store(keys[i], values[i])
// 			}
// 			return nil
// 		})
// 	}

// 	return cc.Do()
// }
// func (c *etcdCache) Set(ctx context.Context, key string, value interface{}) error {

// 	cc := concurrency.New()
// 	if bulkProcessor != nil && c.search != nil {
// 		cc.Add(func() error {
// 			c.update(ctx, key, value)
// 			return nil
// 		})
// 	}
// 	cc.Add(func() error {
// 		v, err := json.Marshal(value)
// 		if err != nil {
// 			glog.Errorln(err)
// 			return err
// 		}
// 		_, err = c.cli.Put(ctx, c.makeKVKey(key), string(v))
// 		if err != nil {
// 			glog.Errorln(err)
// 		} else {
// 			buf, err := protoEncode(value)
// 			if err != nil {
// 				glog.Errorln(err)
// 			}
// 			c.localCache.Store(key, &storeCache{value, buf})
// 			//c.localCache.Store(key, value)
// 		}
// 		return nil
// 	})

// 	return cc.Do()
// }

// func (c *etcdCache) Del(ctx context.Context, keys ...string) error {
// 	if len(keys) == 0 {
// 		return nil
// 	}

// 	cc := concurrency.New()
// 	for _, _key := range keys {
// 		key := _key
// 		cc.Add(func() error {
// 			if dresp, err := c.cli.Delete(ctx, c.makeKVKey(key)); err == nil && dresp.Deleted == 1 {
// 				c.localCache.Delete(key)
// 			}
// 			return nil
// 		})
// 	}
// 	if bulkProcessor != nil && c.search != nil {
// 		cc.Add(func() error {
// 			c.delete(ctx, keys...)

// 			return nil
// 		})
// 	}

// 	return cc.Do()
// }
// func (c *etcdCache) makeKVKey(key string) string {
// 	return fmt.Sprintf("%s/%s/%s", cachePrefix, c.table, key)
// }
// func (c *etcdCache) GetAll() map[string]interface{} {
// 	res := make(map[string]interface{})

// 	c.localCache.Range(func(k, v interface{}) bool {
// 		if _k, ok := k.(string); ok {
// 			if _v, ok := v.(*storeCache); ok {
// 				res[_k] = _v.v
// 			}

// 		}
// 		return true
// 	})
// 	return res
// }
