package cache

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"github.com/golang/glog"
	protobuf "github.com/golang/protobuf/proto"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/tikv/client-go/rawkv"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/concurrency"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/copier"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/database"
	tracingHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/opentracing"
)

var (
	tikvTag = opentracing.Tag{Key: string(ext.Component), Value: "tikvTag"}
	version = "2.0"
)

type tikvCache struct {
	esSearch
	tikv *rawkv.Client
}

func (c *tikvCache) importDB(ctx context.Context, db *sqlx.DB, data protobuf.Message) error {

	migkey := []byte(c.makeKVKey("TiKVMigrate"))
	v, err := c.tikv.Get(ctx, migkey)
	if err != nil {
		return err
	}
	if version == string(v) {
		return nil
	}
	glog.Infoln("Start migrate ", c.table)
	t0 := time.Now()
	total, limit := int(0), uint64(4<<10)
	lastID := ""
	for {
		query := sq.Select("ID", "coalesce(Data,'{}')").OrderBy("ID DESC").Limit(limit).From(c.table)
		if lastID != "" {
			query = query.Where(sq.Lt{"ID": lastID})
		}
		sql, args, err := query.ToSql()
		if err != nil {
			glog.Errorln(err)
			return err
		}
		rows, err := database.Query(ctx, db, sql, args...)
		if err != nil {
			glog.Errorln(err)
			return err
		}
		defer rows.Close()
		n := 0
		keys, values := make([][]byte, 0, limit), make([][]byte, 0, limit)
		for rows.Next() {
			var k, v string
			//Get k,v from db
			if err = rows.Scan(&k, &v); err != nil {
				glog.Errorln(err)
				return err
			}
			if lastID == "" || lastID > k {
				lastID = k
			}
			data.Reset()
			if err := json.Unmarshal([]byte(v), data); err != nil {
				glog.Errorln(err)
				continue
			}
			if pb, err := protoEncode(data); err != nil {
				glog.Errorln(err)
				continue
			} else {
				keys = append(keys, []byte(c.makeKVKey(k)))
				values = append(values, pb)
			}

			n++
		}
		if n == 0 {
			break
		}
		if err := c.tikv.BatchPut(ctx, keys, values); err != nil {
			glog.Errorln(err)
			return err
		}
		glog.Infoln(c.table, lastID, total, time.Since(t0))
		if n < int(limit) {
			break
		}
	}
	if err := c.tikv.Put(ctx, migkey, []byte(version)); err != nil {
		glog.Errorln(err)
	}
	glog.Infoln("Finish migrate "+c.table+" : ", time.Since(t0))

	return nil

}
func (c *tikvCache) sync(ctx context.Context, rows []interface{}, n int) error {
	update := map[string][]byte{}
	var err error
	for i := 0; i < n; i++ {
		if row, ok := rows[i].(*writeRow); ok {
			if update[row.id], err = protoEncode(row.data); err != nil {
				glog.Errorln(err)
				return err
			}
		}
	}
	if n = len(update); n <= 0 {
		return nil
	}
	keys, values := make([][]byte, n), make([][]byte, n)
	i := 0
	for k, v := range update {
		keys[i] = []byte(c.makeKVKey(k))
		values[i] = v
		i++
	}
	return c.tikv.BatchPut(ctx, keys, values)
}
func (c *tikvCache) ResetData() {
}
func (c *tikvCache) GetInCache(ctx context.Context, value interface{}, key string) error {
	return c.Get(ctx, value, key)
}
func (c *tikvCache) MGet(ctx context.Context, resp interface{}, keys ...string) error {
	if len(keys) == 0 {
		return nil
	}
	c.once.Do(func() {
		c.dataType = getDataType(resp)
	})
	n := len(keys)
	_, span := tracingHelper.NewClientSpanFromContext(ctx, tikvTag, "MGet")
	values := reflect.MakeSlice(reflect.SliceOf(reflect.PtrTo(c.dataType)), n, n)
	k := make([][]byte, n)

	for i, key := range keys {
		k[i] = []byte(c.makeKVKey(key))
		values.Index(i).Set(reflect.New(c.dataType))
	}
	v, err := c.tikv.BatchGet(ctx, k)
	if err == nil {
		for i, _v := range v {
			value := values.Index(i).Interface()
			if _v != nil {
				if err = protoDecode(string(_v), value); err != nil {
					glog.Errorln(string(_v), err)
					break
				}
			}

		}
		err = copier.Copy(resp, values.Interface())

	} else {
		glog.Errorln(err)
	}

	tracingHelper.FinishClientSpan(span, err)
	return err
}
func (c *tikvCache) Get(ctx context.Context, value interface{}, key string) error {
	key = c.makeKVKey(key)
	_, span := tracingHelper.NewClientSpanFromContext(ctx, tikvTag, "Get")
	data, err := c.tikv.Get(ctx, []byte(key))
	if err != nil {
		glog.Errorln(err)
	} else if data == nil {
		err = sql.ErrNoRows
	} else {
		err = protoDecode(string(data), value)
	}
	tracingHelper.FinishClientSpan(span, err)
	return err
}
func (c *tikvCache) MSet(ctx context.Context, keys []string, values []interface{}) error {
	var (
		err error
	)
	n := len(keys)
	if len(keys) != len(values) {
		err = fmt.Errorf("Invalid size %d:%d", len(keys), len(values))
		glog.Errorln(err)
		return err
	}
	if n == 0 {
		return nil
	}

	cc := concurrency.New()
	//Es update
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			c.updateMulti(ctx, keys, values)
			return nil
		})
	}
	cc.Add(func() error {
		//try to write to tikv for some immediate reads
		var err error
		n := len(keys)
		tiKeys, tiValues := make([][]byte, n), make([][]byte, n)

		for i := 0; i < len(keys); i++ {
			if tiValues[i], err = protoEncode(values[i]); err == nil {
				//c.tikv.Put(ctx, []byte(c.makeKVKey(keys[i])), data)
				tiKeys[i] = []byte(c.makeKVKey(keys[i]))
			} else {
				glog.Errorln(err)
				return err
			}
		}
		if err = c.tikv.BatchPut(ctx, tiKeys, tiValues); err != nil {
			glog.Errorln(err)
		}

		return err
	})

	return cc.Do()
}
func (c *tikvCache) Set(ctx context.Context, key string, value interface{}) error {

	cc := concurrency.New()
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			//esHelper.DoBulkProcessorAdd(ctx, bulkProcessor, c.BulkIndexRequest().Id(key).Doc(c.toEs(value)))
			c.update(ctx, key, value)
			return nil
		})
	}
	cc.Add(func() error {
		if data, err := protoEncode(value); err == nil {
			c.tikv.Put(ctx, []byte(c.makeKVKey(key)), data)
		} else {
			glog.Errorln(err)
			return err
		}
		return nil
	})

	return cc.Do()
}

func (c *tikvCache) Del(ctx context.Context, keys ...string) error {
	if len(keys) == 0 {
		return nil
	}

	cc := concurrency.New()
	cc.Add(func() error {
		k := make([][]byte, len(keys))
		for i, key := range keys {
			k[i] = []byte(c.makeKVKey(key))

		}
		return c.tikv.BatchDelete(ctx, k)
	})
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			c.delete(ctx, keys...)

			return nil
		})
	}

	return cc.Do()
}
func (c *tikvCache) Options(opts ...Option) Cache {
	option := NewOption(opts...)
	c.refresh = option.Refresh
	return c
}
