package cache

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"sync"
	"time"

	"golang.org/x/time/rate"

	sq "github.com/Masterminds/squirrel"
	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/jmoiron/sqlx"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/batch"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/concurrency"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/copier"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/database"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/env"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/lock"
	tracingHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/opentracing"
	es "gopkg.in/olivere/elastic.v6"
)

const (
	//CacheKey ...
	CacheKey = "ID"
	//CacheValue ...
	CacheValue = "Data"
	//CacheExpire ...

	cacheVersion    = "04042019"
	batchSize       = 1024
	deleteKeyScript = ` local keys=redis.call('keys',KEYS[1])
	if next(keys) ~= nil then
		return redis.call('del', unpack(keys))
	end
	return 0
	`
	sqlCreateCacheTbl = `CREATE TABLE IF NOT EXISTS %s  (
		ID varchar(256),
		Data MEDIUMTEXT,
		PRIMARY KEY (ID)
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`
)

var (
	//CacheExpire ...
	CacheExpire = time.Hour * 30 * 24
	redisTag    = opentracing.Tag{Key: string(ext.Component), Value: "redisTag"}
)

type redisCache struct {
	esSearch
	redis      *redis.Client
	db         *sqlx.DB
	key, value string
	expire     time.Duration
	limiter    *rate.Limiter
}

func createRedisCache(search *es.Client, redis *redis.Client, db *sqlx.DB, expire time.Duration, tables ...string) *redisCache {
	c := &redisCache{
		redis:   redis,
		db:      db,
		expire:  expire,
		key:     CacheKey,
		value:   CacheValue,
		limiter: rate.NewLimiter(rate.Every(time.Second/2), 60*5),
	}

	c.toEs = GetESMap
	c.batchProcessor = batch.NewProcessor().Name(tables[0]).Start(c.sync)
	c.start(search, func(ctx context.Context, value interface{}, key ...string) error {
		return c.MGet(ctx, value, key...)
	}, tables...)
	c.get = c.Get
	if c.db != nil {
		c.migrate()
	}

	return c
}

//NewRedisCache ...
func NewRedisCache(search *es.Client, redis *redis.Client, db *sqlx.DB, table string, expire time.Duration) Cache {
	return createRedisCache(search, redis, db, expire, table)
}
func (c *redisCache) ResetData() {
	if c.db == nil || env.IsProd() {
		glog.Warningln("Invalid ...")
		return
	}
	ids, ctx := []string{}, context.Background()
	if e := database.Select(ctx, c.db, &ids, "Select ID from "+c.table); e != nil {
		glog.Errorln(e)
	}
	glog.Infoln(ids)
	if len(ids) > 0 {
		if e := c.Del(ctx, ids...); e != nil {
			glog.Errorln(e)
		}
	}
}
func (c *redisCache) sync(ctx context.Context, rows []interface{}, n int) error {
	update := map[string][]byte{}
	mtx := sync.Mutex{}
	var err error
	cc := concurrency.New()

	for i := 0; i < n; i++ {
		if row, ok := rows[i].(*writeRow); ok {
			cc.Add(func() error {
				var err error
				/*if err = c.GetInCache(ctx, row.data, row.id); err != nil {
					glog.Errorln(err)
				}*/
				mtx.Lock()
				if update[row.id], err = json.Marshal(row.data); err != nil {
					mtx.Unlock()
					return err
				}
				mtx.Unlock()
				return nil
			})
		}
	}
	if err = cc.Do(); err != nil {
		glog.Errorln(err)
		return err
	}
	if len(update) <= 0 {
		return nil
	}

	query := sq.Insert(c.table).Columns(c.key, c.value)
	for k, v := range update {
		query = query.Values(k, string(v))
	}
	sql, args, _ := query.ToSql()
	sql = fmt.Sprintf("%s ON DUPLICATE KEY UPDATE %s=VALUES(%s);", sql, c.value, c.value)
	if _, err = database.Exec(ctx, c.db, sql, args...); err != nil {
		glog.Errorln(err)
		retryExecute(c.db, sql, args...)

	}
	return err

}

func (c *redisCache) GetInCache(ctx context.Context, value interface{}, key string) error {

	redisKey := c.makeKVKey(key)
	_, span := tracingHelper.NewClientSpanFromContext(ctx, redisTag, fmt.Sprintf("Redis Get %s", key))
	res, err := c.redis.Get(redisKey).Result()
	tracingHelper.FinishClientSpan(span, err)
	if err != nil { //Not incache
		return sql.ErrNoRows

	} else if err = protoDecode(res, value); err != nil {
		glog.Errorln(err)
		return err
	}

	return err
}

func (c *redisCache) msetExpire(keys ...string) {
	c.redis.Pipelined(func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.Expire(c.makeKVKey(k), c.expire)
		}
		return nil
	})
}

func (c *redisCache) Options(opts ...Option) Cache {
	option := NewOption(opts...)
	c.refresh = option.Refresh
	return c
}
func (c *redisCache) MGet(ctx context.Context, resp interface{}, key ...string) error {

	if len(key) == 0 {
		return nil
	}
	c.once.Do(func() {
		c.dataType = getDataType(resp)
	})
	values := reflect.MakeSlice(reflect.SliceOf(reflect.PtrTo(c.dataType)), len(key), len(key)) //make([]interface{}, len(key))

	redisKey := make([]string, len(key))
	for i, k := range key {
		redisKey[i] = c.makeKVKey(k)
		values.Index(i).Set(reflect.New(c.dataType))

	}
	keys := []string{}
	method := "Redis Get"
	if len(key) < 64 {
		method = fmt.Sprintf("Redis Get %v", key)
	}
	_, span := tracingHelper.NewClientSpanFromContext(ctx, redisTag, method)
	resRedis, err := c.redis.MGet(redisKey...).Result()
	tracingHelper.FinishClientSpan(span, err)
	type redisUpdate struct {
		item   interface{}
		update bool
	}
	updateRedis := map[string]*redisUpdate{}

	if err != nil {
		glog.Errorln(err)
	} else {
		//check key not in redis cache
		for i, _res := range resRedis {
			value := values.Index(i).Interface()
			if _res == nil {
				updateRedis[key[i]] = &redisUpdate{item: value, update: false}
				keys = append(keys, key[i])
			} else if str, ok := _res.(string); ok {
				if err := protoDecode(str, value); err != nil {
					glog.Errorln(err)
					updateRedis[key[i]] = &redisUpdate{item: value, update: false}
					keys = append(keys, key[i])
					resRedis[i] = nil
					//return err
				}

			}

		}

	}
	updateCnt := 0
	//Get data in db
	if len(keys) > 0 && c.db != nil {
		limit := 4 << 10
		for i := 0; i < len(keys); i += limit {
			to := i + limit
			if to > len(keys) {
				to = len(keys)
			}
			sql, args, err := sq.Select(c.key, fmt.Sprintf("coalesce(%s,'{}')", c.value)).From(c.table).Where(sq.Eq{c.key: keys[i:to]}).ToSql()
			if err != nil {
				glog.Errorln(err)
				return err
			}
			rows, err := database.Query(ctx, c.db, sql, args...)
			if err != nil {
				glog.Errorln(err)
				return err
			}
			defer rows.Close()

			for rows.Next() {
				var k, v string
				//Get k,v from db
				if err = rows.Scan(&k, &v); err != nil {
					return err
				}
				//Update to redis
				//updateRedis[k] = v
				//Search in result and update result
				if item, ok := updateRedis[k]; ok && !item.update {
					item.update = true
					updateCnt++
					if err := json.Unmarshal([]byte(v), item.item); err != nil {
						glog.Errorln(err)
						return err
					}
				} else {
					glog.Errorln("Invalid key", k)
				}

			}
		}
		if updateCnt > 0 && updateCnt < 64 {
			i := 0
			mset := make([]interface{}, 2*updateCnt)
			keys := make([]string, updateCnt)
			for k, v := range updateRedis {
				if v.update {
					keys[i/2] = k
					mset[i] = c.makeKVKey(k)
					mset[i+1], _ = protoEncode(v.item)
					i += 2
				}
			}
			c.redis.MSet(mset...)
			go c.msetExpire(keys...)
		}

	}
	copier.Copy(resp, values.Interface())
	return nil
}
func (c *redisCache) Get(ctx context.Context, value interface{}, key string) error {

	redisKey := c.makeKVKey(key)
	_, span := tracingHelper.NewClientSpanFromContext(ctx, redisTag, fmt.Sprintf("Redis Get %s", key))
	res, err := c.redis.Get(redisKey).Result()
	tracingHelper.FinishClientSpan(span, err)
	if err != nil { //Not incache
		if err != redis.Nil {
			glog.Errorln(err)
			return err
		}
		query, args, e := sq.Select(fmt.Sprintf("coalesce(%s,'{}')", c.value)).From(c.table).Where(sq.Eq{c.key: key}).ToSql()
		if e != nil {
			glog.Errorln(e)
			return e
		}
		if err = database.Get(ctx, c.db, &res, query, args...); err != nil {
			if err != sql.ErrNoRows {
				glog.Errorln(err)
			}
			return err
		}
		if err = json.Unmarshal([]byte(res), value); err != nil {
			glog.Infoln(res)
			return err
		}
		if res, err := protoEncode(value); err != nil {
			glog.Errorln(err)
		} else {
			c.redis.Set(redisKey, res, c.expire)
		}

	} else if err = protoDecode(res, value); err != nil {
		glog.Errorln(err)
		return err
	}

	return err
}

func retryExecute(db *sqlx.DB, sql string, args ...interface{}) {
	go func() {
		for {
			time.Sleep(5 * time.Minute)
			if _, err := db.Exec(sql, args...); err != nil {
				glog.Errorln(err)
			} else {
				break
			}
		}
	}()
}
func (c *redisCache) MSet(ctx context.Context, keys []string, values []interface{}) error {
	var (
		err error
	)
	if len(keys) != len(values) {
		err = fmt.Errorf("Invalid size %d:%d", len(keys), len(values))
		glog.Errorln(err)
		return err
	}
	if len(keys) == 0 {
		return nil
	}
	cc := concurrency.New()
	//redis update
	cc.Add(func() error {
		var err error
		redisSet := make([]interface{}, 2*len(keys))
		for i, key := range keys {
			redisSet[2*i] = c.makeKVKey(key)
			if redisSet[2*i+1], err = protoEncode(values[i]); err != nil {
				return err
			}
		}
		c.redis.MSet(redisSet...)
		go c.msetExpire(keys...)
		return nil
	})
	//Es update
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			c.updateMulti(ctx, keys, values)
			return nil
		})
	}
	cc.Add(func() error {
		for i := 0; i < len(keys); i++ {
			c.batchProcessor.Add(&writeRow{id: keys[i], data: values[i]})
		}
		return nil
	})

	return cc.Do()
}
func (c *redisCache) ResetCache() {
	glog.Infoln(c.table, "ResetCache")
	db, r := c.db, c.redis
	if db == nil || r == nil {
		glog.Errorln("nil db or redis")
		return
	}
	l := lock.NewRedisLock(r)
	key := c.makeKVKey("ResetCache")
	if !l.Lock(key) {
		glog.Infoln("ResetCache ... process")
		return
	}
	defer l.Unlock(key)
	scanKey := c.makeKVKey("*")
	var cursor uint64
	var n int
	for {
		var keys []string
		var err error
		keys, cursor, err = r.Scan(cursor, scanKey, 1024).Result()
		if err != nil {
			glog.Errorln(err)
			break
		}
		n += len(keys)
		if len(keys) > 0 {
			glog.Infoln(len(keys))
			r.Del(keys...)
		}
		if cursor == 0 {
			break
		}
	}
	glog.Infoln("Finish ResetCache ", n)

}
func (c *redisCache) Set(ctx context.Context, key string, value interface{}) error {

	cc := concurrency.New()

	//Update to cache
	cc.Add(func() error {
		if c.redis != nil {
			if data, err := protoEncode(value); err != nil {
				glog.Errorln(err)
			} else {
				if c.limiter.Allow() {
					c.redis.Set(c.makeKVKey(key), data, c.expire)
				} else {
					c.redis.SetXX(c.makeKVKey(key), data, c.expire)
				}
			}

		}
		return nil
	})
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			c.update(ctx, key, value)
			return nil
		})
	}
	cc.Add(func() error {
		c.batchProcessor.Add(&writeRow{id: key, data: value})
		return nil
	})
	return cc.Do()
}

func (c *redisCache) Del(ctx context.Context, keys ...string) error {
	if len(keys) == 0 {
		return nil
	}
	redisKey := make([]string, len(keys))
	for i, key := range keys {
		redisKey[i] = c.makeKVKey(key)

	}
	cc := concurrency.New()
	cc.Add(func() error {
		if n, err := c.redis.Del(redisKey...).Result(); err != nil {
			glog.Errorln(err, n)
		}
		return nil
	})
	if bulkProcessor != nil && c.search != nil {
		cc.Add(func() error {
			c.delete(ctx, keys...)

			return nil
		})
	}
	cc.Add(func() error {
		limitDel := 5000
		sql, args, err := sq.Delete(c.table).Where(sq.Eq{c.key: keys}).ToSql()
		if err != nil {
			glog.Errorln(err)
			return err
		}
		if len(keys) < limitDel {
			if _, err = database.Exec(ctx, c.db, sql, args...); err != nil {
				glog.Errorln(err)
				retryExecute(c.db, sql, args...)
				return err
			}
		} else {
			sql = fmt.Sprintf("%s limit %d;", sql, limitDel)
			for {
				res, err := database.Exec(ctx, c.db, sql, args...)
				if err != nil {
					glog.Errorln(err)
					return err
				}
				nrows, err := res.RowsAffected()
				if err != nil {
					glog.Errorln(err)
					return err
				}
				if nrows < int64(limitDel) {
					break
				}
			}

		}
		return nil
	})
	return cc.Do()
}
func (c *redisCache) migrate() error {
	if _, err := c.db.Exec(fmt.Sprintf(sqlCreateCacheTbl, c.table)); err != nil {
		glog.Errorln(err)
	}
	return nil
}
