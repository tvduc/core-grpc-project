package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"sync"
	"time"

	"github.com/golang/glog"
	protobuf "github.com/golang/protobuf/proto"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/graceful"
	es "gopkg.in/olivere/elastic.v6"
)

var (
	CacheName     string
	once          sync.Once
	bulkProcessor *es.BulkProcessor
)

type EsQueryFn func(*es.SearchService) *es.SearchService
type EsScrollQuery func(*es.ScrollService) *es.ScrollService

//Cache ...
type Cache interface {
	QueryAll(ctx context.Context, query EsScrollQuery, resp interface{}) (int64, error)
	QueryPaging(ctx context.Context, query EsQueryFn, resp interface{}) (int64, error)
	MGet(ctx context.Context, value interface{}, key ...string) error
	Get(ctx context.Context, value interface{}, key string) error
	Set(ctx context.Context, key string, value interface{}) error
	MSet(ctx context.Context, key []string, value []interface{}) error
	Del(ctx context.Context, key ...string) error
	Search() *es.SearchService
	Scroll() *es.ScrollService
	Count() *es.CountService
	Bulk() *es.BulkService
	Index() *es.IndexService
	Delete() *es.DeleteService
	ListIds(ctx context.Context, query EsScrollQuery) ([]string, error)
	Flush(context.Context)
	GetInCache(ctx context.Context, value interface{}, key string) error
	QueryPaging1(ctx context.Context, query EsQueryFn, resp interface{}) (*es.SearchResult, error)
	ResetData()
	QueryFirst(ctx context.Context, query es.Query, resp interface{}) error
	GetAll() map[string]interface{}
	ResetCache()
	Options(opts ...Option) Cache
}
type writeRow struct {
	id   string
	data interface{}
}

func esAfterFn(executionId int64, requests []es.BulkableRequest, response *es.BulkResponse, err error) {
	if err != nil {
		glog.Errorln(err)
	}
}
func closeBulk() {
	graceful.Stop(time.Second, func() {
		bulk := bulkProcessor
		bulkProcessor = nil
		bulk.Close()
		glog.Infoln("closeBulk")
	})

}
func getDataType(data interface{}) reflect.Type {
	value := reflect.Indirect(reflect.ValueOf(data))

	if value.Kind() == reflect.Slice {
		value := value.Interface()
		newSlice := reflect.New(reflect.TypeOf(value))
		if err := json.Unmarshal([]byte("[{}]"), newSlice.Interface()); err != nil {
			glog.Errorln(err)
			return nil
		}
		valueSlice := reflect.Indirect(newSlice)
		protoItem := valueSlice.Index(0)
		item := reflect.Indirect(protoItem).Interface()
		return reflect.TypeOf(item)

	}
	return nil

}
func protoEncode(msg interface{}) ([]byte, error) {
	if pMsg, ok := msg.(protobuf.Message); ok {
		return protobuf.Marshal(pMsg)
	}
	return nil, fmt.Errorf("%v not proto message", reflect.TypeOf(msg))
}
func protoDecode(data string, msg interface{}) error {
	if pMsg, ok := msg.(protobuf.Message); ok {
		return protobuf.Unmarshal([]byte(data), pMsg)
	}
	return fmt.Errorf("%v not proto message", reflect.TypeOf(msg))
}
