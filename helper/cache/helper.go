package cache

import (
	"context"
	"encoding/json"
	"reflect"
	"strings"
	"sync"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/golang/glog"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/proto"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/core"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/database"
	helperEs "gitlab.com/tvduc/product-management/core-grpc-project/helper/elastic"

	"github.com/go-redis/redis"
	protobuf "github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	pbTime "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jmoiron/sqlx"
	helperRedis "gitlab.com/tvduc/product-management/core-grpc-project/helper/redis"
	"gopkg.in/olivere/elastic.v6"
)

var (
	cache = core.NewPersitentCache()
)

//Open ...
func Open(redisCfg *config.Redis, dbCfg *config.Database, esCfg string, data protobuf.Message, indexSetting interface{}, tables ...string) Cache {
	c, e := cache.Get("Redis Cache"+strings.Join(tables, "-"), func() (interface{}, error) {
		var (
			db       *sqlx.DB
			esCli    *elastic.Client
			redisCli *redis.Client
		)
		if dbCfg != nil {
			db = database.CreateSQLXDB(*dbCfg)
		}
		if len(esCfg) > 0 {
			glog.Infoln("esCfg", esCfg)
			esCli = helperEs.New(esCfg, false)
		}
		if redisCfg != nil {
			redisCli = helperRedis.NewRedisClient(*redisCfg)
		}
		c := createRedisCache(
			esCli,
			redisCli,
			db,
			CacheExpire,
			tables...)
		c.toEs = getESMapV1
		if data != nil {
			c.once.Do(func() {
				c.dataType = reflect.Indirect(reflect.ValueOf(data)).Type()
				glog.Infoln(c.dataType)
			})
		}
		if esCli != nil {
			if ok, err := esCli.IndexExists(c.esIndex).Do(context.Background()); err != nil {
				glog.Errorln(err)
			} else if !ok && db != nil {
				mappings := helperEs.GetEsMapping(data)
				settings := map[string]interface{}{
					"analysis": helperEs.SearchAnalyzer,
				}
				if indexSetting != nil {
					settings["index"] = indexSetting
				}
				body := map[string]interface{}{
					"settings": settings,
					"mappings": mappings,
				}

				if _, err = esCli.CreateIndex(c.esIndex).BodyJson(body).Do(context.Background()); err != nil {
					glog.Errorln(err)
				} else {
					go migrate(data, c)
				}

			} else {
				if _, err = esCli.PutMapping().Index(c.esIndex).Type(helperEs.EsType).BodyJson(helperEs.GetEsMapping(data)).Do(context.Background()); err != nil {
					glog.Errorln(c.esIndex, err)
				}
			}
			if _, err := esCli.IndexPutSettings(c.esIndex).BodyString(`{"index":{"max_result_window":"100000"}}`).Do(context.Background()); err != nil {
				glog.Errorln(c.esIndex, err)
			}
		}
		return c, nil
	})
	if e != nil {
		glog.Errorln(e)
		return nil
	}
	return c.(Cache)
}
func migrate(data protobuf.Message, c *redisCache) error {
	tm := time.Now()
	glog.Infoln(c.table, "Start Migrate")

	db := c.db
	total, limit := int(0), uint64(32<<10)
	ctx := context.Background()
	lastID := ""
	wg := sync.WaitGroup{}
	for {
		query := sq.Select("ID", "coalesce(Data,'{}')").OrderBy("ID DESC").Limit(limit).From(c.table)
		if lastID != "" {
			query = query.Where(sq.Lt{"ID": lastID})
		}
		sql, args, err := query.ToSql()
		if err != nil {
			glog.Errorln(err)
			return err
		}
		t := time.Now()
		rows, err := database.Query(ctx, db, sql, args...)
		if err != nil {
			glog.Errorln(err)
			return err
		}
		defer rows.Close()
		n := 0
		bulk := c.Bulk()
		for rows.Next() {
			var k, v string
			//Get k,v from db
			if err = rows.Scan(&k, &v); err != nil {
				glog.Errorln(err)
				return err
			}
			if lastID == "" || lastID > k {
				lastID = k
			}
			data.Reset()
			if err := json.Unmarshal([]byte(v), data); err != nil {
				glog.Errorln(err)
				continue
			}
			bulkIndexRequest := c.BulkIndexRequest()
			bulk.Add(bulkIndexRequest.Id(k).Doc(c.toEs(data)))
			n++
		}
		if n == 0 {
			break
		}
		wg.Add(1)
		go func() {
			defer wg.Done()
			if _, err := bulk.Do(context.Background()); err != nil {
				glog.Errorln(err)
			}
		}()
		total += n
		glog.Infoln(c.table, lastID, total, time.Since(t))
		if n < int(limit) {
			break
		}
	}
	wg.Wait()
	glog.Infoln(c.table, "Finish Migarate in ", time.Since(tm))
	return nil
}

// func migrateEtcd(data interface{}, c *etcdCache) error {

// 	glog.Infoln(c.table, "Start Migrate")
// 	all := c.GetAll()
// 	flushBlk := 1024
// 	var bulk *es.BulkService
// 	for k, v := range all {
// 		if bulk == nil {
// 			bulk = c.Bulk()
// 		}
// 		bulkIndexRequest := c.BulkIndexRequest()
// 		bulk.Add(bulkIndexRequest.Id(k).Doc(c.toEs(v)))
// 		if bulk.NumberOfActions() >= flushBlk {
// 			if _, err := bulk.Do(context.Background()); err != nil {
// 				glog.Errorln(err)
// 			}
// 			bulk = nil
// 		}
// 	}
// 	if bulk != nil {
// 		if _, err := bulk.Do(context.Background()); err != nil {
// 			glog.Errorln(err)
// 		}
// 	}
// 	glog.Infoln(c.table, "End Migrate")
// 	return nil
// }

// func migrateTikv(data interface{}, c *tikvCache) error {
// 	glog.Infoln(c.table, "Start Migrate")
// 	prefixKey := []byte(c.makeKVKey(""))
// 	endKey := []byte(c.makeKVKey(""))
// 	endKey[len(endKey)-1] = endKey[len(endKey)-1] + 1
// 	pb, ok := data.(protobuf.Message)
// 	if !ok {
// 		glog.Errorln("data is ot proto Message")
// 		return nil
// 	}
// 	prefix := c.makeKVKey("")
// 	ctx := context.Background()
// 	for stop := false; !stop; {
// 		searchkey := append(prefixKey[:0:0], prefixKey...)
// 		searchkey = append(searchkey, byte(0))
// 		keys, values, err := c.tikv.Scan(ctx, searchkey, endKey, 1024)
// 		if err != nil {
// 			glog.Errorln(err)
// 			break
// 		}
// 		if len(keys) == 0 {
// 			break
// 		}
// 		bulk := c.Bulk()
// 		for i, key := range keys {
// 			glog.Infoln(string(key))
// 			if !strings.HasPrefix(string(key), prefix) {
// 				stop = true
// 				break

// 			} else {
// 				pb.Reset()
// 				if err = protobuf.Unmarshal(values[i], pb); err != nil {
// 					glog.Errorln(err)
// 				} else {
// 					bulkIndexRequest := c.BulkIndexRequest()
// 					bulk.Add(bulkIndexRequest.Id(string(key[len(prefix):])).Doc(c.toEs(pb)))
// 				}
// 				if bytes.Compare(prefixKey, key) < 0 {
// 					prefixKey = key
// 				}
// 			}
// 		}
// 		if _, err := bulk.Do(ctx); err != nil {
// 			glog.Errorln(err)
// 		}
// 		if len(keys) < 1024 {
// 			break
// 		}
// 	}

// 	glog.Infoln(c.table, "End Migrate")
// 	return nil
// }
func makeKeyMap(m *map[string]interface{}, key string) *map[string]interface{} {
	if t, ok := (*m)[key]; ok {
		if t, ok := t.(*map[string]interface{}); ok {
			return t
		}
	}
	t := &map[string]interface{}{}
	(*m)[key] = t
	return t
}

//GetESMap ...
func getEsMap(val reflect.Value, esMap *map[string]interface{}) {
	val = reflect.Indirect(val)
	vt := val.Type()
	for i := 0; i < val.NumField(); i++ {
		v := val.Field(i)
		f := vt.Field(i)
		tag, ok := f.Tag.Lookup("es")
		if !ok {
			continue
		}
		zero := reflect.Zero(v.Type()).Interface()
		if reflect.DeepEqual(v.Interface(), zero) {
			continue
		}
		data := v.Interface()
		switch v.Kind() {
		case reflect.Struct, reflect.Ptr:
			if ts, ok := data.(*pbTime.Timestamp); ok {
				if ts != nil {
					tm, err := ptypes.Timestamp(ts)
					if err != nil {
						glog.Errorln(err)
					} else {
						(*esMap)[tag] = tm.UnixNano() / int64(time.Millisecond)

					}
				}
			} else if date, ok := data.(*proto.Date); ok {
				if date != nil {
					tm := proto.DateToTimeSearch(date)
					(*esMap)[tag] = tm.UnixNano() / int64(time.Millisecond)

				}
			} else {
				if !v.IsZero() {
					getEsMap(v, makeKeyMap(esMap, tag))
				} else {
					glog.Errorln(v)
				}
			}
		default:

			(*esMap)[tag] = data
		}

	}
}

//GetESMap ...
func getESMapV1(value interface{}) map[string]interface{} {
	if value == nil {
		return map[string]interface{}{}
	}
	validateTimeStamp(value)
	esMap := map[string]interface{}{}
	if f, ok := value.(interface {
		GetEsMap(esMap *map[string]interface{})
	}); ok {
		f.GetEsMap(&esMap)
	} else {
		getEsMapCache(reflect.ValueOf(value), &esMap)
	}

	return esMap
}

// //OpenTiKV ...
// func OpenTiKV(redisCfg *config.Redis, dbCfg *config.Database, tiKV, esCfg string, data protobuf.Message, indexSetting interface{}, tables ...string) Cache {
// 	tikvCli, e := cache.Get("TiKV Client "+tiKV, func() (interface{}, error) {
// 		cfg := tikvConfig.Default()
// 		return rawkv.NewClient(context.Background(), strings.Split(tiKV, ","), cfg)
// 	})
// 	if e != nil {
// 		glog.Errorln(e)
// 		return nil
// 	}
// 	c, e := cache.Get("TiKV Cache"+strings.Join(tables, "-"), func() (interface{}, error) {
// 		var (
// 			db       *sqlx.DB
// 			esCli    *elastic.Client
// 			redisCli *redis.Client
// 		)
// 		if dbCfg != nil {
// 			db = database.CreateSQLXDB(*dbCfg)
// 		}
// 		if len(esCfg) > 0 {
// 			esCli = helperEs.New(esCfg, false)
// 		}
// 		if redisCfg != nil {
// 			redisCli = helperRedis.NewRedisClient(*redisCfg)
// 		}
// 		c := createTiKVCache(
// 			esCli,
// 			tikvCli.(*rawkv.Client),
// 			tables...)
// 		c.toEs = getESMapV1
// 		if data != nil {
// 			c.once.Do(func() {
// 				c.dataType = reflect.Indirect(reflect.ValueOf(data)).Type()
// 				glog.Infoln(c.dataType)
// 			})
// 		}
// 		if esCli != nil {
// 			if ok, err := esCli.IndexExists(c.esIndex).Do(context.Background()); err != nil {
// 				glog.Errorln(err)
// 			} else if !ok {
// 				mappings := helperEs.GetEsMapping(data)
// 				settings := map[string]interface{}{
// 					"analysis": helperEs.SearchAnalyzer,
// 				}
// 				if indexSetting != nil {
// 					settings["index"] = indexSetting
// 				}
// 				body := map[string]interface{}{
// 					"settings": settings,
// 					"mappings": mappings,
// 				}

// 				if _, err = esCli.CreateIndex(c.esIndex).BodyJson(body).Do(context.Background()); err != nil {
// 					glog.Errorln(err)
// 				} else {
// 					go migrateTikv(data, c)
// 				}

// 			} else {
// 				if _, err = esCli.PutMapping().Index(c.esIndex).Type(helperEs.EsType).BodyJson(helperEs.GetEsMapping(data)).Do(context.Background()); err != nil {
// 					helperEs.DumpElasticError(err)
// 					glog.Errorln(c.esIndex, err)
// 				}
// 			}
// 			if _, err := esCli.IndexPutSettings(c.esIndex).BodyString(`{"index":{"max_result_window":"100000"}}`).Do(context.Background()); err != nil {
// 				glog.Errorln(c.esIndex, err)
// 			}
// 		}
// 		if redisCli != nil {
// 			if lock.NewRedisLock(redisCli).Lock("TiKV Cache" + strings.Join(tables, "-")) {
// 				go c.importDB(context.Background(), db, data)
// 			}
// 		}
// 		return c, nil
// 	})
// 	if e != nil {
// 		glog.Errorln(e)
// 		return nil
// 	}
// 	return c.(Cache)
// }

// //OpenEtcd ...
// func OpenEtcd(etcdAdrrs string, esCfg string, data interface{}, indexSetting interface{}, f changeCallback, tables ...string) Cache {
// 	cli, e := cache.Get("ETCD Client "+etcdAdrrs, func() (interface{}, error) {
// 		return etcd.NewFromURLs(strings.Split(etcdAdrrs, ","))
// 	})
// 	if e != nil {
// 		glog.Errorln(e)
// 		return nil
// 	}
// 	c, e := cache.Get("ETCD Cache"+strings.Join(tables, "-"), func() (interface{}, error) {
// 		var (
// 			esCli *elastic.Client
// 		)

// 		if len(esCfg) > 0 {
// 			esCli = helperEs.New(esCfg, false)
// 		}

// 		c := newEtcdCache(
// 			esCli,
// 			cli.(*etcd.Client),
// 			tables...)
// 		c.toEs = getESMapV1
// 		if data != nil {
// 			c.once.Do(func() {
// 				c.dataType = reflect.Indirect(reflect.ValueOf(data)).Type()
// 				glog.Infoln(c.dataType)
// 			})
// 			c.init(f)
// 		}
// 		if esCli != nil {
// 			if ok, err := esCli.IndexExists(c.esIndex).Do(context.Background()); err != nil {
// 				glog.Errorln(err)
// 			} else if !ok {
// 				mappings := helperEs.GetEsMapping(data)
// 				settings := map[string]interface{}{
// 					"analysis": helperEs.SearchAnalyzer,
// 				}
// 				if indexSetting != nil {
// 					settings["index"] = indexSetting
// 				}
// 				body := map[string]interface{}{
// 					"settings": settings,
// 					"mappings": mappings,
// 				}

// 				if _, err = esCli.CreateIndex(c.esIndex).BodyJson(body).Do(context.Background()); err != nil {
// 					glog.Infoln("mappings", mappings)
// 					glog.Errorln(err)
// 				} else {
// 					go migrateEtcd(data, c)
// 				}

// 			} else {
// 				if _, err = esCli.PutMapping().Index(c.esIndex).Type(helperEs.EsType).BodyJson(helperEs.GetEsMapping(data)).Do(context.Background()); err != nil {
// 					helperEs.DumpElasticError(err)
// 					glog.Errorln(c.esIndex, err)
// 				}
// 			}
// 			if _, err := esCli.IndexPutSettings(c.esIndex).BodyString(`{"index":{"max_result_window":"100000"}}`).Do(context.Background()); err != nil {
// 				glog.Errorln(c.esIndex, err)
// 			}
// 		}
// 		return c, nil
// 	})
// 	if e != nil {
// 		glog.Errorln(e)
// 		return nil
// 	}
// 	return c.(Cache)
// }

type tagInfo struct {
	iField int
	tag    string
	zero   interface{}
}

var (
	cacheMap  = map[reflect.Type][]tagInfo{}
	cacheLock = sync.RWMutex{}
)

func getEsMapCache(val reflect.Value, esMap *map[string]interface{}) {
	val = reflect.Indirect(val)
	vt := val.Type()
	cacheLock.RLock()
	tagInfos, ok := cacheMap[vt]
	cacheLock.RUnlock()
	if !ok {
		cacheLock.Lock()
		tagInfos = []tagInfo{}
		for i := 0; i < val.NumField(); i++ {
			f := vt.Field(i)
			tag, ok := f.Tag.Lookup("es")
			if ok {
				v := val.Field(i)
				tagInfos = append(tagInfos, tagInfo{i, tag, reflect.Zero(v.Type()).Interface()})
			}
		}
		cacheMap[vt] = tagInfos
		cacheLock.Unlock()

	}
	for _, tagInfo := range tagInfos {
		v := val.Field(tagInfo.iField)
		tag := tagInfo.tag

		zero := tagInfo.zero //reflect.Zero(v.Type()).Interface()
		if reflect.DeepEqual(v.Interface(), zero) {
			continue
		}
		data := v.Interface()
		switch v.Kind() {
		case reflect.Struct, reflect.Ptr:
			if ts, ok := data.(*pbTime.Timestamp); ok {
				if ts != nil {
					tm, err := ptypes.Timestamp(ts)
					if err != nil {
						glog.Errorln(err)
					} else {
						(*esMap)[tag] = tm.UnixNano() / int64(time.Millisecond)

					}
				}
			} else if date, ok := data.(*proto.Date); ok {
				if date != nil {
					tm := proto.DateToTimeSearch(date)
					(*esMap)[tag] = tm.UnixNano() / int64(time.Millisecond)

				}
			} else {
				if !v.IsZero() {
					getEsMapCache(v, makeKeyMap(esMap, tag))
				} else {
					glog.Errorln(v)
				}
			}
		default:

			(*esMap)[tag] = data
		}
	}

}
