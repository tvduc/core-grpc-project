package cache

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"sync"
	"time"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/notify"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/proto"

	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes"
	pbTime "github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/batch"
	esHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/elastic"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/structs"
	es "gopkg.in/olivere/elastic.v6"
)

type MGetFn func(ctx context.Context, value interface{}, key ...string) error
type GetFn func(ctx context.Context, value interface{}, key string) error
type esSearch struct {
	search         *es.Client
	table, esIndex string
	dataType       reflect.Type
	once           sync.Once
	mGet           MGetFn
	get            GetFn
	toEs           func(interface{}) map[string]interface{}
	batchProcessor *batch.Processor
	refresh        bool
}

func (c *esSearch) GetAll() map[string]interface{} {
	glog.Errorln("GetAll not yet implemented")
	return nil

}
func (c *esSearch) ResetCache() {
	glog.Errorln("ResetCache not yet implemented")
}
func (c *esSearch) start(search *es.Client, mGet MGetFn, tables ...string) {
	table := tables[0]
	if search == nil {
		c.table = table
		c.mGet = mGet
		return
	}
	once.Do(func() {
		if bulk, err := search.BulkProcessor().Name(table).Workers(1).BulkActions(4096).BulkSize(2 << 20).FlushInterval(3 * time.Second).
			After(esAfterFn).Do(context.Background()); err != nil {
			glog.Errorln(err)
		} else {
			bulkProcessor = bulk
			closeBulk()
		}
	})
	c.search = search
	c.table = table
	if len(tables) < 2 {
		c.esIndex = os.Getenv("SERVICE_PREFIX_NAME") + table
	} else {
		c.esIndex = os.Getenv("SERVICE_PREFIX_NAME") + tables[1]
	}
	//c.esIndex = "sandbox." + table
	c.mGet = mGet
}
func (c *esSearch) Search() *es.SearchService {
	return c.search.Search(c.esIndex).Type(esHelper.EsType).FetchSource(false)
}
func (c *esSearch) Scroll() *es.ScrollService {
	return c.search.Scroll(c.esIndex).Type(esHelper.EsType).FetchSource(false)
}
func (c *esSearch) Count() *es.CountService {
	return c.search.Count(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) Index() *es.IndexService {
	return c.search.Index().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) Delete() *es.DeleteService {
	return c.search.Delete().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) DeleteQuery() *es.DeleteByQueryService {
	return c.search.DeleteByQuery(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) Bulk() *es.BulkService {
	return c.search.Bulk().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) BulkIndexRequest() *es.BulkIndexRequest {
	return es.NewBulkIndexRequest().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) BulkDeleteRequest() *es.BulkDeleteRequest {
	return es.NewBulkDeleteRequest().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) BulkUpdateRequest() *es.BulkUpdateRequest {
	return es.NewBulkUpdateRequest().Index(c.esIndex).Type(esHelper.EsType)
}
func (c *esSearch) Options(opts ...Option) Cache {
	glog.Errorln("Options not yet implement")
	return nil
}
func (c *esSearch) Flush(ctx context.Context) {
	if !c.refresh {
		if err := bulkProcessor.Flush(); err != nil {
			glog.Errorln(err)

		}
		if _, err := c.search.Refresh(c.esIndex).Do(ctx); err != nil {
			glog.Errorln(err)
		}
	}

}
func (c *esSearch) ListIds(ctx context.Context, query EsScrollQuery) ([]string, error) {
	if c.search == nil {
		return nil, fmt.Errorf("Elastic connection fail")
	}
	return esHelper.DoAll(ctx, func() *es.ScrollService {
		return query(c.Scroll())
	})
}
func (c *esSearch) QueryPaging(ctx context.Context, query EsQueryFn, resp interface{}) (int64, error) {
	if c.search == nil {
		return 0, fmt.Errorf("Elastic connection fail")
	}
	keys := []string{}
	res, err := esHelper.DoSearch(ctx, query(c.Search().NoStoredFields()))

	if err != nil {
		return 0, err
	}
	for _, hit := range res.Hits.Hits {
		keys = append(keys, hit.Id)
	}
	return res.TotalHits(), c.mGet(ctx, resp, keys...)
}
func (c *esSearch) QueryFirst(ctx context.Context, query es.Query, resp interface{}) error {
	if c.search == nil {
		return fmt.Errorf("Elastic connection fail")
	}
	res, err := esHelper.DoSearch(ctx, c.Search().Query(query).Size(1).NoStoredFields())

	if err != nil {
		return err
	}
	if len(res.Hits.Hits) > 0 {
		return c.get(ctx, resp, res.Hits.Hits[0].Id)
	}
	return sql.ErrNoRows
}
func (c *esSearch) QueryPaging1(ctx context.Context, query EsQueryFn, resp interface{}) (*es.SearchResult, error) {
	if c.search == nil {
		return nil, fmt.Errorf("Elastic connection fail")
	}
	keys := []string{}
	res, err := esHelper.DoSearch(ctx, query(c.Search().NoStoredFields()))

	if err != nil {
		esHelper.DumpElasticError(err)
		glog.Errorln(err)
		return nil, err
	}
	for _, hit := range res.Hits.Hits {
		keys = append(keys, hit.Id)
	}
	return res, c.mGet(ctx, resp, keys...)
}
func (c *esSearch) QueryAll(ctx context.Context, query EsScrollQuery, resp interface{}) (int64, error) {
	if c.search == nil {
		return 0, fmt.Errorf("Elastic connection fail")
	}
	res, err := esHelper.DoAll(ctx, func() *es.ScrollService {
		return query(c.Scroll())
	})

	if err != nil {
		return 0, err
	}
	if len(res) > 256*1024 {
		return 0, fmt.Errorf("too many record %v", len(res))

	}
	return int64(len(res)), c.mGet(ctx, resp, res...)
}

//MakeRedisCache ...
var (
	cachePrefix = os.Getenv("SERVICE_PREFIX_NAME")
)

//MakeRedisCache ...
func MakeRedisCache(table, key string) string {
	return fmt.Sprintf("%s:%s:%s", cachePrefix, table, key)
}
func (c *esSearch) makeKVKey(key string) string {
	return MakeRedisCache(c.table, key)

}
func validateTimeStamp(date interface{}) error {
	v := reflect.ValueOf(date)
	if v.Kind() != reflect.Ptr || v.IsNil() {
		return nil
	}
	elem := v.Elem()
	hasTimestamp := false
	for i := 0; i < elem.NumField(); i++ {
		if tm, ok := elem.Field(i).Interface().(*pbTime.Timestamp); ok {
			hasTimestamp = true
			if tm.GetSeconds() != 0 || tm.GetNanos() != 0 {
				return nil
			}
		}

	}
	if hasTimestamp {
		k, _ := json.Marshal(date)
		err := fmt.Sprintln(string(k), reflect.TypeOf(date), "is  invalid")
		notify.SendToTeams("validateTimeStamp", err)
		return fmt.Errorf(err)
	}
	return nil
}

//GetESMap ...
func GetESMap(value interface{}) map[string]interface{} {
	if value == nil {
		return map[string]interface{}{}
	}
	validateTimeStamp(value)
	esMap := structs.Map(value)
	for k, v := range esMap {
		ts, ok := v.(*pbTime.Timestamp)
		if ts != nil && ok == true {
			tm, err := ptypes.Timestamp(ts)
			if err != nil {
				glog.Errorln(err)
			} else {
				esMap[k] = tm.UnixNano() / int64(time.Millisecond)
			}
		} else {
			date, ok := v.(*proto.Date)
			if date != nil && ok {
				tm := proto.DateToTimeSearch(date)
				esMap[k] = tm.UnixNano() / int64(time.Millisecond)
			}
		}
	}
	return esMap
}
func (c *esSearch) updateMulti(ctx context.Context, keys []string, values []interface{}) {
	if c.refresh {
		bulkRequest := c.Bulk()
		bulkRequest = bulkRequest.Refresh("true")
		for i, value := range values {
			if value != nil {
				esMap := c.toEs(value)
				bulkRequest.Add(c.BulkIndexRequest().Id(keys[i]).Doc(esMap))
			} else {
				glog.Errorln(keys[i])
			}
		}

		esHelper.DoBulk(ctx, bulkRequest)

	} else {
		for i, value := range values {
			if value != nil {
				esMap := c.toEs(value)
				esHelper.DoBulkProcessorAdd(ctx, bulkProcessor, c.BulkIndexRequest().Id(keys[i]).Doc(esMap))
			} else {
				glog.Errorln(keys[i])
			}

		}
	}
}
func (c *esSearch) update(ctx context.Context, key string, value interface{}) {
	if c.refresh {
		esHelper.DoIndex(ctx, c.Index().Id(key).BodyJson(c.toEs(value)).Refresh("true"))

	} else {
		esHelper.DoBulkProcessorAdd(ctx, bulkProcessor, c.BulkIndexRequest().Id(key).Doc(c.toEs(value)))
	}
}
func (c *esSearch) delete(ctx context.Context, keys ...string) {
	if c.refresh {
		bulkRequest := c.Bulk().Refresh("true")
		for _, key := range keys {
			delRequest := c.BulkDeleteRequest().Id(key)
			bulkRequest.Add(delRequest)
		}
		esHelper.DoBulk(ctx, bulkRequest)

	} else {
		for _, key := range keys {
			esHelper.DoBulkProcessorAdd(ctx, bulkProcessor, c.BulkDeleteRequest().Id(key))
		}
	}
}
