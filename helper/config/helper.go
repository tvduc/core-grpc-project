package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"

	"github.com/BurntSushi/toml"
	"github.com/golang/glog"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/consul"
)

//Redis ....
type Redis struct {
	Addr, Password string
	DB             int
}

//Database ...
type Database struct {
	Addr, User, Password, Databasename, Driver string
}

//Kafka ...
type Kafka struct {
	Addrs           []string
	Topics          []string
	Group           string
	Oldest          bool
	MaxMessageBytes int
	Compress        bool
	Newest          bool
}

//Elastic ...
type Elastic struct {
	Addr  string
	Sniff bool
}

//Sms ...
type Sms struct {
	EndPoint   string
	SecretKey  string
	ClientID   string
	BrandName  string
	PrivateKey string
}

//TiKV ...
type TiKV struct {
	Addr string
}

//Service ...
type Service interface {
	GetConsul() string
	GetOpenTracing() string
	GetPort() int
	Getenv(serviceName string)
}

//ServiceBase ...
type ServiceBase struct {
	Etcd, Consul, OpenTracing string
	Port                      int
	Database                  Database
	Redis                     Redis
	Kafka                     Kafka
	GrpcPort                  int
	Elastic                   Elastic
	TiKV                      TiKV
}

var (
	GlobalConfig *ServiceBase
)

//GetRedis ...
func (s *ServiceBase) GetRedis() Redis {
	if s == nil {
		return Redis{}
	}
	return s.Redis
}

//GetConsul ...
func (s *ServiceBase) GetConsul() string {
	if s == nil {
		return ""
	}
	return s.Consul
}

//GetOpenTracing ...
func (s *ServiceBase) GetOpenTracing() string {
	if s == nil {
		return ""
	}
	return s.OpenTracing
}

//GetPort ...
func (s *ServiceBase) GetPort() int {
	if s == nil {
		return 0
	}
	return s.Port
}

func getEnv(val *string, key string) {
	env := os.Getenv(key)
	if len(env) > 0 && len(*val) == 0 {
		*val = env
	}
}

//Getenv ...
func (s *ServiceBase) Getenv(serviceName string) {
	if s == nil {
		return
	}
	glog.Infoln(s)
	glog.Infoln("Getenv serviceName: ", serviceName)
	getEnv(&s.Consul, "CONSUL_ADDRESS")
	if len(s.Consul) == 0 {
		s.Consul = "127.0.0.1:8500"
	}
	getEnv(&s.OpenTracing, "OPEN_TRACING_ADDRESS")
	getEnv(&s.Database.Databasename, "DB_NAME")
	getEnv(&s.Database.Addr, "DB_ADDR")
	getEnv(&s.Database.Password, "DB_PASS")
	getEnv(&s.Database.User, "DB_USR")
	getEnv(&s.Redis.Addr, "REDIS_ADDR")
	getEnv(&s.Redis.Password, "REDIS_PASS")

	s.Kafka.Group = serviceName
	GlobalConfig = s

}

//Init ...
func Init(serviceName string, s Service) error {

	defer s.Getenv(serviceName)

	if s == nil || len(serviceName) == 0 {
		return errors.New("Empty serviceName")
	}
	if data, err := ioutil.ReadFile("/app/config.json"); err == nil {
		if err = json.Unmarshal(data, s); err != nil {
			glog.Errorln(err)
		} else {
			glog.Infoln(s)
		}
	} else {
		glog.Errorln(err)
	}

	consulAddr := consul.GetAddress(s.GetConsul())
	if consul.GetConfig(consulAddr, serviceName, s) != nil {
		if _, err := toml.DecodeFile("config.toml", s); err != nil {
			return err
		}
	}
	glog.Infoln(s)

	return nil
}

/*
//ConfigServer ...
type ConfigServer struct {
	Redis                    configRedis
	Database                 ConfigMySQL
	Consul                   configConsul
	GrpcServer               configServer
	Kafka                    configKafka
	OpenTracing              openTracing
	KafkaCmd                 configKafka
	UserService              userService
	KafkaWriter              configKafka
	LogFailKafkaWriter       configKafkaMultiTopic
	LogConfig                logConfig
	Zeebe                    configZeebe
	FileServer               configFileServer
	KafkaSPEvent             configKafkaMultiTopic
	Url365                   configUrl365
	KafkaMerchantEvent       configKafkaMultiTopic
	ZaloPayAPIUrl            configZaloPayAPI
	Sms                      ConfigSms
	ZaloPayAdminDatabase     ConfigMySQL
	ZaloPayReconcileDataBase ConfigMySQL
}*/
