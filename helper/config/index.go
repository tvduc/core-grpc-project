package config

const (
	SettingElastic = `{
		"index":{
			"max_result_window":"100000"
		}
	}`
	MerchantBranchTable    = "merchant_branch"
	MerchantBranchESIndex  = "merchant_branch_es"
	MerchantStoreTable     = "merchant_store"
	MerchantStoreESIndex   = "merchant_store_es"
	MerchantCashierTable   = "merchant_cashier"
	MerchantCashierESIndex = "merchant_cashier_es"
)
