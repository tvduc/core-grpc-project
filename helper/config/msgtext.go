package config

const (
	MailSubjectActivateMailForNewRegisterUser = "Thông báo xác thực địa chỉ email"
	MailSubjectWorkflowApprovalMCProfile      = "Thông báo xét duyệt hồ sơ doanh nghiệp"
	MailSubjectWorkflowApprovalMCProfile2     = "Thông báo xét duyệt hồ sơ doanh nghiệp - bộ phận %s"
	MailSubjectAlertNewAppFee                 = "Thông báo thêm phí mới từ OP"
)
