package config

import "os"

var (
	servicePrefixName                   = os.Getenv("SERVICE_PREFIX_NAME")
	UserRegistrationWorkflowDoneTopic   = servicePrefixName + "merchant-tools.user-registration-workflow.done-topic"
	MerchantUserMerchantInfoUpdateTopic = servicePrefixName + "merchant-user.merchant-update"
	MailServiceTopic                    = servicePrefixName + "merchant-tools.mail-service.topic"
	MerchantAssignUpdateTopic           = servicePrefixName + "merchant-assign-update"
	MerchantFullUpdateTopic             = servicePrefixName + "merchant-manage.merchant-update"
	MerchantShortUpdateTopic            = servicePrefixName + "merchant-manage.merchant-short-update"
	MerchantTotalMCUpdateTopic          = servicePrefixName + "merchant-manage.merchant-totalMC-update"
	MerchantPublishAssignKafka          = servicePrefixName + "admin-bo.assign-single-merchant"
	NewUserBySPTopic                    = servicePrefixName + "merchant-user.new-user"
	ServiceProviderCreatedTopic         = servicePrefixName + "service-provider.created"
	SpReportSummaryMonthly              = servicePrefixName + "sp-report.summary-monthly"
	CreateSaleTopic                     = servicePrefixName + "sp-user.create-sale"
	ChangeActivationTopic               = servicePrefixName + "sp-user-activation"
	AssignMultipleMerchantsTopic        = servicePrefixName + "merchant-assign-update"
	AssignSingleMerchantTopic           = servicePrefixName + "admin-bo.assign-single-merchant"
	MerchantAppUpdate                   = servicePrefixName + "merchant-manage.app-update"
	ReconcileLogTopic                   = servicePrefixName + "reconcileLog"
	UpdateUserSessionTopic              = servicePrefixName + "user-session.update"
	ExternalMerchantInfoUpdate          = servicePrefixName + "merchant.info.update"
	ExternalMerchantInfoStatus          = servicePrefixName + "merchant.info.status"
	ZaloPaySyncLog                      = servicePrefixName + "ZaloPaySyncLog"
	ZaloPayProcessLog                   = servicePrefixName + "ZaloPayProcessLog"
	MCCoreLog                           = servicePrefixName + "MCCoreLog"
	RefundLog                           = servicePrefixName + "RefundLog"
	RefundProcessLogOld                 = servicePrefixName + "RefundLogProcess"
	RefundProcessLog                    = servicePrefixName + "RefundLogProcessNew"
	SpUserUpdate                        = servicePrefixName + "sp-user-update"
	ZaloPayProcessLogFail               = ZaloPayProcessLog + "-Fail"
	RefundProcessLogFail                = RefundProcessLog + "-Fail"
	JobsServiceNotifyDoneTopic          = servicePrefixName + "jobs-service.notify-done"
	JobsServiceNotifyMsgTopic           = servicePrefixName + "jobs-service.notify-msg"
	ReconcileReportManualJobTopic       = servicePrefixName + "merchant-tools.admin-reconcile-report.topic"
	MonthlyBillingConfirmed             = servicePrefixName + "merchant-tools.monthly-billing-confirmed"
	ConfirmedSummaryReconcile           = servicePrefixName + "merchant-tools.bo-summary-reconcile-confirmation"
	UpdateMerchantAdminAccountTopic     = servicePrefixName + "user.update-merchant-admin"
	ExcelServiceTopic                   = servicePrefixName + "merchant-tools.excel-service.topic"
	DigitalSignatureTopic               = servicePrefixName + "merchant-tools.digital-signature.topic"
	UserIdRolePublishTopic              = servicePrefixName + "role.department-user-role"
	BackOfficeReconcileTopic            = servicePrefixName + "backofficce.reconcile-elastic-search"
	OperationLogInsertTopic             = servicePrefixName + "operation-log.insert"
	MCReconcileTopic                    = servicePrefixName + "mc.reconcile-elastic-search"
	RefundServiceNotifyTopic            = servicePrefixName + "refund-service.notify"
	ReportServiceToESTranslogTopic      = servicePrefixName + "report-service.elastic-search.translog"
	OtpServiceTopic                     = servicePrefixName + "merchant-tools.otp-service.topic"
	NewMerchantWithSPAndSale            = servicePrefixName + "merchant-tools.new-merchant-with-sp-sale"
	RequestAppendEmailTopic             = servicePrefixName + "request.bo-user-email"
	UserEmailRoleTopic                  = servicePrefixName + "role.user-email"
	MepReportTranslog                   = servicePrefixName + "MepReportTransLog"
	ZpReportTranslog                    = servicePrefixName + "ZPReportTransLog"
	AcquiringReportTranslog             = servicePrefixName + "ZPReportTransLog"
	ZpRefundProcessLog                  = servicePrefixName + "ZPRefundLog"
	ZpRefundProcessLogFail              = ZpRefundProcessLog + "-Fail"
	ZaloPayProxyLog                     = servicePrefixName + "ZaloPayProxyLog"
	ZPDevToolProcessLog                 = servicePrefixName + "ZPDevTooLog"
	ZPESTransLog                        = servicePrefixName + "ZPESTransLog"
	ZPReportCashinLog                   = servicePrefixName + "ZPReportTransferCash"
	ZPReportCashinFinalLog              = servicePrefixName + "ZPReportTransferCashFinalStatus"
	ZPProcessCashinLog                  = servicePrefixName + "ZPTransferCashProcess"
	ZPProcessPromotionDetailLog         = servicePrefixName + "PromotionDDLog"
	ZPProcessVoucherInfoLog             = servicePrefixName + "PromotionServiceLog"
	PromotionMepLog                     = servicePrefixName + "PromotionMepLog"
	ZPProcessCashinLogFail              = ZPProcessCashinLog + "-Fail"
	ZPMepInfoUpdateTopic                = servicePrefixName + "mep.info.update"
	ZPCreateOrderLog                    = servicePrefixName + "ZPCreateOrderLog"
	ZPMobileNotify                      = servicePrefixName + "ZPMobileNotify"
	PromotionConfigInfo                 = servicePrefixName + "promotion-config.info"
	PromotionCBPromotionLog             = servicePrefixName + "promotion-cb-promotion-log"
	PromotionCBDeliveryLog              = servicePrefixName + "promotion-cb-delivery-log"
	PromotionCBCashBackLog              = servicePrefixName + "promotion-cb-cash-back-log"
	PromotionCBCashBackStatusLog        = servicePrefixName + "promotion-cb-cash-back-status-log"
	CashBackPromotionInnerLog           = servicePrefixName + "cash-back-promotion-inner-log"
	ZTELinkTokenLog                     = servicePrefixName + "ZTELinkTokenLog" // thông tin token từ team Gateway
	ZTEPaymentLog                       = servicePrefixName + "ZTEPaymentLog"   // thông tin giao dịch sử dụng token
	WorkflowPmrTopic                    = servicePrefixName + "workflow-pmr-topic"
	Gatewayv2FrontendLog                = "Gatewayv2FrontendLog" // kafka team gateway v2 đẩy, ko có prefix
	MissingDataInternalTopic            = servicePrefixName + "missing-data-internal-topic"
	RollbackDataTopic                   = servicePrefixName + "rollback-data-topic"
	UpdateReportTransLogData            = servicePrefixName + "UpdateReportTransLogData"
	UpdateTransLogMissingField          = servicePrefixName + "UpdateTransLogMissingField"
	FTPProxyUploadTopic                 = servicePrefixName + "merchant-tools.ftp-proxy.upload"
	FTPProxyDownloadTopic               = servicePrefixName + "merchant-tools.ftp-proxy.download"
	PDFGenerateTopic                    = servicePrefixName + "merchant-tools.pdf-service.generate"
	PaymentInfoTopic                    = servicePrefixName + "merchant-tools.app-service.payment"
	ProcessFlowHistoryTopic             = servicePrefixName + "process-flow.history"

	FeeCalculatorNotifyTopic      = servicePrefixName + "merchant-tools.service.fee-calculator-notify-topic"
	FeeCycleCalculatorNotifyTopic = servicePrefixName + "merchant-tools.service.fee-cycle-calculator-notify-topic"

	FeeCalculatorRealTimeTopic  = servicePrefixName + "merchant-tools.service.fee-calculator-realtime-topic"
	FeeCalculatorRealTimeTopic2 = servicePrefixName + "merchant-tools.service.fee-calculator-realtime-topic-2"
	FeeCalculatorRealTimeTopic3 = servicePrefixName + "merchant-tools.service.fee-calculator-realtime-topic-3"
	TransactionRecordTopic      = servicePrefixName + "merchant-tools.transaction-record"
	CloseSettlementPeriodTopic  = servicePrefixName + "merchant-tools.close-settlement-period"
	GeneratePayoutTopic         = servicePrefixName + "merchant-tools.generate-payout"

	BillingCycleUpdateTopic = servicePrefixName + "agreement-service.billing-cycle"
	MerchantFeeUpdateTopic  = servicePrefixName + "agreement-service.merchant-fee"

	UserAgreementUpdateTopic           = servicePrefixName + "merchant-tools.user-agreement"
	AgreementCenterInternalUpdateTopic = servicePrefixName + "agreement-center.update"
	AgreementCenterExternalUpdateTopic = servicePrefixName + "agreement-center.external.update"

	MerchantConfigurationUpdateTopic = servicePrefixName + "merchant-tools.merchant-configuration"

	NotifyFeeChange             = []string{MerchantFeeUpdateTopic}
	NotifySpChange              = []string{OperationLogInsertTopic, NewMerchantWithSPAndSale, SpUserUpdate}
	NotifyMerchantChange        = []string{MerchantFullUpdateTopic}
	NotifyMerchantAppChange     = []string{MerchantAppUpdate}
	NotifyAgreementChange       = []string{BillingCycleUpdateTopic, MerchantFeeUpdateTopic}
	NotifyUserAgreementChange   = []string{UserAgreementUpdateTopic}
	MerchantConfigurationChange = []string{MerchantConfigurationUpdateTopic}

	NotifyItemMgmtChange = []string{ItemMgmtUpdateTopic}

	GoogleRemittanceStatementTopic   = servicePrefixName + "ZPGG_RemittanceDetail"
	MerchantRemittanceStatementTopic = servicePrefixName + "settlement-reconcile-adapter.merchant-remittance-statement"
	BillingRemittanceStatementTopic  = servicePrefixName + "settlement-reconcile-adapter.billing-remittance-statement"
	AdapterReconcileResultTopic      = servicePrefixName + "settlement-reconcile-adapter.reconcile-result"
	RefundMigrateTopic               = servicePrefixName + "refund-migrate"

	BDStoreRegisConfirmationTopic    = servicePrefixName + "merchant-tools.bd-store-regis-confirmation-topic"
	LegalStoreRegisConfirmationTopic = servicePrefixName + "merchant-tools.legal-store-regis-confirmation-topic"

	CallbackCenterTopic = servicePrefixName + "merchant-solutions.callback-center-topic"

	ItemMgmtUpdateTopic = servicePrefixName + "merchant-solutions.item-mgmt.update"
	OAChangeTopic       = servicePrefixName + "OAChange"
	NotifyTransShop     = servicePrefixName + "NotifyTransShop"
)
