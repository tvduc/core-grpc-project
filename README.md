
consul: docker run -d -p 8500:8500 --name consul bitnami/consul:latest

elasticsearch: docker run -d --name elasticsearch  -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node"  elasticsearch:6.8.10

kibana: docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 kibana:6.8.10

TIKV: port 20160

mysql: docker run --name mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -p 3306:3306 -d mysql:8.0 ( conect db./mysql -u root -h 127.0.0.1  -p)CREATE DATABASE db_name;

redis: docker run --name redis -p 6379:6379 -d redis:6.0 ((check redis: redis-cli ping))

List port listening: lsof -i -P -n | grep LISTEN
172.17.0.1: address ping to docker container

