package handler

import (
	"context"

	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/copier"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/domain"
	grpc_helper "gitlab.com/tvduc/product-management/core-grpc-project/helper/grpc"
	merchantManagePb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/middleware/merchant"
)

type serviceHandler struct {
	merchantManageClient merchantManagePb.MerchantStoreServiceAPIClient
}

// NewServiceHandler ...
func NewServiceHandler(ctx context.Context, consulAddr string) pb.MerchantAPIServer {
	return &serviceHandler{
		merchantManageClient: merchantManagePb.NewMerchantStoreServiceAPIClient(grpc_helper.CreateGrpcConn(ctx, consulAddr, domain.MerchantManage)),
	}
}

//Get branch info
func (s *serviceHandler) GetBranch(ctx context.Context, req *pb.GetBranchRequest) (*pb.Branch, error) {
	glog.Infoln("GetBranch")
	respGw := &pb.Branch{}
	reqCore := &merchantManagePb.GetBranchRequest{}
	respCore, err := s.merchantManageClient.GetBranch(ctx, reqCore)
	if err != nil {
		glog.Infoln(err)
	}
	copier.Copy(respGw, respCore)

	return respGw, nil
}

//Get list branch
func (s *serviceHandler) ListBranchs(ctx context.Context, req *pb.ListBranchsRequest) (*pb.ListBranchsResponse, error) {
	glog.Infoln("ListBranchs")
	respGw := &pb.ListBranchsResponse{}
	reqCore := &merchantManagePb.ListBranchsRequest{}
	respCore, err := s.merchantManageClient.ListBranchs(ctx, reqCore)
	if err != nil {
		glog.Infoln(err)
	}
	copier.Copy(respGw, respCore)

	return respGw, nil
}

//Create new branch
func (s *serviceHandler) CreateBranch(ctx context.Context, req *pb.CreateBranchRequest) (*pb.Branch, error) {
	glog.Infoln("CreateBranch", req)
	respGw := &pb.Branch{}
	reqCore := &merchantManagePb.CreateBranchRequest{
		BranchName:  req.GetBranchName(),
		PhoneNumber: req.GetPhoneNumber(),
	}
	respCore, err := s.merchantManageClient.CreateBranch(ctx, reqCore)
	if err != nil {
		glog.Infoln(err)
	}
	copier.Copy(respGw, respCore)

	return respGw, nil
}

//Update branch info
func (s *serviceHandler) UpdateBranch(ctx context.Context, req *pb.Branch) (*pb.Branch, error) {
	glog.Infoln("UpdateBranch")
	return nil, nil
}

//Delete branch
func (s *serviceHandler) DeleteBranch(ctx context.Context, req *pb.DeleteBranchRequest) (*empty.Empty, error) {
	glog.Infoln("DeleteBranch")
	return nil, nil
}
