package main

import (
	"context"
	"os"

	"github.com/golang/glog"
	domain "gitlab.com/tvduc/product-management/core-grpc-project/helper/domain"
	helper "gitlab.com/tvduc/product-management/core-grpc-project/helper/grpc"
	config "gitlab.com/tvduc/product-management/core-grpc-project/middleware/merchant/config"

	hdl "gitlab.com/tvduc/product-management/core-grpc-project/middleware/merchant/handler"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/middleware/merchant"
	"google.golang.org/grpc"
)

func main() {
	cfg := config.ServiceCfg
	glog.Infoln(" cfg.GetConsul()", cfg.GetConsul())
	helper.StartServer(domain.MiddlewareMerchantManage, cfg, func(ctx context.Context, s *grpc.Server) {
		pb.RegisterMerchantAPIServer(s, hdl.NewServiceHandler(ctx, cfg.GetConsul()))
	})
	defer os.Exit(3)
}
