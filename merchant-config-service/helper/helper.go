package helper

import (
	"github.com/golang/glog"
	protobuf "github.com/golang/protobuf/proto"
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/cache"
	configHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	"gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/config"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
)

// CacheType ...
type CacheType int

const (
	//TransactionCache ...
	BranchCache CacheType = iota
)

//OpenCache ...
func OpenCache(Type CacheType) cache.Cache {
	cfg := *config.ServiceCfg
	var (
		indexSetting   interface{}
		table, esIndex string
		pbMsg          protobuf.Message
	)
	switch Type {

	case BranchCache:
		{
			indexSetting = map[string]interface{}{
				"sort.field": "id",
				"sort.order": "desc",
			}
			pbMsg = &pb.Branch{}
			table = configHelper.MerchantBranchTable
			esIndex = configHelper.MerchantBranchESIndex
		}

	default:
		glog.Errorln("Unknow ", Type)
	}
	return cache.Open(&cfg.Redis, &cfg.Database, cfg.Elastic.Addr, pbMsg, indexSetting, table, esIndex)
}
