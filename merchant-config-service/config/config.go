package config

import (
	"gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
)

//Service ...
type Service struct {
	config.ServiceBase
}

var (
	//ServiceCfg config for this project
	ServiceCfg = &Service{}
)
