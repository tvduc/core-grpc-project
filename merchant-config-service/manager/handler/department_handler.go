package handlerManager

import (
	"context"
	"sync"

	repoCache "gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/manager/repo"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
)

var (
	departmentOnce    sync.Once
	departmentHandler *DepartmentHandler = nil
)

type DepartmentHandler struct {
}

func NewDepartmentHandler(ctx context.Context) *DepartmentHandler {
	departmentOnce.Do(func() {
		repoCache.NewBranchRepository(ctx)
		departmentHandler = &DepartmentHandler{}

	})
	return departmentHandler
}

func GetDepartmentHandler() *DepartmentHandler {
	return departmentHandler
}

func (h *DepartmentHandler) GetBranch(ctx context.Context, req *pb.GetBranchRequest) (*pb.Branch, error) {
	return repoCache.GetBranchRepository().Get(ctx, req)
}

func (h *DepartmentHandler) CreateBranch(ctx context.Context, req *pb.CreateBranchRequest) (*pb.Branch, error) {
	return repoCache.GetBranchRepository().Insert(ctx, req)
}

func (h *DepartmentHandler) ListBranch(ctx context.Context, req *pb.ListBranchsRequest) (*pb.ListBranchsResponse, error) {
	return repoCache.GetBranchRepository().List(ctx, req)
}
