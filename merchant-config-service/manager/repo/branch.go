package repoCache

import (
	"context"
	"sync"

	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes/empty"
	cacheHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/cache"
	configHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/config"
	errorHelper "gitlab.com/tvduc/product-management/core-grpc-project/helper/error_util"
	"gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/helper"

	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
)

// Branch
var (
	branchRepo          *branchRepository
	branchRepoOne       sync.Once
	branchRepoESSetting interface{}
	branchESIndex       = configHelper.MerchantBranchESIndex
)

type branchRepository struct {
	branchCache cacheHelper.Cache
}

// NewBranchRepository ...
func NewBranchRepository(ctx context.Context) *branchRepository {

	branchRepoOne.Do(func() {
		branchCache := helper.OpenCache(helper.BranchCache)
		branchRepo = &branchRepository{
			branchCache: branchCache,
		}
	})

	return branchRepo
}

// GetBranchRepository ...
func GetBranchRepository() *branchRepository {
	return branchRepo
}

//GetById ...
func (r *branchRepository) GetByID(ctx context.Context, id string) (*pb.Branch, error) {
	glog.Infoln("branchRepository.Get.GetByID", id)
	resp := &pb.Branch{}
	return resp, nil
}

//Get ...
func (r *branchRepository) Get(ctx context.Context, req *pb.GetBranchRequest) (*pb.Branch, error) {
	glog.Infoln("branchRepository.Get.Req", req)
	resp := &pb.Branch{
		Id:   "123232",
		Name: "DUCTRAN",
	}
	err := r.branchCache.Get(ctx, resp, "123456789")
	if err != nil {
		glog.Infoln("branchRepository.Get", err)
	}
	return resp, nil
}

//List ...
func (r *branchRepository) List(ctx context.Context, req *pb.ListBranchsRequest) (*pb.ListBranchsResponse, error) {
	glog.Infoln("branchRepository.List.Req", req)
	branchs := []*pb.Branch{}
	for i := 0; i < 3; i++ {
		branchs = append(branchs, &pb.Branch{
			Id:   string(i + 1),
			Name: "DucTran",
		})
	}
	resp := &pb.ListBranchsResponse{
		Branchs: branchs,
	}

	return resp, nil
}

//Insert ...
func (r *branchRepository) Insert(ctx context.Context, req *pb.CreateBranchRequest) (*pb.Branch, error) {
	// if req == nil {
	// 	return nil, errorHelper.ErrorInvalidData
	// }

	err := r.branchCache.Set(ctx, "abcd", &pb.Branch{
		Id:          "abcd",
		Name:        "Ducatran",
		PhoneNumber: "036482664",
	})
	if err != nil {
		glog.Infoln("branchRepository.Insert", err)
		return nil, err
	}
	return &pb.Branch{}, nil
}

//Update ...
func (r *branchRepository) Update(ctx context.Context, req *pb.Branch) (*pb.Branch, error) {
	if req == nil {
		return nil, errorHelper.ErrorInvalidData
	}

	return req, nil
}

//Delete ...
func (r *branchRepository) Delete(ctx context.Context, req *pb.DeleteBranchRequest) (*empty.Empty, error) {
	if req == nil {
		return nil, errorHelper.ErrorInvalidData
	}

	return &empty.Empty{}, nil
}
