package main

import (
	"context"

	"gitlab.com/tvduc/product-management/core-grpc-project/helper/domain"
	helper "gitlab.com/tvduc/product-management/core-grpc-project/helper/grpc"
	"gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/config"
	hdl "gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/handler"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
	"google.golang.org/grpc"
)

func main() {

	helper.StartServer(domain.MerchantManage, config.ServiceCfg, func(ctx context.Context, s *grpc.Server) {
		hdl.Initialize(ctx)
		pb.RegisterMerchantStoreServiceAPIServer(s, hdl.NewServiceHandler(ctx))
	})
}
