package handler

import (
	"context"

	"github.com/golang/glog"
	"github.com/golang/protobuf/ptypes/empty"
	handlerManager "gitlab.com/tvduc/product-management/core-grpc-project/merchant-config-service/manager/handler"
	pb "gitlab.com/tvduc/product-management/core-grpc-project/protobuf/merchant-manage"
)

type serviceHandler struct {
}

func Initialize(ctx context.Context) {

	handlerManager.NewDepartmentHandler(ctx)
}

//NewServiceHandler Tạo service Handler
func NewServiceHandler(ctx context.Context) pb.MerchantStoreServiceAPIServer {
	// cfg := config.ServiceCfg
	if handlerManager.GetDepartmentHandler() == nil {
		Initialize(ctx)
	}
	return &serviceHandler{}
}

//Get branch info
func (s *serviceHandler) GetBranch(ctx context.Context, req *pb.GetBranchRequest) (*pb.Branch, error) {
	glog.Infoln("GetBranch")

	return handlerManager.GetDepartmentHandler().GetBranch(ctx, req)
}

//Create new branch
func (s *serviceHandler) CreateBranch(ctx context.Context, req *pb.CreateBranchRequest) (*pb.Branch, error) {
	glog.Infoln("CreateBranch", req)

	return handlerManager.GetDepartmentHandler().CreateBranch(ctx, req)
}

//Update branch info
func (s *serviceHandler) UpdateBranch(ctx context.Context, req *pb.Branch) (*pb.Branch, error) {
	glog.Infoln("UpdateBranch")
	return &pb.Branch{}, nil
}

//Delete branch
func (s *serviceHandler) DeleteBranch(ctx context.Context, req *pb.DeleteBranchRequest) (*empty.Empty, error) {
	glog.Infoln("DeleteBranch")
	return &empty.Empty{}, nil
}

//Get list branch
func (s *serviceHandler) ListBranchs(ctx context.Context, req *pb.ListBranchsRequest) (*pb.ListBranchsResponse, error) {
	glog.Infoln("ListBranchs")
	return handlerManager.GetDepartmentHandler().ListBranch(ctx, req)
}
